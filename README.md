# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* RAs System
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* update composer
* migrate db
* db seed
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

*************************
ALTER TABLE `users` ADD `logged_in` ENUM('login','logout') NULL DEFAULT 'logout' AFTER `comments`;

ALTER TABLE `transactions` ADD `cancelled_by` INT NULL DEFAULT '0' AFTER `days_up_to_due_date`, ADD `cancelled_date` DATETIME NULL DEFAULT NULL AFTER `cancelled_by`;

ALTER TABLE `transactions_invoices` ADD `cancelled_by` INT NULL DEFAULT '0' AFTER `updated_at`;

ALTER TABLE `transactions` ADD `supp_bank` INT NULL DEFAULT '0' AFTER `cancelled_date`;

INSERT INTO `languages` (`id`, `deleted_at`, `name`, `description`, `lang_code`, `active`, `created_at`, `updated_at`) VALUES (NULL, NULL, 'English', NULL, 'en', '1', NULL, NULL), (NULL, NULL, 'Portuguese', NULL, 'pt', '1', NULL, NULL);

ALTER TABLE `available_cash_history` ADD `transaction_id` INT NULL DEFAULT '0' AFTER `customer_id`;

ALTER TABLE `available_cash_history` DROP COLUMN `company`;

=============================================================================================

