<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvailableCashHistory extends Model
{
    
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'available_cash_history';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cutomer_id','transaction_id','bank_id','supplier_id','sales_user','action','date','amount'];

    public function supplier(){

        return $this->belongsTo('App\Supplier');
    }

}
