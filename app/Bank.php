<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','legal_name','cpnj','branch',
    'email','phone','mobile','street_name','address_number',
    'country_name','state','city','zip','complement','neighborhood','status','deleted_at'];

    public function customerCount(){
        return $this->hasMany('App\BankCustomer', 'bank_id')->select(DB::raw('count(bank_id) as customercount, bank_id'))->groupBy('bank_customer.bank_id');
    }

    public function givenMoney(){
        return $this->hasMany('App\BankCustomerMoney','bank_id')->select(DB::raw('sum(bank_customer_allocated_money.total_amount) as totalamount, bank_id'))->groupBy('bank_customer_allocated_money.bank_id');
    }
    
    public function invoiceQty(){
        return $this->hasMany('App\TransactionsInvoices', 'bank_id')->select(DB::raw('count(transactions_invoices.id) as invoice_qty, bank_id'))->groupBy('transactions_invoices.bank_id');
    }

    public function usedMoney(){
        return $this->hasMany('App\Transaction','bank_id')->select(DB::raw('sum(transactions.total_amount) as totalamount, bank_id'))->groupBy('transactions.bank_id');
    }
    
}
