<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashAdd extends Model
{
    //Tablename
    protected $table = "customer_cash_available";
    //primary key
    public $primaryKey = "id";

    protected $fillable = ['cash','customer_id'];

    public function customer(){

    	return $this->belongsTo('App\Customer','customer_id');
    }

}
