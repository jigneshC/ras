<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CbuserChangeGoal extends Model
{
    //Tablename
    protected $table = "cbuser_goal";
    //primary key
    public $primaryKey = "id";
}
