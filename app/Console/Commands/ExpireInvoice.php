<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Invoice;
use Carbon\Carbon;


class ExpireInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
   // protected $signature = 'command:name';
    protected $signature = 'ExpireInvoice:expireinvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire the Invoices when its duedate is passed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $current_date = date('Y-m-d');
        
                $invoices = Invoice::where('status','!=','Expired')
                                    ->where('status','!=','Used')
                                    ->where('status','Available')
                                    ->get();
                                    
                foreach($invoices as $invoice){
                    if($current_date > $invoice->duedate){
                        $invoice->status = 'Expired';
                        $invoice->save();
                    }
                }
                
        $this->info('Invoices are Expired Successfully');
        echo "Invoices are Expired Successfully";

    }
}
