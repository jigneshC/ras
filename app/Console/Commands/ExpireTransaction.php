<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Transaction;
use App\Invoices;
use App\AvailableCashHistory;
use App\TransactionsInvoices;
use Carbon\Carbon;

class ExpireTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ExpireTransaction:expiretransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crone JOb to Expire Transaction which does not approved on same day,';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $current_date = date('Y-m-d');
        
        $transactions = Transaction::where('payment_status','=','Pending')
                                    ->where('is_approve', 0)
                                    ->get();
                          
        foreach($transactions as $transaction){
            if($current_date > $transaction->created_at){
                $transaction->payment_status = 'Expired';
                $transaction->save();

                // Credit the total amount ( Net Value ) back to available cash history when transaction is Expired
                $history = AvailableCashHistory::where('transaction_id' , $transaction->id)->first();
                $history->delete();

                // Make the Invoices Available for use which are used in Expired Transaction 
                $transactions_invoices = TransactionsInvoices::where('transaction_number', $transaction->id)->get();
                 foreach ($transactions_invoices as $trans_in) {
                     $trans_in->payment_status = 'cancelled';
                     $trans_in->approver_id = '0';
                     $trans_in->cancelled_by = Auth::user()->id;
                     $trans_in->save();
                     $invoices = Invoice::find($trans_in->invoice_id);
                     $invoices->is_transaction = '0';
                     $invoices->status = 'Available';
                     $invoices->discount_rate = 0;
                     $invoices->bank_rate = 0;
                     $invoices->save();
                 }
            }
        }
                
        $this->info('Transactions are Expired Successfully');
        echo "Transactions are Expired Successfully";
    }
}
