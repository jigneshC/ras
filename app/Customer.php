<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    //Tablename
    protected $table = "customer";
    //primary key
    public $primaryKey = "id";

    protected $fillable = [ 'name','legal_name','type','cpnj','address_line1','address_line2','country_name','city','state','zip','is_status','typeofcustomer','address_complement','neighborhood','website','expiry_date','image','rate','created_by','updated_by','deleted_at'];

    public function supplier(){
        return $this->belongsToMany('App\Supplier','customer_supplier','customer_id','supplier_id')->with('units');
    }
    public function user(){
        return $this->belongsToMany('App\User','customer_user','customer_id','user_id');
    }

    public function role(){
        return $this->belongsToMany('App\Role','role_user','role_id','user_id');
    }

    public function invoiceCession(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'customer_id', 'invoice_id')->select('type',DB::raw('sum(invoices.total_amount) as invoicesum , count(transactions_invoices.invoice_id) as cession_invoice' ))
        ->where('type', 'Cession')
        ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
        ->groupBy('invoices.customer_id');
    }

    public function invoiceAnticipastion(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'customer_id', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum ,count(invoices.id) as anticipation_invoice') )
            ->where('type', 'Anticipation')
            ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
            ->groupBy('invoices.customer_id');
    } 


    public function transaction(){
        return $this->belongsTo('App\Transaction');
    }

    public function invoiceSum(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'customer_id', 'invoice_id')->select( DB::raw('sum(invoices.total_amount) as invoicesum '))
            ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
            ->groupBy('invoices.customer_id');
    }

  // 'Available for anticipation', use same concept as 'Cash available for cessions', but consider balance of cash made available by customer.
    public function CashAvailableAnticipation(){
        return $this->hasMany('App\AvailableCashHistory','customer_id')
            ->select(DB::raw('sum(available_cash_history.amount) as anticipation,available_cash_history.customer_id'))
            ->whereNull('bank_id')
            ->groupBy('available_cash_history.customer_id');

    }
    public function cash_anticipation(){
        return $this->hasMany('App\CashAdd', 'customer_id')
            ->select(DB::raw('sum(customer_cash_available.cash) as anticipation, customer_cash_available.customer_id'))
            ->groupBy('customer_cash_available.customer_id');
    }

   // 'Cash Available for cession' , It will be the sum of cash made available by banks, but consider the balance. It means, if some transaction is on approval or has been approved (paid), deduct the transaction original amount from previous value.                     
    public function CashAvailableCession(){
        return $this->hasMany('App\AvailableCashHistory','customer_id')
            ->select(DB::raw('sum(available_cash_history.amount) as cession ,available_cash_history.customer_id'))
            ->whereNotNull('bank_id')
            ->whereNull('sales_user')
            ->groupBy('available_cash_history.customer_id');
    }

    public function cashExpiry(){
        return $this->hasMany('App\CashAdd', 'customer_id')
            ->select('*')
            ->orderBy('id','desc');
    }

    public function cashAvailable(){
        return $this->hasMany('App\AvailableCashHistory','customer_id')
            ->select(DB::raw('sum(available_cash_history.amount) as available_cash,available_cash_history.customer_id'))
            ->whereNull('bank_id')
            ->groupBy('available_cash_history.customer_id');
    }

    public function bankmoney(){
        return $this->hasMany('App\BankCustomerMoney','customer_id')
        ->groupBy('bank_customer_allocated_money.bank_id')
        ->select('*',DB::raw('sum(bank_customer_allocated_money.total_amount) as cash'));
    }

    public function transaction_money(){
        return $this->hasMany('App\Transaction','customer_id')
        ->groupBy('transactions.bank_id')
        ->select('*',DB::raw('sum(transactions.total_amount) as cash'));

    }

    public function anticipation_to_be_paid(){
        return $this->hasMany('App\TransactionsInvoices','customer_id')
        ->where('transactions_invoices.payment_type','Anticipation')->with('transaction');
    }

    public function anticipation_to_be_paid_approved_today(){
        return $this->hasMany('App\Transaction','customer_id')->select('customer_id',DB::raw('sum(net_value) as total_to_paid'))->groupBy('customer_id')->whereBetween('approval_date', [date('Y-m-d') . " 00:00:00", date('Y-m-d') . " 23:59:59"]);
    }

    public function anticipation_to_be_done()
    {
        $expDate =  date("Y-m-d",strtotime(date("Y-m-d", strtotime("-7 day"))));
        return $this->hasMany('App\Transaction', 'customer_id')->select('customer_id', DB::raw('sum(net_value) as total_done_paid'))->groupBy('customer_id')->whereDate('approval_date', '<', $expDate);
    }

}

