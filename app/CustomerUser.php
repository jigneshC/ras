<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerUser extends Model
{
    //Tablename
    protected $table = "customer_user";
    //primary key
    public $primaryKey = "id";
    
    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
