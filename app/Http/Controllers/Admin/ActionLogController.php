<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Session;
use App\ActionLog;
use Yajra\Datatables\Datatables;
use Auth;
use App\User;

class ActionLogController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.users.logs');
    }

    public function index(Request $request){

        $startdate = date('01-m-Y');

        $enddate = date('t-m-Y');

        $user_id = "";
        
        if($request->has('user_id') && $request->get('user_id') != "" ){

            $user_id = $request->get('user_id');
        }

        return view('admin.actionlog.index', compact('startdate','enddate','user_id') );
    }

    public function show($id)
    {
        $actionlog = ActionLog::with('company')->findOrFail($id);
        return view('admin.actionlog.show', compact('actionlog'));
    }

    public function datatable(Request $request){
        $actionlog = ActionLog::with('company')->orderBy('created_at','desc');

        if ($request->has('startdate') && $request->get('startdate') != '') {
            $sdate=$request->get('startdate');
            $edate=$request->get('enddate');
            $stdate = str_replace('/','-',$sdate);
            $startdate= date('Y-m-d' , strtotime($stdate));
            $endate = str_replace('/','-',$edate);
            $enddate= date('Y-m-d' , strtotime($endate));      

            $actionlog->whereBetween('created_at',[$startdate,$enddate]);
        }

        if ($request->has('user_id') && $request->get('user_id') != '') {
           $actionlog->where('actioner_id',$request->get('user_id'));
        }

        return Datatables::of($actionlog)
            ->make(true);
            exit;
    }

    public function destroy($id){
        $log = ActionLog::find($id);
        $log->delete();
        Session::flash('flash_message', 'ActionLog deleted!');
        return redirect('admin/action-log');
    }
}
