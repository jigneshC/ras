<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddBankAccounts;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use CountryState;

use App\Bank;
use App\BankAccount;
use App\BankContact;
use App\Customer;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Role;
use App\User;
use Yajra\Datatables\Datatables;


class BankAccountsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.bank.accounts');
        $this->middleware('permission:edit.bank.account')->only(['edit', 'update']);
        $this->middleware('permission:add.bank.account')->only(['create', 'store']);
        $this->middleware('permission:delete.bank.account')->only('destroy');
    }

    public function index($id)
    {
        $bank = Bank::where('id',$id)->FirstOrFail();

        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');

        return view('admin.banks.bankaccount',compact('bank','countries'));
    }

    public function datatableBankAccount($id){

        $bank_account = BankAccount::with('bank')->with('city')->where('bank_id',$id)->get();

        return Datatables::of($bank_account)->make(true);
    }

    public function create()
    {
       // return view('admin.equipments.create');
    }

    public function store(Request $request)
    {

        $code = 200;$message="";

        $this->validate($request, [
            'account_number' => 'required',
            'agency_number' => 'required'
        ]);

        $bank = Bank::where('id',$request->bank_id)->FirstOrFail();

        if($bank){

            $requestData = $request->except('bank_account_id');

            $bank_account = BankAccount::create($requestData);

            $bank_account->save();

            \ActionLog::addToLog("Add Bank Account","Bank Account- " .$bank_account->name ." is added",$bank_account->getTAble(),$bank_account->id);

            $message = "Account created success !!";

            if(\Auth::check()){

                if(\Auth::user()->roles[0]->name == "SU"){
                    //if bank is added by admin, notification send to all backoffice

                    $cbb = Role::Where('name','CBB')->get();

                    if($cbb != ""){
                        foreach($cbb as $roles){
                            foreach($roles->users as $cbbuser){
                                $user_id[] = $cbbuser->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();


                    foreach($users as $bkouser){

                        $bkouser->notify(new AddBankAccounts($bank_account));
                    }
                }else{

                    //if bank is added by any backoffice then notification send to admin
                    $su = Role::Where('name','SU')->get();

                    if($su != ""){
                        foreach($su as $roles){
                            foreach($roles->users as $su_user){
                                $user_id[] = $su_user->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();

                    foreach($users as $adminuser){

                        $adminuser->notify(new AddBankAccounts($bank_account));
                    }
                }
            }
        
        }else{

            $code = 400;
            $message = "No bank found";
        }
        return response()->json(['messages' => $message],$code);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = BankAccount::where('id',$id)->FirstOrFail();
        if($data){
            $code= 200;
        }else{
            $code = 400;
        }
        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');
        $country_id=$data->country;      
        $state_id=$data->state;
        $city_id=$data->city;
        $states = DB::table("states")
                    ->where("country_id",$country_id)
                    ->pluck("name","id");
                   
        $cities = DB::table("cities")
                    ->where("state_id",$state_id)
                    ->pluck("name","id");
        return response()->json(['data' => $data,'country'=> $countries,'country_id'=>$country_id,'states'=>$states,'state_id'=>$state_id,'cities'=>$cities,'city_id'=>$city_id],$code);

    }

    public function update($id, Request $request)
    {
        $code = 200; $message="";
        $this->validate($request, [
            'account_number' => 'required',
            'agency_number' => 'required' 
        ]);
        $bank = Bank::where('id',$request->bank_id)->FirstOrFail();
        $bank_account = BankAccount::find($id);

        if($bank && $bank_account){
            $requestData = $request->except('bank_account_id');
            $bank_account->update($requestData);
            $message = "Account Updated success !!";

            \ActionLog::addToLog("Edit Bank Account","Bank Account-" .$bank_account->name . " is updated",$bank_account->getTAble(),$bank_account->id);

        }else{
            $code = 400;
            $message = "No bank found";
        }
        return response()->json(['messages' => $message],$code);
    }

    public function destroy($id)
    {
        $bank_account = BankAccount::find($id);

        \ActionLog::addToLog("Bank Account Deleted","Bank Account-" .$bank_account->name . "is deleted",$bank_account->getTAble(),$bank_account->id);

        $bank_account->delete();
        $message = "Account Deleted success !!";

       
        return response()->json(['messages' => $message],200);
    }
}
