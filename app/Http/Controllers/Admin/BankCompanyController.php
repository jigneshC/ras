<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddBankCompany;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use CountryState;
use App\Customer;
use App\Bank;
use App\BankCustomerMoney;
use App\BankCustomer;
use App\BankContact;
use App\BankAccount; 
use App\AvailableCashHistory;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Role;
use App\User;
use App\CustomerUser;
use Yajra\Datatables\Datatables;
use App\ActionLog;
use App\Transaction;


class BankCompanyController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.bank.company');
       // $this->middleware('permission:add.money.for.company')->only(['add', 'update']);
      //  $this->middleware('permission:add.bank.company')->only(['create', 'store','add']);
        $this->middleware('permission:delete.bank.company')->only('destroy');
    }

    public function index($id)
    {   
        $bank = Bank::where('id',$id)->FirstOrFail();
        $bank_contact = BankContact::where('bank_id',$id)->pluck('name', 'id')->prepend('Select contact','');
        $bank_account = BankAccount::where('bank_id', $id)->pluck('account_number', 'id')->prepend('Select account','');
        $bank_cust = BankCustomer::where('bank_id',$bank->id)->get();

        if(count($bank_cust) == 0){

            $companies =  Customer::where('is_status','active')->orderBy('name','asc')->get();

        }else{

            foreach($bank_cust as $value){
                $cust_id[] = $value->customer_id;
            }

            if(isset($cust_id)){

                $companies =  Customer::whereNotIn('id',$cust_id)->where('is_status', 'active')->orderBy('name', 'asc')->get(); 
            }
        }



        return view('admin.banks.bankcompany',compact('bank','companies', 'bank_contact', 'bank_account'));
    }

    public function add(Request $request,$id,$bank_id){
        $id  = $request->id;
        $customer = Customer::find($id);
        $allbankdetail[0]['id'] = $id;
        $allbankdetail[0]['name'] = $customer->name;
        $banks = BankCustomerMoney::where('bank_customer_allocated_money.bank_id', $bank_id)->with('bank', 'customer')->select('*', DB::raw('sum(bank_customer_allocated_money.total_amount) as total'))->groupBY('bank_customer_allocated_money.customer_id')->where('bank_customer_allocated_money.customer_id',$id)->get();
        $transactions = Transaction::where('bank_id', $bank_id)->where('approver_id', '!=', '')->where('customer_id',$id)->where('type', 'Cession')->get();
        $bankcust = BankCustomer::where('bank_id', $bank_id)
            ->where('customer_id', $id)->get();
        foreach ($banks as $key => $bank) {
            $allbankdetail[$key] = $bank;
            $allbankdetail[$key]['cash_consumed'] = 0;
            $allbankdetail[$key]['cash_still_available'] = 0;
            foreach ($transactions as $transaction) {
                if ($transaction->customer_id == $bank->customer_id) {
                    $allbankdetail[$key]['cash_consumed'] = $allbankdetail[$key]['cash_consumed'] + $transaction->total_amount;
                }
            }
            $allbankdetail[$key]['bank_account'] = $bankcust[0]->bank_account_number;
            $allbankdetail[$key]['bank_contact'] = $bankcust[0]->bank_contact;
            $bankExpiryDate = BankCustomerMoney::where('bank_id', $bank_id)->where('customer_id', $id)->orderBy('id', 'desc')->first();
            $allbankdetail[$key]['expriryDate'] = $bankExpiryDate->duedate;
            $allbankdetail[$key]['bank_rate'] = $bankExpiryDate->bank_rate;
            $allbankdetail[$key]['cash_still_available'] = $allbankdetail[$key]['total'] - $allbankdetail[$key]['cash_consumed'];
        }
        
        return response()->json($allbankdetail[0]);
    }


    public function datatableBankCompany($id){
        $allbankdetail = array();
        if (!empty($id)) {
            $banks = BankCustomerMoney::where('bank_customer_allocated_money.bank_id', $id)->with('bank', 'customer')->select('*', DB::raw('sum(bank_customer_allocated_money.total_amount) as total'))->groupBY('bank_customer_allocated_money.customer_id')->get();
            
            $transactions = Transaction::where('bank_id', $id)->where('approver_id', '!=', '')->where('type', 'Cession')->get();
            foreach ($banks as $key => $bank) {
                $allbankdetail[$key] = $bank;
                $allbankdetail[$key]['cash_consumed'] = 0;
                $allbankdetail[$key]['cash_still_available'] = 0;
                
                foreach ($transactions as $transaction) {
                    if ($transaction->customer_id == $bank->customer_id) {
                        $allbankdetail[$key]['cash_consumed'] = $allbankdetail[$key]['cash_consumed'] + $transaction->total_amount;
                    }
                }
                $bankExpiryDate = BankCustomerMoney::where('bank_id', $id)->where('customer_id', $bank->customer_id)->orderBy('id', 'desc')->first();
                $allbankdetail[$key]['expriryDate'] = $bankExpiryDate->duedate;
                $allbankdetail[$key]['bank_rate'] = $bankExpiryDate->bank_rate;
                $allbankdetail[$key]['cash_still_available'] = $allbankdetail[$key]['total'] - $allbankdetail[$key]['cash_consumed'];
            }
        }
        
        $allbankdetail = collect($allbankdetail);
        
        return Datatables::of($allbankdetail)
            ->make(true);
        exit;
 
        
    }

    public function update(Request $request,$id){

        if(!empty($request->submit)){
            

            $bankcust = BankCustomer::where('bank_id', $request->bank_id )
            ->where('customer_id',$request->customer_id)->get();
            
            if(count($bankcust) == 0){

                $bank_cust = new BankCustomer;
                $bank_cust->bank_id =  $request->bank_id;
                $bank_cust->customer_id = $request->customer_id;
                $bank_cust->bank_account_number = $request->accounts;
                $bank_cust->bank_contact = $request->contacts;
                $bank_cust->save();

                \ActionLog::addToLog("Bank added new Customer"," Bank ID - " . $bank_cust->bank_id ." added Customer ID-".$bank_cust->customer_id, $bank_cust->getTAble() , $bank_cust->id);

            }else{
                $bankcust = BankCustomer::find($bankcust[0]->id);
                $bankcust->bank_account_number = $request->accounts;
                $bankcust->bank_contact = $request->contacts;
                $bankcust->save();

            }
           
            $bank_customer = new BankCustomerMoney;
            $bank_customer->bank_id = $request->bank_id;
            $bank_customer->customer_id = $request->customer_id;
            $bank_customer->total_amount = $request->total_amount;
            $bank_customer->bank_rate = $request->bank_rate;
            $date = str_replace('/','-',$request->date);
            $bank_customer->date= date('Y-m-d' , strtotime($date));
            $duedate = str_replace('/','-',$request->duedate);
            $bank_customer->duedate= date('Y-m-d' , strtotime($duedate));
            $bank_customer->type = $request->type;
            $bank_customer->save();

            \ActionLog::addToLog("Bank added money for the Customer"," Bank ID - " . $bank_customer->bank_id ." added amount of ".$bank_customer->total_amount."  to Customer ID-".$bank_customer->customer_id, $bank_customer->getTAble() , $bank_customer->id);


            $cust_user = CustomerUser::where('customer_id', $bank_customer->customer_id)->get();

            foreach ($cust_user as $cust_users) {
                    
                    $user = $cust_users->user_id;

                    $notify = User::find($user);

                    $notify->notify(new AddBankCompany($bank_customer));
            }

            if(\Auth::check()){

                    if(\Auth::user()->roles[0]->name == "SU"){
                        //if bank is added by admin, notification send to all backoffice

                        $cbb = Role::Where('name','CBB')->get();

                        if($cbb != ""){
                            foreach($cbb as $roles){
                                foreach($roles->users as $cbbuser){
                                    $user_id[] = $cbbuser->id ;
                                } 
                            }
                        }
                        $users = User::whereIN('id',$user_id)->get();


                        foreach($users as $bkouser){

                            $bkouser->notify(new AddBankCompany($bank_customer));
                        }
                    }else{

                        //if bank is added by any backoffice then notification send to admin
                        $su = Role::Where('name','SU')->get();

                        if($su != ""){
                            foreach($su as $roles){
                                foreach($roles->users as $su_user){
                                    $user_id[] = $su_user->id ;
                                } 
                            }
                        }
                        $users = User::whereIN('id',$user_id)->get();

                        foreach($users as $adminuser){

                            $adminuser->notify(new AddBankCompany($bank_customer));
                        }
                    }
                }

            $history = new AvailableCashHistory;
            $history->customer_id = $bank_customer->customer_id;
            $history->bank_id = $bank_customer->bank_id;
            $history->action = "credit";
            $history->date = date('Y-m-d');
            $history->amount = $bank_customer->total_amount;
            $history->save();
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        BankCustomerMoney::destroy($id);
        $message = "Company Deleted success !!";
        return response()->json(['messages' => $message],200);
    }
}
