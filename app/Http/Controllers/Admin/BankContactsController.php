<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddBankContacts;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use CountryState;

use App\Bank;
use App\BankAccount;
use App\BankContact;
use App\Customer;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Role;
use App\User;
use Yajra\Datatables\Datatables;


class BankContactsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.bank.contacts');
      //  $this->middleware('permission:change.status.bank.contacts');        
        $this->middleware('permission:edit.bank.contacts')->only(['edit', 'update']);
        $this->middleware('permission:add.bank.contacts')->only(['create', 'store']);
        $this->middleware('permission:delete.bank.contacts')->only('destroy');
        
    }

    public function index($id)
    {
        $bank = Bank::where('id',$id)->FirstOrFail();
        return view('admin.banks.bankcontact',compact('bank'));
    }

    public function datatableBankContact($id){

        $bank_account = BankContact::with('bank')->where('bank_id',$id)->get();
        return Datatables::of($bank_account)->make(true);
    }

    public function create()
    {
       // return view('admin.equipments.create');
    }

    public function store(Request $request)
    {
        $code = 200;$message="";

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);

        $bank = Bank::where('id',$request->bank_id)->FirstOrFail();

        if($bank){

            $requestData = $request->except('bank_contact_id');

            $bank_contact = BankContact::create($requestData);

            $bank_contact->save();

            \ActionLog::addToLog("Add Bank Contact","Bank Contact- " .$bank_contact->name ." is added",$bank_contact->getTAble(),$bank_contact->id);

            $message = "Contact created success !!";

            if(\Auth::check()){

                if(\Auth::user()->roles[0]->name == "SU"){
                    //if bank is added by admin, notification send to all backoffice

                    $cbb = Role::Where('name','CBB')->get();

                    if($cbb != ""){
                        foreach($cbb as $roles){
                            foreach($roles->users as $cbbuser){
                                $user_id[] = $cbbuser->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();


                    foreach($users as $bkouser){

                        $bkouser->notify(new AddBankContacts($bank_contact));
                    }
                }else{

                    //if bank is added by any backoffice then notification send to admin
                    $su = Role::Where('name','SU')->get();

                    if($su != ""){
                        foreach($su as $roles){
                            foreach($roles->users as $su_user){
                                $user_id[] = $su_user->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();

                    foreach($users as $adminuser){

                        $adminuser->notify(new AddBankContacts($bank_contact));
                    }
                }
            }

        }else{

            $code = 400;
            $message = "No bank found";
        }
        return response()->json(['messages' => $message],$code);
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $data = BankContact::where('id',$id)->FirstOrFail();
        if($data){
            $code= 200;
        }else{
            $code = 400;
        }
        return response()->json(['data' => $data],$code);

    }

    public function update($id, Request $request)
    {
        $code = 200;$message="";
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);
        $bank = Bank::where('id',$request->bank_id)->FirstOrFail();
        $bank_contact = BankContact::find($id);
        if($bank && $bank_contact){
            $requestData = $request->except('bank_contact_id');
            $bank_contact->update($requestData);
            $message = "Contact Updated success !!";

            \ActionLog::addToLog("Edit Bank Contact","BankContact-" .$bank_contact->name . " is updated",$bank_contact->getTAble(),$bank_contact->id);

        }else{
            $code = 400;
            $message = "No bank found";
        }
        return response()->json(['messages' => $message],$code);
    }

    public function destroy($id)
    {
        $bank_contact = BankContact::find($id);

        \ActionLog::addToLog("Bank Contact Deleted","Bank Contact-" .$bank_contact->name . "is deleted",$bank_contact->getTAble(),$bank_contact->id);

        $bank_contact->delete();

        $message = "Contact Deleted success !!";
        return response()->json(['messages' => $message],200);
    }
}
