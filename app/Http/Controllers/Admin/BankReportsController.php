<?php

namespace App\Http\Controllers\Admin;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bank;
use App\Transaction;
use DB;

class BankReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.reports');
        $this->middleware('permission:access.reports.banks.daily');
        $this->middleware('permission:access.reports.banks.monthly');
        
    }
    
    public function index(Request $request){

        if ($request && $request->bank_id != '' && $request->startdate != '' && $request->enddate != '') {
            $bank_id = $request->bank_id;
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $bankDailyUpdate = Transaction::
                whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->with('customer', 'supplier', 'supplierunitName')
                ->where('transactions.is_approve', '=', '1')
                ->where('transactions.customer_status', '=', 'Approved')
                ->orderBy('transactions.updated_at', 'asc')
                ->select(DB::raw('*'))
                ->where('bank_id',$bank_id)
                ->where('transactions.type', '=', 'Cession')
                ->get();
            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totaltranscount = 0;
            $bankrevenue = 0;
            if (count($bankDailyUpdate) > 0) {
                foreach ($bankDailyUpdate as $rows) {
                    $finalArra[$i]['updated_at'] = date('d/m/Y',strtotime($rows->updated_at));
                    $finalArra[$i]['transaction_id'] = $rows->id;
                    $finalArra[$i]['customer'] = ($rows->customer) ? $rows->customer->name : '';
                    $finalArra[$i]['customer_cnpj'] = ($rows->customer) ? $rows->customer->cpnj : '';
                    $finalArra[$i]['supplier'] = ($rows->supplier) ? $rows->supplier->name : '';
                    $finalArra[$i]['supplier_unit_name'] = ($rows->supplierunitName) ? $rows->supplierunitName[0]->unit_name : '';
                    $finalArra[$i]['supplier_unit_cnpj'] = ($rows->supplierunitName) ? $rows->supplierunitName[0]->cpnj : '';
                    $finalArra[$i]['total_amount'] = $rows->total_amount;
                    $finalArra[$i]['net_val'] = $rows->net_value;
                    $finalArra[$i]['cost'] = $rows->cost;
                    $totaltranscount = $totaltranscount + 1;
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $bankrevenue = $bankrevenue + $rows->cost;
                    $i++;
                }
                if(count($finalArra) > 0){
                    $finalArra[$i]['updated_at'] = 'TOTAL';
                    $finalArra[$i]['transaction_id'] = $totaltranscount;
                    $finalArra[$i]['customer'] = '';
                    $finalArra[$i]['customer_cnpj'] = '';
                    $finalArra[$i]['supplier'] = '';
                    $finalArra[$i]['supplier_unit_name'] = '';
                    $finalArra[$i]['supplier_unit_cnpj'] = '';
                    $finalArra[$i]['total_amount'] = $totalorigval;
                    $finalArra[$i]['net_val'] = $totalnetval;
                    $finalArra[$i]['cost'] = $bankrevenue;
                    $filename = 'file.csv';
                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    $handle = fopen($filename, 'w+');
                    $data = array('Date', 'Transaction ID', 'Customer','CNPJ','Supplier','Unit','CNPJ', 'Original Value', 'Net Value', 'Bank Revenue');
                    fputcsv($handle, $data);
                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);
                    return response()->download($filename, 'daily_cession_bank_reports ' . date("d-m-Y H:i") . '.csv', $headers);

                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
                
            }
        }
        $banks = Bank::orderBy('name','asc')->pluck('name', 'id')->prepend('Select Bank', '');
        $start_date = date('01/m/Y', time());
        $end_date = date('t/m/Y');
        return view('admin.banks.reports.index',compact ('banks','start_date','end_date'));
        exit;
    }

    public function bankdailyreportswithcbuser(Request $request){

        if ($request && $request->bank_id != '' && $request->startdate != '' && $request->enddate != '') {
            $bank_id = $request->bank_id;
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $bankDailyUpdate = Transaction::
                whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->with('customer', 'supplier', 'supplierunitName')
                ->where('transactions.is_approve', '=', '1')
                ->where('transactions.customer_status', '=', 'Approved')
                ->orderBy('transactions.updated_at', 'asc')
                ->select(DB::raw('*'))
                ->where('bank_id',$bank_id)
                ->where('transactions.type', '=', 'Cession')
                ->get();
            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totaltranscount = 0;
            $bankrevenue = 0;
            if (count($bankDailyUpdate) > 0) {
                foreach ($bankDailyUpdate as $rows) {
                    $finalArra[$i]['updated_at'] = date('d/m/Y',strtotime($rows->updated_at));
                    $finalArra[$i]['transaction_id'] = $rows->id;
                    $finalArra[$i]['customer'] = ($rows->customer) ? $rows->customer->name : '';
                    $finalArra[$i]['customer_cnpj'] = ($rows->customer) ? $rows->customer->cpnj : '';
                    $finalArra[$i]['supplier'] = ($rows->supplier) ? $rows->supplier->name : '';
                    $finalArra[$i]['supplier_unit_name'] = ($rows->supplierunitName) ? $rows->supplierunitName[0]->unit_name : '';
                    $finalArra[$i]['supplier_unit_cnpj'] = ($rows->supplierunitName) ? $rows->supplierunitName[0]->cpnj : '';
                    $finalArra[$i]['total_amount'] = $rows->total_amount;
                    $finalArra[$i]['net_val'] = $rows->net_value;
                    $finalArra[$i]['cost'] = $rows->cost;
                    $totaltranscount = $totaltranscount + 1;
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $bankrevenue = $bankrevenue + $rows->cost;
                    $i++;
                }
                if(count($finalArra) > 0){
                    $finalArra[$i]['updated_at'] = 'TOTAL';
                    $finalArra[$i]['transaction_id'] = $totaltranscount;
                    $finalArra[$i]['customer'] = '';
                    $finalArra[$i]['customer_cnpj'] = '';
                    $finalArra[$i]['supplier'] = '';
                    $finalArra[$i]['supplier_unit_name'] = '';
                    $finalArra[$i]['supplier_unit_cnpj'] = '';
                    $finalArra[$i]['total_amount'] = $totalorigval;
                    $finalArra[$i]['net_val'] = $totalnetval;
                    $finalArra[$i]['cost'] = $bankrevenue;
                    $filename = 'file.csv';
                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    $handle = fopen($filename, 'w+');
                    $data = array('Date', 'Transaction ID', 'Customer','CNPJ','Supplier','Unit','CNPJ', 'Original Value', 'Net Value', 'Bank Revenue');
                    fputcsv($handle, $data);
                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);
                    return response()->download($filename, 'daily_cession_bank_reports ' . date("d-m-Y H:i") . '.csv', $headers);

                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
                
            }
        }
        $banks = Bank::orderBy('name','asc')->orderBy('name','asc')->pluck('name', 'id')->prepend('Select Bank', '');
        $start_date = date('01/m/Y', time());
        $end_date = date('t/m/Y');
        return view('admin.banks.reports.cbuser_report',compact ('banks','start_date','end_date'));
        exit;
    }

    public function monthlyFinancialRevenue(Request $request){
        if ($request && $request->bank_id != '' && $request->startdate != '' && $request->enddate != '') {
            $bank_id = $request->bank_id;
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $bankMonthlyUpdate = Transaction::whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')
                ->where('transactions.customer_status', '=', 'Approved')
                ->select( DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"), DB::raw('COUNT(*) AS transaction_count,
                    SUM(`transactions`.`total_amount`) AS total_amount,
                    SUM(`transactions`.`net_value`) AS net_value,
                    SUM(`transactions`.`discount_value`) AS discount_value,
                    SUM(`transactions`.`cost`) AS costs,
                    MONTH(`transactions`.`updated_at`) AS MONTH,
                    YEAR(`transactions`.`updated_at`) AS YEAR,
                    `transactions`.`id` AS `transaction_id`'))
                ->where('bank_id', $bank_id)
                ->groupBy('monthyear')
                ->where('transactions.type', '=', 'Cession')
                ->get();

            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totaltranscount = 0;
            $bankrevenue = 0;
            if (count($bankMonthlyUpdate) > 0) {
                foreach ($bankMonthlyUpdate as $rows) {
                    $finalArra[$i]['month'] = $rows->monthyear;
                    $finalArra[$i]['transactions'] = $rows->transaction_count;
                    $finalArra[$i]['original_amount'] = $rows->total_amount;
                    $finalArra[$i]['net_val'] = $rows->net_value;
                    $finalArra[$i]['revenue'] = $rows->costs;
                    $totaltranscount = $totaltranscount + $rows->transaction_count;
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $bankrevenue = $bankrevenue + $rows->costs;
                    $i++;
                }
                if (count($finalArra) > 0) {
                    $finalArra[$i]['month'] = 'Total';
                    $finalArra[$i]['transactions'] = $totaltranscount;
                    $finalArra[$i]['original_amount'] = $totalorigval;
                    $finalArra[$i]['net_val'] = $totalnetval;
                    $finalArra[$i]['revenue'] = $bankrevenue;
                    $filename = 'file.csv';
                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    $handle = fopen($filename, 'w+');
                    $data = array('Month', 'Transactions', 'Original Value', 'Net Value', 'Bank Revenue');
                    fputcsv($handle, $data);
                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);
                    return response()->download($filename, 'monthly_cession_bank_reports ' . date("d-m-Y H:i") . '.csv', $headers);

                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }

            }
        }
        $banks = Bank::orderBy('name','asc')->pluck('name', 'id')->prepend('Select Bank', '');
        $start_date = date('01/m/Y', time());
        $end_date = date('t/m/Y');
        return view('admin.banks.reports.monthlyfinancialrevenue', compact('banks', 'start_date', 'end_date'));
        
    }
}
