<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddBank;
use App\Notifications\AddBankAccounts;
use App\Notifications\EditBankCPNJ;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ActionLog;
use App\Bank;
use App\BankAccount;
use App\BankContact;
use App\Transaction;
use App\Supplier;
use App\Customer;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Role;
use App\User;
use Yajra\Datatables\Datatables;

class BanksController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.banks');
        $this->middleware('permission:access.banks.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.banks.create')->only(['create', 'store']);
        $this->middleware('permission:access.banks.delete')->only('destroy');
      //  $this->middleware('permission:change.bank.status')->only('show');
        $this->middleware('permission:access.bank.summary')->only('bankSummary');
        $this->middleware('permission:access.bank.cession')->only('cession');
    }
    
    public function index(Request $request)
    {
        return view('admin.banks.index',compact('banks'));
    }

    public function datatable(Request $request){
        
        $banks = Bank::all();
        if ($request->get('search')) {
            $search = $request->get('search');
            $search = $search['value'];
            $banks = Bank::where('name','like','%'.$search.'%');
        }
        return Datatables::of($banks)
            ->make(true);
          exit; 
    }

    public function create()
    {
        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');
        return view('admin.banks.create',compact('countries'));
    }

    public function store(Request $request)
    {   
        $messages = [
            'cpnj.unique' =>"Found Bank, $request->cpnj using this CNPJ",
        ];

        $this->validate($request, [
            'name' => 'required',
            'legal_name' =>'required',
            'cpnj' => 'required|unique:banks,cpnj',
        ],$messages);
        
        $requestData = $request->all();
		$requestData['status']='active';
        
        $bank = Bank::create($requestData);
        if($bank){
            
            \ActionLog::addToLog("Add Bank"," New Bank ". $bank->name ." is added ",$bank->getTAble(),$bank->id);

            $bank_account = new BankAccount;
            $bank_account->bank_id = $bank->id;
            $bank_account->branch = $request->get('account_branch') ;
            $bank_account->account_number =$request->get('account_number') ;
            $bank_account->agency_number =$request->get('agency_number') ;
            $bank_account->street_name =$request->get('account_street_name') ;
            $bank_account->address_number =$request->get('account_address_number') ;
            $bank_account->complement =$request->get('account_complement') ;
            $bank_account->neighborhood =$request->get('account_neighborhood') ;
            $bank_account->country =$request->get('account_country_name') ;
            $bank_account->state =$request->get('account_state') ;
            $bank_account->city =$request->get('account_city') ;
            $bank_account->zip =$request->get('account_zip') ;
            $bank_account->save();

            if(\Auth::check()){

                if(\Auth::user()->roles[0]->name == "SU"){
                    //if bank is added by admin, notification send to all backoffice

                    $cbb = Role::Where('name','CBB')->get();

                    if($cbb != ""){
                        foreach($cbb as $roles){
                            foreach($roles->users as $cbbuser){
                                $user_id[] = $cbbuser->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();


                    foreach($users as $bkouser){

                        $bkouser->notify(new AddBank($bank));

                        $bkouser->notify(new AddBankAccounts($bank_account));
                    }
                }else{

                    //if bank is added by any backoffice then notification send to admin
                    $su = Role::Where('name','SU')->get();

                    if($su != ""){
                        foreach($su as $roles){
                            foreach($roles->users as $su_user){
                                $user_id[] = $su_user->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();

                    foreach($users as $adminuser){

                        $adminuser->notify(new AddBank($bank));

                        $adminuser->notify(new AddBankAccounts($bank_account));
                    }
                }
            }

        }
        Session::flash('flash_message', 'Bank added!');
        return redirect('admin/banks');
    }

    public function show(Request $request,$id)
    {
        $bank = Bank::findOrFail($id);
        $status = $request->get('bankstatus');
		
        if(!empty($status)){
            if($status == 'active' ){
                $bank->status= 'inactive';
                $bank->update();

                \ActionLog::addToLog("Bank Status Changed","Bank - ". $bank->name ."'s status changed to Inactive",$bank->getTAble(),$bank->id);


                return redirect()->back();
            }else{
                $bank->status= 'active';
                $bank->update();
                
                \ActionLog::addToLog("Bank Status Changed","Bank - ". $bank->name ."'s status changed to Active",$bank->getTAble(),$bank->id);
                
                return redirect()->back();
            }
        }
        return view('admin.banks.show', compact('bank'));
    }

    public function edit($id)
    {
        $bank = Bank::findOrFail($id);

        return view('admin.banks.edit', compact('bank'));
    }

    public function update($id, Request $request)
    {   
        $messages = [
            'cpnj.unique' =>"Found Bank, $request->cpnj using this CNPJ",
        ];

        $this->validate($request, [
			'name' => 'required',
            'legal_name' =>'required',
            'cpnj' => 'required|unique:banks,cpnj,'.$id,

		],$messages);

        $requestData = $request->all();

        $bank = Bank::findOrFail($id);

        $old_cpnj = $bank->cpnj;

        $bank->update($requestData);

        \ActionLog::addToLog("Edit Bank"," Bank ". $bank->name ." is updated ",$bank->getTAble(),$bank->id);

        if($old_cpnj != $request->input('cpnj')){

            if(\Auth::check()){

                if(\Auth::user()->roles[0]->name == "SU"){
                    //if cpnj is changed by admin, notification send to all backoffice

                    $cbb = Role::Where('name','CBB')->get();

                    if($cbb != ""){
                        foreach($cbb as $roles){
                            foreach($roles->users as $cbbuser){
                                $user_id[] = $cbbuser->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();


                    foreach($users as $bkouser){

                        $bkouser->notify(new EditBankCPNJ($bank,$old_cpnj));
                    }
                }else{

                    //if cpnj is changed by any backoffice then notification send to admin
                    $su = Role::Where('name','SU')->get();

                    if($su != ""){
                        foreach($su as $roles){
                            foreach($roles->users as $su_user){
                                $user_id[] = $su_user->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();

                    foreach($users as $adminuser){

                        $adminuser->notify(new EditBankCPNJ($bank, $old_cpnj));

                    }
                }
            }
        }

        Session::flash('flash_message', 'Bank updated!');

        return redirect('admin/banks');
    }

    public function destroy($id)
    {
        $bank = Bank::find($id);
        \ActionLog::addToLog("Bank Deleted"," Bank-" .$bank->name . "is deleted",$bank->getTAble(),$bank->id);
        $bank->delete();
       $message = "Bank Deleted successfully !!";
       return response()->json(['messages' => $message],200);
    }
   
    public function bankcompany(Request $request,$id){
        $bank = Bank::where('id',$id)->FirstOrFail();
        $companies =  Customer::all()->pluck('name','id')->prepend('Select Company ');
        $cid = $request->get('company');
        if(!empty($cid)){
            $company = Customer::where('id',$id)->get();
        }
        return view('admin.banks.bankcompany',compact('bank','companies','company'));
    }

    public function cession($id){
        $bank_id = $id;
        return view('admin.banks.cession',compact('bank_id'));
    }

    public function cessiondatatable($id){
        $transaction = Transaction::with('customerCession','supplier')->where('bank_id',$id)->where('type','cession')->get();
        return Datatables::of($transaction)
            ->make(true);
        
    }

    public function summarydatatable()
    {
        $banks = Bank::with('customerCount','givenMoney', 'invoiceQty', 'usedMoney')->get();

        return Datatables::of($banks) ->make(true);

    }

    public function bankSummary(){
        return view('admin.banks.summary');
    }

}
