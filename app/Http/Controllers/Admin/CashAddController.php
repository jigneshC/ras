<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddCash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CashAdd;
use App\Customer;
use function GuzzleHttp\json_encode;
use App\AvailableCashHistory;
use APP\ActionLog;
use App\Role;
use App\User;
use DB;


class CashAddController extends Controller
{   
    public function __construct()
    {
      //  $this->middleware('permission:update.cash.customer')->only(['store']);
      //  $this->middleware('permission:add.cash.customer')->only(['store']);
    }
    
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
      
        if($request){
    
            $customer = Customer::with('supplier','user')->find($request->input('customer_id'));
            $sales_user = '';
            foreach ($customer->user as $user){
                if($user->role[0]->name == 'CBS'){
                    $sales_user = $user->name;
                }
            }         

            if($customer){
               
                $cashAdd = new CashAdd;
                if($request->input('cash') == null){
                    $cashAdd->cash = 0; 
                }else{
                    $cashAdd->cash = $request->input('cash');
                }
                
                $cashAdd->customer_id = $request->input('customer_id');
                $cashAdd->save();
                
                \ActionLog::addToLog("New Cash Added"," Cash ID - " . $cashAdd->id ." is added", $cashAdd->getTAble() , $cashAdd->id);
                
                if(\Auth::check()){
                    if(\Auth::user()->roles[0]->name == "SU"){
                        //if cash is added by admin, notification send to customers sales's user CB_Sales

                        foreach ($customer->user as $user){
                            if($user->role[0]->name == 'CBS'){
                                $sales_user = $user->name;
                                $users = User::find($user->id);
                                $users->notify(new AddCash($cashAdd));
                            }
                        }

                    }else{

                        //if cash is added by any sales user then notification send to admin
                        $su = Role::Where('name','SU')->get();
                        if($su != ""){
                            foreach($su as $roles){
                                foreach($roles->users as $su_user){
                                    $user_id[] = $su_user->id ;
                                } 
                            }
                        }
                        $users = User::whereIN('id',$user_id)->get();
                        foreach($users as $adminuser){
                            $adminuser->notify(new AddCash($cashAdd));
                        }
                    }
                }
                $duedate = str_replace('/','-',$request->input('cash_expiry'));
                $customer->expiry_date= date('Y-m-d' , strtotime($duedate));
                $customer->save();
                $history = new AvailableCashHistory;
                $history->customer_id = $cashAdd->customer_id;
                $history->sales_user = $sales_user ;
                $history->action = "credit";
                $history->date = date('Y-m-d');
                $history->amount = $cashAdd->cash;
                $history->save();
                return json_encode(array('msg'=>'Success','data'=>$customer->expiry_date));
                exit;
            }
        }else{
            return json_encode(array('msg' => 'Error'));
            exit;
        }
    }

    public function show($id)
    {
        $cashtotal = Customer::with('CashAvailableAnticipation')->find($id);

        $cashexpiry_date=Customer::where('id',$id)->select(DB::Raw('expiry_date as expiry_date'))->get();

        return response()->json(['cashtotal' => $cashtotal,'expiry_date' => $cashexpiry_date]);
       
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
