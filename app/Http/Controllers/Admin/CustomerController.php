<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddCustomer;
use App\Notifications\SupplierCustomer;
use App\Notifications\ChangeCBSales;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Role;
use App\User;
use App\Address;
use App\CustomerUser;
use App\Supplier;
use App\Invoice;
use App\CustomerSupplier;
use Session;
use CountryState;
use DB;
use Yajra\Datatables\Datatables;
use App\ActionLog;
use App\Transaction;
use App\TransactionsInvoices;
use App\CashAdd;
use App\DiscountRate;
use App\AvailableCashHistory;
use Auth;
use App\SupplierUser;
use App\BankCustomerMoney;

class CustomerController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.customers');
        $this->middleware('permission:access.customers.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.customers.create')->only(['create', 'store']);
        $this->middleware('permission:access.customers.delete')->only('destroy');
       // $this->middleware('permission:access.customer.status')->only('show');
        $this->middleware('permission:change.cb.rep.customer')->only('changeCbRepCustomer');
        
    }
    
    public function index(Request $request)
    {
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id', 'roles.label as role_label')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['CBS'])
            ->where('users.user_status','=','active')->get();
            
        return view('admin.customer.index',compact('user'));
    }

    public function datatable(Request $request){
        $day = $request->get('cashavailableexpire');
        $cashavailable = $request->get('cashavailable');
        $date = date("Y-m-d");
        $name = '';
        $end_date = date('Y-m-d', strtotime($date . "+ " . $day . " days"));
        $user = User::find(auth()->user()->id);
        $customers = Customer::leftjoin('customer_user','customer_user.customer_id','=','customer.id')
        ->leftJoin('users','users.id','=','customer_user.user_id')
        ->leftJoin('role_user','role_user.user_id','=','users.id')
        ->leftJoin('roles','roles.id','=','role_user.role_id')
        ->where('roles.name','=','CBS')
        ->with('invoiceSum', 'CashAvailableAnticipation','CashAvailableCession');

        if ($cashavailable != '') {
            $customers->select(DB::raw('sum(customer_cash_available.cash) as cashavailable'));
            $customers->leftjoin('customer_cash_available', 'customer_cash_available.customer_id', '=', 'customer.id')->havingRaw('sum(customer_cash_available.cash) <= ' . $cashavailable)->groupBy('customer_cash_available.customer_id');
        }
        if($user->roles[0]->name =='CBS' || $user->roles[0]->name =='CM'){
            $user = User::with('customer')->find(auth()->user()->id);
            $customers_id = array();
            foreach($user->customer as $userCustomer){
                $customers_id[] = $userCustomer->id;
            }
            
            $customers->whereIn('customer.id',$customers_id);
        }
        if ($day != '') {
            $customers->where('customer.expiry_date', '<=', $end_date);
        }
        $customers->select('customer.*','users.name as user_name','users.id as user_id')->distinct()->get();
		return Datatables::of($customers)
        ->make(true);
        exit;
    }

    public function create()
    {

        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');
        //suppliers
        $supplier = array();
        $suppliers = Supplier::all();
        foreach($suppliers as $oldSuppliers){
            $supplier[$oldSuppliers->id] = $oldSuppliers->name;
        }
        $roles = Role::where('name','SLO')->orwhere('name','SLA')->get();
        $supllierUsers = array();
        foreach($roles as $role){
            foreach($role->users as $user){
                $supllierUsers[$role->name][$user->id] = $user->name;
            }
        }
        //cbsales user
        $salesUsers = ["" => "Selecione"]; //select
        $cbsroles = Role::where('name','CBS')->get();
        if($cbsroles != ""){
            foreach($cbsroles as $cbsrole){
                foreach($cbsrole->users as $user){
                    $salesUsers[$user->id] = $user->name;
                }
            }
        }

        return view('admin.customer.create', compact('countries', 'users','supplier','supllierUsers','salesUsers'));
    }


    public function store(Request $request)
    {   
        $messages = [
            'cpnj.unique' =>"Found Customer, $request->cpnj using this CNPJ",
        ];

        $this->validate($request, [
            'name' => 'required',
            'legal_name' => 'required',
            'cpnj' => 'required|unique:customer,cpnj',
            'address1' => 'required',
            'address2' => 'required',
            'country_name' => 'required',
            'state' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'neighborhood' => 'required',
            'rate' => 'required',
            'cbuser' => 'required'
        ],$messages);

        $customer = new Customer;
        $customer->name = $request->input('name');
        $customer->legal_name = $request->input('legal_name');
        $customer->cpnj = $request->input('cpnj');
        $customer->address_line1 = $request->input('address1');
        $customer->address_line2 = $request->input('address2');
        $customer->address_complement = $request->input('address_complement');
        $customer->neighborhood = $request->input('neighborhood');
        $customer->country_name = $request->input('country_name');
        $customer->city = $request->input('city');
        $customer->state = $request->input('state');
        $customer->zip = $request->input('zip');
        $customer->neighborhood = $request->get('neighborhood');
        $customer->rate = $request->get('rate');

        if ($request->file('image')) {
            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $filename);
            $customer->image = $filename;
        }

    	$customer->save();
	    Session::flash('flash_success', __('Customer added!'));
        \ActionLog::addToLog("Add Customer","Customer- " .$customer->name .   " is added",$customer->getTAble(),$customer->id);

        if($customer->save() && !empty($request->get('cbuser'))){	
            $cbsuser = new CustomerUser();
            $cbsuser->customer_id = $customer->id;
            $cbsuser->user_id = $request->input('cbuser');
            $cbsuser->save();

            $cbuser = User::where('id',$request->get('cbuser'))->FirstorFail();

            \ActionLog::addToLog("Add ConnectBahn Sales","Customer -" .$customer->name . " has add sales user", $customer->getTAble() , $customer->id , array('customer'=> $customer->name ,  'date'=>date('d-m-Y') ,'cbuser'=>$cbuser->name,'old_salesUsers'=>'','changed_by' => Auth::user()->name ) );

            $user = User::find($cbsuser->user_id);

            $user->notify(new AddCustomer($customer));
        }
        return redirect('admin/customer');
    }

    public function show(Request $request, $id)
    {
        $role = Role::where("name", "CM")->first();
        foreach ($role->users as $user) {
            $users[$user->id] = $user->name;
        }
        $countries = CountryState::getCountries();
        $customer = Customer::with('supplier','user')->find($id);
        $status = $request->get('status');
        if (!empty($status)) {
            if ($status == 'active') {
                $customer->is_status = 'inactive';
                $customer->update();

                \ActionLog::addToLog("Customer Status Changed","Customer - ". $customer->name ."'s status changed to Inactive",$customer->getTAble(),$customer->id);

                return redirect()->back();
            } else {
                $customer->is_status = 'active';
                $customer->update();

                \ActionLog::addToLog("Customer Status Changed","Customer - ". $customer->name ."'s status changed to Active",$customer->getTAble(),$customer->id);

                return redirect()->back();
            }
        }
        $suppliers = Supplier::all();
        foreach($suppliers as $oldSuppliers){
            $supplier[$oldSuppliers->id] = $oldSuppliers->name;
        }
        $states = CountryState::getStates($customer->country_name);
        $customer->country_name = $countries[$customer->country_name];
        $customer->state = $states[$customer->state];

        return view('admin.customer.show', compact('countries', 'states', 'operator', 'customer','supplier','customer'));
    }

    public function edit($id)
    {   
      
        $customer = Customer::with('supplier','user')->find($id);
        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');
        $country_id=$customer->country_name;
      
        $state_id=$customer->state;
        $city_id=$customer->city;
        $states = DB::table("states")
                    ->where("country_id",$country_id)
                    ->pluck("name","id");
      
        $cities = DB::table("cities")
                    ->where("state_id",$state_id)
                    ->pluck("name","id");
        $salesCustUsers = '';
        foreach ($customer->user as $user){
            if($user->role[0]->name == 'CBS'){
                $salesCustUsers = $user->id;
            }
        }
        $cbsroles = Role::where('name','CBS')->get();
        $salesUsers = array();
        if($cbsroles != ""){
            foreach($cbsroles as $cbsrole){
                foreach($cbsrole->users as $user){
                    $salesUsers[$user->id] = $user->name;
                }
            }
        }

        //operator
        $operatorCustUsers = '';
        foreach ($customer->user as $user){
            if($user->role[0]->name == 'CM'){
                $operatorCustUsers = $user->id;
            }
        }
        $oproles = Role::where('name','CM')->get();
        $operatorUsers = array();
        if($oproles != ""){
            foreach($oproles as $oprole){
                foreach($oprole->users as $user){
                    $operatorUsers[$user->id] = $user->name;
                }
            }
        }

        //Add Cash
        $cashAdd = CashAdd::where('customer_id',$id)->get();
        $cashAdded = (count($cashAdd) > 0) ? 1 : 0 ;
        return view('admin.customer.edit', compact('state_id','states','countries','country_id','cities','city_id','users', 'customer','supplier','supllierUsers','salesUsers','salesCustUsers','operatorUsers','operatorCustUsers','cashAdded'));
    }

    public function update(Request $request, $id)
    {   
        $messages = [
            'cpnj.unique' =>"Found Customer, $request->cpnj using this CNPJ",
        ];
        $this->validate($request, [
            'name' => 'required',
            'legal_name' => 'required',
            'cpnj' => 'required|unique:customer,cpnj,'.$id,
            'address1' => 'required',
            'address2' => 'required',
            'country_name' => 'required',
            'state' => 'required',
            'city' => 'required',
            'zip' => 'required',
        ],$messages);

        $customer = Customer::find($id);

        $customer->name = $request->input('name');
        $customer->legal_name = $request->input('legal_name');
        $customer->cpnj = $request->input('cpnj');
        $customer->address_line1 = $request->input('address1');
        $customer->address_line2 = $request->input('address2');
        $customer->address_complement = $request->input('address_complement');
        $customer->neighborhood = $request->input('neighborhood');
        $customer->country_name = $request->input('country_name');
        $customer->city = $request->input('city');
        $customer->state = $request->input('state');
        $customer->zip = $request->input('zip');

        if ($request->file('image')) {
            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $filename);
            $customer->image = $filename;
        }

        $customer->update();

        if($request->get('old_salesUsers') == ''){
            $customer_users = new CustomerUser;
            $customer_users->customer_id = $customer->id;
            $customer_users->user_id = $request->get('cbuser');
            $customer_users->save();
        }elseif($request->get('old_salesUsers') != ''){
            $customer_users = CustomerUser::where('customer_id',$customer->id)->where('user_id',$request->get('old_salesUsers'))->FirstorFail();
            if(!empty($request->get('cbuser')) && $request->get('cbuser') != $request->get('old_salesUsers') ){
                $customer_users->user_id = $request->get('cbuser');
                $customer_users->save();
                \ActionLog::addToLog("Change ConnectBahn Sales","Customer -" .$customer->name . " has changed sales user",$customer->getTAble(),$customer->id,array('customer'=>$customer->id,'cbuser'=>$request->get('cbuser'),'old_salesUsers'=>$request->get('old_salesUsers') ));
            }else{
                $customer_users->save();
            }
        }

        Session::flash('flash_success', __('Customer Updated!'));
        \ActionLog::addToLog("Edit Customer","Customer-" .$customer->name . " is updated",$customer->getTAble(),$customer->id);
        return redirect('admin/customer');
    }

    public function destroy($id)
    {
        $customer = Customer::find($id);
        
        if($customer->image != ""){
           $file = public_path('images/' .$customer->image);
            if (file_exists($file)) {
                unlink($file);
            } 
        }
        
        $customer->delete();
        $cust_user = CustomerUser::where('customer_id',$id)->get();
        foreach($cust_user as $cust){
            $cust->delete();
        }
        \ActionLog::addToLog("Customer Deleted","Customer-" .$customer->name . "is deleted",$customer->getTAble(),$customer->id);
        return redirect('admin/customer');
    }

    public function getstate(){
        if($_GET['country_id'] != ''){
            $states = CountryState::getStates($_GET['country_id']);
            return $states;
        }else{
            return false;
        }
    }

    public function customersupplier(Request $request,$id){
        
        $customer_id = $id;
        $customer = Customer::find($id);
        $perPage = 5;

        //display discount rate for admin
        $discountRate = DiscountRate::where('customer_id','=','0')->get();

        //display  discount rate for customer
        $customer_discount_rate = DiscountRate::where('customer_id',$customer_id)->get();

        $custsup = CustomerSupplier::with('customer', 'supplier','discountRate','customer_discount_rate')
            ->where('customer_id',$customer_id)->paginate($perPage);  
        
		$custsup_get = CustomerSupplier::with('customer','supplier')->where('customer_id',$customer_id)->get();
        foreach ($custsup as $value) {
            $supp[] = $value->supplier_id;
        }
        if(isset($supp)){
            $suppliers = Supplier::whereNotIn('id',$supp)->pluck('name','id');
        }else{
            $suppliers = Supplier::all()->pluck('name','id');
        }
        $sup = $request->get('supplier');
        if(!is_array($sup) && $request->get('supplier') != ''){
            $sup = array();
            $sup[] = $request->get('supplier');
        }
        //find rate class  'E'
        $rate = DiscountRate::where('class','E')->first();
        if(!empty($sup)){
            
            foreach ($sup as $supplier_id) {
                $cust_sup = new CustomerSupplier;
                $cust_sup->customer_id = $customer_id;
                $cust_sup->supplier_id = $supplier_id;
                $cust_sup->customer_supplier_rate_by_commercial_user = $rate->id; //storing Deafult id of rate class 'E'
                $cust_sup->save();

                $supplier_name = Supplier::find($supplier_id);

                \ActionLog::addToLog("Customer Linked with Supplier","Customer- ". $customer->name ." is linked with new Supplier- ". $supplier_name->name ,$cust_sup->getTAble(),$cust_sup->id);

                //send notification to CM or CB-Sales
                $all_cust = CustomerUser::where('customer_id',$customer_id)->get();

                foreach ($all_cust as $all ) {
                
                    $cust_user =  $all->user_id;

                    $notify_cust = User::find($cust_user);

                    $notify_cust->notify(new SupplierCustomer($cust_sup));
                }

                //send notification to SLA,SLO or CB-Commercial
                $all_sup = SupplierUser::where('supplier_id',$supplier_id)->get();

                foreach ($all_sup as $all ) {
                
                    $sup_user =  $all->user_id;

                    $notify_sup = User::find($sup_user);

                    $notify_sup->notify(new SupplierCustomer($cust_sup));
                }

            }   

            return redirect()->back();     
        }
        return view('admin.customer.customer_supplier',compact('custsup','customer_id','suppliers','supplier', 'customer', 'discountRate','customer_discount_rate'));
    }


    public function deleteCustSup($id){
        $cust_sup = CustomerSupplier::find($id);
        $cust_sup->delete();
        Session::flash('flash_message', 'Customer relation to supplier deleted!');
        return redirect()->back();
    }

    public function summary_customer(){

        $user = User::find(Auth::user()->id) ;
        if($user->roles[0]->name == "CBS" || $user->roles[0]->name == "CM"){
            $cust_user = CustomerUser::where('user_id',$user->id)->pluck('customer_id');
            $customer = Customer::whereIn('id',$cust_user)->where('is_status','active')->latest()->paginate(10);
        }elseif($user->roles[0]->name == "SU"){           
            $customer = Customer::where('is_status','active')->latest()->paginate(10);
        }else{
            $customer = Customer::where('is_status','active')->latest()->paginate(10);
        }
        return view('admin.customer.summary_customer',compact('customer'));
    }

    public function transactionHistory($id){
        $startdate = date('01-m-Y');
        $enddate = date('t-m-Y');
        $sdate = date('Y-m-d', strtotime($startdate));
        $edate = date('Y-m-d', strtotime($enddate));
        $customer = Customer::with('user')->find($id);
        $customer = Customer::with('user')->with(
            [
                'cash_anticipation' => function($query) use ($sdate, $edate) {
                    $query->whereBetween('customer_cash_available.created_at', [$sdate . " 00:00:00", $edate . " 23:59:59"]);
                }
            ])
            ->with([
                'cashAvailable' =>function($query) use ($sdate, $edate){
                    $query->whereBetween('available_cash_history.created_at', [$sdate . " 00:00:00", $edate . " 23:59:59"]);
                }
            ])
            ->with(['anticipation_to_be_paid_approved_today'])
            ->with( 'anticipation_to_be_done')->with (['cashExpiry' => function($query){
                $query->first();
                }
            ])->
            
            find($id);
        
        return view('admin.customer.transactionhistory',compact('id','startdate','enddate','customer'));
    }

    public function transactionfilter(Request $request){
        
        $id = $request->get('customer_id');
        $sdate = $request->get('startdate');
        $edate = $request->get('enddate');
        $stdate = str_replace('/', '-', $sdate);
        $startdate = date('Y-m-d', strtotime($stdate));
        $endate = str_replace('/', '-', $edate);
        $enddate = date('Y-m-d', strtotime($endate));
        $customer = Customer::with('user')->with(
            [
                'cash_anticipation' => function ($query) use ($startdate, $enddate) {
                    $query->whereBetween('customer_cash_available.created_at', [$startdate . " 00:00:00", $enddate . " 23:59:59"]);
                }
            ]
        )
            ->with([
                'cashAvailable' => function ($query) use ($startdate, $enddate) {
                    $query->whereBetween('available_cash_history.created_at', [$startdate . " 00:00:00", $enddate . " 23:59:59"]);
                }
            ])
            ->with(['anticipation_to_be_paid_approved_today'])
            ->with('anticipation_to_be_done')->with(['cashExpiry' => function ($query) {
                $query->first();
            }])->find($id);
        $data['cashavailable'] = 0;
        
        if(count($customer->cashAvailable) > 0 ){
            $data['cashavailable'] = $customer->cashAvailable[0]->available_cash;
        }
        $data['cashavailableforanticipation'] = 0;
        if (count($customer->cash_anticipation) > 0) {
            $data['cashavailableforanticipation'] = $customer->cash_anticipation[0]->anticipation;
        }
         
        return json_encode($data);
        
    }
    public function transactionHistoryDatatable(Request $request){
        $id = $request->get('customer_id');
        $sdate=$request->get('startdate');
        $edate=$request->get('enddate');
        $stdate = str_replace('/','-',$sdate);
        $startdate= date('Y-m-d' , strtotime($stdate));
        $endate = str_replace('/','-',$edate);
        $enddate= date('Y-m-d' , strtotime($endate));
        $search = $request->get('searchText');

        $customerWithSupplier = Customer::with([
            'supplier' => function ($q) use ($search) {
                if($search != ''){
                    $q->where('supplier.name','like','%'.$search.'%');
                }
            }
        ])->find($id);
        $suppliers = $customerWithSupplier->supplier;
        
        $transactions = TransactionsInvoices::select('transactions_invoices.*', 'invoices.duedate','invoices.anticipation_date', 'invoices.total_amount', 'invoices.discount_rate', 'invoices.discounted_value', 'invoices.cost', 'invoices.status')
            ->join('invoices', 'invoices.id', '=', 'transactions_invoices.invoice_id')
            ->where('transactions_invoices.customer_id', $id)
            ->where('transactions_invoices.approver_id','>','0')
            ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
            ->whereBetween('transactions_invoices.created_at', [$startdate . " 00:00:00",$enddate . " 23:59:59"])
            ->get();
        
        
         
        if($suppliers->count() == 0){
           
            return Datatables::of($suppliers)
            ->make(true);
            exit;
        
        }else{
            
        foreach ($suppliers as $key => $supplier) {
            $mean_rate = 0; 
            $transactionInvoice = array();
            $alldataSupplier[$key] = $supplier;
            $alldataSupplier[$key]['totalInvoiceCession'] = 0;
            $alldataSupplier[$key]['totalInvoiceAnticipation'] = 0;
            $alldataSupplier[$key]['totalInvoice'] = 0;
            $alldataSupplier[$key]['amounttotal'] = 0;
            $alldataSupplier[$key]['cessiontotal'] = 0;
            $alldataSupplier[$key]['anticipationtotal'] = 0;
            $alldataSupplier[$key]['rate'] = 0;
            $alldataSupplier[$key]['revenuewForAnticipation'] = 0;
            $alldataSupplier[$key]['revenuewForCession'] = 0;
            $alldataSupplier[$key]['rate_value'] = 0;
            $alldataSupplier[$key]['mean_rate'] = 0;

            foreach ($transactions as $transaction) {
                if ($supplier['id'] == $transaction['supplier_id']) {
                    if( $transaction['transaction_number'] != ''){
                        if(!in_array($transaction['transaction_number'], $transactionInvoice)){
                            $transactionInvoice[$transaction['transaction_number']] = $transaction['transaction_number'];
                            $transactionNumber = $transaction['transaction_number'];
                            $transactionGet = Transaction::find($transactionNumber); 
                           
                            $alldataSupplier[$key]['rate_value'] = $alldataSupplier[$key]['rate_value'] +  ( $transactionGet->discount_rate / 100 ) * $transactionGet->total_amount  ;
                            
                            
                            if($transactionGet->type == 'Cession'){
                                if ($alldataSupplier[$key]['revenuewForCession'] != '') {
                                    $alldataSupplier[$key]['revenuewForCession'] = $alldataSupplier[$key]['revenuewForCession'] + $transactionGet->discount_value - $transactionGet->cost;
                                }else{
                                    $alldataSupplier[$key]['revenuewForCession'] =  $transactionGet->discount_value - $transactionGet->cost;
                                }
                            }else{
                                if ($alldataSupplier[$key]['revenuewForAnticipation'] != '') {
                                    $alldataSupplier[$key]['revenuewForAnticipation'] = $alldataSupplier[$key]['revenuewForAnticipation'] + $transactionGet->discount_value - $transactionGet->cost;
                                } else {
                                    $alldataSupplier[$key]['revenuewForAnticipation'] = $transactionGet->discount_value - $transactionGet->cost;
                                }
                            }

                                if ($transactionGet->type == 'Cession') {
                                    
                                    $alldataSupplier[$key]['totalInvoiceCession'] = $alldataSupplier[$key]['totalInvoiceCession'] + 1;
                                }

                                if ($transactionGet->type == 'Anticipation') {
                                   
                                    $alldataSupplier[$key]['totalInvoiceAnticipation'] = $alldataSupplier[$key]['totalInvoiceAnticipation'] + 1;
                                } 
                            
                        }
                    }
                    if ($transaction['payment_type'] == 'Cession') {
                        if ($alldataSupplier[$key]['cessiontotal'] != '') {
                            $alldataSupplier[$key]['cessiontotal'] = $alldataSupplier[$key]['cessiontotal'] + $transaction['total_amount'];
                        } elseif ($transaction['total_amount'] > 0) {
                            $alldataSupplier[$key]['cessiontotal'] = $transaction['total_amount'];
                        } else {
                            $alldataSupplier[$key]['cessiontotal'] = 0;
                        }
                      
                    } elseif (!isset($alldataSupplier[$key]['cessiontotal'])) {
                        $alldataSupplier[$key]['cessiontotal'] = 0;
                    }
                    if ($transaction['payment_type'] == 'Anticipation') {
                        if ($alldataSupplier[$key]['anticipationtotal'] != '') {
                            $alldataSupplier[$key]['anticipationtotal'] = $alldataSupplier[$key]['anticipationtotal'] + $transaction['total_amount'];
                        } elseif ($transaction['total_amount'] > 0) {
                            $alldataSupplier[$key]['anticipationtotal'] = $transaction['total_amount'];
                        } else {
                            $alldataSupplier[$key]['anticipationtotal'] = 0;
                        }

                    } elseif (!isset($alldataSupplier[$key]['anticipationtotal'])) {
                        $alldataSupplier[$key]['anticipationtotal'] = 0;
                    }

                    if ($alldataSupplier[$key]['amounttotal'] != '') {
                        $alldataSupplier[$key]['amounttotal'] = $alldataSupplier[$key]['amounttotal'] + $transaction['total_amount'];
                    } else {
                        $alldataSupplier[$key]['amounttotal'] = $transaction['total_amount'];
                    }
                    if ($alldataSupplier[$key]['rate'] != '') {
                        $alldataSupplier[$key]['rate'] = $alldataSupplier[$key]['rate'] + $transaction['discount_rate'];
                    } else {
                        $alldataSupplier[$key]['rate'] = $transaction['discount_rate'];
                    }
                    $alldataSupplier[$key]['totalInvoice'] = $alldataSupplier[$key]['totalInvoice'] + 1;

                    $mean_rate  = ( $alldataSupplier[$key]['rate_value'] / $alldataSupplier[$key]['amounttotal'] ) * 100 ; 
                    $alldataSupplier[$key]['mean_rate'] = round($mean_rate,4);
                   
                }
            }
            

        }
                
        $alldataSupplier = collect($alldataSupplier);
        return Datatables::of($alldataSupplier)
            ->make(true);
        }   
        
    }

    public function cashHistoryDatatable(Request $request){
        $history = AvailableCashHistory::with('supplier')
            ->where('customer_id',$request->get('customer_id'))
            ->orWhere('bank_id','=','')
           ->get();
          
        return Datatables::of($history)
            ->make(true);
        exit;
    }

    public function changeCbRepCustomer(Request $request){

        if($request->input('submit')){

            if($request->input('cbrepforcustomer') != 0 && $request->input('oldcbrep_id') != '' ){

                if($request->input('cbrepforcustomer') == $request->input('oldcbrep_id')){

                    return json_encode(array('msg' => 'Success'));

                }else{

                    if($request->input('oldcbrep_id') != ''){

                        $customer_users = CustomerUser::where('customer_id', $request->input('cbcustomer_id'))->where('user_id', $request->input('oldcbrep_id'))->FirstorFail();

                        $customer_users->user_id = $request->get('cbrepforcustomer');

                        $customer_users->save();

                        $customer_name = Customer::where('id',$request->input('cbcustomer_id'))->FirstorFail();

                        $cbuser = User::where('id',$request->get('cbrepforcustomer'))->FirstorFail();

                        \ActionLog::addToLog("Change ConnectBahn Sales","Customer -" .$customer_name->name . " has changed sales user", $customer_users->getTAble() , $customer_users->id , array('customer'=> $customer_name->name ,  'date'=>date('d-m-Y') ,'cbuser'=>$cbuser->name,'old_salesUsers'=>$request->input('oldcbrep_id'),'changed_by' => Auth::user()->name ) );

                        $old_user = User::find($request->input('oldcbrep_id'));

                        $old_user->notify(new ChangeCBSales($customer_users,$old_user));

                        $cbuser->notify(new ChangeCBSales($customer_users,$old_user));

                        return json_encode(array('msg' => 'Success'));

                    }elseif($request->input('oldcbrep_id') == '0'){

                        $customer_users = new CustomerUser;
                        $customer_users->customer_id = $request->input('cbcustomer_id');
                        $customer_users->user_id = $request->get('cbrepforcustomer');
                        $customer_users->save();

                        $customer_name = Customer::where('id',$request->input('cbcustomer_id'))->FirstorFail();

                        $cbuser = User::where('id',$request->get('cbrepforcustomer'))->FirstorFail();

                        \ActionLog::addToLog("Change ConnectBahn Sales","Customer -" .$customer_name->name . " has changed sales user", $customer_users->getTAble() , $customer_users->id , array('customer'=> $customer_name->name ,  'date'=>date('d-m-Y') ,'cbuser'=>$cbuser->name,'old_salesUsers'=>$request->input('oldcbrep_id'),'changed_by' => Auth::user()->name ) );

                        return json_encode(array('msg' => 'Success'));
                    
                    }else{

                        return json_encode(array('msg' => 'Error'));            
                    }
                }
            }else{
                return json_encode(array('msg' => 'Error'));    
            }
        }else{

            return json_encode(array('msg' => 'Error'));
        }

    }

    public function customer_log($id){

        $customer_name = Customer::where('id',$id)->pluck('name')->first();

        $log = ActionLog::whereIn('action', array('Change ConnectBahn Sales', 'Add ConnectBahn Sales'))->get();
        $detail = array();
        foreach ($log as $key) {
            $detail[] = unserialize($key->sub_detail);
        }
        foreach ($detail as $key => $value){
            if($customer_name == $value["customer"]){
                $pass[] = $value;
            }else{ 
                $pass[] = "";
            }
        }
        return view('admin.customer.customer_log',compact('pass'));

    }

    public function bank_assigned(Request $request,$id){
        $customer = Customer::where('id',$id)->FirstorFail();
        return view('admin.customer.bank_assigned',compact('customer'));
    }

    public function bank_assigned_datatable(Request $request){
        $allbankdetail = array();
        if(!empty($request->get('customer_id'))){
            
            $banks = BankCustomerMoney::where('bank_customer_allocated_money.customer_id',$request->get('customer_id'))->with('bank','customer')->select('*',DB::raw('sum(bank_customer_allocated_money.total_amount) as total' ))->groupBY('bank_customer_allocated_money.bank_id')->get();
            $transactions  = Transaction::where('customer_id', $request->get('customer_id'))->where('approver_id','!=',0)->where('payment_status','=','Approved')->where('type','Cession')->get();
            foreach($banks as $key => $bank){
                $allbankdetail[$key] = $bank;
                $allbankdetail[$key]['cash_consumed'] = 0;
                $allbankdetail[$key]['cash_still_available'] = 0;
                foreach($transactions as $transaction){
                    if($transaction->bank_id == $bank->bank_id){
                        $allbankdetail[$key]['cash_consumed'] = $allbankdetail[$key]['cash_consumed'] + $transaction->total_amount;
                    }
                }
                $bankExpiryDate = BankCustomerMoney::where('bank_id', $bank->bank_id)->where('customer_id', $bank->customer_id)->orderBy('id', 'desc')->first();
                $allbankdetail[$key]['duedate'] = $bankExpiryDate->duedate;
                $allbankdetail[$key]['bank_rate'] = $bankExpiryDate->bank_rate;
                $allbankdetail[$key]['cash_still_available'] = $allbankdetail[$key]['total'] - $allbankdetail[$key]['cash_consumed'];
            }
        }
        $allbankdetail = collect($allbankdetail);
        return Datatables::of($allbankdetail)
            ->make(true);
        exit;
    }
    

}
