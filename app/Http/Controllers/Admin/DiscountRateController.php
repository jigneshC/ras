<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DiscountRate;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;
use App\SupplierToCustomerRate;
use App\CustomerSupplier;
use App\Customer;
use App\Supplier;
use Illuminate\Validation\Rule; 

class DiscountRateController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.discount.rate');
        $this->middleware('permission:edit.discount.rate')->only(['edit', 'update']);
        $this->middleware('permission:create.discount.rate')->only(['create', 'store']);
        $this->middleware('permission:delete.discount.rate')->only('destroy');
    }

    public function index(Request $request)
    {
        $customer_id = '';
        if(!empty($request->get('customer_id'))){
            $customer_id = $request->get('customer_id');
            $customer = Customer::find($customer_id);
        }
        
        return view('admin.discount-rate.index',compact('customer_id','customer'));
    }

    public function datatable(Request $request){

        if(!empty($request->get('customer_id'))){
            $discountrate = DiscountRate::orderBy('class','asc')->where('customer_id',$request->get('customer_id'));
        }else{
            $discountrate = DiscountRate::orderBy('class','asc')->where('customer_id','=','0');
        }
       
        return Datatables::of($discountrate)
            ->make(true);
            exit;
    }

    public function create(Request $request)
    {   

        if(!empty($request->get('customer_id'))){
            $customer_id = $request->get('customer_id');
            $customer = Customer::find($customer_id);
        }
        return view('admin.discount-rate.create',compact('customer','customer_id'));
    }


    public function store(Request $request)
    {
        if($request->has('customer_id')){

            $query = DiscountRate::where('customer_id','=',$request->has('customer_id'))->pluck('customer_id','class')->toArray();
            if(array_key_exists($request->class,$query)){
                $this->validate($request, [
                    'class' => 'required | unique:discount_rate,class',
               ]);
            }

        }else{
            $query = DiscountRate::where('customer_id' ,'=','0')->pluck('customer_id','class')->toArray();
            
                   if(array_key_exists($request->class,$query)){

                     $this->validate($request, [
                       'class' => 'required | unique:discount_rate,class',
            
                   ]);
            }
        }
        $requestData = $request->all();
        
        $dis = DiscountRate::create($requestData);

        \ActionLog::addToLog("Add Discount Rate Class Added"," New Discount rate Class- ". $dis->class ." is added ",$dis->getTAble(),$dis->id);
        
        if($request->has('customer_id')){
            
            return redirect('admin/discount-rate?customer_id='.$request->get('customer_id')); 
        }

        Session::flash('flash_message', 'DiscountRate added!');

        return redirect('admin/discount-rate');
    }

    public function show($id)
    {
        $discountrate = DiscountRate::findOrFail($id);

        return view('admin.discount-rate.show', compact('discountrate'));
    }

    public function edit($id)
    {
        $discountrate = DiscountRate::findOrFail($id);

        return view('admin.discount-rate.edit', compact('discountrate'));
    }

    public function update($id, Request $request)
    {   
        $discountrate = DiscountRate::findOrFail($id);
            
            $query = DiscountRate::where('id' ,'!=',$id)
            ->where('class', $request->class);
            if($discountrate->customer_id > 0){ 
                $query->where('customer_id','=',$discountrate->customer_id);
            }else{
                $query->where('customer_id','=',0);
            }
            $res = $query->pluck('customer_id', 'class')->toArray();
            $class = $request->class;
            if(array_key_exists($class, $res)){
                $this->validate($request, [
                        'class' => 'required | unique:discount_rate,class,'.$id,
                ]);
            }
            
        $requestData = $request->all();
        
        $discountrate = DiscountRate::findOrFail($id);

        $discountrate->update($requestData);
    
        \ActionLog::addToLog("Discount Rate Class Updated","Discount rate Class- ". $discountrate->class ." is updated ",$discountrate->getTAble(),$discountrate->id);
            
        if($discountrate->customer_id == 0){
            Session::flash('flash_message', 'DiscountRate updated!');
            
            return redirect('admin/discount-rate');
        }else{
            return redirect('admin/discount-rate?customer_id='.$discountrate->customer_id);
        }

        
    }

    public function destroy($id)
    {
        $dis = DiscountRate::find($id);

        \ActionLog::addToLog("Discount Rate Class Deleted","Discount rate Class- ". $dis->class ." is Deleted",$dis->getTAble(),$dis->id);

        $dis->delete();

        Session::flash('flash_message', 'DiscountRate deleted!');

    }

    public function addClassSupplierToCustomer(Request $request){
        if($request){
            $add_class = CustomerSupplier::where('customer_id',$request->input('customer_id'))->where('supplier_id',$request->input('supplier_id'))->get();

            if(count($add_class) > 0 && $request->input('customer_supplier_id') > 0 && $request->input('customer_supplier_id') == $add_class[0]->id ){

                $add_class = CustomerSupplier::find($add_class[0]->id);
                $add_class->customer_id = $request->input('customer_id');
                $add_class->supplier_id = $request->input('supplier_id');
                $add_class->customer_supplier_rate_by_commercial_user = $request->input('class');
                $add_class->save();
                
                $sup = Supplier::find($request->get('supplier_id'));
                $sup->cb_rate_class = $request->input('class');
                $sup->save();

                $cust = Customer::find($request->get('customer_id'));

                \ActionLog::addToLog("CB Rate Class Changed","CB rate Class changed for Supplier- ".$sup->name." and Customer- ".$cust->name , $add_class->getTAble(),$add_class->id);

                return json_encode(array('msg'=> 'Success'));
            }else{
                return json_encode(array('msg' => 'Error'));
            }
        }

    }

    public function addClassByCustomer(Request $request){
        
        if($request){
            $add_class = CustomerSupplier::where('customer_id',$request->get('customer_id'))->where('supplier_id',$request->get('supplier_id'))->get();
            if(count($add_class) > 0 && $request->input('customer_supplier_id') > 0 && $request->input('customer_supplier_id') == $add_class[0]->id ){
                $add_class = CustomerSupplier::find($add_class[0]->id);
                $add_class->customer_id = $request->input('customer_id');
                $add_class->supplier_id = $request->input('supplier_id');
                $add_class->customer_supplier_rate = $request->input('class');
                $add_class->save();

                $sup = Supplier::find($request->get('supplier_id'));
                $cust = Customer::find($request->get('customer_id'));

                \ActionLog::addToLog("Customer Rate Class Changed","Customer rate Class changed for Supplier- ".$sup->name." and Customer- ".$cust->name , $add_class->getTAble(),$add_class->id);

                return json_encode(array('msg'=> 'Success'));
            }else{
                return json_encode(array('msg' => 'Error'));
            }
        }

        
    }
}
