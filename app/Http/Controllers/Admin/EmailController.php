<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use Mail;
use View;

class EmailController extends Controller
{
    public function sendTransactionApprovalAction($trans,$cession_contract_pdf=null, $comm_dis_letter=null ) {
		
        $customer_id=$trans->customer_id;
        $customeruser=Transaction::with('getCustomerApproveTransactionRoles','getSupplierApproveTransactionRoles')
		->leftJoin('banks','banks.id','=','transactions.bank_id')
		->where('customer_id','=',$customer_id)->select('*','banks.name as banks_name')->where('transactions.id',$trans->id)->get();

		$subject = "ConnectBahn alert: transaction #.'$trans->id'. approved";	
		$to='Admin';
			
		foreach($customeruser as $customer)	{
		   foreach($customer->getCustomerApproveTransactionRoles as $customermail){
			  Mail::send('admin.emails.approvetransaction', compact('customeruser','to','trans'), function ($message) use ($subject,$customermail) {
					$message->to($customermail->email)->subject($subject);
				}); 
			}
			foreach($customer->getSupplierApproveTransactionRoles as $suppliermail){
				$customremail = $suppliermail;
		  //echo $mailid=$suppliermail->email;
			  Mail::send('admin.emails.approvetransaction', compact('customeruser','to','trans'), function ($message) use ($subject,$customremail) {
					$message->to($customremail->email)->subject($subject);
				}); 
			}
	   
	   }

    }
}
