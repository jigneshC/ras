<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Hollyday;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class HollydayController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'holiday_date' => 'required'
        ]);

        $requestData = $request->all();


        Hollyday::create($requestData);
        
        Session::flash('flash_success',"Hollyday Added Success");
        
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $ob = Hollyday::findOrFail($id);

        if($ob){
            Hollyday::where('id',$id)->delete();
            Session::flash('flash_success', 'Delete Success !');
        }else{
            Session::flash('flash_error', 'No Data Found !');
        }
        
        return redirect()->back();
    }
}
