<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Supplier;
use App\Customer;
use App\Role;
use App\User;
use DB;
use Yajra\Datatables\Datatables;
use App\ActionLog;
use App\CustomerSupplier;
use App\BankCustomerMoney;
use App\AvailableCashHistory;
use App\SupplierUnit;
use App\SupplierUser;
use App\Transaction;
use Session;
class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.invoices');
        $this->middleware('permission:access.invoices.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.invoices.create')->only(['create', 'store']);
        $this->middleware('permission:access.invoices.delete')->only('destroy');
    }
   
    public function index(Request $request)
    {   
        if(!empty($request->customerid)){

            $suppliers_id =  CustomerSupplier::where('customer_id',$request->customerid)->pluck('supplier_id');
            $user = User::find(auth()->user()->id);
            if($user->roles[0]->name == 'CBC' || $user->roles[0]->name =='SLO' || $user->roles[0]->name == 'SLA' ) {
                $supp_user = SupplierUser::where('user_id',$user->id)->whereIn('supplier_id',$suppliers_id)->pluck('supplier_id');
                $supplier = Supplier::whereIn('id',$supp_user)->orderBy('name')->pluck('name','id');
            }
            if($user->role[0]->name == 'SU' || $user->roles[0]->name == 'CBS' || $user->roles[0]->name =='CM'  ){
                $supplier = Supplier::whereIn('id',$suppliers_id)->orderBy('name')->pluck('name','id');
            }
            return json_encode($supplier);
        }

        if(!empty($request->supplierid)){

            $customers_id =  CustomerSupplier::where('supplier_id',$request->supplierid)->pluck('customer_id');
            $customer = Customer::whereIn('id',$customers_id)->orderBy('name')->pluck('name','id');
            return json_encode($customer);
        }

        $customer = array();
        $supplier = array();

        $user = User::find(auth()->user()->id);

        if($user->roles[0]->name == 'CBC' || $user->roles[0]->name =='SLO' || $user->roles[0]->name == 'SLA' ) {
            $users = User::with([
                'supplier' => function($q){
                $q
                ->Join('customer_supplier', 'customer_supplier.supplier_id','=','supplier.id')
                ->Join('customer as cdetail', 'cdetail.id','=', 'customer_supplier.customer_id')
                ->select('supplier.*', 'customer_supplier.*', 'cdetail.name as cname')
                ;
            }
            ])->find(auth()->user()->id);
           
            foreach ($users->supplier as $suppliers) {    
                $suppliers_id[] =  $suppliers->supplier_id;
                $customers_id[] = $suppliers->customer_id;
            }
            
            $supplier = Supplier::whereIn('id',$suppliers_id)->orderBy('name')->get();

            $customer = Customer::whereIn('id',$customers_id)->orderBy('name')->get();
            
        }

        if($user->roles[0]->name == 'CBS'  ) {
            $users = User::with([
                'customer' => function($q){
                $q
                ->Join('customer_supplier', 'customer_supplier.customer_id','=','customer.id')
                ->Join('supplier as sdetail', 'sdetail.id','=', 'customer_supplier.supplier_id')
                ->select('customer.*', 'customer_supplier.*', 'sdetail.name as sname')
                ;
            }
            ])->find(auth()->user()->id);
           
            foreach ($users->customer as $customers) {    
                
                $customers_id[] = $customers->customer_id;
            }

            $customer = Customer::whereIn('id',$customers_id)->orderBy('name')->get();
            
        }

        if($user->role[0]->name == 'SU' ){
            $customer = Customer::where('is_status','active')->orderBy('name')->get();
        }

        //supplier
        if($user->roles[0]->name == 'CM'  ) {
            $users = User::with([
                'customer' => function($q){
                $q
                ->Join('customer_supplier', 'customer_supplier.customer_id','=','customer.id')
                ->Join('supplier as sdetail', 'sdetail.id','=', 'customer_supplier.supplier_id')
                ->select('customer.*', 'customer_supplier.*', 'sdetail.name as sname')
                ;
            }
            ])->find(auth()->user()->id);
           
            foreach ($users->customer as $customers) {    
                
                $supplier_id[] = $customers->supplier_id;
            }

            $supplier = Supplier::whereIn('id',$supplier_id)->orderBy('name')->get();
            
        }
  
        return view('admin.invoice.index',compact('customer','supplier'));
    }

    public function datatable(request $request){
        $selected_sup_id = $request->supplier_id;
        $selected_cust_id = $request->customer_id;
        $start_date = date('Y-m-d',strtotime(implode('-',explode('/',$request->startdate)))). ' 00:00:00';
        $end_date = date('Y-m-d',strtotime(implode('-', explode('/', $request->enddate)))) . ' 23:59:59';
        $daysdue  = $request->daysdue;
        $invoice = array();
        $user = User::find(auth()->user()->id);
        if($user->roles[0]->name =='SLO' || $user->roles[0]->name == 'SLA' || $user->roles[0]->name == 'CBC'){
             $user = User::with('supplier')->find(auth()->user()->id);
             $supplier_id[] = '';
            foreach($user->supplier as $supplier){
                $supplier_id[] = $supplier->id;
            }
            $discount_value[] = 0;
            $supplier_id = array_unique($supplier_id);

            if(!empty($selected_sup_id)){
                $supplier_id = array();
                $supplier_id[] =  $selected_sup_id;
            }


                $invoice = Invoice::select('invoices.*',
                DB::raw('customer.name as customer_name,
                customer.id as cId,
                supplier.name as supplier_name'),
                DB::raw('(CASE WHEN ISNULL(dr_customer.min) THEN dr_commercial.min ELSE dr_customer.min END) AS min_rate'),
                DB::raw('(CASE WHEN ISNULL(dr_customer.max) THEN dr_commercial.max ELSE dr_customer.max END) AS max_rate'))
                    ->leftjoin('customer','customer.id','=','invoices.customer_id')
                    ->leftjoin('supplier','supplier.id','=','invoices.supplier_id') 
                    ->leftjoin('customer_supplier', function($join)
                    {
                      $join->on('customer_supplier.customer_id', '=', 'invoices.customer_id')
                        ->on('customer_supplier.supplier_id', '=', 'invoices.supplier_id');
                    })
                    ->leftjoin('discount_rate as dr_customer','dr_customer.id','=','customer_supplier.customer_supplier_rate') 
                    
                    ->leftjoin('discount_rate as dr_commercial','dr_commercial.id','=','customer_supplier.customer_supplier_rate_by_commercial_user')      
                    ->groupBy('invoices.id')      
                    ->with('bank_cost')
                    ->with('transaction')
                    ->whereIn('invoices.supplier_id',$supplier_id);  

                if ($request->has('filter_status') && $request->get('filter_status') != '') {
                    
                    $status = $request->get('filter_status');
                    if($status == 'Used'){
                        $invoice->where('status','Used'); 
                    }elseif($status == 'Expired'){
                        $invoice->where('status','Expired'); 
                    }elseif($status == 'All'){
                        
                    }else{
                        $invoice->where('status','Available');
                    }
                                       
                }else{
                    $invoice->where('status','Available');
                }
                if(!empty($selected_cust_id)){
                    $customer_id[] =  $selected_cust_id;
                    $invoice->whereIn('invoices.customer_id',$customer_id);
                }
                if ($request->startdate != '' && $request->enddate != '') {
                    $invoice->whereBetween('duedate', array($start_date, $end_date));
                }

                if ($request->daysdue != '' && $request->startdate == '' && $request->enddate == '') {
                    $invoice->whereBetween('duedate', array(date('Y-m-d', time()), date('Y-m-d', strtotime("+" . $daysdue . " days"))));
                }
                $invoice->latest(); 
                return Datatables::of($invoice)
            ->make(true);
            exit;
        }elseif($user->roles[0]->name == "CBS" || $user->roles[0]->name == "CM"){

            $user = User::with('customer')->find(auth()->user()->id);
            $customer_id[] = '';
            foreach($user->customer as $customer){
                $customer_id[] = $customer->id;
            }
            $discount_value[] = 0;
            $customer_id = array_unique($customer_id);

            if(!empty($selected_cust_id)){
                $customer_id = array();
                $customer_id[] =  $selected_cust_id;
            }

                $invoice = Invoice::select('invoices.*',
                DB::raw('customer.name as customer_name,
                customer.id as cId,
                supplier.name as supplier_name'),
                DB::raw('(CASE WHEN ISNULL(dr_customer.min) THEN dr_commercial.min ELSE dr_customer.min END) AS min_rate'),
                DB::raw('(CASE WHEN ISNULL(dr_customer.max) THEN dr_commercial.max ELSE dr_customer.max END) AS max_rate'))
                    ->leftjoin('customer','customer.id','=','invoices.customer_id')
                    ->leftjoin('supplier','supplier.id','=','invoices.supplier_id') 
                    ->leftjoin('customer_supplier', function($join)
                    {
                      $join->on('customer_supplier.customer_id', '=', 'invoices.customer_id')
                        ->on('customer_supplier.supplier_id', '=', 'invoices.supplier_id');
                    })
                    ->leftjoin('discount_rate as dr_customer','dr_customer.id','=','customer_supplier.customer_supplier_rate') 
                    
                    ->leftjoin('discount_rate as dr_commercial','dr_commercial.id','=','customer_supplier.customer_supplier_rate_by_commercial_user')      
                    ->groupBy('invoices.id')      
                    ->with('bank_cost')
                    ->with('transaction')
                    ->whereIn('invoices.customer_id',$customer_id); 

                if ($request->has('filter_status') && $request->get('filter_status') != '') {
                    
                    $status = $request->get('filter_status');
                    if($status == 'Used'){
                        $invoice->where('status','Used'); 
                    }elseif($status == 'Expired'){
                        $invoice->where('status','Expired'); 
                    }elseif($status == 'All'){
                        
                    }else{
                        $invoice->where('status','Available');
                    }
                                       
                }else{
                    $invoice->where('status','Available');
                }

                if(!empty($selected_sup_id)){
                    $supplier_id[] =  $selected_sup_id;
                    $invoice->whereIn('invoices.supplier_id',$supplier_id);
                }
                if ($request->startdate != '' && $request->enddate != '') {
                    $invoice->whereBetween('duedate', array($start_date, $end_date));
                }

                if ($request->daysdue != '' && $request->startdate == '' && $request->enddate == '') {
                    $invoice->whereBetween('duedate', array(date('Y-m-d', time()), date('Y-m-d', strtotime("+" . $daysdue . " days"))));
                }
                $invoice->latest();
                return Datatables::of($invoice)
            ->make(true);
            exit;

        }
        else{
            if($user->roles[0]->name == 'SU'){ //  BAckoffice user does not have acces to this page
                $role = Role::with('users')->where("name", "SLO")->orwhere('name','CBC')->get();
                foreach($role as $roles){
                    foreach($roles->users as $users){
                        $user_id[] = $users->id;
                    }
                }
                $supplier_id = SupplierUser::whereIn('user_id',$user_id)->pluck('supplier_id');

                if(!empty($selected_sup_id)){
                    $supplier_id = array();
                    $supplier_id[] =  $selected_sup_id;
                }
                    

                $invoice = Invoice::select('invoices.*',
                DB::raw('customer.name as customer_name,
                customer.id as cId,
                supplier.name as supplier_name'),
                DB::raw('(CASE WHEN ISNULL(dr_customer.min) THEN dr_commercial.min ELSE dr_customer.min END) AS min_rate'),
                DB::raw('(CASE WHEN ISNULL(dr_customer.max) THEN dr_commercial.max ELSE dr_customer.max END) AS max_rate'))
                    ->leftjoin('customer','customer.id','=','invoices.customer_id')
                    ->leftjoin('supplier','supplier.id','=','invoices.supplier_id') 
                    ->leftjoin('customer_supplier', function($join)
                    {
                      $join->on('customer_supplier.customer_id', '=', 'invoices.customer_id')
                        ->on('customer_supplier.supplier_id', '=', 'invoices.supplier_id');
                    })
                    ->leftjoin('discount_rate as dr_customer','dr_customer.id','=','customer_supplier.customer_supplier_rate') 
                    
                    ->leftjoin('discount_rate as dr_commercial','dr_commercial.id','=','customer_supplier.customer_supplier_rate_by_commercial_user')      
                    ->groupBy('invoices.id')      
                    ->with('bank_cost')
                    ->with('transaction')
                    ->whereIn('invoices.supplier_id',$supplier_id);

                    if ($request->has('filter_status') && $request->get('filter_status') != '') {
                        
                        $status = $request->get('filter_status');

                        if($status == 'Used'){
                            $invoice->where('status','Used'); 
                        }elseif($status == 'Expired'){
                            $invoice->where('status','Expired'); 
                        }elseif($status == 'All'){

                        }else{
                            $invoice->where('status','Available');
                        }
                                           
                    }else{
                        $invoice->where('status','Available');
                    }
                    if(!empty($selected_cust_id)){
                        $customer_id[] =  $selected_cust_id;
                        $invoice->whereIn('invoices.customer_id',$customer_id);
                    }

                if($request->startdate != '' && $request->enddate != ''){
                    $invoice->whereBetween('duedate',array($start_date,$end_date));
                }

                if($request->daysdue != '' && $request->startdate == '' && $request->enddate == ''){
                    $invoice->whereBetween('duedate',array(date('Y-m-d',time()), date('Y-m-d', strtotime("+".$daysdue." days"))));
                }
                $invoice->latest();
                return Datatables::of($invoice)
                    ->make(true);
                exit;
            }else{
                return Datatables::of(collect($invoice))
                ->make(true);
                exit;
            }
        }
        
        exit;
    }

    public function customerClassRate(Request $request){
        if($request->get('supplierid') > 0 && $request->get('customerid') > 0){

            $customer = CustomerSupplier::with('customer','discountRateCustomer', 'discountRate', 'cashAvailable','bank_cost')
            ->where('customer_id', $request->get('customerid'))
            ->where('supplier_id', $request->get('supplierid'))->get();
            
            $customers = $customer[0];
            $bank_id = 0;
            $total = 0;
            
  
                if(isset($customers->bank_cost)){
                    foreach($customers->bank_cost as $bank){
                        $a[] = $bank->bank_rate;
                        $total = $total + $bank->total_amount; 
                    }
                    
                }
                

            if(isset($a)){
                $min_rate = min($a);
                if($min_rate != ''){
                    
                    $bank = BankCustomerMoney::where('bank_rate','=',$min_rate)
                    ->where('customer_id',$request->get('customerid'))->first();
                    $bank_id = $bank->bank_id;
                }
            }
          
            if(count($customer) > 0){
                
                $jsonArray['min'] = ($customers->discountRateCustomer != '') ? $customers->discountRateCustomer->min : (($customers->discountRate != '') ? $customers->discountRate->min : '0') ;

                $jsonArray['max'] = ($customers->discountRateCustomer != '') ? $customers->discountRateCustomer->max : (($customers->discountRate != '') ? $customers->discountRate->max : '0') ;

                $jsonArray['cash'] = (count($customers->cashAvailable) > 0 )? $customers->cashAvailable[0]->available_cash : 0 ;

                $jsonArray['rate_class'] = ($customers->discountRate != '') ? $customers->discountRate->class :(($customers->discountRateCustomer != '') ? $customers->discountRateCustomer->class : '0');

                $jsonArray['expiry_date'] = ($customers->customer != '') ? $customers->customer->expiry_date : 0;

                
            }else{
                $jsonArray['min'] = 0;
                $jsonArray['max'] = 0;
                $jsonArray['cash'] = 0;
                $jsonArray['rate_class'] = 0;
                $jsonArray['expiry_date'] = 0;
                  
            }
            return json_encode($jsonArray);exit;
        }
        return json_encode(array());
    }

    public function customer_bank_data(Request $request){
        if($request->get('supplierid') > 0 && $request->get('customerid') > 0 && $request->get('total_amount') > 0){

            $bankAmount = array(
                'bankAmount' => 0,
                'bank_id' => 0,
                'bank_rate' => 0,
                'customer_id' => 0,
                'bank_name' => '',

            );
            $invoice_total_amount = $request->get('total_amount');
            $invoice_total_amount = (float)$invoice_total_amount;
            
        //    $total_amount = $request->get('total_amount');
        //    $finalBankdetail = array();

        //     $bankallocatemoney = BankCustomerMoney::
        //         join('banks','banks.id','=', 'bank_customer_allocated_money.bank_id')
        //         ->select(DB::raw('bank_customer_allocated_money.bank_id, MIN(bank_rate) as bank_rate,bank_customer_allocated_money.customer_id,banks.name as bank_name , bank_customer_allocated_money.duedate as due_date'))
        //         ->where('bank_customer_allocated_money.customer_id', $request->get('customerid'))
        //         ->where('bank_customer_allocated_money.duedate', '>=', date('Y-m-d', time()))
        //         ->groupBy('bank_customer_allocated_money.bank_id')->get();
                
        //     $availabalCashHistory = AvailableCashHistory::select(DB::raw('SUM(available_cash_history.amount) as bankAmount,customer_id,bank_id'))
        //         ->groupBy('available_cash_history.bank_id')->groupBy('available_cash_history.customer_id')->whereNotNull('available_cash_history.bank_id')->get();
            
        //     $i = 0;
        //     foreach($bankallocatemoney as $key=>$bankrate){
        //         $bank_id = $bankrate->bank_id;
        //         $customer_id = $bankrate->customer_id;
        //         foreach($availabalCashHistory as $cashkey => $cashValue){
        //             if($cashValue->bank_id == $bank_id && $cashValue->customer_id == $customer_id){
        //                 $finalBankdetail[$i]['bankAmount'] = $cashValue->bankAmount;
        //                 $finalBankdetail[$i]['customer_id'] = $cashValue->customer_id;
        //                 $finalBankdetail[$i]['bank_id'] = $cashValue->bank_id;
        //                 $finalBankdetail[$i]['bank_name'] = $bankrate->bank_name;
        //                 $finalBankdetail[$i]['bank_rate'] = $bankrate->bank_rate;
        //             }
        //         }
        //         $i++;

        //     }
        //     //$finalBankdetail = array();
        //     foreach($finalBankdetail as $key => $value){
        //         $bank_amount = $value['bankAmount'];
                
        //         if($bank_amount > $total_amount ){
        //             $bankAmount = $value;        
        //             break;
        //         }
        //     }
           
            //get all the banks who gave money to customer
            $allbankdetail = array();
            $banks = BankCustomerMoney::where('bank_customer_allocated_money.customer_id',$request->get('customerid'))->with('bank','customer')->select('*',DB::raw('sum(bank_customer_allocated_money.total_amount) as total' ))->groupBY('bank_customer_allocated_money.bank_id')->get();

            $transactions  = Transaction::where('customer_id', $request->get('customerid'))->where('approver_id','!=',0)->where('payment_status','=','Approved')->where('type','Cession')->get();

            foreach($banks as $key => $bank){
                $allbankdetail[$key] = $bank;
                $allbankdetail[$key]['cash_consumed'] = 0;
                $allbankdetail[$key]['cash_still_available'] = 0;
                foreach($transactions as $transaction){
                    if($transaction->bank_id == $bank->bank_id){
                        $allbankdetail[$key]['cash_consumed'] = $allbankdetail[$key]['cash_consumed'] + $transaction->total_amount;
                    }
                }
                $bankExpiryDate = BankCustomerMoney::where('bank_id', $bank->bank_id)->where('customer_id', $bank->customer_id)->orderBy('id', 'desc')->first();
                $allbankdetail[$key]['duedate'] = $bankExpiryDate->duedate;
                $allbankdetail[$key]['bank_rate'] = $bankExpiryDate->bank_rate;
                $allbankdetail[$key]['cash_still_available'] = $allbankdetail[$key]['total'] - $allbankdetail[$key]['cash_consumed'];
            }

            $allbankdetail = collect($allbankdetail);
			//dd($allbankdetail);
            //Get the final bank with minimum bankrate 
            $min = 0 ;
            $index = 0 ;
			$today = date('Y-m-d',time());
            if(count($allbankdetail) > 0){
                foreach($allbankdetail as $key => $all_bank){
					$final_bank = array();
                    if($key == 0 && $all_bank->cash_still_available >=  $invoice_total_amount  &&  $all_bank->duedate >= $today){
                        $index = $key;
						$final_bank = $all_bank;
                        $min = $all_bank->bank_rate;
                    }
                    if( $all_bank->bank_rate < $min && $all_bank->cash_still_available >=  $invoice_total_amount  &&  $all_bank->duedate >= $today ){
                        $index = $key;
						$final_bank = $all_bank;
                        $min = $all_bank->bank_rate;
					}
					
					if(!empty($final_bank)){ 
				    //check whether the bank's due date is greater than today &  have sufficient amount to give to customer
						$bankAmount['bankAmount']  = $final_bank->cash_still_available ;
				
						$bankAmount['bank_id']  = $final_bank->bank_id ; 
				
						$bankAmount['bank_rate']  = $final_bank->bank_rate; 
				
						$bankAmount['customer_id']  = $final_bank->customer_id ; 
				
						$bankAmount['bank_name']  = $final_bank->bank->name; 
				
					} 
                } 
				
				/*
				echo $index;
                $final_bank = $allbankdetail[$index];
				print_r($allbankdetail[$index]);
				$today = date('Y-m-d',time()) ;
        
                if($final_bank){ 
                    //check whether the bank's due date is greater than today &  have sufficient amount to give to customer
                    if( $final_bank->cash_still_available >=  $invoice_total_amount  &&  $final_bank->duedate >= $today){
                   
                        $bankAmount['bankAmount']  = $final_bank->cash_still_available ;
                        $bankAmount['bank_id']  = $final_bank->bank_id ; 
                        $bankAmount['bank_rate']  = $final_bank->bank_rate; 
                        $bankAmount['customer_id']  = $final_bank->customer_id ; 
                        $bankAmount['bank_name']  = $final_bank->bank->name; 
                    }
    
                }  */          
            }
			
            return json_encode($bankAmount);
            exit;
        } 
    }

    
    public function create()
    {
        return view('admin.invoice.create');
    }

    public function store(Request $request)
    {
        if(!empty($_FILES)){
           
            $file = $request->file('file');
            //if($file->getClientOriginalExtension() == 'csv' || $file->getClientOriginalExtension() == 'xlsx'){
                 $csvFile['name'] = $file->getClientOriginalName();
            //     $file->getClientOriginalExtension();
            //     $path = $file->getRealPath();
            //     $file->getSize();
            //     $file->getMimeType();
            //    // $data = \Excel::load($path, function($reader) {})->get();
                $destinationPath = public_path('/uploads');
                $file->move($destinationPath,$csvFile['name']);
                $contents = public_path('/uploads/'). $csvFile['name'];
                $file = fopen($contents,"r");

                while(!feof($file)){
                    $rowData[]=fgetcsv($file);
                }
                $data = array();
                for($i=1;$i<count($rowData);$i++){
                    $j = 0;
                    if($rowData[$i][$j] != ''){
                        foreach($rowData[0] as $key=>$value){
                            $data[$i][$value] = $rowData[$i][$j];
                            $j++;
                        } 
                    }
                }
                foreach($data as $key=>$value){

                    $invoice_no = $value['Invoice_No'];

                    $supplierCpnj = trim($value['Supplier'],' ');

                    $customerCpnj = trim($value['Customer'],' ');

                    $due_date = date('Y-m-d',strtotime(str_replace('/', '-', trim($value['Due Date'],' '))));

                    //$date_of_anticipation = date('Y-m-d',strtotime(str_replace('/', '-', trim($value['Date Of Anticipation'],''))));
                    //$days_upto_anticipation = (strtotime($due_date)-strtotime($date_of_anticipation))/(60 * 60 * 24);

                    $amount = $value['Amount'];

                    $status = $value['Status']; 

                    if($supplierCpnj != '' && $customerCpnj != '' && $invoice_no != ''){

                        $customer = Customer::with('supplier')->where('cpnj',$customerCpnj)->first();
                        
                        if(empty($customer)){
                            Session::flash('flash_message', "We didn't find any Supplier or Customer !");
                            return redirect()->back();
                        }

                        foreach($customer->supplier as $supplier){
                            $supplier_id[] = $supplier->id;
                        }

                        $supplierUnit = SupplierUnit::where('cpnj', $supplierCpnj)->whereIn('supplier_id', $supplier_id)->get();

                        if(count($supplierUnit) > 0){

                            $invoices = Invoice::where('customer_id',$customer->id)->where('supplier_id', $supplierUnit[0]->supplier_id)->where('supplier_unit_id', $supplierUnit[0]->id)->where('invoice_id',$invoice_no)->get();
                            
                            if(count($invoices) == 0){

                                $invoice = new Invoice;
                                $invoice->customer_id = $customer->id;
                                $invoice->supplier_id = $supplierUnit[0]->supplier_id;
                                $invoice->duedate = $due_date;
                                $invoice->total_amount = $amount;
                                $invoice->status = $status;
                                $invoice->invoice_id = $invoice_no;
                                $invoice->supplier_unit_id = $supplierUnit[0]->id;
                                $invoice->save();

                                Session::flash('flash_success', "Invoices Added Successfully!");

                                \ActionLog::addToLog("New Invoice Added"," Invoice number - " . $invoice->invoice_id . " of ". $invoice->total_amount . " is added",$invoice->getTAble(),$invoice->id);
                            
                            }else{
                                foreach($invoices as $invoice){
                                    if($invoice->status == 'Available'){
                                        $invoice->duedate = $due_date;
                                        $invoice->total_amount = $amount;
                                        $invoice->status = $status;
                                        $invoice->save(); 
    
                                        Session::flash('flash_success', "Invoices Updated Successfully!");
    
                                    }else{
                                        Session::flash('flash_message', "Some Invoices are already Used in Transaction!");
                                    }
                                }
                                

                            }
                        }
                    }
                    
                }
            return redirect('admin/invoice'); 
                
        }
    }


}
