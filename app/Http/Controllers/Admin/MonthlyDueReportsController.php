<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use Carbon\Carbon;
use DB;
use App\Transaction;
use App\Invoice;
use App\TransactionsInvoices;
use App\Bank;
use Session;
use Illuminate\Support\Facades\Redirect;

class MonthlyDueReportsController extends Controller
{
    public function index(Request $request){
        $customer = Customer::pluck('name','id');

        if(!empty($request->name)) {
          
                $cust_id=$request->name;
                $startdate=$request->startdate;
                $enddate=$request->enddate;              
                $startday = Carbon::createFromFormat('Y-m-d', $startdate)->day;
                $endday = Carbon::createFromFormat('Y-m-d', $enddate)->day;
                $endyear = Carbon::createFromFormat('Y-m-d', $enddate)->year;

                $dailyupdate = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$startdate, $enddate])
                ->where('transactions.is_approve','=','1')
                ->where('transactions.customer_status','=','1')->orderBy('transactions.updated_at','asc') 
                ->select('*','transactions.id as transaction_id',DB::raw('DATE(transactions.updated_at) AS updates_date'),DB::raw('MONTH(transactions.updated_at) AS updates_month'))
                /*Define Relations for Transaction Invoice Table in Transaction Model*/
                ->with('monthlydueReports','bankName')
                ->get();

                $i=0;  
                $finalArra =''; 
                $month = '';
                $year = '';

                function get_months($startdate, $enddate, $startday,$endday) { 
					$time1  = strtotime($startdate); 
					$time2  = strtotime($enddate); 
					$my     = date('mY', $time2); 
					$startday = $startday;
                    

					$months = array(date('Y-m-'.$startday.'', $time1)); 

                while($time1 < $time2) { 
                    $time1 = strtotime(date('Y-m-d', $time1).' +1 month'); 
                    if(date('mY', $time1) != $my && ($time1 < $time2)) 
                        $months[] = date('Y-m-01', $time1); 
                        
                } 
                    $months[] = date('Y-m-'.$endday.'', $time2); 
                    return $months; 
                }  


                $months = get_months($startdate, $enddate, $startday,$endday);

                $numItems = count($months);
                $i = 0;

                foreach($months as $month =>$val)
                {
                  $year = Carbon::createFromFormat('Y-m-d', $val)->year;
                  $month = Carbon::createFromFormat('Y-m-d', $val)->month;
                    if ($i == 0) {
                        $startmonthday=$startdate;
                    } 
                    else{
                        $startmonthday=$year.'-'.$month.'-01';
                    }
                    if(++$i === $numItems) {
                        $endmonthday=$enddate;
                    }
                    else{
                        $endmonthday=$year.'-'.$month.'-31';
                    }
   
                    $monthlyrecords[] = Transaction:: where('transactions.customer_id',$cust_id)
                                -> whereBetween('transactions.updated_at',[$startmonthday, $endmonthday])
                                ->where('transactions.is_approve','=','1')
                                ->Where('transactions.customer_status','=','1')->orderBy('transactions.updated_at','asc') 
                                -> join('supplier','supplier.id','=','transactions.supplier_id')
                                -> join('transactions_invoices','transactions_invoices.transaction_number','=','transactions.id')
                                -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
                                 -> join('banks','banks.id','=','transactions.bank_id')
                                -> join('customer','customer.id','=','transactions_invoices.customer_id')
                                -> select('*',DB::raw('DATE(transactions.updated_at) AS updates_date'),
                                        'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount','supplier.name as supplier_name','banks.name as bank_name','customer.rate as rate')
                                ->get();  
                    $monthlyrecords1[] = Transaction:: where('transactions.customer_id',$cust_id)
                                -> whereBetween('transactions.updated_at',[$startmonthday, $endmonthday])
                                ->where('transactions.is_approve','=','1')
                                ->Where('transactions.customer_status','=','1')->orderBy('transactions.updated_at','asc') 
                                -> join('supplier','supplier.id','=','transactions.supplier_id')
                                -> join('transactions_invoices','transactions_invoices.transaction_number','=','transactions.id')
                                -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
                                -> join('banks','banks.id','=','transactions.bank_id')
                                -> join('customer','customer.id','=','transactions_invoices.customer_id')
                                -> select('*',DB::raw('DATE(transactions.updated_at) AS updates_date'),
                                DB::raw('sum(invoices.total_amount) AS original_value'), DB::raw('sum(invoices.discount_value) AS discount_value'),DB::raw('sum(invoices.net_value) AS net_value'),DB::raw('sum(invoices.cost) AS cost'),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount','supplier.name as supplier_name','banks.name as bank_name','customer.rate as rate')
                                ->get();  

                }
              //  dd($monthlyrecords);

            $j=0; 
            $k=0; 
            $finalArra=''; 
            $finalArras='';
            foreach($monthlyrecords as  $rows){
                 
                foreach($rows as $data){
                    if($data->updated_at!=''){
                        if($data->updates_date!=''){
                        $finalyear = Carbon::createFromFormat('Y-m-d', $data->updates_date)->year;
                        $finalmonth = Carbon::createFromFormat('Y-m-d', $data->updates_date)->month;
                        $monthName = date("F", mktime(0, 0, 0, $finalmonth, 10));
                        }
                        $finalArra[$monthName.'-'.$finalyear][$j]['updated_at'] = $data->updates_date;
                        $finalArra[$monthName.'-'.$finalyear][$j]['id'] = $data->transaction_number;
                        $finalArra[$monthName.'-'.$finalyear][$j]['name'] = $data->supplier_name;
                        $finalArra[$monthName.'-'.$finalyear][$j]['payment_type'] = $data->payment_type;
                        $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] = $data->bank_name;
                        
                        $finalArra[$monthName.'-'.$finalyear][$j]['original_value'] = $data->total_amount;
                        /* Find Net Vlaue */
                        $net_val=$data->total_amount-$data->discount_val-$data->cost;
                        $finalArra[$monthName.'-'.$finalyear][$j]['net_val'] = $net_val;
                        $finalArra[$monthName.'-'.$finalyear][$j]['cost_val'] = $data->cost;
                       
                         $total= $data->total_amount-$net_val-$data->cost;
                         $finalArra[$monthName.'-'.$finalyear][$j]['total']=$total;
                         $finalArra[$monthName.'-'.$finalyear][$j]['duetototal'] =  $total*(1-$data->rate);
                        $j++;
                    }
                    else{
                         Session::flash('flash_warning', __('Data is not Available!')); 
                         return Redirect::back();       
                    }
                }
            }
           
             foreach($monthlyrecords1 as $records){
              
                 if($records[0]['updates_date']!=''){  
                     $finalyear = Carbon::createFromFormat('Y-m-d', $records[0]['updates_date'])->year;
                
                    $finalmonth = Carbon::createFromFormat('Y-m-d', $records[0]['updates_date'])->month;
                    $monthName = date("F", mktime(0, 0, 0, $finalmonth, 10));

                    $finalArra[$monthName.'-'.$finalyear][$j]['updated_at'] = $monthName.'-'.$finalyear;
                    $finalArra[$monthName.'-'.$finalyear][$j]['id'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['name'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['payment_type'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['original_value'] = $records[0]['original_value'];
                    $net_val=$records[0]['total_amount']-$records[0]['discount_val']-$records[0]['cost'];
                    $finalArra[$monthName.'-'.$finalyear][$j]['net_val'] = $net_val;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cost_val'] = $data->cost;
                    $finalArra[$monthName.'-'.$finalyear][$j]['total']='';
                    $finalArra[$monthName.'-'.$finalyear][$j]['duetototal'] ='';
                    
                    $j++;
                          
                 }   
                 else{
                      Session::flash('flash_warning', __('Data is not Available!')); 
                      return Redirect::back();       
                 }     
            
            }
           
            if ( count($finalArra) > 0 && $rows[0]['transaction_number']!='') {
                $filename = "file.csv";
                $handle = fopen($filename, 'w+');
                $data = array('Date','Transaction Id','Supplier','Type','Bank','Original Value','Net Value','Cost','Total','Due to ConnectBahn');

                fputcsv($handle, $data);

                foreach($finalArra as $value) {
                    foreach($value as $value2){
                        fputcsv($handle, $value2);
                    }

                }
                
                fclose($handle);

                $headers = array(
                    'Content-Type' => 'text/csv',
                );
                return response()->download($filename, 'monthlydue_reports '.date("d-m-Y H:i").'.csv', $headers);
            }else{
                 Session::flash('flash_warning', __('Data is not Available!')); 
                return Redirect::back();
            }
}
        return view('admin.monthlyduereports.index',compact('customer'));
    }
}
