<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;


class NotificationController extends Controller
{
    //

    public function markasread(Request $request){
        
        $user = \Auth::user() ;
        if($request->id != ''){
            foreach($user->notifications as $notification){
                if($notification->id == $request->id){
                    $notification->markAsRead();
                }
            }
            return json_encode(['code'=> 200,'message' => 'Marked as Read']);
        }

    }

    public function markasunread(Request $request){
        
        $user = \Auth::user() ;
        if($request->id != ''){
            foreach($user->notifications as $notification){
                if($notification->id == $request->id){
                    $notification->update(['read_at' => null]);
                }
            }
            return json_encode(['code'=> 200,'message' => 'Marked as Unread']);
        }

    }
    
    

    public function notifications(){
        
        $notifications = \Auth::user()->notifications()->latest()->paginate(10);

        return view('admin.notifications',compact('notifications'));
    }

    public function delete(Request $request)
    {
        $user = \Auth::user() ;
        if($request->id != ''){
            foreach($user->notifications as $notification){
                if($notification->id == $request->id){
                    $notification->delete();
                }
            }

            $message = 'Notification Deleted';
            Session::flash('flash_success', $message);
            return redirect()->back();
            
        }       
    }
}
