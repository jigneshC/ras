<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;
use Session;

class PermissionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.permissions');
        $this->middleware('permission:access.permission.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.permission.create')->only(['create', 'store']);
        $this->middleware('permission:access.permission.delete')->only('destroy');
    }

    public function index(Request $request)
    {


        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $permissions = Permission::with('child')->parent()->where('name', 'LIKE', "%$keyword%")->orWhere('label', 'LIKE', "%$keyword%")
                ->get();
        } else {
            $permissions = Permission::with('child')->parent()->get();
        }


        return view('admin.permissions.index', compact('permissions'));
    }

    public function create()
    {

        $permissions = Permission::select()->parent()->pluck('name', 'id')->prepend('No Parent', 0);


        return view('admin.permissions.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);


        $permission = Permission::create($request->all());

        \ActionLog::addToLog("Add Permission"," New Permission is added ",$permission->getTAble(),$permission->id);

        Session::flash('flash_success', __('Permission added!'));

        return redirect('admin/permissions');
    }

    public function show($id)
    {
        $permission = Permission::findOrFail($id);

        return view('admin.permissions.show', compact('permission'));
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);

        $permissions = Permission::select()->parent()->pluck('name', 'id')->prepend('No Parent', 0);


        return view('admin.permissions.edit', compact('permission', 'permissions'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        \ActionLog::addToLog("Edit Permission"," Permission - ".$permission->id." is updated ",$permission->getTAble(),$permission->id);


        Session::flash('flash_success', __('Permission updated!'));

        return redirect('admin/permissions');
    }

    public function destroy($id)
    {
        $permission = Permission::find($id);

        \ActionLog::addToLog("Delete Permission"," Permission - ".$permission->id." is deleted ",$permission->getTAble(),$permission->id);

        $permission->delete();

        Session::flash('flash_success', __('Permission deleted!'));

        return redirect('admin/permissions');
    }
}
