<?php

namespace App\Http\Controllers\Admin;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Illuminate\Support\Facades\Lang;
use App\Notifications\ChangePassword;

class ProfileController extends Controller
{

    public function index()
    {
        flash(__('Your profile updated successfully.'), 'flash_success');
        $user = Auth::user();

//        return $user->lang;
        return view('admin.profile.index', compact('user'));
    }

    public function edit()
    {
        $user = Auth::user();
        return view('admin.profile.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $normal = [
            'name' => 'required',
        ];

        $user = Auth::user();
        $user->name = $request->name;
        if ($user->save()) {
        }
        flash(__('Your profile updated successfully.'), 'flash_success');

        return redirect()->back();

    }

    public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('photo')) {

//            dd($request->file('image'));
            $file = $request->file('photo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }

    public function changePassword()
    {
        return view('admin.profile.changePassword');
    }

    public function updatePassword(Request $request)
    {

        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
            'password.regex' => __('Password must contain at least one: number, capital letter, lower case, special symbol. '),
        ];

        $rules = [
            'password' => 'required|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/',
            'password_confirmation' => 'required|same:password',
        ];
        
        if(Auth::user()->password =="" || Auth::user()->password ==null || !Auth::user()->password){
            
        }else{
            $rules['current_password'] = "required";
        }
        $this->validate($request,$rules, $messages);


        $cur_password = $request->input('current_password');


        $user = Auth::user();

        if (Hash::check($cur_password, $user->password) || !isset($rules['current_password'])) {

            $user->password = Hash::make($request->input('password'));
            $user->save();

            flash('Password changed successfully.', 'success');

            $user->notify(new ChangePassword($user));

            return redirect()->route('profile.index');

        } else {
            $error = array('current-password' => __('Please enter correct current password'));
//            return response()->json(array('error' => $error), 400);
            return redirect()->back()->withErrors($error);
        }

        flash('Something wrong. Please try again latter.', 'error');

        return redirect()->back();


    }
    public function completeProfile($email="",$remember_token="")
    {
        $user = User::where("email",$email)->where("remember_token",$remember_token)->first();

        if($user) {
            Auth::login($user);
            return redirect('admin/profile/change-password');
        }else{
             return redirect('admin');
        }
    }


}
