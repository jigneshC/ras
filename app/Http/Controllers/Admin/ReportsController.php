<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;
use App\Customer;
use App\Transaction;
use App\TransactionsInvoices;
use App\Invoice;
use App\Supplierunit;
use App\Bank;
use App\Bankacoount;
use Illuminate\Support\Facades\Redirect;

class ReportsController extends Controller
{
    public function index(Request $request)
    {
        $customer = Customer::orderBy('name','asc')->pluck('name','id')->prepend('Select Customer','');
        if(!empty($request->name)) {
          
            $cust_id=$request->name;
            $startDateOfWeek= $request->startdate.' '.'00:00:00';
            $endDateOfWeek =$request->enddate.' '.'23:59:59';
           
            $dailyupdate = Transaction::where('transactions.customer_id',$cust_id)
            ->whereBetween('transactions.updated_at',[$startDateOfWeek, $endDateOfWeek])
            ->where('transactions.is_approve','=','1')->where('transactions.customer_status','=','Approved')->where('transactions.type','=','Anticipation')->orderBy('transactions.updated_at','asc')
            ->with('supplierName','bankDetails','bankName','supplierunitName')
            ->select('*',(DB::raw('DATE(transactions.updated_at) AS updates_date')))
            ->get();
            $i=0;  
            $finalArra =array(); 
            $totalorigval=0;
            $totalnetval=0;
            $totaltranscount=0;
            if(count($dailyupdate)>0){
                foreach($dailyupdate as $rows){
                
                        $finalArra[$i]['updated_at'] = $rows->updates_date;
                        $finalArra[$i]['id'] = $rows->id;
                        $finalArra[$i]['name'] = $rows->supplierName->supplier_name;
                        $finalArra[$i]['unit'] = $rows->supplierunitName[0]->unit_name;
                        $finalArra[$i]['cpnj'] = $rows->supplierunitName[0]->cpnj;
                        $finalArra[$i]['payment_type'] = $rows->type;
                        $finalArra[$i]['bank_name'] = ($rows->bankName) ? $rows->bankName->name : '';
                        $finalArra[$i]['original_value'] = $rows->total_amount;
                        $finalArra[$i]['net_val'] = $rows->net_value;
                        $finalArra[$i]['bank'] = '';
                        $finalArra[$i]['agency'] = '';
                        $finalArra[$i]['account'] = '';
                        if ($rows->bankDetails) {
                            $finalArra[$i]['bank'] = $rows->bankDetails->agency_name;
                            $finalArra[$i]['agency'] = $rows->bankDetails->agency_number;
                            $finalArra[$i]['account'] = $rows->bankDetails->account_number; 
                        }       
                        $totalorigval= $totalorigval + $rows->total_amount;
                        $totalnetval=$totalnetval+$rows->net_value;
                        $totaltranscount=$totaltranscount+1;
                        $i++;
                    
                    
                }
                $finalArra[$i]['updated_at'] = 'TOTAL';
                $finalArra[$i]['id'] = $totaltranscount;
                $finalArra[$i]['name'] = '';
                $finalArra[$i]['unit'] = '';
                $finalArra[$i]['cpnj'] = '';
                $finalArra[$i]['payment_type'] ='';
                $finalArra[$i]['bank_name'] = '';
                $finalArra[$i]['original_value']=$totalorigval;
                $finalArra[$i]['net_val']=$totalnetval;
                if (count($finalArra) > 0 && $rows->id!='') {
                /* Generate Csv File */
                    $filename = "file.csv";
                    $handle = fopen($filename, 'w+');
                    $data = array('Date','Transaction ID','Supplier','Unit','CNPJ','Type','Bank','Original Value','Net Value','Bank','Agency','Account');

                    fputcsv($handle, $data);

                    foreach($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return response()->download($filename, 'daily_reports '.date("d-m-Y H:i").'.csv', $headers);
                }
                else{
                    Session::flash('flash_warning', __('Data is not Available!')); 
                    return Redirect::back();
                }
            }
            else{
                Session::flash('flash_warning', __('Data is not Available!')); 
                return Redirect::back();
            }
        
        }
             
       return view('admin.customerreports.customer_report', compact('customer'));
    }
    public function customermonthlyreports(Request $request)
    {
        $customer = Customer::orderBy('name','asc')->pluck('name','id')->prepend('Select Customer','');
           
            if(!empty($request->name)) {
          
                $cust_id=$request->name;
                /*convert  month and year using Carbon*/

              $startmonth =  Carbon::createFromFormat('d-m-Y H:s:i',"1-". $request->startdate." 00:00:00");
              $endmonth =  Carbon::createFromFormat('d-m-Y H:s:i',"31-". $request->enddate." 23:59:59");
              
                $dailyupdate = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$startmonth, $endmonth])
                ->where('transactions.is_approve','=','1')
                ->where('transactions.customer_status','=','Approved')->orderBy('transactions.updated_at','asc')
                ->join('customer', 'customer.id', '=', 'transactions.customer_id')
                ->select((DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear")),'transactions.id as transaction_id',DB::raw('DATE(transactions.updated_at) AS updates_date'), DB::raw('sum(transactions.total_amount) AS original_value'), DB::raw('sum(transactions.net_value) AS net_value'), DB::raw('sum(transactions.discount_value) AS discount_val'), DB::raw('sum(transactions.cost) AS cost_val'), DB::raw('count(transactions.id) AS total_transaction'), 'customer.rate as rate')
                //DB::raw('sum(invoices.total_amount) AS original_value'), DB::raw('sum(invoices.discount_value) AS discount_val'), DB::raw('sum(invoices.cost) AS cost_val'), DB::raw('count(transactions_invoices.transaction_number) AS total_transaction'),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount','customer.rate as rate')
                ->groupBy('monthyear')
                ->get();
                $i=0;  
                $finalArra =array(); 
                $totaltranscount=0;
                $totalorigval=0;
                $totalnetval=0;
                $totalcostval=0;
                $totalamount=0;
                $totalrevenue=0;
                //dd($dailyupdate);
                if(count($dailyupdate)>0){
                    foreach($dailyupdate as  $rows){

                            $finalArra[$i]['updated_at'] =  $rows->monthyear;
                            $finalArra[$i]['total_transaction'] = $rows->total_transaction;
                            $finalArra[$i]['original_value'] = $rows->original_value;              
                            $finalArra[$i]['net_val'] = $rows->net_value;
                            $finalArra[$i]['cost_val'] = $rows->cost_val;
                            $total =  $rows->original_value -($rows->net_value)- ($rows->cost_val);
                            $finalArra[$i]['total']= $total;
                            $finalArra[$i]['revenue'] = $total * ($rows->rate);
                            $totaltranscount = $totaltranscount + $rows->total_transaction;
                            $totalorigval = $totalorigval + $rows->original_value;
                            $totalnetval = $totalnetval + $rows->net_value;
                            $totalcostval = $totalcostval + $rows->cost_val;
                            $totalamount = $totalamount + $total;
                            $totalrevenue = $totalrevenue +($total*($rows->rate));
                            $i++;
                        
                    }

                    if ( count($finalArra) > 0) {
                        $finalArra[$i]['updated_at'] = 'Total';
                        $finalArra[$i]['total_transaction'] = $totaltranscount;
                        $finalArra[$i]['original_value'] = $totalorigval;
                        $finalArra[$i]['net_val'] = $totalnetval;
                        $finalArra[$i]['cost_val'] = $totalcostval;
                        $finalArra[$i]['total'] = $totalamount;
                        $finalArra[$i]['revenue'] = $totalrevenue;

                        $filename = "file.csv";
                        $handle = fopen($filename, 'w+');
                        $data = array('Month','Transactions','Original Value','Net Value','Cost','Total','revenue');

                        fputcsv($handle, $data);

                        foreach($finalArra as $value) {
                            fputcsv($handle, $value);
                        }
                        fclose($handle);

                        $headers = array(
                            'Content-Type' => 'text/csv',
                        );
                        return response()->download($filename, 'monthly_reports '.date("d-m-Y H:i").'.csv', $headers);
                    }
                    else{
                        Session::flash('flash_warning', __('Data is not Available!')); 
                        return Redirect::back();
                    }
            
                }else{
                   Session::flash('flash_warning', __('Data is not Available!')); 
                    return Redirect::back();
                }
            }

            return view('admin.customerreports.monthly_report',compact('customer'));
    }
    public function customermonthlyduereports(Request $request){
        $customer = Customer::orderBy('name','asc')->pluck('name','id')->prepend('Select Customer','');

        if(!empty($request->name)) {
            
            $cust_id=$request->name;
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';

            $transaction = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$start_date, $end_date])
                ->where('transactions.is_approve','=','1')
                ->Where('transactions.customer_status','=','Approved')->orderBy('transactions.updated_at','asc') 
                -> join('supplier','supplier.id','=','transactions.supplier_id')
                -> join('transactions_invoices','transactions_invoices.transaction_number','=','transactions.id')
                -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
                    -> join('banks','banks.id','=','transactions.bank_id')
                -> join('customer','customer.id','=','transactions_invoices.customer_id')
                -> select(DB::raw('DATE(transactions.updated_at) AS updates_date'),DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount','supplier.name as supplier_name','banks.name as bank_name','customer.rate as rate', 'transactions.cost as transaction_cost_value')
                ->get();
            $transactionMonthYear = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$start_date, $end_date])
                ->where('transactions.is_approve','=','1')
                ->Where('transactions.customer_status','=','Approved')->orderBy('transactions.updated_at','asc') 
                -> join('supplier','supplier.id','=','transactions.supplier_id')
                -> join('transactions_invoices','transactions_invoices.transaction_number','=','transactions.id')
                -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
                    -> join('banks','banks.id','=','transactions.bank_id')
                -> join('customer','customer.id','=','transactions_invoices.customer_id')
                -> select('*',DB::raw('count(*) as transaction_count,DATE(transactions.updated_at) AS updates_date,SUM(invoices.total_amount) as transaction_total_amount,SUM(invoices.discount_value) as transaction_discount_value,SUM(transactions.cost) as transaction_cost_value'),DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','supplier.name as supplier_name','banks.name as bank_name','customer.rate as rate')
                ->groupBy('monthyear')
                ->get(); 
              
            $j = 0;
            $finalArra = array();
            if(count ($transaction) > 0 && count($transactionMonthYear) > 0 ){
                foreach($transaction as $key => $data){
                    $finalyear = Carbon::createFromFormat('Y-m-d', $data->updates_date)->year;
                    $finalmonth = Carbon::createFromFormat('Y-m-d', $data->updates_date)->month;
                    $monthName = date("F", mktime(0, 0, 0, $finalmonth, 10));
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['updated_at'] = $data->updates_date;
                    $finalArra[$monthName.'-'.$finalyear][$j]['id'] = $data->transaction_number;
                    $finalArra[$monthName.'-'.$finalyear][$j]['name'] = $data->supplier_name;
                    $finalArra[$monthName.'-'.$finalyear][$j]['payment_type'] = $data->payment_type;
                    $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] = '';
                    if($data->bank_name){
                        $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] = $data->bank_name;
                    }                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['original_value'] = $data->total_amount;
                    /* Find Net Vlaue */
                    $net_val=$data->total_amount-$data->discount_val-$data->transaction_cost_value;
                    $finalArra[$monthName.'-'.$finalyear][$j]['net_val'] = $net_val;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cost_val'] = $data->transaction_cost_value;
                
                    $total= $data->total_amount - $net_val ;
                    $finalArra[$monthName.'-'.$finalyear][$j]['total']=$total;
                    $finalArra[$monthName.'-'.$finalyear][$j]['duetototal'] =  $total*(1-$data->rate);
                    $j++;
                }
                foreach($transactionMonthYear as $key => $data){
                    $finalyear = Carbon::createFromFormat('Y-m-d', $data->updates_date)->year;
                    $finalmonth = Carbon::createFromFormat('Y-m-d', $data->updates_date)->month;
                    $monthName = date("F", mktime(0, 0, 0, $finalmonth, 10));
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['updated_at'] = $monthName.'-'.$finalyear;
                    $finalArra[$monthName.'-'.$finalyear][$j]['id'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['name'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['payment_type'] = '';
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] ='';
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['original_value'] = $data->transaction_total_amount;
                    /* Find Net Vlaue */
                    $net_val=$data->transaction_total_amount-$data->transaction_discount_value-$data->transaction_cost_value;
                    $finalArra[$monthName.'-'.$finalyear][$j]['net_val'] = $net_val;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cost_val'] = $data->transaction_cost_value;
                
                    $total= $data->transaction_total_amount-$net_val;
                    $finalArra[$monthName.'-'.$finalyear][$j]['total']=$total;
                    $finalArra[$monthName.'-'.$finalyear][$j]['duetototal'] =  $total*(1-$data->rate);
                    $j++;
                }
				//dd($finalArra);
                    if ( count($finalArra) > 0 ) {
                        $filename = "file.csv";
                        $handle = fopen($filename, 'w+');
                        $data = array('Date','Transaction Id','Supplier','Type','Bank','Original Value','Net Value','Cost','Total','Due to ConnectBahn');

                        fputcsv($handle, $data);
						
                        foreach($finalArra as $value) {
							$usedId = array();
                            foreach($value as $value2){
								if(!in_array($value2['id'],$usedId)){
									$usedId[] = $value2['id'];
									fputcsv($handle, $value2);
								}
                            }
							

                        }
                        
                        fclose($handle);
                        $headers = array(
                            'Content-Type' => 'text/csv',
                        );
                        return response()->download($filename, 'monthlydue_reports '.date("d-m-Y H:i").'.csv', $headers);
                    }else{
                        Session::flash('flash_warning', __('Data is not Available!')); 
                        return Redirect::back();
                    }
            }else{
                Session::flash('flash_warning', __('Data is not Available!')); 
                 return Redirect::back();
            }
            
        }
        $start_date = date('01/m/Y',time());
        $end_date = date('t/m/Y'); 
        return view('admin.customerreports.duemonthly_report',compact('customer','start_date','end_date'));
    }
     public function customeranticipationwithcbuser(Request $request)
    {
        $customer = Customer::orderBy('name','asc')->pluck('name','id')->prepend('Select Customer','');
        if(!empty($request->name)) {
          
            $cust_id=$request->name;
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $dailyupdate = Transaction::where('transactions.customer_id',$cust_id)
            ->whereBetween('transactions.updated_at',[$start_date, $end_date])
            ->where('transactions.is_approve','=','1')->where('transactions.customer_status','=','Approved')->where('transactions.type','=','Anticipation')->orderBy('transactions.updated_at','asc')
            ->with('supplierName','bankDetails','bankName','supplierunitName','getCustomerRoles')
            ->select('*',(DB::raw('DATE(transactions.updated_at) AS updates_date')))
            ->get();
            $i=0;  
            $finalArra =array(); 
            $totalorigval=0;
            $totalnetval=0;
            $totaltranscount=0;
            if(count($dailyupdate)>0){
                foreach($dailyupdate as $rows){
                
                        $finalArra[$i]['updated_at'] = $rows->updates_date;
                        $finalArra[$i]['id'] = $rows->id;
                        $finalArra[$i]['name'] = $rows->supplierName->supplier_name;
                        $finalArra[$i]['unit'] = $rows->supplierunitName[0]->unit_name;
                        $finalArra[$i]['cpnj'] = $rows->supplierunitName[0]->cpnj;
                        $finalArra[$i]['payment_type'] = $rows->type;
                        $finalArra[$i]['bank_name'] = ($rows->bankName) ? $rows->bankName->name : '';                        
                        $finalArra[$i]['original_value'] = $rows->total_amount;
                        $finalArra[$i]['net_val'] = $rows->net_value;
                        $finalArra[$i]['bank'] = '';
                        $finalArra[$i]['agency'] = '';
                        $finalArra[$i]['account'] = '';
                        if ($rows->bankDetails) {
                            $finalArra[$i]['bank'] = $rows->bankDetails->agency_name;
                            $finalArra[$i]['agency'] = $rows->bankDetails->agency_number;
                            $finalArra[$i]['account'] = $rows->bankDetails->account_number; 
                        }
                        $finalArra[$i]['cbuser'] = $rows->getCustomerRoles->user_name;      
                        $totalorigval= $totalorigval + $rows->total_amount;
                        $totalnetval=$totalnetval+$rows->net_value;
                        $totaltranscount=$totaltranscount+1;
                        $i++;
                    
                    
                }
                $finalArra[$i]['updated_at'] = 'TOTAL';
                $finalArra[$i]['id'] = $totaltranscount;
                $finalArra[$i]['name'] = '';
                $finalArra[$i]['unit'] = '';
                $finalArra[$i]['cpnj'] = '';
                $finalArra[$i]['payment_type'] ='';
                $finalArra[$i]['bank_name'] = '';
                $finalArra[$i]['original_value']=$totalorigval;
                $finalArra[$i]['net_val']=$totalnetval;
                if (count($finalArra) > 0 && $rows->id!='') {
                /* Generate Csv File */
                    $filename = "file.csv";
                    $handle = fopen($filename, 'w+');
                    $data = array('Date','Transaction ID','Supplier','Unit','CNPJ','Type','Bank','Original Value','Net Value','Bank','Agency','Account','CB-User Name');

                    fputcsv($handle, $data);

                    foreach($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return response()->download($filename, 'daily_reports_cbuser '.date("d-m-Y H:i").'.csv', $headers);
                }
                else{
                    Session::flash('flash_warning', __('Data is not Available!')); 
                    return Redirect::back();
                }
            }
            else{
                Session::flash('flash_warning', __('Data is not Available!')); 
                return Redirect::back();
            }
            
        }
        $start_date = date('d/m/Y',time());
        $end_date = date('d/m/Y');    
       return view('admin.customerreports.cbuser_report', compact('customer','start_date','end_date'));
    }

    public function connectbahnrevenuereports(Request $request){
        $customer = Customer::orderBy('name','asc')->pluck('name','id')->prepend('Select Customer','');

        if(!empty($request->name)) {
            
            $cust_id=$request->name;
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';

            $transaction = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$start_date, $end_date])
                ->where('transactions.is_approve','=','1')
                ->Where('transactions.customer_status','=','Approved')->orderBy('transactions.updated_at','asc') 
                -> join('supplier','supplier.id','=','transactions.supplier_id')
                -> join('transactions_invoices','transactions_invoices.transaction_number','=','transactions.id')
                -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
                    -> join('banks','banks.id','=','transactions.bank_id')
                -> join('customer','customer.id','=','transactions_invoices.customer_id')
                -> select('*',DB::raw('DATE(transactions.updated_at) AS updates_date'),DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount','supplier.name as supplier_name','banks.name as bank_name','customer.rate as rate','transactions.net_value as transaction_net_val', 'transactions.cost as transaction_cost')
                ->get();  
                
            $transactionMonthYear = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$start_date, $end_date])
                ->where('transactions.is_approve','=','1')
                ->Where('transactions.customer_status','=','Approved')->orderBy('transactions.updated_at','asc') 
                -> join('supplier','supplier.id','=','transactions.supplier_id')
                -> join('transactions_invoices','transactions_invoices.transaction_number','=','transactions.id')
                -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
                    -> join('banks','banks.id','=','transactions.bank_id')
                -> join('customer','customer.id','=','transactions_invoices.customer_id')
                -> select('*',DB::raw('count(*) as transaction_count,DATE(transactions.updated_at) AS updates_date,SUM(invoices.total_amount) as transaction_total_amount,SUM(invoices.discount_value) as transaction_discount_value,SUM(transactions.net_value) as transaction_netvalue,SUM(transactions.cost) as transaction_cost_value'),DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','supplier.name as supplier_name','banks.name as bank_name','customer.rate as rate')
                ->groupBy('monthyear')
                ->get(); 
               
            $j = 0;
			
            $finalArra = array();
            if(count ($transaction) > 0 && count($transactionMonthYear) > 0 ){
                foreach($transaction as $key => $data){
                    $finalyear = Carbon::createFromFormat('Y-m-d', $data->updates_date)->year;
                    $finalmonth = Carbon::createFromFormat('Y-m-d', $data->updates_date)->month;
                    $monthName = date("F", mktime(0, 0, 0, $finalmonth, 10));
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['updated_at'] = $data->updates_date;
                    $finalArra[$monthName.'-'.$finalyear][$j]['id'] = $data->transaction_number;
                    $finalArra[$monthName.'-'.$finalyear][$j]['name'] = $data->supplier_name;
                    $finalArra[$monthName.'-'.$finalyear][$j]['payment_type'] = $data->payment_type;
                    $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] = '';
                    if($data->bank_name){
                        $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] = $data->bank_name;
                    }                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['original_value'] = $data->total_amount;
                    /* Find Net Vlaue */
                    $finalArra[$monthName.'-'.$finalyear][$j]['net_val'] = $data->transaction_net_val;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cost_val'] = $data->transaction_cost;
                
                    $total= $data->total_amount-($data->transaction_net_val);
                    $finalArra[$monthName.'-'.$finalyear][$j]['total']=$total;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cbrevenue'] =  $total*(1-$data->rate);
                    $j++;
                }
                foreach($transactionMonthYear as $key => $data){
                    $finalyear = Carbon::createFromFormat('Y-m-d', $data->updates_date)->year;
                    $finalmonth = Carbon::createFromFormat('Y-m-d', $data->updates_date)->month;
                    $monthName = date("F", mktime(0, 0, 0, $finalmonth, 10));
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['updated_at'] = $monthName.'-'.$finalyear;
                    $finalArra[$monthName.'-'.$finalyear][$j]['id'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['name'] = '';
                    $finalArra[$monthName.'-'.$finalyear][$j]['payment_type'] = '';
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['bank_name'] ='';
                    
                    $finalArra[$monthName.'-'.$finalyear][$j]['original_value'] = $data->transaction_total_amount;
                    /* Find Net Vlaue */
                    $net_val=$data->transaction_total_amount-$data->transaction_discount_value-$data->transaction_cost_value;
                    $finalArra[$monthName.'-'.$finalyear][$j]['net_val'] = $data->transaction_netvalue;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cost_val'] = $data->transaction_cost_value;
                
                    $total= $data->transaction_total_amount-$data->transaction_netvalue;
                    $finalArra[$monthName.'-'.$finalyear][$j]['total']=$total;
                    $finalArra[$monthName.'-'.$finalyear][$j]['cbrevenue'] =  $total*(1-$data->rate);
                    $j++;
                }
                //dd($finalArra);
                    if ( count($finalArra) > 0 ) {
                        $filename = "file.csv";
                        $handle = fopen($filename, 'w+');
                        $data = array('Date','Transaction Id','Supplier','Type','Bank','Original Value','Net Value','Cost','Total','CB Revenue');

                        fputcsv($handle, $data);

                        foreach($finalArra as $value) {
							$usedId = array();
                            foreach($value as $value2){
								if(!in_array($value2['id'],$usedId)){
									$usedId[] = $value2['id'];
									fputcsv($handle, $value2);
								}
                            }

                        }
                        
                        fclose($handle);

                        $headers = array(
                            'Content-Type' => 'text/csv',
                        );
                        return response()->download($filename, 'connectbahn_monthlydue_reports cbrevenue'.date("d-m-Y H:i").'.csv', $headers);
                    }else{
                        Session::flash('flash_warning', __('Data is not Available!')); 
                        return Redirect::back();
                    }
            }else{
                Session::flash('flash_warning', __('Data is not Available!')); 
                 return Redirect::back();
            }
            
        }
        $start_date = date('01/m/Y',time());
        $end_date = date('t/m/Y'); 
        return view('admin.customerreports.connectbahn_monthly_duereport',compact('customer','start_date','end_date'));
    }
}
