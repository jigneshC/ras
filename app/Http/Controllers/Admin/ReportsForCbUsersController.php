<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bank;
use App\Transaction;
use DB;
use App\Supplier;
use Session;

class ReportsForCbUsersController extends Controller
{
    
    public function index(Request $request){
        if ($request && $request->supplier_id != '' && $request->startdate != '' && $request->enddate != '') {
            $supplier_id = $request->get('supplier_id');
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
        //$start_date = date('Y-m-d',strtotime($request->startdate))  . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $doneByCbUser = Transaction::where('transactions.supplier_id', $supplier_id)
                ->whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')
                ->where('transactions.customer_status', '=', 'Approved')->orderBy('transactions.updated_at', 'asc')
                ->leftjoin('customer_supplier', function ($join) {
                    $join->on('customer_supplier.customer_id', '=', 'transactions.customer_id')
                        ->on('customer_supplier.supplier_id', '=', 'transactions.supplier_id');
                })
                ->leftjoin('discount_rate as dr_customer', 'dr_customer.id', '=', 'customer_supplier.customer_supplier_rate')

                ->leftjoin('discount_rate as dr_commercial', 'dr_commercial.id', '=', 'customer_supplier.customer_supplier_rate_by_commercial_user')
                ->leftjoin('customer','customer.id','=', 'transactions.customer_id')
                ->with('supplier', 'bankName', 'getCreatedBy')
                ->select('*',
                    'transactions.updated_at as transaction_update_date',
                    'transactions.id as transactions_id','customer.name as customer_name',
                    'transactions.type as transactions_type',
                    DB::raw('(CASE WHEN ISNULL(dr_customer.min) THEN dr_commercial.min ELSE dr_customer.min END) AS min_rate'),
                    DB::raw('(CASE WHEN ISNULL(dr_customer.max) THEN dr_commercial.max ELSE dr_customer.max END) AS max_rate')
                )
                ->get();
            
            if(count ($doneByCbUser) > 0){
                $finalArray = array();
                $i = 0;
                $transactionsTotalId= 0;
                $totalOrignalAmount = 0;
                $totalNetValue = 0;
                $transactionsTotalId = 0;
                $minRateSum = 0;
                $maxRateSum = 0;
                $usedRateSum = 0;
                $deviationRateSum = 0;
                $roles = array('SU','CBS','CBC','CBB', 'CM');
                foreach ($doneByCbUser as $key => $data){
                    if(in_array($data->getCreatedBy->role[0]->name,$roles)){
                        $role = 'CB';
                    }else{
                        $role = 'Supplier';
                    }

                    $finalArray[$i]['approval_Date'] = date('d-m-Y',strtotime($data->transaction_update_date));
                    $finalArray[$i]['transaction_id'] = $data->transactions_id;
                    $finalArray[$i]['created_by'] = $role;
                    $finalArray[$i]['customer'] = $data->customer_name;
                    $finalArray[$i]['supplier'] = ($data->supplier) ? $data->supplier->name : '';
                    $finalArray[$i]['type'] = $data->transactions_type;
                    $finalArray[$i]['bank'] = ($data->bankName) ? $data->bankName->name : '';
                    $finalArray[$i]['original_value'] = $data->total_amount;
                    $finalArray[$i]['net_value'] = $data->net_value;
                    $finalArray[$i]['min_rate'] = $data->min_rate;
                    $finalArray[$i]['max_rate'] = $data->max_rate;
                    $rate_used = $finalArray[$i]['used_rate'] = ($data->negotiate_rate > 0) ? $data->negotiate_rate : $data->max_rate; 
                    $deviation = $finalArray[$i]['deviation'] = (($rate_used - $data->min_rate)/ ($data->max_rate - $data->min_rate))*100;
                    $transactionsTotalId = $transactionsTotalId + 1;
                    $totalOrignalAmount = $totalOrignalAmount + $data->total_amount;
                    $totalNetValue = $totalNetValue + $data->net_value;
                    $minRateSum  = $minRateSum + ($data->total_amount * $data->min_rate/100);
                    $maxRateSum = $maxRateSum + ($data->total_amount * $data->max_rate / 100);
                    $usedRateSum = $usedRateSum + ($data->total_amount * $rate_used / 100);
                    $deviationRateSum = $deviationRateSum + ($data->total_amount * $deviation / 100);
                    $i++;
                }
                if(count($finalArray) > 0){
                    $finalArray[$i]['approval_Date'] = 'Total';
                    $finalArray[$i]['transaction_id'] = $transactionsTotalId;
                    $finalArray[$i]['created_by'] = '';
                    $finalArray[$i]['customer'] = '';
                    $finalArray[$i]['supplier'] = '';
                    $finalArray[$i]['type'] = '';
                    $finalArray[$i]['bank'] = '';
                    $finalArray[$i]['original_value'] = $totalOrignalAmount;
                    $finalArray[$i]['net_value'] = $totalNetValue;
                    $minrateSum = ($minRateSum / $totalOrignalAmount) *100;
                    $maxRateSum = ($maxRateSum / $totalOrignalAmount) * 100;
                    $usedRateSum = ($usedRateSum / $totalOrignalAmount) * 100;
                    $deviationRateSum = ($deviationRateSum / $totalOrignalAmount) * 100;
                    $finalArray[$i]['min_rate'] = $minrateSum;
                    $finalArray[$i]['max_rate'] = $maxRateSum;
                    $rate_used = $finalArray[$i]['used_rate'] = $usedRateSum;
                    //$rate_used = $finalArray[$i]['used_rate'] = ($data->negotiate_rate > 0) ? $data->negotiate_rate : $data->max_rate;
                    $finalArray[$i]['deviation'] = $deviationRateSum;
                    // $finalArray[$i]['deviation'] = (($rate_used - $data->min_rate) / ($data->max_rate - $data->min_rate)) * 100;

                    $filename = 'file.csv';
                    $handle = fopen($filename, 'w+');
                    $data = array('Date', 'Transaction ID', 'Created By', 'Customer', 'Supplier', 'Type', 'Bank', 'Original Value', 'Net Value', 'Min Rate', 'Max Rate', 'Rate Used', 'Deviation');

                    fputcsv($handle, $data);

                    foreach ($finalArray as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return response()->download($filename, 'Deals closed by CB-commercial user' . date("d-m-Y H:i") . '.csv', $headers);
                }
            }else{
                Session::flash('flash_warning', __('Data is not Available!'));
                return redirect()->back();
            }
            exit;
        }
        $suppliers = Supplier::orderBy('name','asc')->pluck('name', 'id')->prepend('Select Suppliers', '');
        $start_date = date('01/m/Y', time());
        $end_date = date('t/m/Y');
        return view('admin.users.cbusersreport.index', compact('suppliers', 'start_date', 'end_date'));
        exit;
    }

    public function revenueForConnectBahn(Request $request){
        if($request && $request->startdate !='' && $request->enddate != ''){
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
        //$start_date = date('Y-m-d',strtotime($request->startdate))  . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';

            $revenueForConnectBahn = Transaction::whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')
                ->where('transactions.customer_status', '=', 'Approved')->orderBy('transactions.updated_at', 'asc')
                ->select(DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"), DB::raw('COUNT(*) AS transaction_count,
                    SUM(`transactions`.`total_amount`) AS total_amount,
                    SUM(`transactions`.`net_value`) AS net_value,
                    SUM(`transactions`.`discount_value`) AS discount_value,
                    SUM(`transactions`.`cost`) AS cost,
                    MONTH(`transactions`.`updated_at`) AS MONTH,
                    YEAR(`transactions`.`updated_at`) AS YEAR,
                    `transactions`.`id` AS `transaction_id`'))
                //->with('monthlyReports')
                ->groupBy('monthyear')
                ->get();
            $i = 0;
            $finalArray = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totalfinancialcost = 0;
            $totaltranscount = 0;
            $totaltransactioncost = 0;
            $totalvalue = 0;
            if (count($revenueForConnectBahn) > 0) {
                foreach ($revenueForConnectBahn as $rows) {
                    $finalArray[$i]['updated_at'] = $rows->monthyear;
                    $finalArray[$i]['total_transaction'] = $rows->transaction_count;
                    $finalArray[$i]['original_value'] = $rows->total_amount;
                    $finalArray[$i]['net_val'] = $rows->net_value;
                    $finalArray[$i]['cost'] = $rows->cost;
                    $total = $rows->total_amount- $rows->net_value ;
                    $finalArray[$i]['total'] = $total;
                    $finalArray[$i]['revenue'] = $total*(1-0.6);
                    $totaltranscount = $totaltranscount + $rows->transaction_count;
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $totalfinancialcost = $totalfinancialcost + $rows->discount_value;
                    $totaltransactioncost = $totaltransactioncost + $rows->cost;
                    $totalvalue = $totalvalue + $total;
                    $i++;
                }
                if (count($finalArray) > 0) {
                    $finalArray[$i]['updated_at'] = 'Total';
                    $finalArray[$i]['total_transaction'] = $totaltranscount;
                    $finalArray[$i]['original_value'] = $totalorigval;
                    $finalArray[$i]['net_val'] = $totalnetval;
                    $finalArray[$i]['cost'] = $totaltransactioncost;
                    $finalArray[$i]['total'] = $totalvalue;
                    $finalArray[$i]['revenue'] = $totalvalue * (1 - 0.6);
                    $filename = 'file.csv';
                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    $handle = fopen($filename, 'w+');
                    $data = array('Month', 'Transactions', 'Original Value', 'Net Value','Cost','Total', 'Cb Revenue');
                    fputcsv($handle, $data);
                    foreach ($finalArray as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);
                    return response()->download($filename, 'ConnectBahn-monthly_reports ' . date("d-m-Y H:i") . '.csv', $headers);

                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
            } else {
                Session::flash('flash_warning', __('Data is not Available!'));
                return redirect()->back();
            }
        }
        $start_date = date('01/m/Y', time());
        $end_date = date('t/m/Y');
        return view('admin.users.cbusersreport.revenueForConnectBahn', compact( 'start_date', 'end_date'));
        exit;
    }

    
}
