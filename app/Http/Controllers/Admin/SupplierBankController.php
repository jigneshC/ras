<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddSupplierBank;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use CountryState;

use App\Bank;
use App\BankAccount;
use App\BankContact;
use App\Supplier;
use Illuminate\Http\Request;
use Session;
use DB;
use App\SupplierBank;
use App\SupplierUser;
use App\User;
use App\Role;
use Yajra\Datatables\Datatables;


class SupplierBankController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.supplier.bank.change.status')->only('statusupdate');
        $this->middleware('permission:access.supplier.bank.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.suppliers.bank.add')->only(['create', 'store']);
        $this->middleware('permission:access.supplier.bank.delete')->only('destroy');
    }


    public function index($id)
    {
        $supplier = Supplier::where('id',$id)->FirstOrFail();

        return view('admin.suppliers.supplier_bank',compact('supplier'));
    }

    public function datatableSupplierBank($id){

        $supplier_bank = SupplierBank::with('supplier')->where('supplier_id',$id)->get();

        return Datatables::of($supplier_bank)->make(true);
    }

    public function create()
    {
       // return view('admin.equipments.create');
    }

    public function store(Request $request)
    {

        $code = 200;$message="";
        $this->validate($request, [
            'account_number' => 'required',
            'agency_number' => 'required'
        ]);

        $supplier = Supplier::where('id',$request->supplier_id)->FirstOrFail();

        if($supplier){

            $requestData = $request->except('bank_account_id');

            $sup_bank = SupplierBank::create($requestData);

            $sup_bank->save();

            \ActionLog::addToLog("Add Supplier-Bank","Supplier-" .$supplier->name . "added a new bank  ".$sup_bank->bank_name." ",$sup_bank->getTAble(),$sup_bank->id);

            $message = "Account created success !!";
            
            // notification
            $sup_user = SupplierUser::where('supplier_id',$supplier->id)->get();

            foreach ($sup_user as $sup_users) {
                
                $user_id = $sup_users->user_id;
                
                $user = User::find($user_id);

                $user->notify(new AddSupplierBank($sup_bank));
            }

            if(\Auth::check()){

                if(\Auth::user()->roles[0]->name == "SU"){
                    //if bank is added by admin, notification send to all backoffice

                    $cbb = Role::Where('name','CBB')->get();

                    if($cbb != ""){
                        foreach($cbb as $roles){
                            foreach($roles->users as $cbbuser){
                                $user_idd[] = $cbbuser->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_idd)->get();

                    foreach($users as $bkouser){

                        $bkouser->notify(new AddSupplierBank($sup_bank));
                    }
                }else{

                    //if bank is added by any backoffice then notification send to admin
                    $su = Role::Where('name','SU')->get();

                    if($su != ""){
                        foreach($su as $roles){
                            foreach($roles->users as $su_user){
                                $user_idd[] = $su_user->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_idd)->get();

                    foreach($users as $adminuser){

                        $adminuser->notify(new AddSupplierBank($sup_bank));
                    }
                }
            }

            
        }else{
            $code = 400;
            $message = "No bank found";
        }

        return response()->json(['messages' => $message],$code);
        
    }

    public function statusupdate(Request $request, $id){
        $status = $request->get('status');
        if (!empty($status)) {
            $supplierBankAccount = SupplierBank::findOrFail($id);
            if ($status == 'active') {

                $supplierBankAccount->status = 'inactive';
                $supplierBankAccount->update();

                \ActionLog::addToLog("Supplier Bank Status Changed","Bank - ". $supplierBankAccount->bank_name ."'s status changed to Inactive",$supplierBankAccount->getTAble(),$supplierBankAccount->id);

                return redirect()->back();

            } elseif ($status == 'inactive') {

                $supplierBankAccount->status = 'active';
                $supplierBankAccount->update();

                \ActionLog::addToLog("Supplier Bank Status Changed","Bank - ". $supplierBankAccount->bank_name ."'s status changed to Active",$supplierBankAccount->getTAble(),$supplierBankAccount->id);

                return redirect()->back();
            }
            return redirect()->back();
        }
    }
 
    public function show($id)
    {

    }

    public function edit($id)
    {
        $data = SupplierBank::where('id',$id)->FirstOrFail();

        if($data){
            $code= 200;
        }else{
            $code = 400;
        }
        

        return response()->json(['data' => $data],$code);

    }

    public function update($id, Request $request)
    {
        $code = 200; $message="";
        $this->validate($request, [
            'account_number' => 'required',
            'agency_number' => 'required' 
        ]);

        $supplier = Supplier::where('id',$request->supplier_id)->FirstOrFail();
        $supplier_bank = SupplierBank::find($id);


        if($supplier && $supplier_bank){

            $requestData = $request->except('bank_account_id');
            $supplier_bank->update($requestData);

            \ActionLog::addToLog("Edit Supplier Bank","Supplier Bank - ". $supplier_bank->bank_name ." is Updated",$supplier_bank->getTAble(),$supplier_bank->id);

            $message = "Account Updated success !!";
        }else{
            $code = 400;
            $message = "No bank found";
        }

       // return response()->json(['messages' => $message],$code);
    }


    public function destroy($id)
    {

        $supplier_bank = SupplierBank::find($id);

        \ActionLog::addToLog("Delete Supplier Bank","Supplier Bank - ". $supplier_bank->bank_name ." is Deleted",$supplier_bank->getTAble(),$supplier_bank->id);

        $supplier_bank->delete();

        $message = "Account Deleted success !!";

        return response()->json(['messages' => $message],200);
    }
}
