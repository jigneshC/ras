<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddSupplier;
use App\Notifications\ChangeCBCommercial;
use App\Notifications\AddSupplierBank;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ActionLog;
use App\DiscountRate;
use App\Supplier;
use App\Address;
use App\Transaction;
use App\Role;
use App\User;
use App\SupplierUser;
use Illuminate\Http\Request;
use Session;
use DB;
use App\Customer;
use Auth;
use Yajra\Datatables\Datatables;
use App\SupplierBank;
use App\TransactionsInvoices;
use App\CustomerSupplier;
use App\SupplierUnit;

class SuppliersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.suppliers');
        $this->middleware('permission:access.suppliers.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.suppliers.create')->only(['create', 'store']);
        $this->middleware('permission:access.suppliers.delete')->only('destroy');
        $this->middleware('permission:change.cb.rep.supplier')->only('changeCbRepSupplier');
        
    }



    public function index(Request $request)
    {

        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id', 'roles.label as role_label')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['CBC'])
            ->where('users.user_status','=','active')->get();

        return view('admin.suppliers.index',compact('user'));
    }

    public function datatable(Request $request){
        $search = $request->get('search');
        $search = $search['value'];
        $user = User::find(auth()->user()->id);
        $suppliers = Supplier::select('supplier.*','users.name as user_name','users.id as user_id')
            ->leftJoin('supplier_user', 'supplier_user.supplier_id','=','supplier.id')
            ->leftJoin('users','users.id','=','supplier_user.user_id')
            ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles','roles.id','=','role_user.role_id')
            ->where('roles.name', '=', 'CBC')
            ->with('invoiceCession', 'invoiceAnticipastion', 'invoicedueSupplier','cb_rate_class');
        if($user->roles[0]->name =='CBC' || $user->roles[0]->name =='SLO' || $user->roles[0]->name =='SLA'){
            $user = User::with('supplier')->find(auth()->user()->id); 
            $supplier_id = array();
            foreach($user->supplier as $supplier){
                $supplier_id[] = $supplier->id;
            }
            $suppliers->whereIn('supplier.id',$supplier_id);
        }
        return Datatables::of($suppliers)
            ->make(true);
            exit;
    }

    public function create(Request $request)
    {
        $supllierUsers = array();
        $roles = Role::where('name','SLO')->orwhere('name','SLA')->get();

        if($roles != ""){
            foreach($roles as $role){
                foreach($role->users as $user){

                    $supllierUsers[$role->name][$user->id] = $user->name;
                }
            }
        }
        
        $commercialUsers = [""=>"Selecione"];  //select
        $cbcroles = Role::where('name','CBC')->get();

        if($cbcroles != ""){
            foreach($cbcroles as $cbcrole){
                foreach($cbcrole->users as $user){
                    $user_id[] = $user->id ;
                } 
            }
            $users = User::whereIN('id',$user_id)->where('user_status','active')->get();

            if(count($users) != 0){

               foreach ($users as $user) {
                    
                    $commercialUsers[$user->id] = $user->name;  
                }

            }     

        }
        $discountRate = DiscountRate::where('customer_id', '=', '0')->orderBy('class')->get();
		$countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');
        return view('admin.suppliers.create',compact('countries','supllierUsers','commercialUsers', 'discountRate'));
    }

    public function store(Request $request)
    {   
        $messages = [
            'name.unique' => "There's already a Supplier Group Name with same Name",
            'unit_name.unique' => "There's already a Supplier Unit with same Unit Name",
            'cpnj.unique' =>"Found Supplier, $request->cpnj using this CNPJ",
            
        ];

        $this->validate($request, [
			'name' => 'required|unique:supplier,name',
            'unit_name' => 'required|unique:supplier_unit,unit_name',
            'cpnj' => 'required|unique:supplier_unit,cpnj',
            'unit_legal_name' => 'required',
            'website' => 'required',
            'city' => 'required',
            'state' => 'required',
            'countries' => 'required',
            'zip' => 'required',
            'neighborhood' => 'required',
            'typeofsupplier' => 'required',
            'cbuser' => 'required'
        ],$messages);
        
        $supplier = new Supplier;
        $supplier->name = $request->get('name');
        $supplier->typeofsupplier = $request->get('typeofsupplier');
        $supplier->website = $request->get('website');
        $supplier->cb_rate_class = $request->get('cb_rate_class');

        if ($request->file('image')) {
            
            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $filename);
            $supplier->image = $filename;
            
        }
        $supplier->save();

        //discount_rate
        if(!empty($request->get('cb_rate_class'))){
            $customer_supplier = CustomerSupplier::where('supplier_id',$supplier->id)->get();

            foreach($customer_supplier as $custsup){
                $custsup->customer_supplier_rate_by_commercial_user = $request->get('cb_rate_class');
                $custsup->save();
            }

        }

        if(!empty($request->get('cbuser'))){
            $supplier_user = new SupplierUser; 
            $supplier_user->supplier_id = $supplier->id;
            $supplier_user->user_id = $request->get('cbuser');
            $supplier_user->save();

            $commercial_user = User::find($request->get('cbuser'));

            \ActionLog::addToLog("Add ConnectBahn Commercial","Supplier -" .$supplier->name . " has add commercial user",$supplier->getTAble(),$supplier->id,array('supplier'=>$supplier->name,'cbuser'=>$commercial_user->name,  'date'=>date('d-m-Y') ,'old_commercialUsers'=>'', 'changed_by'=>Auth::user()->name));

            $user = User::find($supplier_user->user_id);

            $user->notify(new AddSupplier($supplier));
        }

        if($supplier->save()){

            $supunit = new SupplierUnit;
            $supunit->supplier_id = $supplier->id;
            $supunit->unit_name = $request->get('unit_name');
            $supunit->unit_legal_name = $request->get('unit_legal_name');
            $supunit->cpnj = $request->get('cpnj');
            $supunit->neighborhood = $request->get('neighborhood');
            $supunit->city = $request->get('city');
            $supunit->state = $request->get('state');
            $supunit->country = $request->get('countries');
            $supunit->address_number = $request->get('address_number');
            $supunit->address_street = $request->get('address_street');
            $supunit->complement = $request->get('address_complement');
            $supunit->zip = $request->get('zip');
            $supunit->status = "active";
            $supunit->save();

            \ActionLog::addToLog("Add Supplier-Unit","Supplier-" .$supplier->name . "added a new Unit- ".$supunit->unit_name ,$supunit->getTAble(),$supunit->id);

        }


        if($supplier->save()){


            $supbank = new SupplierBank;
            $supbank->supplier_id = $supplier->id;
            $supbank->bank_name =  $request->get('bank_name');
            $supbank->bank_number = $request->get('bank_number');
            $supbank->agency_name = $request->get('agency_name');
            $supbank->agency_number = $request->get('agency_number');
            $supbank->account_number = $request->get('account_number');
            $supbank->account_info = $request->get('account_info');
            $supbank->info = $request->get('info');
            $supbank->status = "active";
            $supbank->save();


            \ActionLog::addToLog("Add Supplier-Bank","Supplier-" .$supplier->name . "added a new bank ".$supbank->bank_name." ",$supbank->getTAble(),$supbank->id);

            $sup_user = SupplierUser::where('supplier_id',$supplier->id)->get();

            foreach ($sup_user as $sup_users) {
                
                $user_id = $sup_users->user_id;
                
                $user = User::find($user_id);

                $user->notify(new AddSupplierBank($supbank));
            }

            if(\Auth::check()){

                if(\Auth::user()->roles[0]->name == "SU"){
                    //if bank is added by admin, notification send to all backoffice

                    $cbb = Role::Where('name','CBB')->get();
                    $user_id = array();
                    
                    if($cbb != ""){
                        foreach($cbb as $roles){
                            foreach($roles->users as $cbbuser){
                                $user_id[] = $cbbuser->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();


                    foreach($users as $bkouser){

                        $bkouser->notify(new AddSupplierBank($supbank));
                    }
                }else{

                    //if bank is added by any backoffice then notification send to admin
                    $su = Role::Where('name','SU')->get();

                    if($su != ""){
                        foreach($su as $roles){
                            foreach($roles->users as $su_user){
                                $user_id[] = $su_user->id ;
                            } 
                        }
                    }
                    $users = User::whereIN('id',$user_id)->get();

                    foreach($users as $adminuser){

                        $adminuser->notify(new AddSupplierBank($supbank));
                    }
                }
            }
        }


        Session::flash('flash_message', 'Supplier added!');

        \ActionLog::addToLog("Add Supplier","Supplier-" .$supplier->name . " is added",$supplier->getTAble(),$supplier->id);

        if($request->get('ajax')){
            $suppliers = Supplier::all();
            foreach($suppliers as $oldSuppliers){
                $newsupplier[$oldSuppliers->id] = $oldSuppliers->name;
            }
            $json['msg'] =  'Success';
            $json['supplier'] = $newsupplier;
            echo json_encode($json);
            exit;
        }else{
            return redirect('admin/suppliers');
        }
        
    }

    public function show($id)
    {
        $supplier = Supplier::with('user')->find($id);

        foreach($supplier->user as $suplierUser){
            $role = $suplierUser->roles;
            $supplier_user[$role[0]['name']] = $suplierUser->name;
        }
        return view('admin.suppliers.show', compact('supplier','supplier_user'));
    }

    public function edit($id)
    {
        $roles = Role::where('name','SLO')->orwhere('name','SLA')->get();
         $supllierUsers = array();
         foreach($roles as $role){
             foreach($role->users as $user){
                  $supllierUsers[$role->name][$user->id] = $user->name;
             }
         }
        $discountRate = DiscountRate::where('customer_id', '=', '0')->orderBy('class')->get();
        
        $supplier = Supplier::with('user')->find($id);
        //echo '<pre>';print_r($supplier->users);echo '</pre>';
        foreach($supplier->user as $suplierUser){
            $role = $suplierUser->roles;
            $supplier_user[$role[0]['name']] = $suplierUser->id;
        }
        $commercialSuppUsers = '';
        //$supplier_user = SupplierUser::where('supplier_id',$id)->FirstorFail();
        foreach ($supplier->user as $user){
            if($user->role[0]->name == 'CBC'){
                $commercialSuppUsers = $user->id;
            }
        }
        $cbroles = Role::where('name','CBC')->get();
        $commercialUsers = array();
        if($cbroles != ""){
            foreach($cbroles as $role){

                foreach($role->users as $user){
                    $commercialUsers[$user->id] = $user->name;
                }
            }
        }
        $sup_data = Supplier::find($id);
        $discount_rate = DiscountRate::where('id',$sup_data->cb_rate_class)->first();

        return view('admin.suppliers.edit', compact('supplier','supllierUsers','supplier_user','commercialUsers','commercialSuppUsers', 'discountRate','discount_rate'));
    }

    public function update($id, Request $request)
    {   
 
        $this->validate($request, [
            'name' => 'required|unique:supplier,name,'.$id,
            'website' => 'required',
            'typeofsupplier' => 'required',
        ]);

        $supplier = Supplier::findOrFail($id);
        $supplier->name = $request->get('name');
        $supplier->typeofsupplier = $request->get('typeofsupplier');
        $supplier->website = $request->get('website');
        $supplier->cb_rate_class = $request->get('cb_rate_class');

        if ($request->file('image')) {
            
                        $image = $request->file('image');
                        $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            
                        $image->move(public_path('images'), $filename);
            
                        $supplier->image = $filename;
            
        }
        $supplier->update();

        //discount_rate
        if(!empty($request->get('cb_rate_class'))){
            $customer_supplier = CustomerSupplier::where('supplier_id',$supplier->id)->get();

            foreach($customer_supplier as $custsup){
                $custsup->customer_supplier_rate_by_commercial_user = $request->get('cb_rate_class');
                $custsup->save();
            }

        }

        Session::flash('flash_message', 'Supplier updated!');

        \ActionLog::addToLog("Edit Supplier","Supplier-" .$supplier->name . " is updated",$supplier->getTAble(),$supplier->id);

        return redirect('admin/suppliers');
    }

    public function destroy($id)
    {
        $supplier = Supplier::find($id);

        if($supplier->image != ""){
            $file = public_path('images/' .$supplier->image);

            if (file_exists($file)) {
                unlink($file);
            }
        }
        $supplier->delete();

        $supuser = SupplierUser::where('supplier_id',$id)->get();
        
        foreach ($supuser as $sup){
            $sup->delete();
        }
        
        $supbank = SupplierBank::where('supplier_id',$id)->get();
        
        foreach ($supbank as $sup){
            $sup->delete();
        }

        $supcust = CustomerSupplier::where('supplier_id',$id)->get();
        
        foreach ($supcust as $sup){
            $sup->delete();
        }
        

        \ActionLog::addToLog("Supplier Deleted","Supplier-" .$supplier->name . "is deleted",$supplier->getTAble(),$supplier->id);

        
        Session::flash('flash_message', 'Supplier deleted!');
        return response()->json('200');
       
    }


    public function manage(Request $request){
        
        $sup_id = $request->get('sup');
        if($sup_id < 1){
            $sup_id = 0;
        }


        return view('admin.suppliers.manage_supplier',compact('sup_id'));

    }

    public function managedatatable(Request $request){
        $search = $request->get('search');
        $search = $search['value'];
        $suppliers = Supplier::select('supplier.*','users.name as user_name')
            ->leftJoin('supplier_user', 'supplier_user.supplier_id', '=', 'supplier.id')
            ->leftJoin('users', 'users.id', '=', 'supplier_user.user_id')
            ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', '=', 'CBC')
            ->with('invoiceCession', 'invoiceAnticipastion', 'invoicedueSupplier');
            
        if($search != ''){
           // $suppliers->where('supplier.name','like','%'.$search.'%');
        }


        return Datatables::of($suppliers)
           ->make(true);
            exit;   

    }
    public function managelogdatatable(Request $request){

        $sup_id = $request->get('supp_id');

        if(!empty($sup_id) && $sup_id != 0){

            $supplier_name = Supplier::where('id',$sup_id)->pluck('name')->first();
            
            $log = ActionLog::whereIn('action',array('Change ConnectBahn Commercial', 'Add ConnectBahn Commercial'))->get();
            $detail = array();
            foreach ($log as $key) {
                $detail[] = unserialize($key->sub_detail);
            }
            
             $details = collect($detail);
            if(isset($detail) && isset($supplier_name)){
                
                foreach($details as $key => $value){
                    if($supplier_name == $value["supplier"]){
                       
                        $det[] = $value; 
                          
                    }

                }
                $dets = collect($det);
                        return Datatables::of($dets)
                        ->make(true);
                        exit;   
                     
                
            }                  

        }
        
        else{
            
            $log = ActionLog::whereIn('action', array('Change ConnectBahn Commercial', 'Add ConnectBahn Commercial'))->get();
            $detail = array();

            foreach ($log as $key) {
                $detail[] = unserialize($key->sub_detail);
            }

            $detail = collect($detail);

            return Datatables::of($detail)
           ->make(true);
            exit;
            
        }
            
    }

    public function supplier_log($id){

        $supplier_name = Supplier::where('id',$id)->pluck('name')->first();

        $log = ActionLog::whereIn('action', array('Change ConnectBahn Commercial', 'Add ConnectBahn Commercial'))->get();
            $detail = array();
            foreach ($log as $key) {
                $detail[] = unserialize($key->sub_detail);
            }
            
            
            foreach ($detail as $key => $value){

                if($supplier_name == $value["supplier"]){

                    $pass[] = $value;
                    
                }else{ 
                    $pass[] = "";
                }

            }

        return view('admin.suppliers.supplier_log',compact('pass'));

    }

    public function summary_supplier(){

        $user = User::find(Auth::user()->id) ;
        
        if($user->roles[0]->name == "CBC" || $user->roles[0]->name == "SLO" || $user->roles[0]->name == "SLA"){
            $sup_user = SupplierUser::where('user_id',$user->id)->pluck('supplier_id');
            $supplier = Supplier::whereIn('id',$sup_user)->latest()->paginate(10);

        }elseif($user->roles[0]->name == "SU"){
           
            $supplier = Supplier::latest()->paginate(10);

        }else{
            $supplier = Supplier::latest()->paginate(10);
        }
        return view('admin.suppliers.summary_supplier',compact('supplier'));

    }

    public function transactionHistory($id){

        $startdate = date('01-m-Y');
        
        $enddate = date('t-m-Y');

        $supplier = Supplier::where('id',$id)->FirstorFail();

        $cbcroles = Role::where('name','CBC')->get();

        if($cbcroles != ""){
            foreach($cbcroles as $cbcrole){
                foreach($cbcrole->users as $user){
                    $user_id[] = $user->id ;
                } 
            }
        }
        $supp_user = SupplierUser::where('supplier_id',$supplier->id)
        ->whereIN('user_id',$user_id)->with('user')->first();

        return view('admin.suppliers.transaction_history_supplier',compact('supplier','startdate','enddate','supp_user'));
    }

    public function transactionHistoryDatatable(Request $request){
        
        $id = $request->get('supplier_id');

        $sdate=$request->get('startdate');
        $edate=$request->get('enddate');
        $stdate = str_replace('/','-',$sdate);
        $startdate= date('Y-m-d' , strtotime($stdate));
        $endate = str_replace('/','-',$edate);
        $enddate= date('Y-m-d' , strtotime($endate));

        $search = $request->get('searchText');
        
        $supplierWithCustomer = Supplier::with([
            'customer' => function ($q) use ($search) {
                if($search != ''){
                    $q->where('customer.name','like','%'.$search.'%');
                }
            }
        ])->find($id);


        $customers = $supplierWithCustomer->customer;


        $transactions = TransactionsInvoices::select('transactions_invoices.*', 'invoices.duedate','invoices.anticipation_date', 'invoices.total_amount', 'invoices.discount_rate', 'invoices.discounted_value', 'invoices.cost', 'invoices.status')
            ->join('invoices', 'invoices.id', '=', 'transactions_invoices.invoice_id')
            ->where('transactions_invoices.supplier_id', '=',$id)
            ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
            ->whereBetween('transactions_invoices.created_at', [$startdate . " 00:00:00",$enddate . " 23:59:59"])->get();


        
        $alldataCustomer = array();
        
        foreach ($customers as $key => $customer) {
            $transactionInvoice = array();
            $alldataCustomer[$key] = $customer;
            $alldataCustomer[$key]['totalInvoiceCession'] = 0;
            $alldataCustomer[$key]['totalInvoiceAnticipation'] = 0;
            $alldataCustomer[$key]['totalInvoice'] = 0;
            $alldataCustomer[$key]['amounttotal'] = 0;
            $alldataCustomer[$key]['cessiontotal'] = 0;
            $alldataCustomer[$key]['anticipationtotal'] = 0;
            $alldataCustomer[$key]['revenuewForAnticipation'] = 0;
            $alldataCustomer[$key]['revenuewForCession'] = 0;
            foreach ($transactions as $transaction) {
                
                if ($customer['id'] == $transaction['customer_id']) {
                    if ($transaction['transaction_number'] != '') {
                        if (!in_array($transaction['transaction_number'], $transactionInvoice)) {
                            $transactionInvoice[$transaction['transaction_number']] = $transaction['transaction_number'];
                            $transactionNumber = $transaction['transaction_number'];
                            $transactionGet = Transaction::find($transactionNumber); 
                            // if($transactionGet->customer_status != 'Approved'){
                            //     continue;
                            // }
                            if ($transactionGet->type == 'Cession') {
                                if ($alldataCustomer[$key]['revenuewForCession'] != '') {
                                    $alldataCustomer[$key]['revenuewForCession'] = $alldataCustomer[$key]['revenuewForCession'] + $transactionGet->discount_value - $transactionGet->cost;
                                } else {
                                    $alldataCustomer[$key]['revenuewForCession'] = $transactionGet->discount_value - $transactionGet->cost;
                                }
                            } else {
                                if ($alldataCustomer[$key]['revenuewForAnticipation'] != '') {
                                    $alldataCustomer[$key]['revenuewForAnticipation'] = $alldataCustomer[$key]['revenuewForAnticipation'] + $transactionGet->discount_value - $transactionGet->cost;
                                } else {
                                    $alldataCustomer[$key]['revenuewForAnticipation'] = $transactionGet->discount_value - $transactionGet->cost;
                                }
                            }

                            if($transactionGet->type == 'Cession') {
                                $alldataCustomer[$key]['totalInvoiceCession'] = $alldataCustomer[$key]['totalInvoiceCession'] + 1;
                            }

                            if($transactionGet->type == 'Anticipation') {
                                $alldataCustomer[$key]['totalInvoiceAnticipation'] = $alldataCustomer[$key]['totalInvoiceAnticipation'] + 1;
                            }

                        }
                    }

                    if ($transaction['payment_type'] == 'Cession') {

                        if ($alldataCustomer[$key]['cessiontotal'] != '') {

                            $alldataCustomer[$key]['cessiontotal'] = $alldataCustomer[$key]['cessiontotal'] + $transaction['total_amount'];
                        
                        } elseif ($transaction['total_amount'] > 0) {

                            $alldataCustomer[$key]['cessiontotal'] = $transaction['total_amount'];
                        } else {
                            $alldataCustomer[$key]['cessiontotal'] = 0;
                        }

                       // $alldataCustomer[$key]['totalInvoiceCession'] = $alldataCustomer[$key]['totalInvoiceCession'] + 1;

                    } elseif (!isset($alldataCustomer[$key]['cessiontotal'])) {
                        $alldataCustomer[$key]['cessiontotal'] = 0;
                    }
                    if ($transaction['payment_type'] == 'Anticipation') {

                        if ($alldataCustomer[$key]['anticipationtotal'] != '') {

                            $alldataCustomer[$key]['anticipationtotal'] = $alldataCustomer[$key]['anticipationtotal'] + $transaction['total_amount'];

                        } elseif ($transaction['total_amount'] > 0) {
                            $alldataCustomer[$key]['anticipationtotal'] = $transaction['total_amount'];
                        
                        } else {

                            $alldataCustomer[$key]['anticipationtotal'] = 0;
                        }

                      //  $alldataCustomer[$key]['totalInvoiceAnticipation'] = $alldataCustomer[$key]['totalInvoiceAnticipation'] + 1;
                    
                    } elseif (!isset($alldataCustomer[$key]['anticipationtotal'])) {
                        $alldataCustomer[$key]['anticipationtotal'] = 0;
                    }

                    if ($alldataCustomer[$key]['amounttotal'] != '') {
                        $alldataCustomer[$key]['amounttotal'] = $alldataCustomer[$key]['amounttotal'] + $transaction['total_amount'];
                    
                    } else {

                        $alldataCustomer[$key]['amounttotal'] = $transaction['total_amount'];
                    }
                    $alldataCustomer[$key]['totalInvoice'] = $alldataCustomer[$key]['totalInvoice'] + 1;
                }
            }
        }
        $alldataCustomer = collect($alldataCustomer);
        
      
        return Datatables::of($alldataCustomer)
            ->make(true);
        
    }

    public function changeCbRepSupplier(Request $request){
    
        if($request->input('submit')){

            if($request->input('cbrepforsupplier') != 0 && $request->input('oldcbrep_id') != '' ){

                if($request->input('cbrepforsupplier') == $request->input('oldcbrep_id')){

                    return json_encode(array('msg' => 'Success'));

                }
                else{

                    if($request->input('oldcbrep_id') != '' ){

                        $supplier_users = SupplierUser::where('supplier_id', $request->input('cbsupplier_id'))->where('user_id', $request->input('oldcbrep_id'))->FirstorFail();

                        $supplier_users->user_id = $request->get('cbrepforsupplier');

                        $supplier_users->save();

                        $supplier_name = Supplier::where('id',$request->input('cbsupplier_id'))->FirstorFail();

                        $cbuser = User::where('id',$request->get('cbrepforsupplier'))->FirstorFail();

                        \ActionLog::addToLog("Change ConnectBahn Commercial","Supplier -" .$supplier_name->name . " has changed commercial user", $supplier_users->getTAble() , $supplier_users->id , array('supplier'=> $supplier_name->name ,  'date'=>date('d-m-Y') ,'cbuser'=>$cbuser->name,'old_commercialUsers'=>$request->input('oldcbrep_id'),'changed_by' => Auth::user()->name ) );

                        $old_user = User::find($request->input('oldcbrep_id'));

                        $old_user->notify(new ChangeCBCommercial($supplier_users,$old_user));

                        $cbuser->notify(new ChangeCBCommercial($supplier_users,$old_user));

                            return json_encode(array('msg' => 'Success'));

                        }elseif($request->input('oldcbrep_id') == '0'){

                            $supplier_users = new SupplierUser;
                            $supplier_users->supplier_id = $request->input('cbsupplier_id');

                            $supplier_users->user_id = $request->get('cbrepforsupplier');

                            $supplier_users->save();

                            $supplier_name = Supplier::where('id',$request->input('cbsupplier_id'))->FirstorFail();

                            $cbuser = User::where('id',$request->get('cbrepforsupplier') )->FirstorFail();


                            \ActionLog::addToLog("Change ConnectBahn Commercial","Supplier -" .$supplier_name->name . " has changed commercial user", $supplier_users->getTAble() , $supplier_users->id , array('supplier'=> $supplier_name->name ,  'date'=>date('d-m-Y') ,'cbuser'=>$cbuser->name,'old_commercialUsers'=>$request->input('oldcbrep_id'),'changed_by' => Auth::user()->name ) );

                            return json_encode(array('msg' => 'Success'));

                        }else{
                            return json_encode(array('msg' => 'Error'));            
                        }
                }
            }else{
                return json_encode(array('msg' => 'Error'));    
            }
        }else{
            return json_encode(array('msg' => 'Error'));
        }
        
        
    }
    

}
