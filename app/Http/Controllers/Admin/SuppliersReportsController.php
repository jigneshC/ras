<?php

namespace App\Http\Controllers\Admin;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplier;
use App\Transaction;
use DB;
use App\User;

class SuppliersReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.reports');
        $this->middleware('permission:access.reprorts.supplierdaily');
        $this->middleware('permission:access.reports.suppliermonthlyfinancialcost');

    }

    public function index(Request $request){
        
        if ($request && $request->supplier_id != '' && $request->startdate != '' && $request->enddate != '') {
            $supplier_id = $request->get('supplier_id');
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1]. '-' . $startdate[0] . ' 00:00:00';
			$start_date = date('Y-m-d',strtotime($start_date))  . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $dailyupdate = Transaction::where('transactions.supplier_id', $supplier_id)
                ->whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')->where('transactions.customer_status', '=', 'Approved')->orderBy('transactions.updated_at', 'asc')
                ->with('supplierName', 'bankDetails', 'bankName', 'supplierunitName')
                ->select('*', (DB::raw('DATE(transactions.updated_at) AS transaction_update_date')))
                ->where('transactions.type', '=', 'Anticipation')
                ->get();
            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totaltranscount = 0;
            if (count($dailyupdate) > 0) {
                foreach ($dailyupdate as $rows) {
                    $finalArra[$i]['updated_at'] = date('d-m-Y',strtotime($rows->transaction_update_date));
                    $finalArra[$i]['id'] = $rows->id;
                    $finalArra[$i]['name'] = $rows->supplierName->supplier_name;
                    $finalArra[$i]['unit'] = $rows->supplierunitName[0]->unit_name;
                    $finalArra[$i]['cpnj'] = $rows->supplierunitName[0]->cpnj;
                    $finalArra[$i]['payment_type'] = $rows->type;
                    $finalArra[$i]['bank_name'] = ($rows->bankName) ? $rows->bankName->name : '';
                    $finalArra[$i]['original_value'] = $rows->total_amount;
                    $finalArra[$i]['net_val'] = $rows->net_value;
                    $finalArra[$i]['bank'] = '';
                    $finalArra[$i]['agency'] = '';
                    $finalArra[$i]['account'] = '';
                    if ($rows->bankDetails) {
                        $finalArra[$i]['bank'] = $rows->bankDetails->agency_name;
                        $finalArra[$i]['agency'] = $rows->bankDetails->agency_number;
                        $finalArra[$i]['account'] = $rows->bankDetails->account_number;
                    }
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $totaltranscount = $totaltranscount + 1;
                    $i++;
                }
                
                if (count($finalArra) > 0 && $rows->id != '') {
                    $finalArra[$i]['updated_at'] = 'Total';
                    $finalArra[$i]['id'] = $totaltranscount;
                    $finalArra[$i]['name'] = '';
                    $finalArra[$i]['unit'] = '';
                    $finalArra[$i]['cpnj'] = '';
                    $finalArra[$i]['payment_type'] = '';
                    $finalArra[$i]['bank_name'] = '';
                    $finalArra[$i]['original_value'] = $totalorigval;
                    $finalArra[$i]['net_val'] = $totalnetval;
                    $finalArra[$i]['bank'] = '';
                    $finalArra[$i]['agency'] = '';
                    $finalArra[$i]['account'] = '';
                /* Generate Csv File */
                    //$filename = public_path("").'\csv\file.csv';
                    $filename = 'file.csv';
                    $handle = fopen($filename, 'w+');
                    $data = array('Date', 'Transaction ID', 'Supplier', 'Unit', 'CNPJ', 'Type', 'Bank', 'Original Value', 'Net Value', 'Bank', 'Agency', 'Account');

                    fputcsv($handle, $data);

                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return response()->download($filename, 'daily_reports supplier ' . date("d-m-Y H:i") . '.csv', $headers);
                    
                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
            } else {
                Session::flash('flash_warning', __('Data is not Available!'));
                return redirect()->back();
            }
        }
        
        $suppliers = Supplier::orderBy('name','asc')->pluck('name', 'id')->prepend('Select Suppliers','');
        $user = User::find(auth()->user()->id);
        if ($user->roles[0]->name == 'SLO' || $user->roles[0]->name == 'SLA' || $user->roles[0]->name == 'CBC') {
            $suppliers = Supplier::leftJoin('supplier_user', 'supplier_user.supplier_id', '=', 'supplier.id')
                ->leftJoin('users', 'users.id', '=', 'supplier_user.user_id')
                ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('roles.name', $user->roles[0]->name == 'SLO')->where('users.id', '=', auth()->user()->id)->pluck('name', 'id')->prepend('Select Suppliers', '');
        }
        $start_date = date('01/m/Y',time());
        $end_date = date('t/m/Y');
        return view('admin.suppliers.reports.index', compact('suppliers', 'start_date', 'end_date'));
        exit;
    }

    public function monthlyFinancialCosts(Request $request){
        if ($request && $request->supplier_id != '' && $request->startdate != '' && $request->enddate != '') {
            
            $supplier_id = $request->get('supplier_id');
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
        //$start_date = date('Y-m-d',strtotime($request->startdate))  . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            
            $dailyupdate = Transaction::where('transactions.supplier_id', $supplier_id)
                ->whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')
                ->where('transactions.customer_status', '=', 'Approved')->orderBy('transactions.updated_at', 'asc')
                ->select(DB::raw("CONCAT_WS('-',MONTH(transactions.updated_at),YEAR(transactions.updated_at)) as monthyear"), DB::raw('COUNT(*) AS transaction_count,
                    SUM(`transactions`.`total_amount`) AS total_amount,
                    SUM(`transactions`.`net_value`) AS net_value,
                    SUM(`transactions`.`discount_value`) AS discount_value,
                    MONTH(`transactions`.`updated_at`) AS MONTH,
                    YEAR(`transactions`.`updated_at`) AS YEAR,
                    `transactions`.`id` AS `transaction_id`'))
                //->with('monthlyReports')
                ->groupBy('monthyear')
                ->get();
            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totalfinancialcost = 0;
            $totaltranscount = 0;
            if (count($dailyupdate) > 0) {
                foreach ($dailyupdate as $rows) {
                    $finalArra[$i]['updated_at'] = $rows->monthyear;
                    $finalArra[$i]['total_transaction'] = $rows->transaction_count;
                    $finalArra[$i]['original_value'] = $rows->total_amount;
                    $finalArra[$i]['net_val'] = $rows->net_value;
                    $finalArra[$i]['revenue'] = $rows->discount_value;
                    $totaltranscount = $totaltranscount + $rows->transaction_count;
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $totalfinancialcost = $totalfinancialcost + $rows->discount_value;
                    $i++;
                }
                if (count($finalArra) > 0) {
                    $finalArra[$i]['updated_at'] = 'Total';
                    $finalArra[$i]['total_transaction'] = $totaltranscount;
                    $finalArra[$i]['original_value'] = $totalorigval;
                    $finalArra[$i]['net_val'] = $totalnetval;
                    $finalArra[$i]['revenue'] = $totalfinancialcost;
                    $filename = 'file.csv';
                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    $handle = fopen($filename, 'w+');
                    $data = array('Month', 'Transactions', 'Original Value', 'Net Value', 'Revenue');
                    fputcsv($handle, $data);
                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);
                    return response()->download($filename, 'monthly_reports ' . date("d-m-Y H:i") . '.csv', $headers);
                    
                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
            } else {
                Session::flash('flash_warning', __('Data is not Available!'));
                return redirect()->back();
            }
                
        }
        $suppliers = Supplier::orderBy('name','asc')->pluck('name', 'id')->prepend('Select Suppliers', '');
        $user = User::find(auth()->user()->id);
        if ($user->roles[0]->name == 'SLO' || $user->roles[0]->name == 'SLA' || $user->roles[0]->name == 'CBC') {
            $suppliers = Supplier::leftJoin('supplier_user', 'supplier_user.supplier_id', '=', 'supplier.id')
                ->leftJoin('users', 'users.id', '=', 'supplier_user.user_id')
                ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('roles.name', $user->roles[0]->name == 'SLO')->where('users.id', '=', auth()->user()->id)->pluck('name', 'id')->prepend('Select Suppliers', '');
        }
        $start_date = date('01/m/Y', time());
        $end_date = date('t/m/Y');
        return view('admin.suppliers.reports.monthlyfinancialcosts', compact('suppliers', 'start_date', 'end_date'));
        exit;
    }

    public function supplierdailyreportswithcbuser(Request $request){
        
        if ($request && $request->supplier_id != '' && $request->startdate != '' && $request->enddate != '') {
            $supplier_id = $request->get('supplier_id');
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
        //$start_date = date('Y-m-d',strtotime($request->startdate))  . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $dailyupdate = Transaction::where('transactions.supplier_id', $supplier_id)
                ->whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')->where('transactions.customer_status', '=', 'Approved')->orderBy('transactions.updated_at', 'asc')
                ->with('supplierName', 'bankDetails', 'bankName', 'supplierunitName','getSupplierRoles')
                ->select('*', (DB::raw('DATE(transactions.updated_at) AS transaction_update_date')))
                ->where('transactions.type', '=', 'Anticipation')
                ->get();
            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totaltranscount = 0;
            if (count($dailyupdate) > 0) {
                foreach ($dailyupdate as $rows) {
                    $finalArra[$i]['updated_at'] = date('d-m-Y',strtotime($rows->transaction_update_date));
                    $finalArra[$i]['id'] = $rows->id;
                    $finalArra[$i]['name'] = $rows->supplierName->supplier_name;
                    $finalArra[$i]['unit'] = $rows->supplierunitName[0]->unit_name;
                    $finalArra[$i]['cpnj'] = $rows->supplierunitName[0]->cpnj;
                    $finalArra[$i]['payment_type'] = $rows->type;
                    $finalArra[$i]['bank_name'] = ($rows->bankName) ? $rows->bankName->name : '';
                    $finalArra[$i]['original_value'] = $rows->total_amount;
                    $finalArra[$i]['net_val'] = $rows->net_value;
                    $finalArra[$i]['bank'] = '';
                    $finalArra[$i]['agency'] = '';
                    $finalArra[$i]['account'] = '';
                    if ($rows->bankDetails) {
                        $finalArra[$i]['bank'] = $rows->bankDetails->agency_name;
                        $finalArra[$i]['agency'] = $rows->bankDetails->agency_number;
                        $finalArra[$i]['account'] = $rows->bankDetails->account_number;
                    }
                     $finalArra[$i]['cbuser'] = $rows->getSupplierRoles->user_name;
                    $totalorigval = $totalorigval + $rows->total_amount;
                    $totalnetval = $totalnetval + $rows->net_value;
                    $totaltranscount = $totaltranscount + 1;
                    $i++;
                }
                
                if (count($finalArra) > 0 && $rows->id != '') {
                    $finalArra[$i]['updated_at'] = 'Total';
                    $finalArra[$i]['id'] = $totaltranscount;
                    $finalArra[$i]['name'] = '';
                    $finalArra[$i]['unit'] = '';
                    $finalArra[$i]['cpnj'] = '';
                    $finalArra[$i]['payment_type'] = '';
                    $finalArra[$i]['bank_name'] = '';
                    $finalArra[$i]['original_value'] = $totalorigval;
                    $finalArra[$i]['net_val'] = $totalnetval;
                    $finalArra[$i]['bank'] = '';
                    $finalArra[$i]['agency'] = '';
                    $finalArra[$i]['account'] = '';
                    $finalArra[$i]['cbuser'] = '';
                /* Generate Csv File */
                    $filename = 'file.csv';
                    $handle = fopen($filename, 'w+');
                    $data = array('Date', 'Transaction ID', 'Supplier', 'Unit', 'CNPJ', 'Type', 'Bank', 'Original Value', 'Net Value', 'Bank', 'Agency', 'Account','CB User Name');

                    fputcsv($handle, $data);

                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return response()->download($filename, 'daily_reports suppliercbuser ' . date("d-m-Y H:i") . '.csv', $headers);
                    
                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
            } else {
                Session::flash('flash_warning', __('Data is not Available!'));
                return redirect()->back();
            }
        }
        
        $suppliers = Supplier::orderBy('name','asc')->orderBy('name','asc')->pluck('name', 'id')->prepend('Select Suppliers','');
        $user = User::find(auth()->user()->id);
        if ($user->roles[0]->name == 'SLO' || $user->roles[0]->name == 'SLA' || $user->roles[0]->name == 'CBC') {
            $suppliers = Supplier::leftJoin('supplier_user', 'supplier_user.supplier_id', '=', 'supplier.id')
                ->leftJoin('users', 'users.id', '=', 'supplier_user.user_id')
                ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('roles.name', $user->roles[0]->name == 'SLO')->where('users.id', '=', auth()->user()->id)->pluck('name', 'id')->prepend('Select Suppliers', '');
        }
        $start_date = date('01/m/Y',time());
        $end_date = date('t/m/Y');
        return view('admin.suppliers.reports.cbuser', compact('suppliers', 'start_date', 'end_date'));
        exit;
    }

     public function commercialusertransaction(Request $request){
        
        if ($request->startdate != '' && $request->enddate != '') {
            $startdate = explode('/', $request->startdate);
            $start_date = $startdate[2] . '-' . $startdate[1] . '-' . $startdate[0] . ' 00:00:00';
            $end_date = explode('/', $request->enddate);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0] . ' 23:59:59';
            $dailyupdate = Transaction::whereBetween('transactions.updated_at', [$start_date, $end_date])
                ->where('transactions.is_approve', '=', '1')->where('transactions.customer_status', '=', 'Approved')->orderBy('transactions.updated_at', 'asc')
                ->with('getSupplierRoles')
                -> join('customer','customer.id','=','transactions.customer_id')
                ->select('*', (DB::raw('DATE(transactions.updated_at) AS transaction_update_date,COUNT(transactions.id) AS transaction_count,SUM(transactions.total_amount) AS transaction_totalamount,SUM(transactions.net_value) AS transaction_netvalue,SUM(transactions.discount_rate) AS transaction_meanrate')))
                ->groupby('transactions.supplier_id')
                ->get();
            // dd($dailyupdate);
            $i = 0;
            $finalArra = array();
            $totalorigval = 0;
            $totalnetval = 0;
            $totaltranscount = 0;
            $totalmeanratecount=0;
            if (count($dailyupdate) > 0) {
                foreach ($dailyupdate as $rows) {
                    $finalArra[$i]['user_name'] = $rows->getSupplierRoles->user_name;
                    $finalArra[$i]['transaction_qty'] = $rows->transaction_count;
                    $finalArra[$i]['original_value'] = $rows->transaction_totalamount;
                    $finalArra[$i]['net_val'] = $rows->transaction_netvalue;
                    $finalArra[$i]['mean_rate'] = $rows->transaction_meanrate;
                    $finalArra[$i]['cb_revenue'] =$rows->transaction_totalamount*(1-$rows->rate);
                    $totaltranscount = $totaltranscount + $rows->transaction_count;
                    $totalorigval = $totalorigval + $rows->transaction_totalamount;
                    $totalnetval = $totalnetval + $rows->transaction_netvalue;
                    $totalmeanratecount = $totalmeanratecount + $rows->transaction_meanrate;
                   
                    $i++;
                }
                if (count($finalArra) > 0) {
                    // $finalArra[$i]['user_name'] = 'Total';
                    // $finalArra[$i]['transaction_qty'] = $totaltranscount;
                    // $finalArra[$i]['original_value'] = $totalorigval;
                    // $finalArra[$i]['net_val'] = $totalnetval;
                    // $finalArra[$i]['mean_rate'] = $totalmeanratecount;
                    //  $finalArra[$i]['cb_revenue'] ='';
                /* Generate Csv File */
                    $filename = 'file.csv';
                    $handle = fopen($filename, 'w+');
                    $data = array('User', 'Transaction qty', 'Original Value', 'Net Value', 'Mean Rate','CB Revenue Generated');

                    fputcsv($handle, $data);

                    foreach ($finalArra as $value) {
                        fputcsv($handle, $value);
                    }
                    fclose($handle);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );
                    return response()->download($filename, 'all_commercialuser transaction ' . date("d-m-Y H:i") . '.csv', $headers);
                    
                } else {
                    Session::flash('flash_warning', __('Data is not Available!'));
                    return redirect()->back();
                }
            } else {
                Session::flash('flash_warning', __('Data is not Available!'));
                return redirect()->back();
            }
        }
        
        $start_date = date('01/m/Y',time());
        $end_date = date('t/m/Y');
        return view('admin.suppliers.reports.commercialuserdaily_transaction', compact('start_date', 'end_date'));
        exit;
    }
    
}
