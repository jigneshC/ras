<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\TransactionApproved;
use App\Notifications\TransactionCancelled;
use \Crypt;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Customer;
use App\Supplier;
use App\SupplierUser;
use App\CustomerUser;
use App\CustomerSupplier;
use App\User;
use App\Role;
Use DB;
use Auth;
use Hash;
use App\Transaction;
use Illuminate\Http\Request;
use App\TransactionsInvoices;
use Yajra\Datatables\Datatables;
use Session;
use App\ActionLog;
use App\AvailableCashHistory;
use App\SupplierBank;
use App\BankCustomer;
use File;
use mPDF;

class TransactionsController extends Controller
{	
	 public function __construct()
    {   
        
        $this->middleware('permission:access.transaction');
      //  $this->middleware('permission:access.transaction.approve')->only(['approveTransaction','cancleTransaction']);
        $this->middleware('permission:access.transaction.create')->only(['create', 'store']);
        $this->mail_function = new EmailController();

        $this->mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $this->mPdf->SetDisplayMode('fullpage');
        $this->mPdf->list_indent_first_level = 0;
        
    } 

    public function save_pdf( $htmlview, $filename ){
        
        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename);

    }

    public function download_pdf( $htmlview, $filename ) {

        $pdf = PDF::loadHTML($htmlview);
        return $pdf->download($filename);
    }

    public function view_pdf( $htmlview, $filename ){

        $mPdf = new mPDF('utf-8', 'A4', '0', '0', 0, 0, 0, 0, 0, 0);
        $mPdf->SetDisplayMode('fullpage');
        $mPdf->list_indent_first_level = 0;
        $mPdf->WriteHTML($htmlview);
        return $mPdf->Output($filename, 'I');
    }

    public function index()
    {
        $id = 0;
        return view('admin.transactions.index',compact('id'));
    }

    public function datatable(Request $request){
        
        if($request->get('supplier') > 0){
            $transaction = Transaction::select(DB::raw('transactions.*,customer.name as customer_name,supplier.name as supplier_name,banks.name as bank_name'))
            ->with('total_invoice','requester','approver','bank')
                ->join('customer', 'customer.id', '=', 'transactions.customer_id')
                ->join('supplier','supplier.id','=','transactions.supplier_id')
                ->leftjoin('banks','banks.id','=','transactions.bank_id')
                ->where('transactions.supplier_id', $request->get('supplier'));
            return Datatables::of($transaction)
                ->make(true);
            exit;
        }else{
            $user = User::find(auth()->user()->id);
        }
        
        if($user->roles[0]->name =='SLO' || $user->roles[0]->name == 'SLA' || $user->roles[0]->name == 'CBC'){
            $user = User::with('supplier')->find(auth()->user()->id);
            if($user->supplier != ''){
                foreach($user->supplier as $supplier){
                    $supplier_id[] = $supplier->id;
                }
            }else{
                $supplier_id[] = '';
            }
            $transaction = Transaction::select(DB::raw('transactions.*,customer.name as customer_name,supplier.name as supplier_name,banks.name as bank_name'))
            ->with('total_invoice','requester','approver','bank')
                ->join('customer','customer.id','=','transactions.customer_id')
                ->join('supplier', 'supplier.id', '=', 'transactions.supplier_id')
                ->leftjoin('banks','banks.id','=','transactions.bank_id')
                ->whereIn('transactions.supplier_id',$supplier_id)->orderBy('transactions.created_at','desc');  
            return Datatables::of($transaction)
                ->make(true);
            exit;

        }elseif($user->roles[0]->name == 'SU' || $user->roles[0]->name == 'CBB' ){
                $role = Role::where("name", "SLO")->first();
                $supllierUsers = array();
                $transaction = array();
                foreach($role->users as $user){
                    $user = User::with('supplier')->find($user->id);
                    if($user->supplier != ''){
                        foreach($user->supplier as $supplier){
                            $supplier_id[] = $supplier->id;
                        }
                    }else{
                        $supplier_id[] = '';
                    }
                }
                        $transaction = Transaction::select(DB::raw('transactions.*,customer.name as customer_name,supplier.name as supplier_name,banks.name as bank_name'))
                        ->with('total_invoice','requester','approver','bank')
                        ->join('customer','customer.id','=','transactions.customer_id')
                        ->join('supplier', 'supplier.id', '=', 'transactions.supplier_id')
                        ->leftjoin('banks','banks.id','=','transactions.bank_id')
                        ->whereIn('transactions.supplier_id',$supplier_id)->orderBy('transactions.created_at','desc');
                        
                return Datatables::of($transaction)
                    ->make(true);
                exit;

            }elseif($user->roles[0]->name == 'CM' || $user->roles[0]->name =='CBS' ){
                $user = User::with('customer')->find(auth()->user()->id);
                if($user->customer != ''){
                    foreach($user->customer as $customer){
                        $customer_id[] = $customer->id;
                    }
                }else{
                    $customer_id[] = '';
                }
                
                $transaction = Transaction::select(DB::raw('transactions.*,supplier.name as supplier_name,customer.name as customer_name,banks.name as bank_name'))
                ->with('total_invoice','requester','approver','bank')
                ->join('customer','customer.id','=','transactions.customer_id')
                ->join('supplier','supplier.id','=','transactions.supplier_id')
                ->leftjoin('banks','banks.id','=','transactions.bank_id')
                ->whereIn('transactions.customer_id',$customer_id)->orderBy('transactions.created_at','desc');
 
                return Datatables::of($transaction)
                    ->make(true);
                exit;
            }
        
        exit;
    }

    public function supplierTransaction($id){

        return view('admin.transactions.index', compact('id'));
    }

    public function create()
    {
        return view('admin.transactions.create');
    }

    public function store(Request $request)
    {   
      
        if(!$request->ajax){
            
            $this->validate($request, [
                'customer_id' => 'required'
            ]);
            $requestData = $request->all();
            Transaction::create($requestData);
            Session::flash('flash_message', 'Transaction added!');
            return redirect('admin/transactions');
            exit;

        }else{
            
            $invoice_id = $request->invoice;
            $transaction = new Transaction;
            $transaction->customer_id = $request->company_id;
            $transaction->supplier_id = $request->supplier_id;
            $cus = $request->company_id;
            $sup = $request->supplier_id;
            if($request->negotiate_rate == null || $request->negotiate_rate == '0'){
                $transaction->discount_rate = $request->discount_rate;
                
            }else{
                $transaction->discount_rate = $request->negotiate_rate;
            }
            
            if($request->type == "Cession"){
                $transaction->bank_id = $request->bank_id ; 
                  $transaction->bank_rate = $request->bank_rate ; 
            }
            $transaction->type = $request->type;
            $transaction->total_amount = $request->total_amount;
            $transaction->discount_value = $request->discount;
            $transaction->discounted_value = $request->total_amount - $request->discount;
            if($request->discounted_value > 0){
                $transaction->discounted_value = $request->discounted_value;
            }
            
            
            $transaction->cost = $request->cost;
            $transaction->net_value = $request->net_value;
            $transaction->requester_id = auth()->user()->id;
            $transaction->payment_status = "Pending";
            $transaction->date_of_anticipation = date('Y-m-d',time());            

            if($transaction->save()){

            \ActionLog::addToLog("New transaction"," Transaction for Customer - " . $transaction->customer_id , $transaction->getTAble(), $transaction->id);

                $transactiId = $transaction->id;
                foreach($invoice_id as $id){
                    $invoice = Invoice::where('id',$id)->update(['discount_rate'=> $transaction->discount_rate, 'anticipation_date'=>date('Y-m-d'),'type'=> $request->type ]);
                    $invoice = Invoice::where('id', $id)->first();
                    $transactionInvoice = new TransactionsInvoices;
                    $transactionInvoice->transaction_number = $transactiId;
                    $transactionInvoice->invoice_id = $id;
                    $transactionInvoice->customer_id = $invoice->customer_id;
                    $transactionInvoice->supplier_id = $invoice->supplier_id;
                    $transactionInvoice->requester_id = auth()->user()->id;
                    $transactionInvoice->anticipation_date = $invoice->anticipation_date;
                    $transactionInvoice->payment_status = $invoice->status;
                    $transactionInvoice->payment_type = $request->type;
                    if($request->type == "Cession"){
                        $transactionInvoice->bank_id = $request->bank_id ;
                        //find bank contact & bank Account 
                        $bank_detail = BankCustomer::where('bank_id',$request->bank_id)->where('customer_id',$invoice->customer_id)->first();
                        if($bank_detail){
                            $transactionInvoice->bank_contacts_id =  $bank_detail->bank_contact;
                            $transactionInvoice->bank_accounts_id =  $bank_detail->bank_account_number;
                        } 

                    }

                    if($transactionInvoice->save()){
                        $updateInvoice = Invoice::find($id);
                        $updateInvoice->is_transaction = '1';
                        $updateInvoice->status = 'Used';
                        $updateInvoice->discount_rate = $transaction->discount_rate;
                        $updateInvoice->bank_rate = $transaction->bank_rate;
                        $updateInvoice->save();
                    }

                }
                
               
                // Debit the total amount ( Net Value ) of transaction when transaction is created
                    $history = new AvailableCashHistory;
                    $history->customer_id = $transaction->customer_id;
                    $history->supplier_id = $transaction->supplier_id;
                    $history->action = "debit";
                    $history->amount = -$transaction->net_value;
                    $history->date = date('Y-m-d');
                    if($transaction->type == "Cession"){
                        $history->bank_id = $transaction->bank_id ;
                    }
                    $history->transaction_id = $transaction->id;
                    $history->save();
                        

                $json['msg'] = 'success';
                $json['transaction_id'] = $transactiId;
            }else{
                $json['msg'] = 'error';
            }
            echo json_encode($json);exit;
        }
    }


    public function verifyuser(Request $request)
    {   
        if (Hash::check($request->input('password'), Auth::user()->password)){
            
            $code = 200 ; 
            $message = "" ;
            $data = $request->all();

        }else{

            $code = 400;
            $message = "You entered wrong Password";
            $data = "";
        }
        
        return response()->json(['messages' => $message,'data' => $data],$code);

    }
    public function show(Request $request,$id)
    {   
        
        $transaction =array();
        $user = array();
        $cession =array();
        $anticipation = array();
        $approver = array();
        $transaction = Transaction::with('supplier','approver','requester','canceller')->where('id',$id)->first();
        
        $bank = SupplierBank::select(
            DB::raw("CONCAT(bank_name,' - ',account_info) AS bank_name"),'id')->where('supplier_id','=',$transaction->supplier->id)->pluck('bank_name','id')->prepend('Select Bank');
        
        $supp_user = SupplierUser::where('supplier_id',$transaction->supplier->id)->pluck('user_id');

        $invoices = TransactionsInvoices::join('invoices','invoices.id','=','transactions_invoices.invoice_id')->where('transaction_number',$id)->get();
        $anticipation = array('total_amount'=>'0','type'=>'Anticipation');
        $cession = array('total_amount'=>'0','type'=>'Cession');


        foreach($invoices as $invoice){

            $transaction_number[] = $invoice->transaction_number;
            
            if($invoice->type == 'Anticipation'){

                $anticipation['total_amount'] = $anticipation['total_amount'] + $invoice->total_amount;
                
            }elseif($invoice->type == 'Cession'){

                $cession['total_amount'] = $cession['total_amount'] + $invoice->total_amount;
                
            }

        }
        
        //customer_status
        $trans = Transaction::whereIn('id',$transaction_number)->first();

        return view('admin.transactions.show', compact('transaction' ,'user','cession','anticipation','approver','status','trans','bank'));
    }
    
    public function approveTransaction(Request $request, $id){

        $trans = Transaction::find($id);
        dd($trans);
        $cession_contract_pdf = null; $comm_dis_letter = null;

        //check whether the Customer or Bank have enough money to approve the transaction
        if($trans->type == "Cession"){
            // get the cession availability of customer
            $cession_amount_availability =  AvailableCashHistory::where('customer_id',$trans->customer_id)
            ->where('bank_id',$trans->bank_id)
            ->select(DB::raw('sum(available_cash_history.amount) as cession'))
            ->whereNotNull('bank_id')
            ->whereNull('sales_user')
            ->groupBy('available_cash_history.customer_id')->get();
            $cession_amount = 0;
            if($cession_amount_availability){
                $cession_amount =  $cession_amount_availability[0]->cession ;
            }
        
            // get the amount which we debited from customer available cash history for this transaction
            $total_cession_amount = $cession_amount  + $trans->net_value ;
            if($total_cession_amount < $trans->net_value){

                Session::flash('flash_message', 'This Transaction could not be Approved. Please Create it Again');
                return redirect('admin/transactions/'.$id);
            }
            // Generate Cession Contract PDF: applies only if transaction is of Cession type
            $cession_contract_pdf = $this->cession_contract() ; 

        }
        if($trans->type == "Anticipation"){

             // get the anticipation availability of customer
             $anticipation_amount_availability =  Customer::with('CashAvailableAnticipation')->find($trans->customer_id);
             $anticipation_amount = 0;
             if($anticipation_amount_availability->CashAvailableAnticipation){
                 $anticipation_amount =  $anticipation_amount_availability->CashAvailableAnticipation[0]->anticipation ;
             }
             // get the amount which we debited from customer available cash history for this transaction
             $total_anticipation_amount = $anticipation_amount  + $trans->net_value ;
             if($total_anticipation_amount < $trans->net_value){
 
                 Session::flash('flash_message', 'This Transaction could not be Approved. Please Create it Again');
                 return redirect('admin/transactions/'.$id);
             }
        }
        //Generate  Commercial Discount letter PDF: applies for Anticipation and Cession transaction types.
        $comm_dis_letter = $this->comm_dis_letter() ;

        if(Auth::user()->role[0]->name == "CM"){

            $trans->customer_status = "Approved";
            $trans->save();
			$this->mail_function->sendTransactionApprovalAction($trans, $cession_contract_pdf, $comm_dis_letter);
        
        }else{

            $trans->is_approve = '1';
            $trans->approver_id = auth()->user()->id;
            $trans->payment_status = "Approved";
            $trans->approval_date = date('Y-m-d H:i:s');
            $trans->supp_bank = $request->bank_id ;
            $trans->save();
        }
        
            
            $transactions = TransactionsInvoices::where('transaction_number',$id)->get();
            foreach($transactions as $transaction){
                $transaction = TransactionsInvoices::find($transaction->id);
                $transaction->approver_id = auth()->user()->id;
                $transaction->save();
            }

            \ActionLog::addToLog("Transaction Approved"," Transaction approved by - " . auth()->user()->name , $trans->getTAble() , $trans->id);
            
            //If transaction is approved by SLA then supplier's SLO, CB-Commercial and Customer's CM, CB-Sales and admin will be notified
            if(\Auth::user()->roles[0]->name == "SLA"){
                $sup_user = SupplierUser::where('supplier_id',$trans->supplier_id)
                                        ->where('user_id','!=' , \Auth::user()->id)->get();
                
                $cust_user = CustomerUser::where('customer_id',$trans->customer_id)->get();

                foreach ($sup_user as $sup) {
                    $supp = $sup->user_id;
                    $supp_user = User::find($supp);
                    if($supp_user){
                        $supp_user->notify(new TransactionApproved($trans));
                    }
                    
                }
                foreach ($cust_user as $cust) {
                    $custt = $cust->user_id;
                    $custt_user = User::find($custt);
                    if($custt_user){
                        $custt_user->notify(new TransactionApproved($trans));
                    }
                    
                }

                $user = User::with('roles')->get(); 
                if(count($user) > 0){
                    foreach ($user as $users){
                        if(count($users->roles)){
                            if ($users->roles[0]->name == "SLO") {
                                $notify = User::find($users->id);
                                if($notify){
                                    $notify->notify(new TransactionApproved($trans));
                                }
                                
                            }
                        }
                    }
                }
            }
        
        Session::flash('flash_message', 'Transaction Approved!');
        return redirect('admin/transactions/'.$id);

    }

    public function cancleTransaction($id){
        $trans = Transaction::find($id);

        if(Auth::user()->role[0]->name == "CM"){
            $trans->customer_status = "Cancelled";
            $trans->save(); 
        }else{

            $trans->is_approve = '0';
            $trans->approver_id = '0';
            $trans->cancelled_by = Auth::user()->id;
            $trans->cancelled_date = date('Y-m-d H:i:s');
            $trans->payment_status = "Cancelled";
            $trans->save();
        }
        $transactions = TransactionsInvoices::where('transaction_number', $id)->get();

        foreach ($transactions as $transaction) {
            $transaction_invoice = TransactionsInvoices::find($transaction->id);
            $transaction_invoice->payment_status = 'cancelled';
            $transaction_invoice->approver_id = '0';
            $transaction_invoice->cancelled_by = Auth::user()->id;
            $transaction_invoice->save();
            $invoices = Invoice::find($transaction->invoice_id);
            $invoices->is_transaction = '0';
            $invoices->status = 'Available';
            $invoices->discount_rate = 0;
            $invoices->bank_rate = 0;
            $invoices->save();
        }
        \ActionLog::addToLog("Transaction Cancelled", " Transaction cancelled by - " . auth()->user()->name, $trans->getTAble(), $trans->id);

        //If transaction is cancelled by SLA then supplier's SLO, CB-Commercial and Customer's CM, CB-Sales and admin will be notified
        if(\Auth::user()->roles[0]->name == "SLA"){
            $sup_user = SupplierUser::where('supplier_id',$trans->supplier_id)
                                    ->where('user_id','!=' , \Auth::user()->id)->get();
            
            $cust_user = CustomerUser::where('customer_id',$trans->customer_id)->get();
            

            foreach ($sup_user as $sup) {
                $supp = $sup->user_id;
                $supp_user = User::find($supp);
                if($supp_user){
                    $supp_user->notify(new TransactionCancelled($trans));
                }
            }
            foreach ($cust_user as $cust) {
                $custt = $cust->user_id;
                $custt_user = User::find($custt);
                if($custt_user){
                    $custt_user->notify(new TransactionCancelled($trans));
                }
                
            }

            $user = User::with('roles')->get(); 
            foreach ($user as $users){
               if(count($users->roles) > 0 && $users->roles[0]->name == "SLO"){
                    $notify = User::find($users->id);
                    if($notify){
                        $notify->notify(new TransactionCancelled($trans));
                    }
                    
               }
            }
        }

             // Credit the total amount ( Net Value ) back to available cash history when transaction is cancelled
            $history = AvailableCashHistory::where('transaction_id' , $trans->id)->first();
            if($history){
                $history->delete();
            }
           
        Session::flash('flash_message', 'Transaction Cancelled!');
        return redirect('admin/transactions/'.$id);

    }

    public function invoiceData($id){
        
        $transaction = TransactionsInvoices::where('transaction_number',$id)->with('customer','invoice','transaction')->get();
        return Datatables::of($transaction)->make(true); 
        exit;
    }

    public function edit($id)
    {
        $transaction = Transaction::findOrFail($id);
        return view('admin.transactions.edit', compact('transaction'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
			'customer_id' => 'required' 
		]);
        $requestData = $request->all();
        
        $transaction = Transaction::findOrFail($id);
        $transaction->update($requestData);
                
        \ActionLog::addToLog("Transaction Update"," Transaction ID - " . $transaction->id ." is updtaed", $transaction->getTAble() , $transaction->id);
        Session::flash('flash_message', 'Transaction updated!');
        return redirect('admin/transactions');
    }

    public function destroy($id)
    {
        $trans = Transaction::find($id);
        \ActionLog::addToLog("Transaction deleted"," Transaction ID - " . $tran->id ." is deleted", $trans->getTAble() , $trans->id); 
        $trans->delete();
        Session::flash('flash_message', 'Transaction deleted!');
        return redirect('admin/transactions');
    }

    public function cession_contract(){

        //create pdf via bank html 

        /*
        supplier detail
            Supplier unit legal name
 		    Supplier unit CNPJ
 		    Address street, number – complemente
            Zip, city/statecode
            supplier approver name (user who approved transaction)
            supplier approver phone (user who approved transaction) 
            
        bank detail
            Bank legal name
            Bank CNPJ
            Address street, number – complemente
            Zip, city/statecode
            Bank_address_City, 

        transaction
            transaction_ID
            discounted_value 
            net_value
            date transaction is approved 

        bank contact detail
            b_bank_number
            bc_bank_agency_number
            bc_bank_account_number

        supplier bank details
            su_bank_code
            sb_bank_agency_number
            sb_bank_account_number


        */
    }

    public function comm_dis_letter(){

    }

}




