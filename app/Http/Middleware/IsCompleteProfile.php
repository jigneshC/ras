<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class IsCompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (!Auth::user()->password || Auth::user()->password=="" || Auth::user()->password==null)
       {
            return redirect('admin/profile/change-password');
       }

       return $next($request);
    }
}
