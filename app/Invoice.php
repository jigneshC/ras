<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //Tablename
    protected $table = "invoices";
    //primary key
    public $primaryKey = "id";

    public function customerinvoice(){
        $this->belongsToMany('App\Invoice','customer_id','customer_id');
    }

    public function supplier(){
    	return $this->belongsTo('App\Supplier','supplier_id');
    }
    public function supplier_unit(){
        return $this->belongsTo('App\SupplierUnit','supplier_unit_id');
    }

    public function totalAnticipastion()
    {
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum'))
            ->where('type', 'Anticipation');
    }
    
    public function discountRateClass(){
        return $this->belongsTo('App\CustomerSupplier','customer_id','customer_id')->with('discountRate');
    }

    public function discountRateClassCustomer(){
        return $this->belongsTo('App\CustomerSupplier', 'customer_id', 'customer_id')->with('discountRateCustomer');
    }
    public function bank_cost(){
        return  $this->hasMany('App\BankCustomerMoney','customer_id','customer_id')->orderBy('bank_rate','asc');
    }
    
    public function transaction(){
        return $this->hasOne('App\TransactionsInvoices','invoice_id','id');
    }
   
    
}
