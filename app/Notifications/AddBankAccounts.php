<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddBankAccounts extends Notification
{
    use Queueable;

    public $bank_acc;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bank_account)
    {
        //
        $this->bank_acc = $bank_account;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'bank_id' => $this->bank_acc->bank_id,
            'bank_name' => $this->bank_acc->bank->name,
            'bank_branch' => $this->bank_acc->bank->branch,
            'bank_account_id' => $this->bank_acc->id,
            'bank_account_number' => $this->bank_acc->account_number,
            'added_by' => \Auth::user()->name,

        ];
    }
}
