<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddBankContacts extends Notification
{
    use Queueable;

    public $bank_contacts; 

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bank_contact)
    {
        //
        $this->bank_contacts = $bank_contact;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'bank_id' => $this->bank_contacts->bank->id,
            'bank_name' => $this->bank_contacts->bank->name,
            'bank_branch' => $this->bank_contacts->bank->branch,
            'bank_contact_id' => $this->bank_contacts->id,
            'bank_contact_name' => $this->bank_contacts->name,
        ];
    }
}
