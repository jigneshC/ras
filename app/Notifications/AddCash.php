<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddCash extends Notification
{
    use Queueable;

    public $cashadd;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($cashAdd)
    {
        //
        $this->cashadd = $cashAdd;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'customer_id' => $this->cashadd->customer_id,
            'cash' => $this->cashadd->cash,
            'customer_name' => $this->cashadd->customer->name,
            'added_by' => \Auth::user()->name,
        ];
    }
}
