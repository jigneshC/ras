<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ChangeCBCommercial extends Notification
{
    use Queueable;

    public $commercial;
    public $old_commercial;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($supplier_users,$old_user)
    {
        //
        $this->commercial = $supplier_users;
        $this->old_commercial = $old_user;

     }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            //
            'supplier_id' => $this->commercial->supplier_id,
            'new_commercial_user_id' => $this->commercial->user_id,
            'new_commercial_user_name' => $this->commercial->user->name,
            'supplier_name' => $this->commercial->supplier->name,
            'changed_by' => \Auth::user()->name,
            'old_commercial_user_id' => $this->old_commercial->id,
            'old_commercial_user_name' => $this->old_commercial->name,

        ];
    }
}
