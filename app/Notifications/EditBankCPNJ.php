<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EditBankCPNJ extends Notification
{
    use Queueable;

    public $banks;

    public $old_cpnj; 

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bank,$old_cpnj)
    {
        //
        $this->banks = $bank;
        $this->old_cpnj = $old_cpnj;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'bank_id' => $this->banks->id,
            'bank_name' => $this->banks->name,
            'old_cpnj' => $this->old_cpnj,
            'new_cpnj' => $this->banks->cpnj,
            'changed_by' =>\Auth::user()->name
        ];
    }
}
