<?php

namespace App\Providers;

use App\WebSite;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Schema;
use App\Language;

class AppServiceProvider extends ServiceProvider
{

    protected static $_websites = null;

    protected static $_websites_pluck = null;

    protected static $_lang = null;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Switch case directive
        Blade::extend(function($value, $compiler){
            $value = preg_replace('/(\s*)@switch\((.*)\)(?=\s)/', '$1<?php switch($2):', $value);     
            $value = preg_replace('/(\s*)@endswitch(?=\s)/', '$1endswitch; ?>', $value);
            $value = preg_replace('/(\s*)@case\((.*)\)(?=\s)/', '$1case $2: ?>', $value);
            $value = preg_replace('/(?<=\s)@default(?=\s)/', 'default: ?>', $value);
            $value = preg_replace('/(?<=\s)@breakswitch(?=\s)/', '<?php break;', $value);
            return $value;
        });

        $_lang = 'en'; //default language;
        self::$_lang = Language::where('active',1)->get();

        view()->composer('*', function ($view) use ($_lang) {
            
            $view->with('_lang', self::$_lang);
        });



    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
