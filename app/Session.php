<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Session extends Model
{
    protected $tabel = "sessions";

    public $primaryKey = "id";

    
	public function user()
    {
        return $this->hasOne('App\User','id');
    }
     

}
