<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supplier';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','typeofsupplier','website','cb_rate_class','image','created_at','updated_at','deleted_at'];

    public function address(){

       return $this->hasOne('App\Address','field_id');
    }
/*
    public function customersupplier(){
        return $this->belongsToMany('App\CustomerSupplier','supplier_id');
    }
    */

    public function customer(){
        return $this->belongsToMany('App\Customer','customer_supplier','supplier_id','customer_id');
    }

    public function user(){
        return $this->belongsToMany('App\User','supplier_user','supplier_id','user_id');
    }

    public function invoiceCession()
    {
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum'))
            ->where('type', 'Cession')
            ->whereNotIn('transactions_invoices.payment_status',array('cancelled'))
            ->whereRaw('invoices.created_at > DATE_SUB(now(), INTERVAL 1 MONTH)')
            ->groupBy('invoices.supplier_id');
    }

    public function invoiceAnticipastion()
    {
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum'))
            ->where('type', 'Anticipation')
            ->whereRaw('invoices.created_at > DATE_SUB(now(), INTERVAL 1 MONTH)')
            ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
            ->groupBy('invoices.supplier_id');
    }

    public function invoicedueSupplier()
    {
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')
		->select(DB::raw('sum(invoices.total_amount) as invoicesum'))
            ->where('approver_id', '0')
            ->whereNotIn('transactions_invoices.payment_status', array('cancelled'))
            ->groupBy('invoices.supplier_id');
    }

    public function role(){
        return $this->belongsToMany('App\Role','role_user','role_id','user_id');
    }

    public function commercialusers(){
        return $this->belongsToMany('App\User','supplier_user','supplier_id','user_id');
    }
	
	public function summary_invoice()
    {
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')
				->select(DB::raw('count(invoices.id) as total_invoice'),DB::raw('sum(invoices.total_amount) as invoicesum'))
                ->where('transactions_invoices.payment_status','!=','cancelled')
                ->where('transactions_invoices.approver_id','!=',0)
                ->groupBy('invoices.supplier_id'); 
	}
    public function summary_anticipation(){

        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')
				->select(DB::raw('sum(invoices.total_amount) as anticipation'))
				->where('type','Anticipation')
                ->where('transactions_invoices.payment_status','!=','cancelled')
				->where('transactions_invoices.approver_id','!=',0);
            
    }
    public function summary_cession(){

        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')
				->select(DB::raw('sum(invoices.total_amount) as cession'))
				->where('type','Cession')
                ->where('transactions_invoices.payment_status','!=','cancelled')
				->where('transactions_invoices.approver_id','!=',0);
            
    }
    public function cbuser(){

        return $this->belongsToMany('App\User','supplier_user','supplier_id','user_id')->select('users.name as user_name')
            ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', '=', 'CBC');
    }

    public function rate(){

         return $this->belongsTo('App\CustomerSupplier','supplier_id')->with('discountRate');
    }
    //In related Suppliers - CMPJ will be first supplier-unit's CNPJ 
    public function unit(){
        return $this->hasOne('App\SupplierUnit','supplier_id');
    }

    //units of suppliers
    public function units(){
        return $this->hasMany('App\SupplierUnit','supplier_id')->where('supplier_unit.status','active');
    }

    public function cb_rate_class(){

        return $this->belongsTo('App\DiscountRate','cb_rate_class','id');
    }
    

}
