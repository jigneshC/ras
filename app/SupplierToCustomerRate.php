<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierToCustomerRate extends Model
{
    //Tablename
    protected $table = "customer_supplier_rate";
    //primary key
    public $primaryKey = "id";


    public function supplier(){
    	return $this->hasMany('App\Supplier','supplier_id');
    }

    public function discount_rate(){

    	return $this->belongsTo('App\DiscountRate','rate_class');
    }
}
