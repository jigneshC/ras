<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;

class SupplierUnit extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supplier_unit';
    
        /**
        * The database primary key value.
        *
        * @var string
        */
        protected $primaryKey = 'id';
    
        /**
         * Attributes that should be mass-assignable.
         *
         * @var array
         */
        protected $fillable = ['supplier_id','unit_name','unit_legal_name','email','cpnj','neighborhood','city','state','country','address_number','zip','status','address_street','complement'];

        public function city(){
            return $this->belongsTo('App\City','city');
        }

        public function state(){
            return $this->belongsTo('App\State','state');
        }
}
