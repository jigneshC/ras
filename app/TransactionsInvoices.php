<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TransactionsInvoices extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions_invoices';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    public function customer(){
       return $this->belongsTo('App\Customer');
    }

    public function invoice(){
        return $this->belongsTo('App\Invoice','invoice_id')->with('supplier_unit');
    } 

    public function anticipationAmount(){
        
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'transaction_number', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum ,count(invoices.id) as anticipation_invoice','transactions_invoices.supplier_id') )
            ->where('type', 'Anticipation')
            ->where('transactions_invoices.approver_id', '!=', '0')
            ->groupBy('transactions_invoices.supplier_id');
    }

    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }

    public function transaction(){
        return $this->hasOne('App\Transaction','id','transaction_number');
    }
    
}
