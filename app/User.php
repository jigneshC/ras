<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HasRoles;
use App\Traits\BaseModelTrait;
use DB;
class User extends Authenticatable
{


    //only for user model
    public $is_user = true;

    use Notifiable, HasRoles;


    use  BaseModelTrait;

    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','cpnj','mobile','phone','comments','user_status','dob','deleted_at','remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by'
    ];

    public function role()
    {
        return $this->belongsToMany('App\Role','role_user','user_id','role_id');
    }

    public function supplier(){
        return $this->belongsToMany('App\Supplier','supplier_user','user_id','supplier_id');
    }

    public function customer()
    {
        return $this->belongsToMany('App\Customer','customer_user','user_id','customer_id');
    }

    public function session(){
        return $this->hasOne('App\Session','user_id','id');
    }

    public function transaction(){

        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'supplier_id', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum'))
            ->where('transactions_invoices.approver_id', '!=' , 0)
            ->groupBy('invoices.customer_id');

    }

    public function supplierCount(){
        return $this->hasMany('App\SupplierUser');
    }

    public function customerCount()
    {
        return $this->hasMany('App\CustomerUser');
    }
}
