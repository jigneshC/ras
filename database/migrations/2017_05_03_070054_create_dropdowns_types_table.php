<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDropdownsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
//
//Table DropdownsTypes
//
//ID -> Integer Auto Numbered
//Name -> Varchar(50)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)
//Apr 30 at 7:23 AM - Reply
//Pascal Hartmann (pascalhartmann1)
//Pascal Hartmann
//Table Dropdowns


    public function up()
    {
        Schema::create('dropdowns_types', function (Blueprint $table) {
            $table->increments('id');


            $table->string('name', 50);
            $table->boolean('active')->default(false);

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dropdowns_types');
    }
}
