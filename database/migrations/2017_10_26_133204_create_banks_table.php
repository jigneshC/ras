<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',55);
            $table->string('legal_name',55);
            $table->string('cpnj',55);
            $table->string('branch',55);

            $table->string('email',55);
            $table->string('phone',55);
            $table->string('mobile',55);
            $table->string('address_line1',155);
            $table->string('address_line2',155);
            $table->string('country_name',55);
            $table->string('city',55);
            $table->string('state',55);
            $table->string('zip',55);

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
