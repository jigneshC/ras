<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankCustomerFundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_customer_allocated_money', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned();
            $table->integer('customer_id')->unsigned();


            $table->float('total_amount')->default(0);
            $table->date('date')->nullable();
            $table->date('duedate')->nullable();

            $table->string('type')->default(0);

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_customer_allocated_money');
    }
}
