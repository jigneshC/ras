<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id');
            $table->string('name',100)->default(null);
            $table->string('phone',55)->default(null);
            $table->string('mobile',55)->default(null);
            $table->string('email',100)->default(null);

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);
            $table->softDeletes();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_contacts');
    }
}
