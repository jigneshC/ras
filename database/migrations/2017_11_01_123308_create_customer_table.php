<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name',100)->default(null);
            $table->string('cpnj',55);


            $table->string('email',55);
            $table->string('phone',55);
            $table->string('mobile',55);  
            $table->string('address_line1',155); //will be used as Address Number
            $table->string('address_line2',155); // will be used as Address StreetName
            $table->string('country_name',55);
            $table->string('city',55);
            $table->string('state',55);
            $table->string('zip',55);

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);
           // $table->string('deleted_at','2')->nullable()->default(0);

           $table->softDeletes();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
