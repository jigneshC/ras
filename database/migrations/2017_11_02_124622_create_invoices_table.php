<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->comment = "customer id";
            $table->integer('supplier_id')->unsigned()->comment = "supplier id";

            $table->date('duedate')->nullable()->comment = "due date";
            $table->date('anticipation_date')->nullable()->comment = "anticipation_date";
            $table->float('total_amount')->default(0);
            $table->float('discount_rate')->default(0);
            $table->float('discounted_value')->default(0);
            $table->float('cost')->default(0);
            $table->string('type')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
