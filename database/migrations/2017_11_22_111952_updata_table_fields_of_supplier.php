<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdataTableFieldsOfSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->dropColumn('mobile');
            $table->string('address_complement',155);
            $table->string('neighborhood',155);
            $table->string('website',155);
            $table->string('legalname', 155);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier', function (Blueprint $table) {
            $table->dropColumn('address_complement');
            $table->dropColumn('neighborhood');
            $table->dropColumn('website');
            $table->dropColumn('legalname');
        });
    }
}
