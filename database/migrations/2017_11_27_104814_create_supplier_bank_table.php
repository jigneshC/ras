<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_bank', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('supplier_id');
            $table->string('bank_name');
            $table->string('bank_number');
            $table->string('agency_name');
            $table->string('agency_number');
            $table->string('account_number');
            $table->string('account_info');
            $table->string('info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_bank');
    }
}
