<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvilableCashHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_cash_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable(); // customer id
            $table->string('company')->nullable(); //customer name
            $table->integer('bank_id')->nullable(); // bank id
            $table->integer('supplier_id')->nullable(); // supplier id
            $table->string('sales_user')->nullable();
            $table->string('action')->nullbale(); //credit, debit
            $table->string('date')->nullbale();
            $table->float('amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_cash_history');
    }
}
