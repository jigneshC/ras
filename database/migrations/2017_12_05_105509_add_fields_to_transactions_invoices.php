<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTransactionsInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions_invoices', function (Blueprint $table) {
            $table->string('negotiate_rate')->nullable()->default(0);
            $table->string('rate_class')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions_invoices', function (Blueprint $table) {
            $table->dropColumn('negotiate_rate')->nullable()->default(0);
            $table->dropColumn('rate_class')->nullable()->default(0);
        });
    }
}
