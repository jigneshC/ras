<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankCostField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_customer_allocated_money', function (Blueprint $table) {
            //
            $table->string('bank_rate')->default('null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_customer_allocated_money', function (Blueprint $table) {
            //
            $table->dropColumn('bank_rate');
        });
    }
}
