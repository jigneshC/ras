<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBankCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_customer', function (Blueprint $table) {
            //
            $table->string('bank_account_number')->nullable();
            $table->string('bank_contact')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_customer', function (Blueprint $table) {
            //
            $table->dropColumn('bank_account_number');
            $table->dropColumn('bank_contact');
        });
    }
}
