<?php

return [


    'label'=>[
        'select_from_dropdown'=>'select..',
        'add_new'=>'Add New',
        'mark'=>'Mark',
        'action'=>'Actions',
        'cancel'=>'Cancel',
        'save'=>'Save',
        'update'=>'Update',
        'submit'=>'Submit',
        'create'=>'Create',
        'status'=>'Status',
        'edit'=>'Edit',
        'back'=>'Back',
        'edit'=>'Edit',
        'delete'=>'Delete',
        'created'=>'Created',
        'select_subject' => "Select subject",
        'select_site' => "Select site",
    ],
    'datatable' =>[
        'search' => 'Search',
        'show' => 'Show',
        'entries' => 'Entries',
        'showing' => 'Showing', // Showing _START_ to _END_ of _TOTAL_ entries
        'to' => 'to',
        'of' => 'of',
        'small_entries' => 'entries',
        'paginate' => [
            'next' => 'Next',
            'previous' => 'Previous',
            'first'=>'First',
            'last'=>'Last',
        ],

    ],
    'daterange' =>[
        'all'=>'All',
        'today'=>'Today',
        'yesterday'=>'Yesterday',
        'last7day'=>'Last 7 Days',
        'last30day'=>'Last 30 Days',
        'thismonth'=>'This Month',
        'lastmonth'=>'Last Month',
        'thisyear'=>'This Year',
        'lastyear'=>'Last Year',
        'customeRange'=>'Custome Range',
        'applyBtn'=>'Apply',
        'cancelBtn'=>'Cancle',
    ],
    'responce_msg' =>[
        'something_went_wrong'=>'Something went wrong , Please try again later.',
        'you_have_no_permision_to_delete_record'=>'You have not permission to delete this Record',
        'record_deleted_succes'=>'Record deleted Success'
    ],
    'js_msg' =>[
        'confirm_for_delete'=>'Are You To Sure Delete :item_name ?',
    ]

];
