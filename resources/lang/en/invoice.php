<?php

return[ 

	'create_new_invoices' => 'Create New Invoices',
	'back' => 'Back',
	'upload_file' => 'Upload File',
	'invoices' => 'Invoices',
	'add_new_invoice_csv' => 'Add New Invoice CSV',
	'due_date' => 'Due Date',
	'to' => 'To',
	'days_upto_due_date' => 'Days Upto Due date',
	'company' => 'Comapny',
	'supplier' => 'Supplier',
	'minimum' => 'Minimum',
	'maximum' => 'Maximum',
	'available_cash' => 'Available Cash',
	'negotiated_rate' => 'Negotiated Rate',
	'type' => 'Type',
	'amount_to_anticipate' => 'Amount To Anticipate',
	'mean_rate' => 'Mean Rate',
	'discount' => 'Discount',
	'costs' => 'Costs',
	'net_value' => 'Net Value',
	'execute' => 'Execute',
	'id' => 'ID',
	'invoice_number' => 'Invoice Number',
	'supplier_unit' => 'Supplier Unit',
	'customer' => 'Customer',
	'original_due_date' => 'Original Due Date',
	'date_of_anticipation' => 'Date Of Anticipation',
	'original_value' => 'Original Value',
	'discount_rate' => 'Discount Rate',
	'discounted_value' => 'Discounted Value',
	'status' => 'Status',
	'discount_value' => 'Discount Value',
	'total_amount' => 'Total Amount',
	'filter_by_status' => 'Filter By Status',
	'samplecsv' => 'Download Sample Csv',
	'bank_name'=> 'Bank Name'


];