<?php
return [

        'customer_reports' => 'Customer Reports(Daily)',
        'create' => 'Submit',
        'to' => 'To',
        'due_date' => 'Due Date',
        'active' => 'Active',
        'customer_name' => 'Customer',
        'customer_reports_cbuser' => 'Customer Reports With CB Users (Daily)',
        'connectbahn_reports'=>'ConnectBahn revenue Reports (Monthly Due Value)',


];