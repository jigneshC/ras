<?php

return[

	'recievable_summary' => 'Receivable Summary',
	'supplier' => 'Supplier',
	'total_invoices' => 'Total Invoices',
	'total_amount' => 'Total Amount',
	'anticipated_amount' => 'Anticiapted Amount',
	'available_for_anticipation' => 'Available For Anticipation',
	'mean_rate' => 'Mean rate',
	'minimum_rate' => 'Minimum Rate',
	'maximum_rate' => 'Maximum Rate',
	'operation_user' => 'Operation User',
	'back' => 'Back',
	'goals' => 'Goals',
	'performed' => 'Performed',
	'percentage' => 'Percentage',
	'period' => 'Period',
	'from' => 'From',
	'to' => 'To',
	'show' => 'Show',
	'search_by_supplier' => 'Search by Supplier',


];