<?php

return[

	'view' =>'Voir',
	'edit' => 'Modifier',
	'delete' => 'Delete',
	'back' => 'Retour',
	'action_log' => 'Action Log',
	'actioner_id' => 'ID Actioner',
	'actioner_name' => "Nom de l'Actioner",
	'actioner_role' => 'Rôle Actioner',
	'action' => 'Action',
	'detail' => 'Détail',
	'role' => 'Rôle',
	'table_name' => 'Nom de table',
	'field_id' => 'Id Champ',
	'url' => 'URL',
	'ip' => 'IP',
	'id' => 'ID',
	'date' => 'Date',


];