<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'status_dropdown' => [
        'new' => 'નવું',
        'open' => 'ખોલો',
        'pending' => 'બાકી',
        'on-hold' => 'હોલ્ડ પર',
        'solved' => 'હલ',
        'closed' => 'બંધ',
    ],
    'label'=>[
        'dashboard' => 'ડેશબોર્ડ',
        'all_tickets' => 'બધા ટિકિટ્સ',
    ],

];
