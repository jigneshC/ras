<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Essas credenciais nÃ£o correspondem aos nossos registros.',
  'throttle' => 'Entre em contato com o suporte para desbloquear ou clique em Esqueceu sua senha para criar uma nova senha.',

];
