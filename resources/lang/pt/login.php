<?php

return [


    'label'=>[
        'e_mail' => 'O email',
        'password' => 'Senha',
        'remember' => 'Lembre-se',
        'submit' => 'Enviar',
        'sign_up' => 'Inscrever-se',
        'register' => 'registo',
        'log_in' => 'Entrar',
        'remember_me' => 'Lembre de mim',
        'sign_in' => 'Assinar em',
        'forgot_your_password' => 'Esqueceu sua senha',

    ]

];