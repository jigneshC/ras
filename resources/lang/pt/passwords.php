<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'As senhas devem ter pelo menos seis caracteres e corresponder Ã  confirmaÃ§Ã£o.',
    'reset' => 'Sua senha foi alterada!',
    'sent' => 'Enviamos um e-mail para o seu link de redefiniÃ§Ã£o de senha!',
    'token' => 'Este token de redefiniÃ§Ã£o de senha Ã© invÃ¡lido.',
    'user' => 'NÃ£o conseguimos encontrar um usuÃ¡rio com esse endereÃ§o de e-mail.',

];
