<?php

return [
	'my_profile' => 'Meu perfil',
	'edit_profile' => 'Editar Perfil',
	'change_password' => 'Mudar senha',
	'name' => 'Nome',
	'email' => 'O email',
	'language' => 'LÃ­ngua',
	'joined' => 'Ingressou',
	'back' => 'Costas',
	'current_password' => 'senha atual',
	'password' => 'Senha',
	'password_confirmation' => 'ConfirmaÃ‡Ã£o Da Senha',
	'update_profile' => 'Atualizar perfil',

];