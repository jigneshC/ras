<?php 

return [

	'supplier' => 'Fornecedor',
	'add_new' => 'Adicionar novo',
	'id' => 'identidade',
	'name' => 'Nome',
	'email' => 'O email',
	'phone' => 'telefone',
	'mobile' => 'MÃ³vel',
	'cpnj' => 'CNPJ',
	'address' => 'EndereÃ§o',
	'city' => 'Cidade',
	'state' => 'Estado',
	'country' => 'PaÃ­s',
	'zip' => 'Fecho eclair',
	'operator' => 'Operador',
	'suppliers' => 'Fornecedores',
	'actions' => 'AÃ§Ãµes',
	'edit' => 'Editar',
	'view' => 'VisÃ£o',
	'delete' => 'Excluir',
	'create_customer' => 'Criar cliente',
	'create_new_supplier' => 'Criar novo fornecedor',
	'back' => 'Costas',
	'edit_supplier' => 'Editar fornecedor',
	'approver' => 'Aprovador',
	'add_supplier' => 'Adicionar fornecedor',
	'close' => 'Fechar',
	'commercial' => 'UsuÃ¡rio Comercial',
	'recievable' => 'Total de RecebÃ­veis',
	'anticipationvalue' => 'AntecipaÃ§Ã£o significa valor',
	'cessionvalue' => 'Cessions mean value',
	'legal' => 'Nome legal',
	'website' => 'Local na rede Internet',
	'address_street' => 'EndereÃ§o Rua',
	'address_number' => 'NÃºmero do endereÃ§o',
	'address_complement' => 'Complemento de endereÃ§o',
	'neighborhood' => 'Bairro',
	'transaction' => 'Verificar transaÃ§Ã£o',
	'type' => 'Tipo',


	'cbuser' => 'Cb Commercial user',
	'manage_suppliers' => 'Gerenciar fornecedores',
	'supplier_name' => 'Nome do Fornecedor',
	'total_amount_of_receivables' => 'Valor Total de RecebÃ­veis',
	'anticipated_by_company' => 'Anticiada pela empresa',
	'cession_to_banks' => 'CessÃ£o BancÃ¡ria',
	'commercial_user' => 'UsuÃ¡rio Comercial',
	'user_who_made_change' => 'UsuÃ¡rio que fez a mudanÃ§a',
	'date' => 'Encontro',
	'suppliers_bank' => 'Banco do fornecedor',
	'bank_id' => 'ID do banco',
	'bank_name' => 'Nome do banco',
	'bank_number' => 'NÃºmero do banco',
	'agency_name' => 'Nome da AgÃªncia',
	'agency_number' => 'NÃºmero da agÃªncia',
	'account_number' => 'NÃºmero da conta',
	'account_info' => 'InformaÃ§Ãµes da Conta',
	'information' => 'Em formaÃ§Ã£o',
	'bank' => 'Banco',
	'suppliers_log' => 'Log do fornecedor',
	'customer' => 'clientes',
	'transaction_history_per_supplier' => 'HistÃ³rico de transaÃ§Ãµes por fornecedor',
	'total_invoices_amount' => 'Valor Total da Fatura',
	'total_financial_cost' => 'Custo Financeiro Total',
	'cessions_cost' => 'Custos de CessÃµes',
	'anticipation_cost' => 'Custos de AntecipaÃ§Ã£o',
	'anticipated_amount' => 'Valor Antecipado',
	'transaction_qty' => 'Quantia de transaÃ§Ã£o',
	'cession_amount' => 'Valor da cessÃ£o',
	'cession_transaction' => 'TransaÃ§Ã£o de cessÃ£o',
	'mean_rate' => 'Taxa MÃ©dia',
	'image' => 'Imagem',
	'main_contact' => 'Contato principal',
	'to' => 'Para',
	'search' => 'Pesquisa',
	'period' => 'PerÃ­odo',
	'supplier_image' => 'Imagem do fornecedor',
	'add_supplier_bank_account' => 'Adicionar conta bancÃ¡ria do fornecedor',
	'logs' => 'Logs',


	'cash_made_available_for_anticipation' => 'Dinheiro disponibilizado para a antecipaÃ§Ã£o',
	'anticipations_done' => 'AntecipaÃ§Ãµes ConcluÃ­das',
	'anticipations_to_be_paid' => 'AntecipaÃ§Ãµes a Pagar',
	'cash_available' => 'Dinheiro disponÃ­vel',
	'date_limit' => 'Limite de data',
	'add_bank_details' => 'Adicionar detalhes do banco',
	'create' => 'Crio',
	'change_cb_rep' => 'Alterar CB-Rep',
	'cb_representative' => 'Representante do CB',
	'change' => 'mudanÃ§a',
	'users' => 'Comercial',
	'transactionHistory' => 'HistÃ³rico de TransaÃ§Ãµes',
	'total_amount_of_receivables' => 'Valor Total de RecebÃ­veis',
	'anticipations_last_30_days' => 'AntecipaÃ§Ãµes Ãšltimos 30 Dias',
	'cessions_last_30_days' => 'CessÃµes Ãšltimos 30 Dias',
	'cb_rate_class' => 'Classe de Taxa CB',
	'supplier_name' => 'Nome do Fornecedor',
	'accounts' => 'Contas',
	'status' => 'Status',
	'supplier_unit' => 'Unidade Fornecedor',
	'unit_name' => 'Nome da unidade',
	'unit_legal_name' => 'Nome legal da unidade',
	'create_new_supplier_unit' => 'Criar nova unidade de fornecedor',
	'units' => 'Unidades',
	'add_new_supplier_unit' => 'Adicionar nova unidade de fornecedor',
	'cb_rep' => 'CB Rep',
	'edit_supplier_unit' => 'Editar unidade de fornecedor',
	'complement' => 'Complemento',
	'active' => 'Ativo',
	'inactive' => 'Inativo',
	'supplier_group_name' => 'Nome do Grupo de Fornecedores',
	'add_unit_details' => 'Adicionar detalhes da unidade',
	'supplierReportsDaily' => 'RelatÃ³rios de Fornecedor - Diariamente',
	'supplierReportsMonthly' => 'RelatÃ³rios de Fornecedor - Custo Financeiro Mensal',
	'start_date' => 'Data de inÃ­cio',
	'end_date' => 'Data final',
	'periode' => 'Periodo',
	'supplierReportsDailyCBuser' => 'RelatÃ³rios de fornecedores com usuÃ¡rio CB - diÃ¡rio',
	'suppliertransactionReportsDaily' => 'Todos os relatÃ³rios de transaÃ§Ã£o de fornecedores',
	'list_of_units_from_supplier' => 'Lista de unidades do fornecedor',
	'main_contact_at_cb' => 'Contato Principal no CB',
	'transaction_summary_per_customer' => 'Resumo de transaÃ§Ã£o por cliente',
	'anticipations_amount' => 'Quantia de AntecipaÃ§Ãµes',
	'anticipation_transaction' => 'TransaÃ§Ã£o de AntecipaÃ§Ã£o',
	

];

 ?>