<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_user' => 'Adicionar usuÃ¡rio',
    'add_new_user' => 'Adicionar novo usuÃ¡rio',
    'back' => 'Costas',
    'edit_user' => 'Editar usuÃ¡rio',
    'update' => 'Atualizar',
    'name' => 'Nome',
    'email' => 'O email',
    'password' => 'Senha',
    'role' => 'FunÃ§Ã£o',
    'related_to' => 'Relacionado a',
    'cpf' => 'CPF',
    'mobile' => 'MÃ³vel',
    'phone' => 'telefone',
    'user_image' => 'User Image',
    'comments' => 'ComentÃ¡rios',
    'create' => 'Crio',
    'users' => 'Comercial',
    'id' => 'identidade',
    'profile' => 'Perfil',
    'company' => 'Empresa',
    'status' => 'Status',
    'actions' => 'AÃ§Ãµes',
    'blocked' => 'Bloqueado',
    'active' => 'Ativo',
    'inactive' => 'Inativo',
    'view' => 'VisÃ£o',
    'logs' => 'Logs',
    'view_user' => 'Visualizar usuÃ¡rio',
    'user' => 'Do utilizador',
    'last_login_at' => 'Ãšltimo login em',
    'dob' => 'DOB',


    'label' => [
        'cbusers' => 'UsuÃ¡rios do ConnectBahn',
        'id' => 'Identidade',
        'profile' => 'Perfil',
        'name' => 'Nome',
        'email' => 'O email',
        'role' => 'FunÃ§Ã£o',
        'company' => 'Empresa',
        'status' => 'Status',
        'actions' => 'AÃ§Ãµes',
        'goal' => 'Objetivo',
        'cbusertransaction' => 'Ãšltimo Valor de TransaÃ§Ãµes de 30d',
        'assigncustorsupp' => 'Clientes / Fornecedores Designados',
        'changegoal' => 'Alterar meta',
        'changegoaluser' => 'Alterar objetivo para o usuÃ¡rio',
        'active' => 'Ativo',
        'inactive' => 'Inativo',
        'blocked' => 'Bloqueado',
        'changeamount' => 'Alterar valor do objetivo',
        'username' => 'Nome de usuÃ¡rio',
        'phone' => 'telefone',
        'mobile' => 'MÃ³vel',

        
    ],

    'cbuser' =>[
        'dealclosed' => 'NegociaÃ§Ã£o encerrada pelo Cb-User',
        'suppliers' => 'Fornecedor',
        'start_date' => 'Data de inÃ­cio',
        'end_date' => 'Data final',
        'periode' => 'Periodo',
        'revenueForConnectBahn' => 'Receita Para ConnectBahn',
        'consolidated_revenue' => 'Receita consolidada da ConnectBahn',
        'consolidated_revenue_all_users' => 'Ofertas consolidadas de todos os usuÃ¡rios comerciais',
    ]

    

];
