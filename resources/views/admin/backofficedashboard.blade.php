@extends('layouts.backend')


@section('title',trans('dashboard.label.back_dashboard'))

@push('js')
<style>

</style>
@endpush

@section('content')
    @if(Auth::user()->can('access.backoffice.dashboard'))
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i>
                    @lang('dashboard.label.back_dashboard')
                </div>

            </div>
            <div class="panel-body">


                <div class="row">
                <div class="row state-overview">
                <div class="col-lg-3 col-sm-6 dash">
                            <a href="{{url('/admin/users')}}">
                                <section class="panel dash_panel dash_panel_trans">
                                    <div class="value">
                                        <h1 class="dash_text">  @lang('dashboard.label.users')</h1>						
                                    </div>
                                </section>
                            </a>
                        </div>
                        
                        <div class="col-lg-3 col-sm-6 dash">
                            <a href="{{url('/admin/action-log')}}">
                                <section class="panel dash_panel dash_panel_trans">
                                    <div class="value">
                                        <h1 class="dash_text">  @lang('dashboard.label.users_log')</h1>							
                                    </div>
                                </section>
                            </a>
                        </div>
                        <div class="col-lg-3 col-sm-6 dash">
                            <a href="{{url('/admin/banks')}}">
                                <section class="panel dash_panel dash_panel_trans">
                                    <div class="value">
                                        <h1 class="dash_text"> @lang('dashboard.label.manage')   @lang('dashboard.label.banks')</h1>							
                                    </div>
                                </section>
                            </a>
                        </div>
                        <div class="col-lg-3 col-sm-6 dash">
                            <a href="{{url('/admin/customer')}}">
                                <section class="panel dash_panel">
                                    <div class="value">
                                        <h1 class="dash_text">@lang('dashboard.label.manage') <br> @lang('dashboard.label.customers')</h1>							
                                    </div>
                                </section>
                            </a>
                        </div>
                        <div class="col-lg-3 col-sm-6 dash">
                            <a href="{{url('/admin/suppliers')}}">
                                <section class="panel dash_panel">
                                    <div class="value">
                                        <h1 class="dash_text">@lang('dashboard.label.manage') <br>  @lang('dashboard.label.supplier')</h1>							
                                    </div>
                                </section>
                            </a>
                        </div>
                
            </div>
                </div>


                <div class="table-responsive">


                </div>

            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle"></i>
                            @lang('dashboard.label.errormessage')
                        </div>
                    </div>
                </div>
            </div>
        </div>
               
@endif
@endsection



@push('js')
<script>

</script>


@endpush
