@extends('layouts.backend')

@section('title',trans('bank.bank_accounts'))
@section('pageTitle',trans('bank.bank_accounts'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('bank.bank_accounts')
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('bank.back')</button></a>

                        @if(Auth::user()->can('add.bank.account'))
                        <a href="#" id="open_model_bank_account_add"  class="btn btn-success btn-sm" title="Add Bank Accounts">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('bank.add_bank_accounts')
                        </a>
                        @endif
                        <br />
                        <br />

                         <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="bankaccount-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('bank.id')</th>
                                        <th data-priority="2">@lang('bank.bank_name')</th>
                                        <th data-priority="3">@lang('bank.account_number')</th>
                                        <th data-priority="4">@lang('bank.account_info')</th>
                                        <th data-priority="5">@lang('bank.agency_number')</th>
                                        <th data-priority="6">@lang('bank.agency_name')</th>
                                        <th data-priority="7">@lang('bank.information')</th>
                                        <th data-priority="8">@lang('bank.street_name')</th>
                                        <th data-priority="9">@lang('bank.address_number')</th>
                                        <th data-priority="10">@lang('bank.city')</th>
                                        <th data-priority="11">@lang('bank.zip')</th>
                                        <th data-priority="12">@lang('bank.actions')</th>
                                    </tr>
                                </thead>

                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        @include("admin.models.bank_account")





@endsection



@push('script-head')
<script>

        var url ="{{ url('admin/bank/account/datatable') }}/{{$bank->id}}";
        //Permissions
        var edit_bank_account = "<?php echo Auth::user()->can('edit.bank.acount'); ?>";
        var delete_bank_account = "<?php echo Auth::user()->can('delete.bank.account'); ?>";
        
        datatable_a = $('#bankaccount-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { data: 'bank.name',name:'bank.name'},
                    { data: 'account_number',name:'account_number'},
                    { data: 'account_info', name: 'account_info' },
                    { data: 'agency_number', name: 'agency_number' },
                    { data: 'agency_name', name: 'agency_name' },
                    { data: 'info', name: 'info' },
                    { data: 'street_name', name: 'street_name' },
                    { data: 'address_number', name: 'address_number' },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.city != null)
                                return o.city.name;
                            else
                                return '-';
                        }
                    },
                    { data: 'zip', name: 'zip' },
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            if(edit_bank_account){
                                e= "<a href='#' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('bank.edit')</button></a>&nbsp;";
                            }
                               
                            if(delete_bank_account){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('bank.delete')</button></a>&nbsp;";
                            }

                            
                            return e+d;
                        }
                    
                    
                    }
                ]
        });

    $(document).on('click', '#open_model_bank_account_add', function (e) {
        var formid = $("#bank_account_form");
        resetupdateform(formid);
        $('#accounts').modal('show');


    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = "{{url('admin/bank/account')}}/" + id;
        var r = confirm("Are you sure you want to delete Bank Account?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable_a.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        }
    });


    $(document).on('click', '.edit-item', function (e) {
        var id = $(this).attr('data-id');

       var burl = "{{url('admin/bank/account')}}/"+id+"/edit";
        $.get(burl, function (result) {
            var data =result.data;
            var country_id=result.country_id;
            var state_id = result.state_id;
            var city_id = result.city_id;
            var states = result.states;
            var cities = result.cities;
            $('#bank_account_id').val(data.id);
            $('#bank_id').val(data.bank_id);
            $('#account_number').val(data.account_number);
            $('#account_info').val(data.account_info);
            $('#agency_number').val(data.agency_number);
            $('#agency_name').val(data.agency_name);
            $('#info').val(data.info);
            $('#street_name').val(data.street_name);
            $('#address_number').val(data.address_number);
            $('#complement').val(data.complement);
            $('#neighborhood').val(data.neighborhood);
            $('select#country').val(data.country);
            var options = '';
            $.each(states,function(index,json){
                if(state_id == index){
                    //console.log('check')
                    options += '<option value="' + index + '" selected="selected">' + json + '</option>';
                }

                else
                    options += '<option value="' + index + '">' + json + '</option>';
               
            });   

            var city_options = '';
            $.each(cities,function(index,json){
                //console.log(city_id);
                if(city_id == index){
                    //console.log('check')
                    city_options += '<option value="' + index + '" selected="selected">' + json + '</option>';
                }
                else
                    city_options += '<option value="' + index + '">' + json + '</option>';
               
            });  
            
            $('select#state').html(options);
            $('#city').html(city_options);
            $('#zip').val(data.zip);
            $('#country_id').val(country_id);
            $('.error').html("");
            $('.account_form_submit_button').html("Update");
            $('#account_form_model_lable').html("Edit Account Detail");

            $('#accounts').modal('show');

        })
    });

    function resetupdateform(formid) {
        $(formid)[0].reset();
        $('.error').html("");
        $("#bank_account_id").val("");
        $('#account_form_model_lable').html("Add Bank Account Detail");
        $(".account_form_submit_button").html("Create");

    }

$("#bank_account_form").validate({
    rules: {
        account_number: {
            required: true,
        },
        account_info: {
            required: true,
        },
        agency_number: {
            required: true,
        },
        agency_name:{
            required: true,
            
        },
        info:{
            required: true,
        
        },
        street_name:{
            required: true,
        
        },
        address_number:{
            required: true,
        
        },

        neighborhood:{
            required: true,
        
        },
        country:{
            required: true,
        
        },
        state:{
            required: true,
        
        },
        city:{
            required: true,
        
        },
        zip:{
            required: true,
        
        },
    },
    messages: {
        account_number: {
            required: "Please enter Account Number",
        },
        account_info: {
            required: "Please enter Account Info",
        },
        agency_number: {
            required: "Please enter Agency Number",
        },
        agency_name:{
            required: "Please enter Agency Name",
            
        },
        info:{
            required: "Enter Information",
        
        },
        street_name:{
            required: "Please enter Street Name",
            
        },
        address_number:{
            required: "Please enter Address Number",
            
        },
        neighborhood:{
            required: "Please enter neighborhood",
            
        },
        country:{
            required: "Please select country",
            
        },
        state:{
            required: "Please select state",
            
        },
        city:{
            required: "Please enter City",
            
        },
        zip:{
            required: "Please enter Zip",
            
        },
    },

    submitHandler: function (form) {


        var url = "{{url('admin/bank/account')}}";
        var method = "post"

        if($('#bank_account_id').val()!="" && $('#bank_account_id').val()!=0){
            var url = url+"/"+$('#bank_account_id').val();
            var method = "put";
        }

        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {

                $('#accounts').modal('hide');
                datatable_a.draw();
                toastr.success('Added Successfully',result.message)

            },
            error: function (error) {
                $('#accounts').modal('hide');
              //  toastr.error(result.message)
            }
        });
        return false;
    }
});
   



</script>
@endpush













