@extends('layouts.backend')

@section('title',trans('bank.company'))
@section('pageTitle',trans('bank.company'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('bank.company') for {{$bank->name}}
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('bank.back')</button></a>
                        <br><br>
                            
                            <div class="form-group prepend-top">
                                <div class="row">
                                    <div class="col-md-3">
                                        @if(Auth::user()->can('add.bank.comapny'))
                                        
                                        <select  class="form-control" id="selectcompany" >
                                            <option>Add Company</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}} </option>
                                            @endforeach
                                        </select>
                                        @endif

                                        
                                    </div>
                                </div>
                             </div>

                        <br />
                        <br />

                         <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="bankcompany-table">
                                <thead>
                                    <tr>
                                        {{--  <th data-priority="1">@lang('bank.id')</th>
                                        <th data-priority="2">@lang('bank.bank_name')</th>
                                        
                                        <th data-priority="3">@lang('bank.customer_name')</th>
                                        <th data-priority="4">@lang('bank.cpnj')</th>
                                        <th data-priority="5">@lang('bank.address')</th>
                                        <th data-priority="6">@lang('bank.total_amount')</th>
                                        <th data-priority="7">@lang('bank.date')</th>
                                        <th data-priority="8">@lang('bank.due_date')</th>
                                        <th data-priority="9">@lang('bank.type')</th>
                                        <th data-priority="10">@lang('bank.actions')</th>  --}}
                                        <th data-priority="1">@lang('bank.id')</th>
                                        <th data-priority="2">@lang('bank.bank_name')</th>
                                        <th data-priority="3">@lang('bank.customer_name')</th>
                                        <th data-priority="4">@lang('bank.cashmadeavailable')</th>
                                        <th data-priority="5">@lang('bank.cashconsumed')</th>
                                        <th data-priority="6">@lang('bank.cashstillavailable')</th>
                                        <th data-priority="7">@lang('bank.due_date')</th>
                                        <th data-priority="8">@lang('bank.rate')(%)</th>
                                        <th data-priority="9">@lang('bank.actions')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include("admin.models.bank_company")

@endsection

@push('script-head')
<script>
    
    var url ="{{ url('admin/bank/company/datatable') }}/{{$bank->id}}";

    //Permission
    var add_money = "<?php echo Auth::user()->can('add.money.for.comapny'); ?>";

        datatable = $('#bankcompany-table').DataTable({
            processing: true,
            serverSide: true,
            
             ajax: url,
                columns: [
                    { data: 'bank.id',name:'bank.id'},
                    { data: 'bank.name',name:'bank.name'},
                    { data: 'customer.name',name:'customer.name',"searchable" : false},
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.total.toFixed(2);
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                            return o.cash_consumed.toFixed(2);
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.cash_still_available.toFixed(2);
                            
                        }
                    },
                     { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "defaultContent": '-',
                        "render": function (o) {
                            
                                var duedate=o.expriryDate;
                                if(duedate!=null){
                                    var newdate = duedate.replace('-', '/').replace('-', '/');                               
                                    var first_date = moment(newdate).format('DD/MM/YYYY');
                                    return first_date
                                }else{
                                    var newdate ='';
                                    return newdate
                                }                                
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var bank_rate= o.bank_rate;
                            return parseFloat(bank_rate).toFixed(2) + '%';
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var add="";
                            if(add_money){
                                add= "<a href='#' value="+o.customer_id+" data-id="+o.customer_id+" class='addmoney' ><button class='btn btn-warning btn-xs'>@lang('bank.add_money')</button></a>&nbsp;";
                            }
                            return add;
                        }
                    
                    
                    }
                    
                ]
        });


        $(document).on('change', '#selectcompany', function (e) {

        var formid = $("#bank_company_form");
            formid[0].reset();
        var cust_id = document.getElementById("selectcompany").value;   
        
        var burl = "{{url('admin/bank/company/')}}/add/"+cust_id+'/0';
        $.get(burl, function (result) {

            $('#customer_id').val(result.id);
            $('#name').val(result.name);
            // $('#cpnj').val(result.cpnj); 
            // $('#address').val(result.address_line1);
            
            $('#company').modal('show');
        })

        
    });

        $(document).on('click', '.addmoney', function (e) {

        var formid = $("#bank_company_form");

        var cust_id = $(this).attr('data-id');
        
        var burl = "{{url('admin/bank/company/')}}/add/"+cust_id+'/{{$bank->id}}';
        $.get(burl, function (result) {
            $('#customer_id').val(result.customer_id);
            $('#name').val(result.customer.name);
            $('#cpnj').val(result.customer.cpnj);
            $('#address').val(result.customer.address_line1);
            $('#bank_rate').val(result.bank_rate);
            $('#availAmount').val(result.cash_still_available.toFixed(2))
            $('#accounts').val(result.bank_account);
            $('#contacts').val(result.bank_contact);
            var dArr = result.expriryDate.split("-");
            $('#duedate').val(dArr[2]+ "/" +dArr[1]+ "/" +dArr[0]); 
            $('#company').modal('show');
        })

        
    });
 
</script>
@endpush


