@extends('layouts.backend')

@section('title',trans('bank.banks'))
@section('pageTitle',trans('bank.banks'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('bank.transactions')
                                                  </div>

                                </div>
                               <div class="box-content ">

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="banks-table">
                                <thead>
                                    <tr>
                                        <th>@lang('bank.transaction_id')</th>
                                        <th>@lang('bank.customer_name')</th>
                                        <th>@lang('bank.supplier_name')</th>
                                        <th>@lang('bank.total_amount')</th>
                                        <th>@lang('bank.approved')</th>
                                        <th>@lang('bank.action')</th>
                                    </tr>
                                </thead>
                            
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script>

var url ="{{ url('admin/cession-data/') }}/{{$bank_id}}";

var viewurl ="{{ url('/admin/transactions/') }}";

        datatable = $('#banks-table').DataTable({
            processing: true,
            serverSide: true,
             ajax: url,
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    {   'data': null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.customer_cession.name
                        }
                    },
                    {   'data': null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                            return o.supplier.name
                        }
                    },
                    { data: 'total_amount', name: 'total_amount' },
                    { 
                        'data':null, 
                        "render":function(o){
                            if(o.approval_date != null){
                                var approval_date = o.approval_date.replace('-', '/').replace('-', '/');                               
                                approval_date = moment(approval_date).format('DD/MM/YYYY');
                                return approval_date;
                            }else{
                                return "Not Approved"
                            }   
                        }
                    },
                    { 
                        'data':null,
                        'render' : function(o){

                            var v =  "<a href='"+viewurl+"/"+o.id+"' data-id="+o.id+"><button title='View Transaction' class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i></button></a>&nbsp;";
                        return v;
                        }
                    }

                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Bank ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush


