@extends('layouts.backend')

@section('title',trans('bank.banks'))
@section('pageTitle',trans('bank.edit_bank'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('bank.edit_bank') #{{ $bank->id }}
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('bank.back')</button></a>

                        <a href="{{ url('/admin/bank/company/'.$bank->id) }}" class="btn btn-success btn-sm" title="Add Bank Company">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('bank.company')
                        </a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($bank, [
                            'method' => 'PATCH',
                            'url' => ['/admin/banks', $bank->id],
                            'class' => 'form-horizontal',
                            'files' => true,
                            'id'=>'formBank'
                        ]) !!}

                        @include ('admin.banks.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>


@endsection

@push('script-head')
<script>
$(document).ready(function(){
    $( ".countryOnchange" ).change(function() {
        var countryID = $(this).val();    
        if(countryID){
            $.ajax({
                type:"GET",
                url:"{{url('admin/getstate')}}?country_id="+countryID,
                success:function(res){               
                    if(res){
                        console.log(res);
                        $("#state").empty();
                        $.each(res,function(key,value){
                            $("#state").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else{
                       $("#state").empty();
                    }
                }
            });
        }else{
            $("#state").empty();
        }      
    });
    
});
   
</script>
@endpush

