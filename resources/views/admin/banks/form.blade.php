<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('bank.name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('legal_name') ? 'has-error' : ''}}">
    {!! Form::label('legal_name', trans('bank.legal_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('legal_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('legal_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cpnj') ? 'has-error' : ''}}">
    {!! Form::label('cpnj', trans('bank.cpnj'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cpnj', null, ['class' => 'form-control', 'required' => 'required','id'=>'cpnj','placeholder' => '(XX.XXX.XXX/YYYY-ZZ)']) !!}
        {!! $errors->first('cpnj', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(Route::currentRouteName() == 'banks.create')
<div class="form-group">
    <div class="col-md-12 text-center ad_bank_detail">
        {!! Form::label('label', 
         trans('bank.add_bank_accounts_detail')) !!}
    </div>
</div>                          
<div class="form-group {{ $errors->has('account_branch') ? 'has-error' : ''}}">
        {!! Form::label('account_branch', trans('bank.branch'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('account_branch', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('account_branch', '<p class="help-block">:message</p>') !!}
        </div>
</div>
                               
<div class="form-group {{ $errors->has('agency_number') ? 'has-error' : ''}} ">
        <label class="col-md-4 control-label" for="Projects_title">@lang('bank.agency_number')</label>
        <div class="col-md-6">
            <input class="form-control form-control clear" name="agency_number" id="agency_number" type="text" maxlength="85" value="" onkeypress="return isNumber(event)" >
            {!! $errors->first('agency_number', '<p class="help-block">:message</p>') !!}
        </div>
</div>
<div class="form-group {{ $errors->has('account_number') ? 'has-error' : ''}}">
        <label class="col-md-4 control-label" for="Projects_title">@lang('bank.account_number')</label>
        <div class="col-md-6">
            <input class="form-control form-control clear" name="account_number" id="account_number"   type="text" maxlength="85" value="" onkeypress="return isNumber(event)" >
        </div>
</div>
<div class="form-group {{ $errors->has('account_zip') ? 'has-error' : ''}}">
        {!! Form::label('account_zip', trans('bank.zip'), ['class' => ' col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('account_zip', null, ['class' => 'form-control', 'required' => 'required','id' => 'zip','placeholder'=> '00000-000']) !!}
            {!! $errors->first('account_zip', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group {{ $errors->has('account_street_name') ? 'has-error' : ''}}">
    
    {!! Form::label('account_street_name', trans('bank.street_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('account_street_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('account_street_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_address_number') ? 'has-error' : ''}}">
    
    {!! Form::label('account_address_number',trans('bank.address_number'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('account_address_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('account_address_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_complement') ? 'has-error' : ''}} ">
    
    {!! Form::label('account_complement', trans('bank.complement'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('account_complement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('account_complement', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_neighborhood') ? 'has-error' : ''}}">

    {!! Form::label('account_neighborhood', trans('bank.neighborhood'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">    
        {!! Form::text('account_neighborhood', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('account_neighborhood', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('account_country_name') ? ' has-error' : ''}} ">
    {!! Form::label('account_country_name', trans('bank.country'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
	 {!!Form::select('account_country_name',$countries,'null',array('class'=>'form-control','id'=>'country'));!!}
       {!! $errors->first('account_country_name', '<p class="help-block">:message</p>') !!}   
     </div>
</div>

<div class="form-group{{ $errors->has('account_state') ? ' has-error' : ''}}">
    
    {!! Form::label('account_state', trans('bank.state'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('account_state', ['' => 'Select State'],'null', ['class' => 'form-control  selectTag','id'=>'state']) !!}
        {!! $errors->first('account_state', '<p class="help-block">:message</p>') !!}

    </div>
</div>



<div class="form-group {{ $errors->has('account_city') ? 'has-error' : ''}} ">
    {!! Form::label('account_city', trans('bank.city'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">
       {!! Form::select('account_city', ['' => 'Select City'],'null', ['class' => 'form-control  selectTag','id'=>'city']) !!}
        {!! $errors->first('account_city', '<p class="help-block">:message</p>') !!}
    </div>
    
</div>

@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('script-head')

<script type="text/javascript">
    $('#phone').mask('(00) 00000-0000');
    $('#cpnj').mask('00.000.000/0000-00');
    $('#zip').mask('00000-000');
    $("#formBank").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
            cpnj: {
                required: true,
                validateCpf: true
            }
        },
        messages: {
            cpnj: {
                required: "CPNJ is required",
                validateCpf: "Enter Valid CPNJ Code"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod(
        "validateCpf", 
        function(value, element) {
            cnpj = value.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
            
        },
        "Enter Valid CPNJ Code"
    );
    
</script>
@endpush
