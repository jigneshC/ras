@extends('layouts.backend')

@section('title',trans('bank.banks'))
@section('pageTitle',trans('bank.banks'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('bank.banks')
                                                  </div>

                                </div>
                               <div class="box-content ">

                        @if(Auth::user()->can('acsess.banks.create'))
                        <a href="{{ url('/admin/banks/create') }}" class="btn btn-success btn-sm" title="Add New Bank">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('bank.add_new_bank')
                        </a>
                        @endif

                        @if(Auth::user()->can('access.bank.summary'))
                        <a href="{{ url('/admin/banks/summary') }}" class="btn btn-success btn-sm" title="Bank Summary">
                            <i class="fa fa-list-alt" aria-hidden="true"></i> @lang('bank.summary')
                        </a>
                        @endif

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="banks-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('bank.id')</th>
                                        <th data-priority="2">@lang('bank.name')</th>
                                        <th data-priority="3">@lang('bank.legal_name')</th>
                                        <th data-priority="4">@lang('bank.cpnj')</th>
                                        <th data-priority="4">@lang('bank.status')</th>
                                        <th data-priority="8">@lang('bank.actions')</th>
                                        
                                    </tr>
                                </thead>
                            
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script>
var url ="{{ url('/admin/banks/') }}";

        //Permissions
        var bankedit = "<?php echo Auth::user()->can('access.banks.edit'); ?>";
        var bankdelete = "<?php echo Auth::user()->can('access.banks.delete'); ?>";
        var access_account = "<?php echo Auth::user()->can('access.bank.accounts'); ?>";
        var access_contact = "<?php echo Auth::user()->can('access.bank.contacts'); ?>";
        var access_company = "<?php echo Auth::user()->can('access.bank.company'); ?>";
        var access_cession =  "<?php echo Auth::user()->can('access.bank.cession'); ?>";
        
        datatable = $('#banks-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 0, "desc" ]],
             ajax: '{!! route('BankControllerBanksData') !!}',
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { data: 'name',name:'name','searchable':true},
                    { data: 'legal_name',name:'legal_name','searchable':true},
                    { data: 'cpnj', name: 'cpnj' },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var bankstatus = '';
                           if(o.status == 'inactive')
                            
                                bankstatus = '<a href="'+url+'/'+o.id+'?bankstatus=inactive" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("user.inactive")</button></a>';
                            else
                                bankstatus = "<a href='"+url+"/"+o.id+"?bankstatus=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.active')</button></a>";
                            
                            return bankstatus;
                                            
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                          /*  var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('bank.view')</button></a>&nbsp;"; */

                            var e="";var d="";var accounts="";var contacts="";var company="";var cession="";

                            if(bankedit){
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button title='Edit' class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            if(bankdelete){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button title='Delete' class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
						    if(access_account){
							    accounts = "<a href='{{url("admin/bank/account")}}/"+o.id+"' data-id="+o.id+"><button title='Accounts' class='btn btn-warning btn-xs'>@lang('bank.accounts')</button></a>&nbsp;";
                            }
                            if(access_contact){
                                contacts = "<a href='{{url("admin/bank/contact")}}/"+o.id+"' data-id="+o.id+"><button class='btn btn-warning btn-xs' title='Contacts' ><i class='fa fa-phone' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            if(access_company){
                                company = "<a href='{{url("admin/bank/company")}}/"+o.id+"' data-id="+o.id+"><button title='Company' class='btn btn-warning btn-xs'><i class='fa fa-users' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            
                            if(access_cession){
                                cession = "<a href='{{url("admin/bank/cession")}}/"+o.id+"' data-id="+o.id+"><button title='Cession'  class='btn btn-warning btn-xs'><i class='fa fa-money' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                                

                            return e+d+accounts+contacts+company+cession;
                        }
                    }
                    
                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var url ="{{ url('/admin/banks/') }}";
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Bank ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush


