@extends('layouts.backend')

@section('title',trans('bank.banks'))
@section('pageTitle',trans('bank.banks'))

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               @lang('bank.bank') {{ $bank->id }}
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('bank.back')</button></a>
                        <a href="{{ url('/admin/banks/' . $bank->id . '/edit') }}" title="Edit Bank"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {{--
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/banks', $bank->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Bank',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        --}}
                        <a href="{{ url('/admin/bank/account/'.$bank->id) }}" class="btn btn-success btn-sm" title="Add Bank Accounts">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('bank.accounts')
                        </a>

                        <a href="{{ url('/admin/bank/contact/'.$bank->id) }}" class="btn btn-success btn-sm" title="Add Bank Contacts">
                            <i class="fa fa-plus" aria-hidden="true"></i>@lang('bank.contacts')
                        </a>

                        <a href="{{ url('/admin/bank/company/'.$bank->id) }}" class="btn btn-success btn-sm" title="Add Bank Company">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('bank.company')
                        </a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>@lang('bank.id')</th><td>{{ $bank->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.name')</th><td>{{ $bank->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('bank.legal_name') </th><td>{{ $bank->legal_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('bank.cpnj')</th><td>{{ $bank->cpnj }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.branch')</th><td>{{ $bank->branch }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.email')</th><td>{{ $bank->email }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.phone') </th><td>{{ $bank->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.mobile')</th><td>{{ $bank->mobile }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.street_name')</th><td>{{ $bank->street_name }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.address_number')</th><td>{{ $bank->address_number }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.complement') </th><td>{{ $bank->complement }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.neighborhood')</th><td>{{ $bank->neighborhood }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.country') </th><td>{{ $bank->country_name }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.state')</th><td>{{ $bank->state }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.city') </th><td>{{ $bank->city }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('bank.zip') </th><td>{{ $bank->zip }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
