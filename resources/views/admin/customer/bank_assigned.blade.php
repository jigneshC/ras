@extends('layouts.backend')

@section('title',trans('customer.bank_assigned_company'))
@section('pageTitle',trans('customer.bank_assigned_company'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('customer.bank_assigned_company')
                                       </div>
                    </div>
                    <div class="box-content ">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('customer.back')</button></a>
                        <br><br>
                         <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="bankcompany-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('customer.id')</th> 
                                        <th data-priority="2">@lang('customer.customer_name')</th>
                                        <th data-priority="3">@lang('customer.bank_name')</th> 
                                        <th data-priority="4">@lang('customer.cash_made_available')</th>
                                        <th data-priority="5">@lang('customer.cash_consumed')</th>
                                        <th data-priority="6">@lang('customer.cash_still_available')</th>
                                        <th data-priority="7">@lang('customer.limit_date')</th>
                                        <th data-priority="8">@lang('customer.rate')(%)</th>    
                                    </tr>
                                </thead>
                                <tbody>    
                            </table>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script>
    
        datatable = $('#bankcompany-table').DataTable({
            processing: true,
            serverSide: true,
            
             ajax: {
                    url: '{!! route('CustomerControllerCustomerBankData') !!}', // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.customer_id = "{{$customer->id}}";
                    }
                },
                columns: [
                    { data: 'id',name:'id'},
                    { data: 'customer.name',name:'customer.name',"searchable" : false},
                    { data: 'bank.name',name:'bank.name',},
                    { data: 'total',name:'total'},
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                            return o.cash_consumed;
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.cash_still_available;
                        }
                    },
                     { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                                var duedate=o.duedate;
                                if(duedate!=null){
                                    var newdate = duedate.replace('-', '/').replace('-', '/');                               
                                    var first_date = moment(newdate).format('DD/MM/YYYY');
                                    return first_date
                                }else{
                                    var newdate ='';
                                    return newdate
                                }
                            
                        }
                    },
                    { "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var bank_rate= o.bank_rate;
                            return parseFloat(bank_rate).toFixed(2) + '%';
                        }
                    }
                    
                ]
        });

 
</script>
@endpush


