@extends('layouts.backend')



@section('title',trans('customer.add_customer'))


@section('pageTitle')
    <i class="icon-tint"></i>


    <span>@lang('customer.add_customer')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="box bordered-box blue-border">
                                                  <div class="box-header blue-background">
                                                                  <div class="title">
                                                                          <i class="icon-circle-blank"></i>
                                                                     @lang('customer.add_new_customer')              
                                                             </div>
                                                  </div>
                                          </div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('customer.back')</button></a>
                        <br />
                        <br />
                        <div id="supplieradd"></div>
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/customer', 'class' => 'form-horizontal','id'=> 'formCustomer','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.customer.form')

                        {!! Form::close() !!}

                        
                    </div>
                </div>
  
            </div>
        </div>

@endsection

