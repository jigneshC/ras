@extends('layouts.backend')

@section('title',trans('customer.customers_logs'))
@section('pageTitle',trans('customer.customers_logs'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('customer.logs')
                                                  </div>

                               </div>
                               <div class="box-content ">

                                <div class="box-content ">
                                <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back')</button></a>

                        <br/>
                        <br/>
                        
                        <div class="table-responsive">
                            <table class="table table-borderless" id="log-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('customer.customer')</th>
                                        <th data-priority="2">@lang('customer.sales_user')</th>
                                        <th data-priority="3">@lang('customer.user_who_made_change')</th>
                                        <th data-priority="4">@lang('customer.date')</th>
                                        
                                    </tr>
                                </thead>
                                  
                                    @foreach($pass as $key => $value)
                                      @if($value == null)

                                      @else
                                      
                                        <tr>
                                            <td>{{$value['customer']}}</td>
                                            <td>{{$value['cbuser']}}</td>
                                            <td>{{$value['changed_by']}}</td>
                                            <td><?php echo date('d/m/Y', strtotime($value['date'])); ?></td>
                                        </tr>
                                      @endif
                                    @endforeach

                            </table>
                            
                        </div>
                        

                    </div>
                </div>
            </div>
        </div>

        <?php 
/*
        foreach ($pass as $key => $value) {
          # code..
          if($value == Null){
            echo "null";
          }else{
            
              
             echo $value['supplier'];  # code...
             
          }
        }
 */
          ?>
@endsection