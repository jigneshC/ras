<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', trans('customer.name') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('legal_name') ? ' has-error' : ''}}">
    {!! Form::label('legal_name', trans('customer.legal_name') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('legal_name',null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('legal_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('customer.customer_image'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
    {!! Form::file('image', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
    </div>

    @if(isset($customer) && $customer->image)
    <div class="form-group ">
        <img src="{!! asset('images/'.$customer->image) !!}" alt="image" width="50">
    </div>
    @endif

</div>


<div class="form-group{{ $errors->has('cpf') ? ' has-error' : ''}}">
    {!! Form::label('cpnj', trans('customer.cpnj'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cpnj', (isset($customer->cpnj)) ? $customer->cpnj : null, ['class' => 'form-control', 'required' => 'required','id'=>'cpnj','placeholder' => 'XX.XXX.XXX/YYYY-ZZ']) !!}
        {!! $errors->first('cpnj', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('zip') ? ' has-error' : ''}}">
        {!! Form::label('zip',trans('customer.zip'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('zip', null, ['class' => 'form-control', 'required' => 'required','id' => 'zip','placeholder' => '00000-000']) !!}
            {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
        </div>
</div>
<div class="form-group{{ $errors->has('address1') ? ' has-error' : ''}}">
    {!! Form::label('address1', trans('customer.address_street'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address1', (isset($customer->address_line1)) ? $customer->address_line1 : null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('address2') ? ' has-error' : ''}}">
    {!! Form::label('address2',trans('customer.address_number'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address2', (isset($customer->address_line2)) ? $customer->address_line2 : null , ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('address_complement') ? 'has-error' : ''}}">
    {!! Form::label('address_complement',trans('customer.address_complement') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_complement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address_complement', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('neighborhood') ? 'has-error' : ''}}">
    {!! Form::label('neighborhood',trans('customer.neighborhood'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('neighborhood', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('neighborhood', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(Route::currentRouteName() == 'customer.create')
<div class="form-group{{ $errors->has('country_name') ? ' has-error' : ''}} ">
    {!! Form::label('country_name', trans('customer.country'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
	 {!!Form::select('country_name',$countries,'null',array('class'=>'form-control','id'=>'country'));!!}
       {!! $errors->first('country_name', '<p class="help-block">:message</p>') !!}   
     </div>
</div>

<div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
    
    {!! Form::label('state', trans('customer.state'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('state', ['' => 'Select State'],'null', ['class' => 'form-control  selectTag','id'=>'state']) !!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}

    </div>
</div>



<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}} ">
    {!! Form::label('city', trans('customer.city'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">
       {!! Form::select('city', ['' => 'Select City'],'null', ['class' => 'form-control  selectTag','id'=>'city']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
    
</div>

@else
<div class="form-group{{ $errors->has('country_name') ? ' has-error' : ''}} ">
     {!! Form::label('country_name', trans('customer.country'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
	 {!!Form::select('country_name',$countries,$country_id,array('class'=>'form-control','id'=>'country'));!!}
       {!! $errors->first('country_name', '<p class="help-block">:message</p>') !!}   
     </div>
</div>

<div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
    
    {!! Form::label('state', trans('customer.state'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('state',$states,$state_id, ['class' => 'form-control  selectTag','id'=>'state']) !!}
        
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}

    </div>
</div>

<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}} ">
   {!! Form::label('city', trans('customer.city'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">
       {!! Form::select('city', $cities,$city_id, ['class' => 'form-control  selectTag','id'=>'city']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
    
</div>

@endif


<div class="form-group {{ $errors->has('cbuser') ? 'has-error' : ''}}">
    {!! Form::label('cbuser',trans('customer.cb_rep'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if(isset($salesUsers) && count($salesUsers) > 0)
        {!! Form::select('cbuser',$salesUsers,(isset($salesCustUsers) && $salesCustUsers != '' ) ? $salesCustUsers : '',['class' => 'form-control selectTag',(isset($salesCustUsers) && $salesCustUsers != '' ) ? ' disabled="disabled"' : '']) !!}
        {!! $errors->first('cbuser', '<p class="help-block">:message</p>') !!}
        @else
        <p>Add CB-Sales Users for Customer</p>
        @endif
    </div>
</div>
<input type="hidden" name="old_salesUsers" value="<?php echo (isset($salesCustUsers) && $salesCustUsers != '' ) ? $salesCustUsers : ''; ?>" />

@if(Route::currentRouteName() == 'customer.create')
<div class="form-group{{ $errors->has('rate') ? ' has-error' : ''}}">
    {!! Form::label('rate', trans('customer.cb_revenue_share_rate'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('rate', null, ['class' => 'form-control', 'required' => 'required','min' => '0','max' => '100','step' => 'any']) !!}
        {!! $errors->first('rate', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('customer.create'), ['class' => 'btn btn-primary','id' =>'submitForm']) !!}
    </div>
</div>

@push('script-head')

<script type="text/javascript">
    $('#phone').mask('(00) 00000-0000');
    $('#cpnj').mask('00.000.000/0000-00');
    $('#zip').mask('00000-000');
    $("#formCustomer").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
            cpnj: {
                required: true,
                validateCpf: true
            }
        },
        messages: {
            cpnj: {
                required: "CPNJ is required",
                validateCpf: "Enter Valid CPNJ Code"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod(
        "validateCpf", 
        function(value, element) {
            cnpj = value.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
            
        },
        "Enter Valid CPNJ Code"
    );

     $('#country').change(function(){
    var countryID = $(this).val();    
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
    
</script>
@endpush



