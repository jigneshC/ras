@extends('layouts.backend')

@section('title',trans('customer.customers'))
@section('pageTitle',trans('customer.customers'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                            @lang('customer.customers')
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            @if(Auth::user()->can('access.customers.create'))
                                <a href="{{ url('/admin/customer/create') }}" class="btn btn-success btn-sm"
                                   title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('customer.add_new')
                                </a>

                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                                <p>@lang('customer.cash_available_less_then')</p>
                                <input type="number" name="cashavailable" value="" id="cashavailable" />
                            </div>

                            <div  class="col-md-6">
                                <p>@lang('customer.cash_available_to_expiry_days')</p>
                                <input type="number" name="cashavailableexpire" value="" id="cashavailableexpire" />
                            </div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table id="customers-table" class="table table-borderless  datatable responsive">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('customer.id')</th>
                                
                                <th data-priority="2"></th>
                                <th data-priority="3">@lang('customer.customer')</th>
                                <th data-priority="4">@lang('customer.cbrep')</th>
                                <th data-priority="5">@lang('customer.invoices_total_amount')</th>
                                <th data-priority="8">@lang('customer.cessions_availability')</th>
                                <th data-priority="9">@lang('customer.anticipations_availability')</th>
                                <th data-priority="10">@lang('customer.expiry_date')</th>
                                <th data-priority="11">@lang('customer.status')</th>
                                <th data-priority="12">@lang('customer.actions')</th>
                                
                                
                            </tr>
                            </thead>
                        </table>
                    </div>
                    @include("admin.models.cb_change_rep")
					@include("admin.models.cash_add") 
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/customer/') }}";

var img_path = "{{asset('images')}}";

//Permission 
    var edit = "<?php echo Auth::user()->can('access.customers.edit'); ?>";
    var cust_delete = "<?php echo Auth::user()->can('access.customers.delete'); ?>";
    var cust_users = "<?php echo Auth::user()->can('access.users'); ?>";
    var cust_banks = "<?php echo Auth::user()->can('access.banks'); ?>";
    var related_supplier = "<?php echo Auth::user()->can('access.related.supplier'); ?>";
    var cust_cbchange= "<?php echo Auth::user()->can('change.cb.rep.customer'); ?>";
    var update_cash = "<?php echo Auth::user()->can('update.cash.customer'); ?>";
    var discount_rate = "<?php echo Auth::user()->can('access.discount.rate'); ?>"; 
    var cust_status = "<?php echo Auth::user()->can('access.customer.status'); ?>";  

$(document).on('click', '.open_model_for_change_cb_rep', function (e) {
    $('#customerChangeCbRep').html($(this).attr('data-customername'));
    $('#cbcustomer_id').val($(this).attr('data-customerid'));
    $('#oldcbrep_id').val($(this).attr('data-oldcbrep'));
    $("select#cbsales").val($(this).attr('data-oldcbrep'));
    var formid = $("#change_cb_rep_form");
    $('#changeCbRepresentative').modal('show');
});

$("#change_cb_rep_form").validate({
    rules: {
        cbrepforcustomer: {
            required: true,
        }
    },
    messages: {
        cbrepforcustomer: {
            required: "Select CB Representative for Customer",
        }
    },
    submitHandler: function (form) {
        var url = "{{url('admin/customer/changeCbRep')}}";
		
        var method = "post"
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {
                result = JSON.parse(result)
                if(result.msg == 'Success')
                    toastr.success('Changed Successfully')
                else
                    toastr.error('Something Went Wrong, Try Again!')
                $('#changeCbRepresentative').modal('hide');
                datatable.draw(); 
            },
            error: function (error) {
                $('#changeCbRepresentative').modal('hide');
                datatable.draw();
            }
        }); 
        return false;
    }
});
        datatable = $('#customers-table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            "order": [[ 0, "desc" ]],
            ajax: 
                {
                    url: '{!! route('CustomerControllerCustomersData') !!}', // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.cashavailable = $('#cashavailable').val();
                        d.cashavailableexpire = $('#cashavailableexpire').val();
                    }
                },
            columns: [
                    { data: 'id', name: 'customer.id',"searchable": false},
                    
                    { 
                        "data": null,
                        "name": 'image',
                        "orderable": false,
                        "searchable" : false,
                        "render": function(o){
                            var image = o.image;
                            if(image){
                                return '<img src="'+img_path+'/'+image+'" width="50" height="50"></td>';
                            }
                            else{
                                return '<img src="'+img_path+'/avatar.jpg" width="50" height="50"></td>';
                            }
                        }
                    } ,
                    { data: 'name',name:'customer.name'},
                    {
                        data : 'user_name',
                        name : 'users.name',
                        "searchable": true,
                        "orderable": false,
                    },

                    {
                        data : null,
                        name : 'invoicesum',
                        "searchable": false,
                        "orderable": false,
                        "render" : function (o){
                            if(typeof o.invoice_sum[0] == "undefined")
                               return 0;
                            else
                                return o.invoice_sum[0].invoicesum;
                        }

                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            // if(o.cash_available_cession.length > 0){
                            //     if(typeof o.invoice_sum[0] != "undefined")
                            //         return o.cash_available_cession[0].cession - o.invoice_sum[0].invoicesum ;
                            //     else
                            //         return o.cash_available_cession[0].cession ;
                                
                            // }else{
                            //     return '0';
                            // }
                            if(o.cash_available_cession.length > 0){
                                return o.cash_available_cession[0].cession
                            }else{
                                return '0';
                            }
                        }
                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.cash_available_anticipation.length > 0){
								return o.cash_available_anticipation[0].anticipation.toFixed(2)
                            }else{
                                return '0';
                            }
                        }
                    },
                    
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                                var expirydate=o.expiry_date;
                                if(expirydate!=null){                              
                                    var first_date = moment(expirydate).format('DD/MM/YYYY');
                                    return first_date
                                }else{
                                    var newdate ='';
                                    return newdate
                                }     
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';

                            if(cust_status){
                                if(o.is_status == 'inactive'){
                                    status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive" onclick="return(Activeconfirm()); "><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("user.inactive")</button></a>';
                                }else{
                                    status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'  onclick='return(Inactiveconfirm());' ><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.active')</button></a>";
                                }
                                
                                
                                return status;
                            }else{

                                return o.is_status;

                            }                    
                        }

                    },
                                   
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

                            // var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('customer.view')</button></a>&nbsp;";
                            //  var logs = "<a href='{{url("admin/customer")}}/"+o.id+"/logs' data-id="+o.id+"><button class='btn btn-danger btn-xs'>@lang('supplier.logs')</button></a>&nbsp;";
                            // var d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-primary btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('customer.delete')</button></a>&nbsp;"; 

                            var e=""; var users=""; var supplier=""; var cbchange=""; var banks=""; var updatecash=""; var discount_class="";

                            if(edit){
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button title='Edit' class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            if(cust_users){
                                users = "<a href='{{url("admin/users")}}?customer_id="+o.id+"' data-id="+o.id+"><button id='user-list' title='Users' class='btn btn-info btn-xs '><i class='fa fa-user' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            if(related_supplier){
                                supplier =  "<a href='"+url+"/"+o.id+"/customersupplier' data-id="+o.id+"><button title='Supplier' class='btn btn-warning btn-xs'><i class='fa fa-cubes' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            if(cust_cbchange){
                                cbchange =  "<a href='#' class='open_model_for_change_cb_rep' data-customername='"+o.name+"' data-customerid ='"+o.id+"' data-oldcbrep='"+o.user_id+"'><button title='Change CB-Rep' class='btn btn-warning btn-xs'><i class='fa fa-user' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            
                            if(cust_banks){
                                banks = "<a href='{{url("admin/customer")}}/"+o.id+"/banks' data-id="+o.id+"><button title='Banks' class='btn btn-warning btn-xs'><i class='fa fa-bank' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            if(update_cash){
							    updatecash =  "<a href='#' class='btn btn-success btn-xs' id='open_model_for_add_cash' data-customername='"+o.name+"' data-customerid ='"+o.id+"' data-oldcbrep='"+o.user_id+"'>@lang('customer.update_cash')</a>";
                            }

                            if(discount_rate){
                                discount_class = "<a href='{{url("admin/discount-rate")}}?customer_id="+o.id+"' data-id="+o.id+"><button id='discount-list' title='Discount Rate Class' class='btn btn-info btn-xs '><i class='fa fa-tags' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            return e+users+supplier+banks+cbchange+updatecash+discount_class;
                        }
                    }
                ]
        });

    function Activeconfirm() { 
        var r = confirm("Are you sure you want to Active this Customer ?");
        if (r == true) {
            return true;
        }else{
            return false;
        }
    }

    function Inactiveconfirm() { 
        var r = confirm("Are you sure you want to Inactive this Customer ?");
        if (r == true) {
            return true;
        }else{
            return false;
        }
    }
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Customer ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });



    $(document).on('focusout', '#cashavailableexpire', function (e) {
        datatable.draw();
    }); 

    $(document).on('focusout', '#cashavailable', function (e) {
        datatable.draw();
    });
     $(document).on('click', '#open_model_for_add_cash', function (e) {

        var c_id = $(this).attr('data-customerid');
      
        var formid = $("#cash_add_form");

        var cash_url = "{{url('admin/cash')}}";
        var cash_url_link = cash_url +"/"+ c_id;
      
        $('#cashAdd').modal('show');
        /*get total amount and expiry date of customer */
        $('#customer_id').val(c_id);
        
         $.ajax({
            url: cash_url_link,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {

                if(data.cashtotal.id==null){
                    $('#total_amount').html('0');
                    $('#expiry_date').html('-');
                }else{
                    if(data.cashtotal == null || data.cashtotal == ''){
                        prev_avail_cash = 0;
                    }else{
                        prev_avail_cash=data.cashtotal.cash_available_anticipation[0].anticipation;
                    }
                    
                    cur_cash= 'R$ '+ prev_avail_cash;
                    $('#total_amount').html(cur_cash);
                    prev_expiry_date=data.expiry_date[0].expiry_date;
                    var newdate = prev_expiry_date.replace('-', '/').replace('-', '/');                               
                    var first_date = moment(newdate).format('DD/MM/YYYY');
                    $('#expiry_date').html(first_date);
                }      
             
            }
        });

     

    });

    $('#cash_expiry').datetimepicker({
        format: 'DD/MM/YYYY',
    });

    $("#cash_add_form").validate({
     
    rules: {
        add_cash_input: {
            required: true,
        },
        cash_expiry: {
            required: true,
        },
    },
    messages: {
        add_cash_input: {
            required: "Please enter Cash",
        },
        cash_expiry: {
            required: "Please enter Cash Expiry Date",
        },
    },
    submitHandler: function (form) {
        var url = "{{url('admin/cash')}}";
        var method = "post"

        /*if($('#bank_account_id').val()!="" && $('#bank_account_id').val()!=0){
            var url = url+"/"+$('#bank_account_id').val();
            var method = "put";
        } */
        
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
           
            success: function (data)
            {
                
                data = JSON.parse(data)
                if(data.msg == 'Success')
                    toastr.success('Added Successfully',data.message)
                else
                    toastr.error('Added Successfully',data.message)
                $('#cashAdd').modal('hide');
                datatable.draw();
            },
            error: function (error) {
                $('#cashAdd').modal('hide');
               // toastr.error(data.message);
            }
            
        });
        return false;

       

    }
});
</script>
@endpush
