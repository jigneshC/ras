@extends('layouts.backend')

@section('title',trans('customer.view_customer'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('customer.customers')
                                                  </div>

                               </div>
                               <div class="box-content ">
            

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.customers.edit'))
                    <a href="{{ url('/admin/customer/' . $customer->id . '/edit') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit
                        </button>
                    </a>
                    @endif

                    @if(Auth::user()->can('access.transaction.history.customer'))
                    <a href="{{ url('/admin/customer/transaction/' . $customer->id . '') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i>
                            @lang('customer.transaction_history')
                        </button>
                    </a>
                    @endif
                    
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/customer', $customer->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete User',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>


                    <?php
                    //$role = join(' + ', $user->roles()->pluck('label')->toArray());
                    ?>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('customer.id')</td>
                                <td>{{ $customer->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('customer.name')</td>
                                <td>{{ $customer->name }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.legal_name')</td>
                                <td>{{ $customer->legal_name }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.type')</td>
                                <td>{{ $customer->type }}</td>
                            </tr>

                            

                            <tr>
                                <td>@lang('customer.cpnj')</td>
                                <td>{{ $customer->cpnj }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.address_number')</td>
                                <td>{{$customer->address_line1}}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.address_street')</td>
                                <td>{{$customer->address_line2}}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.address_complement')</td>
                                <td>{{$customer->address_complement}}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.neighborhood')</td>
                                <td>{{$customer->neighborhood}}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.city')</td>
                                <td>{{ $customer->city }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.state')</td>
                                <td>{{ $customer->state }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.country')</td>
                                <td>{{ $customer->country_name }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.zip')</td>
                                <td>{{ $customer->zip }}</td>
                            </tr>

                            <tr>
                                <td>@lang('customer.website')</td>
                                <td>{{ $customer->website }}</td>
                            </tr>
                            
                             <tr>
                                <td>@lang('customer.operator')</td>
                                <td>
                                    @if(isset($customer->user[0]->name))
                                        {{ $customer->user[0]->name }}
                                    @endif
                                </td>
                            </tr>
                            

                            

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection