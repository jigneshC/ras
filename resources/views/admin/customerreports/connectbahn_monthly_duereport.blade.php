@extends('layouts.backend')


@section('title',trans('reports.connectbahn_reports'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('reports.connectbahn_reports')</span>


@endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('reports.connectbahn_reports')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('user.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/customerreports/connectbahnrevenue', 'class' => 'form-horizontal','id'=>'formReports','enctype'=>'multipart/form-data']) !!}
                            {{csrf_field()}}
                             <label>@lang('reports.customer_name') </label>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                                
                                    <div class="col-md-6">
    
                                    {!! Form::select('name',$customer, null, ['class' => 'form-control  selectTag','required']) !!}
                                        {!! $errors->first('name', '<p class="help-block with-errors">:message</p>') !!}

                                    </div>

                                    <div class="col-md-6">
                                         <label> @lang('reports.due_date') :</label>
                                         <input type="text" name="startdate" id="startdate" value="{{$start_date}}" />
                                         <label>@lang('reports.to') </label>
                                            <input type="text" name="enddate" id="enddate" value="{{$end_date}}"/>
                                                {!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i> Submit', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-success btn-xs',
                                                'title' => 'Change Period',
                                                'id' => 'changedate'
                                                
                                                ))!!}

                                    </div>
                            </div>      

                        {!! Form::close() !!}

                    </div>

                </div>
            </div>
        </div>
@endsection
@push('script-head')
<script>
$(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
@endpush

