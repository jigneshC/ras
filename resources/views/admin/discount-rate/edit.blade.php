@extends('layouts.backend')

@section('title',trans('discount.discount_rate'))
@section('pageTitle',trans('discount.edit_discount_rate'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('discount.edit_discount_rate') #{{ $discountrate->id }}
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('discount.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($discountrate, [
                            'method' => 'PATCH',
                            'url' => ['/admin/discount-rate', $discountrate->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.discount-rate.form', ['submitButtonText' => trans('discount.update')])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
