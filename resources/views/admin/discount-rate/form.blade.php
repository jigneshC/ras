
@if(isset($customer_id))
<input type="hidden" name="customer_id" value={{$customer_id}}>
@endif
<div class="form-group {{ $errors->has('class') ? 'has-error' : ''}}">
    {!! Form::label('class', trans('discount.class'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('class', null, ['class' => 'form-control','required' => 'required']) !!}
        {!! $errors->first('class', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('min') ? 'has-error' : ''}}">
    {!! Form::label('min', trans('discount.minimum') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('min', null, ['class' => 'form-control','required' => 'required','min' => '0','max' => '100','step' => 'any']) !!}
        {!! $errors->first('min', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('max') ? 'has-error' : ''}}">
    {!! Form::label('Max', trans('discount.maximum'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('max', null, ['class' => 'form-control','required' => 'required','min' => '0','max' => '100','step' => 'any']) !!}
        {!! $errors->first('max', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('discount.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>
