@extends('layouts.backend')

@section('title',trans('discount.discount_rate'))
@section('pageTitle',trans('discount.discount_rate'))

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               @lang('discount.discount_rate') {{ $discountrate->id }}
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('discount.back')</button></a>
                        
                        @if(Auth::user()->can('edit.discount.rate'))
                            <a href="{{ url('/admin/discount-rate/' . $discountrate->id . '/edit') }}" title="Edit DiscountRate"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('discount.edit')</button></a>
                        @endif

                        @if(Auth::user()->can('delete.discount.rate'))
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/discountrate', $discountrate->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete DiscountRate',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                            {!! Form::close() !!}
                        @endif
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>@lang('discount.id')</th><td>{{ $discountrate->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> @lang('discount.class')</th><td> {{ $discountrate->class }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('discount.minimum')</th><td> {{ $discountrate->min }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('discount.maximum')</th><td> {{ $discountrate->max }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
