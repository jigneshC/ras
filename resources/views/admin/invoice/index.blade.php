@extends('layouts.backend')

@section('title',trans('invoice.invoices'))
@section('pageTitle',trans('invoice.invoices'))




@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                            @lang('invoice.invoices')
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">

                        
                                @if(Auth::user()->can('access.invoices.create'))
                                    <div class="col-md-2">
                                    <a href="{{ url('/admin/invoice/create') }}" class="btn btn-success btn-sm" title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i>@lang('invoice.add_new_invoice_csv')
                                    </a> 
                                    </div>
                                @endif

                                </div>

                    <div class="row margin-top-50">


                        <div class="col-md-12">

                                <label> @lang('invoice.due_date') :</label>
                                <input type="text" name="startdate" value="" id="startdate" />
                                <label>@lang('invoice.to') </label>
                                <input type="text" name="enddate" id="enddate" value="" />
                                {!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i> Submit', array(
                                'type' => 'submit',
                                'class' => 'btn btn-success btn-xs',
                                'title' => 'Change Period',
                                'id' => 'changedate'
                                
                                ))!!}

                        </div>

                        <div class="col-md-12">
                                 <label>@lang('invoice.days_upto_due_date') : </label>
                                <input type="number" id="daysdue" />
                        </div>

                        <div  class="col-md-12">

                             {!! Form::open(['method' => 'GET', 'url' => '/admin/invoice', 'class' => 'navbar-form navbar-left', 'role' => 'search'])  !!}
        
                                <label>@lang('invoice.filter_by_status') : </label>
                                {!! Form::select('status',array('Available'=> 'Available','Used'=>'Used','Expired'=>'Expired','All'=>'All'),'',[ 'id'=>'filter_status']) !!}
                               
                                {!! Form::close() !!}
                        </div>

                        @if(Auth::user()->can('access.transaction.commercialuser.dashboard'))
                        <div class="col-md-12 invoicelist-excute-cbc">
                            <div class="table-responsive">
                                <table class="table table-borderless cbc-dashboard" >
                                    <thead>
                                        @if(Auth::user()->roles[0]->name == "CBC" )  
                                            <th data-priority="1">@lang('invoice.supplier')</th>
                                            <th data-priority="2">@lang('invoice.customer')</th>
                                        @else
                                            <th data-priority="1">@lang('invoice.customer')</th>
                                            <th data-priority="2">@lang('invoice.supplier')</th>
                                        @endif
                                        <th data-priority="3">@lang('invoice.minimum')</th>
                                        <th data-priority="4">@lang('invoice.maximum')</th>
                                        <th data-priority="5">@lang('invoice.available_cash')</th>
                                        <th data-priority="6">@lang('invoice.negotiated_rate')</th>
                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @if(Auth::user()->roles[0]->name == "CBC" )

                                            <td>
                                                <select id="supplier_change">
                                                    <option value="0">Select</option>
                                                    @foreach($supplier as $suppliers)
                                                        <option value="{{$suppliers->id}}">{{$suppliers->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select id="customer_change" >
                                                        <option value="">Select</option>
                                                </select>
                                            </td>

                                            @else
                                            <td>
                                                <select id="customer_change">
                                                    <option value="0">Select</option>
                                                    @foreach($customer as $customers)
                                                        <option value="{{$customers->id}}">{{$customers->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select id="supplier_change" >
                                                        <option value="">Select</option>
                                                </select>
                                            </td>
                                            @endif

                                            <td ><input type="text" name="min" id="min" disabled style="width:75px;"></td>
                                            <td><input type="text" name="max" id="max" disabled style="width:75px;"></td>
                                            <td ><input type="text" name="cash"  class="cash" disabled style="width:125px;"></td>
                                             
                                            <td>
                                            <input type="text" step="0.5" name="negotiate_rate" id="negotiate_rate" data-supplierId='' min="0" value="0" data-customerId='' style="width:75px;"/>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        
                        @else
                            @if(Auth::user()->roles[0]->name == "SLA" || Auth::user()->roles[0]->name == "SLO")
                                <div class="col-md-3 invoicelist-excute-cbc">
                                    <div class="table-responsive">
                                        <table class="table table-borderless cbc-dashboard" >
                                            <thead>
                                                <th data-priority="1">@lang('invoice.customer')</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select id="customer_change">
                                                            <option value="0">Select</option>
                                                            @foreach($customer as $customers)
                                                                <option value="{{$customers->id}}">{{$customers->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                            @elseif(Auth::user()->roles[0]->name == "CM" )
                                <div class="col-md-3 invoicelist-excute-cbc">
                                    <div class="table-responsive">
                                        <table class="table table-borderless cbc-dashboard" >
                                            <thead>
                                                <th data-priority="2">@lang('invoice.supplier')</th>              
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select id="supplier_change" >    
                                                            <option value="0">Select</option>
                                                            @foreach($supplier as $suppliers)
                                                                <option value="{{$suppliers->id}}">{{$suppliers->name}}</option>
                                                            @endforeach  
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                            @else
                                <div class="col-md-6 invoicelist-excute-cbc">
                                    <div class="table-responsive">
                                        <table class="table table-borderless cbc-dashboard" >
                                            <thead>
                                                <th data-priority="1">@lang('invoice.customer')</th>
                                                <th data-priority="2">@lang('invoice.supplier')</th>
                                                
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select id="customer_change">
                                                            <option value="0">Select</option>
                                                            @foreach($customer as $customers)
                                                                <option value="{{$customers->id}}">{{$customers->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="supplier_change" >    
                                                                <option value="">Select </option>    
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                            @endif
                        @endif
                                


                        <!-- Hidden Field for calculation -->
                        <input type="hidden" name="cash" class="cash" disabled style="width:125px;">
                        <input type="hidden" name="rate_class" id="rate_class" data-supplierId='' value="0" data-customerId='' />
                        <input type="hidden" name="type" id="type" value=""/>
                        <input type="hidden" name="cust_bank_cost" id="cust_bank_cost" value="0"/>
                        <input type="hidden" name="cust_bank_id" id="cust_bank_id" value=""/>
                        <input type="text" name="cust_bank_amount" id="cust_bank_amount" />  
                        <input type="hidden" name="discounted_value" id="discounted_value" />  


                        @if(Auth::user()->can('access.transaction.create'))
                        <div class="col-md-8 invoicelist-excute-cbc">
                            <div class="table-responsive">
                                <table class="table table-borderless" id="summary">
                                    <thead>
                                        <th data-priority="1">@lang('invoice.type')</th>
                                        <th data-priority="2">@lang('invoice.total_amount')</th>
                                        <th id="discount" data-priority="3">@lang('invoice.discount')</th>
                                       
                                        <th id="net_value" data-priority="4">@lang('invoice.net_value')</th>
                                        <th id="bank_name" style="display:none" data-priority="5" >@lang('invoice.bank_name')</th>
                                        <th id="cost" data-priority="6">@lang('invoice.costs')</th>
                                        @if(Auth::user()->role[0]->name == 'CBC')
                                        <th id="net_cash" data-priority="7">Customer Available Cash</th>
                                        <th id="bank_availe_cash" data-priority="8">Bank Available Cash</th>
                                        <th id="remain_bank_cash" data-priority="9">Remain Cash</th>
                                        @endif
                                        <th id="discount_rate" ></th>
                                    </thead>
                                    <tbody id="transactionDetail" style="display:none">
                                        <tr id="calculation" >

                                        </tr>

                                        
                                    </tbody>
                                </table>
                                <div>
                                    <a href='#' id="CreateTransaction"><button class='btn btn-info'> @lang('invoice.execute')</button></a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="table-responsive">
                        <table id="invoice-table" class="table table-borderless datatable responsive" width="100%">
                            <thead>
                            <tr>
                                <th data-priority="1">#</th>
                                <th data-priority="3">@lang('invoice.invoice_number')</th>
                                <th data-priority="4">@lang('invoice.customer')</th> 
                                <th data-priority="5">@lang('invoice.supplier_unit')</th>                                
                                <th data-priority="6">@lang('invoice.original_due_date')</th>
                                <th data-priority="7">@lang('invoice.date_of_anticipation')</th>
                                <th data-priority="16">@lang('invoice.days_upto_due_date')</th>
                                <th data-priority="8">@lang('invoice.original_value')</th>
                                <th data-priority="9">@lang('invoice.discount_rate')</th>
                                <th data-priority="10">@lang('invoice.discount_value')</th>
                                {{--  <th data-priority="11">@lang('invoice.costs')</th>  --}}
                                <th data-priority="11">@lang('invoice.net_value')</th>                                  
                                <th data-priority="12">@lang('invoice.status')</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script> 
var user = "{{Auth::user()->role[0]->name}}";
var url ="{{ url('/admin/invoice/') }}";
var d = new Date();
var anticipationDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
function getdueday(duedate,stardate){
    var end = new Date(duedate);
    var start   = new Date(stardate);     
    var diff  = new Date(end - start);
    var days  = diff/(1000 * 3600 * 24); 
    return  Math.floor(days);
    /*
    // Moment js
    var startDate = moment(start, "DD.MM.YYYY");
    var endDate = moment(end, "DD.MM.YYYY");
    return endDate.diff(startDate, 'days');
    */
}

//Get Today date to check Available cash of customer is expired or not
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 
today = yyyy + '-' + mm + '-' + dd;

$('#filter_status').change(function() {
    datatable.draw();
});

        var datatable = $('#invoice-table').DataTable({
            "order": [],
            "aaSorting": [],
            processing: true,
            serverSide: true,
            ajax: 
            {
                url: '{!! route('InvoiceControllerInvoicesData') !!}', 
                type: "get",
                data: function (d) {
                    d.startdate = $('#startdate').val();
                    d.enddate = $('#enddate').val();
                    d.daysdue = $('#daysdue').val();
                    d.filter_status = $('#filter_status').val();
                    d.supplier_id = $('#supplier_change').val();
                    d.customer_id = $('#customer_change').val();
                }
            },
                columns: [
                    { 
                        data: null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var v = '';
                            var user_access = "{{Auth::user()->can('access.transaction.create')}}";
                            
                            if(user_access == 1){

                                if(o.is_transaction == '1' || o.status == "Used"){

                                    v =  '<input type="checkbox" class="invoiceSelect_o" data-type="anti" checked="checked" disabled readonly name="" value="'+ o.id+'" />';    
                                
                                }else{
                                    
                                    if(o.type == "Anticipation")
                                    {
                                        v =  '<input type="checkbox" class="invoiceSelect selectedInvoices_'+o.id+'" data-type="anti" name="invoice[]"   value="'+ o.id+'" />'; 
                                    }
                                     
                                    else{
                                        v =  '<input type="checkbox" class="invoiceSelect selectedInvoices_'+o.id+'" data-type="cession" name="invoice[]" value="'+ o.id+'" />'; 
                                    }
                                }
                                if(o.status == "Expired"){
                                    v =  '<input type="checkbox"  disabled readonly class="glyphicon glyphicon-remove"/>'; 
                                }

                            }
                            return v;
                        }
                    },
                    {
                        data:null,
                        "orderable": false,
                        "searchable": false,
                        "defaultContent" : '-',
                        render : function(o){
                            var transaction_page = "";
                            if(o.status == 'Used'){
                                if(o.transaction != null){
                                    transaction_page = '<a href="{{url('admin/transactions/')}}'+'/'+o.transaction.transaction_number+'">'+o.invoice_id+'</a>';
                                    
                                    return transaction_page;
                                }else{
                                    return o.invoice_id; 
                                }
                                    
                            }else{
                                return o.invoice_id;
                            }
                            
                        }
                    },
                    { data: 'customer_name', name: 'customer.name',"searchable" : true,"orderable": false},
                    { data: 'supplier_name', name: 'supplier.name',"searchable": true ,"orderable": false},
                    
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                                var duedate=o.duedate;
                                if(duedate!=null){
                                    var newdate = duedate.replace('-', '/').replace('-', '/');                               
                                    var first_date = moment(newdate).format('DD/MM/YYYY');
                                    return first_date;
                                }else{
                                    var newdate ='';
                                    return newdate
                                }
                            
                        }
                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                                if(o.status == 'Expired'){
                                    return ' - ';
                                }
                                if(o.status == 'Used'){
                                    var anticipation_date = o.anticipation_date ;
                                    if(anticipation_date!=null){
                                        var newdate = anticipation_date.replace('-', '/').replace('-', '/');       
                                        var first_date = moment(newdate).format('DD/MM/YYYY');
                                        return first_date;
                                    }else{
                                        var newdate ='';
                                        return newdate
                                    }
                                }

                                var anticipation_date = anticipationDate;
                                var newdate = anticipation_date.replace('-', '/').replace('-', '/');                               
                                var anti_date = moment(newdate).format('DD/MM/YYYY');
                                return anti_date;
                        }
                    },
                    {
                        data:null,
                        "orderable": false,
                        "searchable": false,
                        render : function(o){
                            if(o.status == 'Expired'){
                                return ' - ';
                            }
                            var anticipation_date = '';
                            if(o.status == 'used'){
                                anticipation_date = o.anticipation_date;
                            }else{
                                anticipation_date = anticipationDate;
                            }
                            return getdueday(o.duedate,anticipation_date);
                        }
                    },
                    { 
                        data: null,
                        "orderable": false,
                        "searchable": false,
                        render : function(o){
                            return o.total_amount;
                                 
                        }
                    },
                    { 
                        data: null,
                        "orderable": false,
                        "searchable": false,
                        render : function(o){
                                var user = "{{Auth::user()->role[0]->name}}";
                                if(o.status == 'Expired'){
                                    return ' - ';
                                }
                                if(o.status == "Used"){
                                    return o.discount_rate + " %";
                                }
                                var max_rate = Math.round( o.max_rate * 10 ) / 10;
                                return max_rate + " % ";
                        }
                    },
                    { 
                        data: null,
                        "orderable": false,
                        "searchable": false,
                        render : function(o){
                            var user = "{{Auth::user()->role[0]->name}}";
                            if(o.status == 'Expired'){
                                return ' - ';
                            }
                            if(o.status == "Used"){
                                var  anticipation_date = o.anticipation_date;
                                var discount_rate = o.discount_rate;
                            }else{
                                var discount_rate = o.max_rate;
                                var anticipation_date = anticipationDate;
                            }
                            
                            var day = getdueday(o.duedate,anticipation_date);
                            var original_value = o.total_amount;
                            var discount_value = (day/30) * original_value * (discount_rate/100);
                            o.discount_value = discount_value;
                            return discount_value.toFixed(2);
                        }
                    },
                    // { 
                    //     data: null,
                    //     "orderable": false,
                    //     "searchable": false,
                    //     "defaultContent":'-',
                    //     render : function(o){
                    //         if(o.status == 'Expired'){
                    //             return ' - ';
                    //         }
                    //         if(o.status == "Used"){
                    //             var original_value = o.total_amount;
                    //             var anticipation_date = o.anticipation_date;
                    //             var day = getdueday(o.duedate,anticipation_date);
                    //             var bank_rate = o.bank_rate;
                    //             var cost = (day/30) * original_value * (bank_rate/100);
                    //             return cost.toFixed(2); 

                    //         }else{
                    //             var cost = 0;
                    //             return cost.toFixed(2); 
                    //         }
                               
                    //     }
                    // },
                    { 
                        data: null,
                        "orderable": false,
                        "searchable": false,
                        render : function(o){
                            var user = "{{Auth::user()->role[0]->name}}";
                            if(o.status == 'Expired'){
                                return ' - ';
                            }

                            if(o.status == "Used"){
                                var original_value = o.total_amount;
                                var anticipation_date = o.anticipation_date;
                                var day = getdueday(o.duedate,anticipation_date);
                                var discount_rate = o.discount_rate;
                                var discount_value = (day/30) * original_value* (discount_rate/100);
                                var net_value = original_value - discount_value;
                                return net_value.toFixed(2);


                            }else{
                                var original_value = o.total_amount;
                                var anticipation_date = anticipationDate;
                                var day = getdueday(o.duedate,anticipation_date);
                                var discount_rate = o.max_rate;
                                var discount_value = (day/30) * original_value* (discount_rate/100);
                                var net_value = original_value - discount_value;
                                return net_value.toFixed(2);

                            }
                            
                        }
                    },
                    { data: 'status', name: 'Status',"orderable": false,"searchable": true },
                ]
        }).columns.adjust()
        .responsive.recalc();

    $(document).on('keydown','#negotiate_rate',function(e) {
        if($('#customer_change').val() == null || $('#customer_change').val() == 0){
            alert("Please select the Customer");
            return false;
            
        }
        if($('#supplier_change').val() == null || $('#supplier_change').val() == 0){
            alert("Please select the Supplier");
            return false;
        } 
        if(e.keyCode == 110 || e.keyCode == 190){
            
            return true;
        }
        if(!((e.keyCode > 95 && e.keyCode < 106)
        || (e.keyCode > 47 && e.keyCode < 58) 
        || e.keyCode == 8 )) {
            return false;
        }

    });

    var cession_count = 0;
    $( ".invoiceSelect" ).live( "click", function() {
       
        var inputSelectInvoice = $(this);
        var user = "{{Auth::user()->role[0]->name}}";
        var user_access = "{{Auth::user()->can('access.transaction.commercialuser.dashboard')}}";
        var data = datatable.row( $(this).parents('tr') ).data();
        var customerId = data.customer_id;
        var supplierId = data.supplier_id;
        var cust_bank_cost = $('#cust_bank_cost').val();
        //If User Does not Change customer or supplier or does not select the supplier or customer then 
        var url =  "{{ url('/admin/customerdata') }}";
        jQuery.ajax({
            url: "{!!url('/admin/customerdata/customer')!!}",
            method: 'get',
            type : 'get',
            data: {
                supplierid: supplierId,
                customerid : customerId
            },
            success: function(result) {
                result = JSON.parse(result);
                if(result != null){
                    if(user_access != 1){
                        $('#min').val(result.min);
                        $('#max').val(result.max);
                        $('.cash').val(result.cash.toFixed(2));
                        //Here the rate_class will store the min discount_rate value
                        $('#rate_class').val(result.min);
                        // selected customer's bank cost rate 
                       
                        $('#customer_change').val(customerId);
                        $('#supplier_change').val(supplierId);
                        
                    }

                        if( result.expiry_date < today ){ 
                            $('.cash').addClass('expired');
                            $('.cash').prop('title', 'Expired');
                        }else{
                            $('.cash').removeClass('expired');
                            $('.cash').prop('title', 'Available Cash');
                        }

                    var favorite = 0;
                    var click_val = $(this).attr('data-type');
                    var flag_check = 0;
            
                    if(this.checked){
                        flag_check = 1;
                    }
                    
                    var cal_total_amount = 0;
                    var cal_discount = 0;
                    var cal_cost = 0;
                    var cal_net_value = 0 ;
                    var check_length = $("input[name='invoice[]']:checked").length;
                    $('#calculation').html('');
                    $('#type').val(""); 
                    var cal_discounted_value = 0;
                    var invoice_array = [];
                    $.each($("input[name='invoice[]']:checked"), function(){  
                        invoice_array.push($(this).val());
                        favorite = 1;
                        var data = datatable.row( $(this).parents('tr') ).data();
                        cal_total_amount = cal_total_amount + data.total_amount; 
                        cal_discount = cal_discount + data.discount_value;
                        cal_discounted_value = cal_discounted_value + data.total_amount -data.discount_value;
                    });
                    if(cal_total_amount != null || cal_total_amount != 0 || cal_total_amount != null || cal_total_amount != 0){
                        var cash = $('.cash').val();
                        var bank_cash = $("#cust_bank_amount").val();
                        var total = cal_total_amount;
                        var rest_amount = ''; 

                        
                        if( total > cash || ( result.expiry_date < today  )  ){ 
                            var confirm_cession = true;
                           
                            if(cession_count == 0){
                                confirm_cession = confirm("No availability from customer. Want to proceed with a cession?");
                            }
                                
                            /*
                            $('#type').val("Cession"); 
                            cal_net_value = cal_total_amount - cal_discount - cal_cost;
                            rest_amount = bank_cash - total;
            
                            if(user == 'CBC' )
                                $('#calculation').append("<td>Cession</td><td>" + cal_total_amount.toFixed(2) + "</td><td>"+cal_discount.toFixed(2)+"</td><td>"+cal_cost.toFixed(2)+"</td><td>"+cal_net_value.toFixed(2)+"</td><td>"+cash+"</td><td>"+bank_cash+"</td><td>"+rest_amount.toFixed(2)+"</td>");
                            else
                                $('#calculation').append("<td>Cession</td><td>" + cal_total_amount.toFixed(2) + "</td><td>"+cal_discount.toFixed(2)+"</td><td>"+cal_cost.toFixed(2)+"</td><td>"+cal_net_value.toFixed(2)+"</td>");
                                */
                            if(confirm_cession == true){
                                cession_count++;

                                jQuery.ajax({
                                    url: "{!!url('/admin/customerbankdata')!!}",
                                    method: 'get',
                                    type : 'get',
                                    data: {
                                        supplierid : data.supplier_id,
                                        customerid : data.customer_id,
                                        total_amount : cal_total_amount

                                    },
                                    success: function(data) {
                                        data = JSON.parse(data);
                                        
                                        if(data != null){
                                            $('#cust_bank_cost').val(data.bank_rate);
                                            cust_bank_cost = data.bank_rate;
                                            // selected customer's bank with minimum cost rate 
                                            $('#cust_bank_id').val(data.bank_id);
                                            $("#cust_bank_amount").val(data.bankAmount.toFixed(2));
                                            $("#bank_name").show();
                                            var bank_name = data.bank_name;
                                            bank_cash = data.bankAmount.toFixed(2);
                                            cal_total_amount = 0;
                                            cal_discount = 0;
                                            cal_discounted_value = 0;
                                            if(data.bank_id == 0){ 
                                                $("#bank_name").hide();
                                                $("input[name='invoice[]']:checked").attr('checked', false)
                                                //inputSelectInvoice.attr('checked', false);
                                                alert("This invoice cannot be added to this transaction as there's no more availability from Bank. Execute this transaction and try again");
                                                return false;
                                               
                                            }
                                            var cal_cost = 0 
                                            $.each($("input[name='invoice[]']:checked"), function(){   
                                                favorite = 1;
                                                var data = datatable.row( $(this).parents('tr') ).data();
                                                cal_total_amount = cal_total_amount + data.total_amount; 
                                                
                                                //calculate cost
                                                cat_discount_rate = data.max_rate;
                                                
                                                var day = getdueday(data.duedate,anticipationDate);

                                                var cat_discount_value = data.total_amount*(cat_discount_rate/100)*(day/30);

                                                cal_discount = cal_discount + cat_discount_value;

                                                var cost = ((data.total_amount*(1-(cat_discount_rate/100)/30*day)*(cust_bank_cost/100)/30*day)/(1-(cust_bank_cost/100)/30*day)) ;

                                                cal_cost = cal_cost + cost;

                                                discounted_value = data.total_amount * (1 - (cat_discount_rate/100)/30*day) / (1-(cust_bank_cost/100)/30*day);
                                                cal_discounted_value = cal_discounted_value + discounted_value;
                                                
                                            });
                                            $('#type').val("Cession"); 
                                            cal_net_value = cal_total_amount - cal_discount ;
                                            rest_amount = bank_cash - total;
                                            var customer_available_cash = cash - cal_total_amount ;
                                            $("#discounted_value").val(cal_discounted_value.toFixed(2));
                                            if($('#type').val() == "Cession" ){
                                                $('#cost').show();  // <td>"+cal_cost.toFixed(2)+"</td>
                                                $('#bank_availe_cash').show(); //<td>"+bank_cash+"</td>
                                                $('#remain_bank_cash').show(); //<td>"+rest_amount.toFixed(2)+"</td>
                                                $('#net_cash').hide(); //<td>"+customer_available_cash+"</td>

                                            }
                            
                                            if(user == 'CBC' )
                                                $('#calculation').append("<td>Cession</td><td>"+cal_total_amount.toFixed(2) + "</td><td>"+cal_discount.toFixed(2)+"</td><td>"+cal_net_value.toFixed(2)+"</td><td>"+bank_name+"</td><td>"+cal_cost.toFixed(2)+"</td><td>"+bank_cash+"</td><td>"+rest_amount.toFixed(2)+"</td>");
                                            else
                                                $('#calculation').append("<td>Cession</td><td>" + cal_total_amount.toFixed(2) + "</td><td>"+cal_discount.toFixed(2)+"</td><td>"+cal_net_value.toFixed(2)+"</td><td>"+data.bank_name+"</td><td>"+cal_cost.toFixed(2)+"</td>"); 
                                        }
                                    }
                                });
                            }    
            
                        }else{

                            $('#type').val("Anticipation");
                            // if type = "Anticipation" hide the fields
                            if($('#type').val() == "Anticipation" ){
                                $('#cost').hide();  // <td>"+cal_cost.toFixed(2)+"</td>
                                $('#bank_availe_cash').hide(); //<td>"+bank_cash+"</td>
                                $('#remain_bank_cash').hide(); //<td>"+rest_amount.toFixed(2)+"</td>
                                $('#net_cash').show();
                            }
                            cal_cost = 0.00 ;
                            cal_net_value = cal_total_amount - cal_discount ;
                            rest_amount = cash - total;
                            var customer_available_cash = cash - cal_total_amount ;
                            $("#discounted_value").val(cal_discounted_value.toFixed(2));
                            if(user == 'CBC'  )
                                $('#calculation').append("<td>Anticipation</td><td>" + cal_total_amount.toFixed(2) + "</td><td>"+cal_discount.toFixed(2)+"</td><td>"+cal_net_value.toFixed(2)+"</td><td>"+customer_available_cash+"</td>");
                            else
                                $('#calculation').append("<td>Anticipation</td><td>" + cal_total_amount.toFixed(2) + "</td><td>"+cal_discount.toFixed(2)+"</td><td>"+cal_net_value.toFixed(2)+"</td>");
                            
                        }
            
                    }
            
                    if(favorite){
                        $('#transactionDetail').show()
                    }else{
                        $('#transactionDetail').hide();
                    }

                }
            }
            }); 

            if(user_access == 1){

                if(user == "CBC"){

                    if($('#supplier_change').val() == null || $('#supplier_change').val() == 0){
                        alert("Please select the Supplier");
                        return false;
                    }
                    if($('#customer_change').val() == null || $('#customer_change').val() == 0){
                        alert("Please select the Customer");
                        return false;
                        
                    }

                }else{
                
                    if($('#customer_change').val() == null || $('#customer_change').val() == 0){
                        alert("Please select the Customer");
                        return false;
                        
                    }
                    if($('#supplier_change').val() == null || $('#supplier_change').val() == 0){
                        alert("Please select the Supplier");
                        return false;
                    } 
                }
                    
                    var cust =  $('#customer_change').val();
                    var sup =  $('#supplier_change').val();
                    var data = datatable.row( $(this).parents('tr') ).data();
                    if(cust != data.customer_id){
                        alert("Select the same Customer for invoices");
                        return false;
                        exit;
                    }
                    if(sup != data.supplier_id){
                        alert("Select the same Supplier for invoices");
                        return false;
                        exit;
                    }
                
            }

 
    });
    
    $(document).on('change', '#customer_change', function () {
        var user = '{{Auth::user()->roles[0]->name}}';
        if(user == "CBC"){

            var supplierId = ($('#supplier_change').val());
            var customerId = ($('#customer_change').val());
            $('#negotiate_rate').attr('data-supplierid', supplierId);
            $('#negotiate_rate').attr('data-customerid', customerId);
            $('#rate_class').attr('data-supplierid', supplierId);
            $('#rate_class').attr('data-customerid', customerId);
    
            jQuery.ajax({
                url: "{!!url('/admin/customerdata/customer')!!}",
                method: 'get',
                type : 'get',
                data: {
                    supplierid: supplierId,
                    customerid : customerId
                },
                success: function(data) {
                    data = JSON.parse(data);
                    
                    if(data != null){
                        $('#min').val(data.min);
                        $('#max').val(data.max);
                        //$('.cash').val(0);
                        if(data.cash > 0){
                            $('.cash').val(data.cash.toFixed(2));
                        }else{
                            $('.cash').val(data.cash);
                        }
                        //Here the rate_class will store the min discount_rate value
                        $('#rate_class').val(data.min);
                        if( data.expiry_date < today ){ 
                            $('.cash').addClass('expired');
                            $('.cash').prop('title', 'Expired');
                        }else{
                            $('.cash').removeClass('expired');
                            $('.cash').prop('title', 'Available');
                        }
                    }
                }
                });
                

                $('input[type="checkbox"]:checked').each(function() {
                    $(".invoiceSelect").prop("checked", false);
                    $(".invoiceSelect").prop("disabled", false);
                    
                });
                datatable.draw();
                $('#calculation').html('');

        }else{

        var customerId = ($('#customer_change').val());
        jQuery.ajax({
            url: "{!!url('/admin/invoice')!!}",
            method: 'get',
            type : 'get',
            data: {
                customerid : customerId
            },
            success: function(data) {
                data = JSON.parse(data);
                    $('#min').val(0);
                    $('#max').val(0);
                    $('.cash').val(0);
                    //Here the rate_class will store the min discount_rate value
                    $('#rate_class').val(data.min);
                    $("#supplier_change").empty();
                    $("#supplier_change").append('<option value="">Select</option>');
                    $.each(data,function(key,value){
                        $("#supplier_change").append('<option value="'+key+'">'+value+'</option>');
                    });
                }
            });
            //remove hidden fields value on customer change
            $('#min').val("");
            $('#max').val("");
            $('.cash').val("");
            $('#rate_class').val("");
            $('#cust_bank_cost').val("");
            $('#cust_bank_id').val("");
            $("#cust_bank_amount").val("");
            $("#negotiate_rate").val(0);
            $('#calculation').html('');
        
            
            if(user == "SLO" || user == "SLA"){
                datatable.draw();
            }
        }
            

    });
    
    $(document).on('change', '#supplier_change', function () {

        if(user == "CBC"){

            var supplierId = ($('#supplier_change').val());
            jQuery.ajax({
                url: "{!!url('/admin/invoice')!!}",
                method: 'get',
                type : 'get',
                data: {
                    supplierid : supplierId
                },
                success: function(data) {
                    data = JSON.parse(data);
                        $('#min').val(0);
                        $('#max').val(0);
                        $('.cash').val(0);
                        //Here the rate_class will store the min discount_rate value
                        $('#rate_class').val(data.min);
                        $("#customer_change").empty();
                        $("#customer_change").append('<option value="">Select</option>');
                        $.each(data,function(key,value){
                            $("#customer_change").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                });
                //remove hidden fields value on customer change
                $('#min').val("");
                $('#max').val("");
                $('.cash').val("");
                $('#rate_class').val("");
                $('#cust_bank_cost').val("");
                $('#cust_bank_id').val("");
                $("#cust_bank_amount").val("");
                $("#negotiate_rate").val(0);
                $('#calculation').html('');

                datatable.draw();

        }else{
            
            var supplierId = ($('#supplier_change').val());
            var customerId = ($('#customer_change').val());
            $('#negotiate_rate').attr('data-supplierid', supplierId);
            $('#negotiate_rate').attr('data-customerid', customerId);
            $('#rate_class').attr('data-supplierid', supplierId);
            $('#rate_class').attr('data-customerid', customerId);

            jQuery.ajax({
                url: "{!!url('/admin/customerdata/customer')!!}",
                method: 'get',
                type : 'get',
                data: {
                    supplierid: supplierId,
                    customerid : customerId
                },
                success: function(data) {
                    data = JSON.parse(data);
                    
                    if(data != null){
                        $('#min').val(data.min);
                        $('#max').val(data.max);
                        //$('.cash').val(0);
                        if(data.cash > 0){
                            $('.cash').val(data.cash.toFixed(2));
                        }else{
                            $('.cash').val(data.cash);
                        }
                        //Here the rate_class will store the min discount_rate value
                        $('#rate_class').val(data.min);
                        // selected customer's bank cost rate 
                        // $('#cust_bank_cost').val(data.bank_cost);
                        // // selected customer's bank with minimum cost rate 
                        // $('#cust_bank_id').val(data.bank_id);
                        // $("#cust_bank_amount").val(data.total_bankCash.toFixed(2))
                        if( data.expiry_date < today ){ 
                            $('.cash').addClass('expired');
                            $('.cash').prop('title', 'Expired');
                        }else{
                            $('.cash').removeClass('expired');
                            $('.cash').prop('title', 'Available');
                        }
                    }
                }
                });
                
                

                $('input[type="checkbox"]:checked').each(function() {
                    $(".invoiceSelect").prop("checked", false);
                    $(".invoiceSelect").prop("disabled", false);
                    
                });
                datatable.draw();
                $('#calculation').html('');
        }

    });


    $(document).on('focusout','#negotiate_rate',function(e){
        var user = '{{Auth::user()->roles[0]->name}}';
        var value = $(this).val();
        if(value == 0){
            datatable.draw();
            return true;
        }
        $('#anticipation').html('');
        $('#cession').html('');
        if(value > '0'){
            var min = $('#min').val();
            var max = $('#max').val();
            if(value < min ){
                alert('Negotiate rate must be between the Minimum & Maximum Discount rate');
                $(this).val(0)
                return false;
            }
            if( max < value){
                alert('Negotiate rate must be between the Minimum & Maximum Discount rate');
                return false;
            }
        }else{
            alert('Please Enter Negotiate Rate');
            return false;
        }
        var invoicesSelected = [];
        $.each($("input[name='invoice[]']:checked"), function(){
             invoicesSelected.push( $(this).val() );
             $(this).attr("checked","false")
        });
        
        datatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            var data = this.data();
            data['max_rate'] = value;
            this.row(rowIdx).data(data);   
        }); 
    });

    $(document).on('click', '#CreateTransaction', function (e) {
        var user = "{{Auth::user()->roles[0]->name}}";
        var r = confirm("Are You To Sure Convert Invoice into Transaction?");
        var company = 0;
        var checkCompany = 0;
        var supplier = 0;
        var checkSupplier = 0;
        var negotiationRate = $('#negotiate_rate').val();
        var discount_rate = 0;
        var bank_rate_from_invoice_data = 0;

        if(negotiationRate != '0'){
            var min = $('#min').val();
            var max = $('#max').val();
            if(negotiationRate < min || negotiationRate > max){
                alert('Negaotiate rate must be between the Minimum & Maximum Discount rate');
                return false;
            }
        }
        
        var checkedValues = $("input[name='invoice[]']:checked").map(function() {
            var data = datatable.row( $(this).parents('tr') ).data();
            //For Discount rate when negotiate rate is not entered
            discount_rate = data.max_rate ;
            // if(data.type =="Cession"){
            // //For bank rate
            //  bank_rate_from_invoice_data = data.bank_cost[0].bank_rate; 
            // }

            if(company == 0){
                company = data.customer_id;
                supplier = data.supplier_id;
                
            }else if(company != data.customer_id){
                checkCompany = 1
            }else if(supplier != data.supplier_id){
                checkSupplier = 1
            }
            return this.value;
        }).get();
        
        
        if(checkCompany){
            alert('Please Select the Invoice with the Same Selected Customer');
            return false;
            exit;
        }
        if(checkSupplier){
            alert('Please Select the Invoice with the Same Selected Supplier');
            return false;
            exit;
        }
        if (checkedValues.length === 0) {
            alert('Please Select atleast One Invoice to Create Transaction!');
            return false;
            exit;
        }

        var cal_row = document.getElementById("summary").rows[1].innerHTML ;
        
        var total_amount = 0;
        var discount= 0;
        var net_value = 0; 
        var cost = 0;

        total_amount = document.getElementById("summary").rows[1].cells[1].innerHTML;
        discount = document.getElementById("summary").rows[1].cells[2].innerHTML;
        net_value  = document.getElementById("summary").rows[1].cells[3].innerHTML;

        var trans_type = $('#type').val();
        if(trans_type == "Cession"){
             cost  = document.getElementById("summary").rows[1].cells[5].innerHTML;
        }
        
        var formData = {
            
                'invoice': checkedValues, //for get email 
                '_token':'<?php echo csrf_token();?>',
                'ajax' : '1',
                'company_id' : company,
                'supplier_id' : supplier,
                'discount_rate' : discount_rate,

        };
    
        if($('#negotiate_rate').attr('data-supplierid') == supplier && $('#negotiate_rate').attr('data-customerid') == company){
            formData.negotiate_rate = negotiationRate;
        }else{
            formData.negotiate_rate = 0;
        }
        var bank_rate =  $('#cust_bank_cost').val();
        formData.bank_rate = bank_rate;
        
        formData.rate_class_min = $('#rate_class').val();
        formData.type = trans_type;
        
        // we have already checked for amount availability from customer availble cash & Bank Available cash
        // var custAvailAmount = $('#cust_bank_amount').val();
        // var bankAvailAmount = $('#bank_cash').val();


        // if(trans_type == 'Anticipation'){
        //     if(Number(total_amount) > Number(custAvailAmount)){
        //         alert('Customer does not have any amount to pay. Transaction can not be generate');
        //         return false;
        //     }
        // }
        // if(trans_type == 'Cession'){
        //     if(Number(total_amount) > Number(bankAvailAmount)){
        //         alert('Bank does not have any amount to pay. Transaction can not be generate');
        //         return false;
        //     }
        // }

        formData.bank_id = $('#cust_bank_id').val();
        formData.total_amount = total_amount;
        formData.discount = discount;
        formData.cost = cost;
        formData.net_value = net_value;
        formData.discounted_value = $('#discounted_value').val();
        //Here the rate_class will store the min discount_rate value
        // selected customer's bank cost rate 
        //formData.bank_rate = data.bank_cost;
        // selected customer's bank with minimum cost rate 
        //formData.bank_id = data.bank_id;
        var url = "{{ url('/admin/transactions/') }}";

        if (r == true) {
            $.ajax({
                type: "post",
                url: url ,
                dataType: 'JSON',
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                data:formData,
                success: function (result) {
                    if(result.msg == 'success'){
                        var tid = result.transaction_id;
                        alert('Transaction Create Successfully');
                        window.location.href = "{{url('/admin/transactions')}}"+"/"+tid;
                    }else if(result.msg == 'error'){
                        alert('Some Error, Try after some time!');
                    }
                },
                error: function (xhr, status, error) {
                }
            });
        }
    
    }); 

    $("#daysdue").focusout(function(){
         datatable.draw();
    })
    $('#changedate').click(function(){
         datatable.draw();
    });
    $(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    })
</script>
@endpush