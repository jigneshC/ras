
<!-- Add  Customer Class Modal -->

<div class="modal fade" id="classAdd_customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="account_form_model_lable">@lang('customer.add_class_supplier_to_customer')</h5> 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="bidder_list">
                        <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                            
                            <div class=" clearfix details-container details-port-container">
                                <form method="post" id="class_add_form_customer" name="formm">
                                        {{csrf_field()}} 
                                    <input  name="customer_supplier_id"  id="discount_customer_supplier_id" type="hidden" value="0" >
                                    <input  name="customer_id"  id="discount_customer_id" type="hidden" value="{{$customer_id}}" >
                                    <input  name="supplier_id"  id="discount_supplier_id" type="hidden" value="" >
                                    
                                    <div class="form-group prepend-top">
                                        <div class="row">
                                            <div class="col-md-12">
    
                                                <label class="pull-left required" for="Projects_title">@lang('customer.customer_name')</label>
                                                <p>: {{$customer->name}}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="pull-left required" for="Projects_title">@lang('customer.supplier_name')</label>
                                                <p id="discount_supplier_name">Supplier Name</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="pull-left required" for="Projects_title">@lang('customer.select_class')</label>
                                                {{--
                                                {!! Form::select('class',$discountRate  ,null, ['class' => 'form-control', 'required' => 'required','id'=>'SelectClass']) !!}
                                                --}}
                                                @if(count($customer_discount_rate)>0)
                                                <select name="class" class="form-control" required id="SelectClass">
                                                    @foreach($customer_discount_rate as $rate)
                                                        <option value="{{$rate->id}}">{{$rate->class}} ( {{$rate->min}}% to {{$rate->max}}% )</option>
                                                    @endforeach
                                                </select>
                                                @else
                                                    <p> : No Discount Rate class Found</p>
                                                @endif
                                                   <p id="discountRate"></p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group prepend-top">
                                        <div class="row">
                                            <div class="col-md-4">
                                                    @if(count($customer_discount_rate)>0)
                                                        <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="Cash Add">
                                                        @lang('customer.create')
                                                    @endif
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Customer Class Modal Close-->
    @push('script-head')
    <script>
    $( document ).ready(function() {
        var value = $('#customer_id').val();
        var url = "{{url('admin/discount-rate')}}"+"?customer_id="+value;
        var element = $('discountRate');
        $('#discountRate').html($("<a class='btn btn-read btn-inverted'></a>").attr("href", url).text('Click to Add Discount Rate Class'));
    });

    

    </script>
    @endpush