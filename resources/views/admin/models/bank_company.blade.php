<!-- Add Bank Company Modal -->
<div class="modal fade" id="company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('bank.add_cession_amount')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="bank_company_form" >
                                {{csrf_field()}}
                                
                                <input  name="bank_id"  id="bank_id" type="hidden" value="{{$bank->id}}" >

                                <input  name="customer_id" id="customer_id"  type="hidden" value="" >
                                
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required " >@lang('bank.from') </label>
                                            <input type="text" readonly class=" form-control clearform-control-plaintext" id="staticEmail" value="{{$bank->name}}">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" >@lang('bank.to')</label>
                                            <input class="form-control form-control clear"  type="text" disabled="disabled" value="" name="name" id="name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                                <label class="pull-left required">@lang('bank.rate')</label>
                                                <input class="form-control form-control clear"  step="0.01" type="number" value="" name="bank_rate" id="bank_rate" required="required">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required " >@lang('bank.current_cession_availability') </label>
                                            <input type="text" readonly class=" form-control clearform-control-plaintext" id="availAmount" value="">
                                        </div>
                                            <input type="hidden"  value="cession" name="type" >
                                        {{--  <div class="col-md-6">
                                            <label class="pull-left required">@lang('bank.cpnj')</label>
                                            <input class="form-control form-control clear" type="text"  disabled value="" name="cpnj" id="cpnj">
                                        </div>  --}}
                                    </div>
                                </div>
                                
                                
                                {{--  <div class="form-group prepend-top">
                                    <div class="row">
                                        {{--  <div class="col-md-6">
                                            <label class="pull-left required" >@lang('bank.address')</label>
                                            <textarea class="form-control autoresized-textarea clear" rows="2" name="address" id="address" disabled ></textarea>
                                        </div>  --}}
                                        {{--  <div class="col-md-6">
                                                <label class="pull-left required">@lang('bank.rate')</label>
                                                <input class="form-control form-control clear" type="text" value="" name="bank_rate" id="bank_rate" required="required">
                                        </div> -}}
                                    </div>
                                </div>  --}}

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" >@lang('bank.amounttobeadded')</label>
                                            <input class="form-control form-control clear"  step="0.01" type="number"  value="" name="total_amount" id="total_amount" required="required" >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" >@lang('bank.due_date')</label>
                                            <input class="form-control form-control clear"  type="text" value="" name="duedate" id="duedate" required="required">
                                        </div>
                                        {{--  <div class="col-md-6">
                                            <label class="pull-left required">@lang('bank.date')</label>
                                            <input class="form-control form-control clear" type="text" value="" name="date" id="date" required="required">
                                        </div>  --}}
                                    </div>
                                </div>

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        
                                         <div class="col-md-6">
                                            <label class="pull-left required">@lang('bank.accounts')</label>
                                            {!!Form::select('accounts',$bank_account,'null',array('class'=>'form-control','id'=>'accounts'));!!}
                                            
                                        
                                        </div>  
                                         <div class="col-md-6">
                                            <label class="pull-left required">@lang('bank.contacts')</label>
                                            {!!Form::select('contacts',$bank_contact,'null',array('class'=>'form-control','id'=>'contacts'));!!}
                                            
                                            
                                        
                                        </div>  
                                    </div>
                                </div>


                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted" id="submit_bank_company_form"
                                            type="submit" name="submit" value="submit">
                                                @lang('bank.create')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Bank Company Modal Close-->

@push('js')
<script>

$(function () {
        /*$('#date').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });*/
        $('#duedate').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        /*
        $("#date").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#duedate').data("DateTimePicker").minDate(e.date);
        });*/
        
    })


 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>
@endpush