<div class="modal fade" id="contacts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="contact_form_model_lable">@lang('bank.add_bank_contacts')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="bidder_list">
          <div class=" bidder no-padding-left no-padding-right gutter-bottom">

            <div class=" clearfix details-container details-port-container">
              <form method="post" id="bank_contact_form">
                <input  name="bank_id"  id="bank_id" type="hidden" value="{{$bank->id}}" >
                <input  name="bank_contact_id" id="bank_contact_id"  type="hidden" value="" >
                {{csrf_field()}}
                <div class="form-group prepend-top">
                  <div class="row">
                    <div class="col-md-6">
                      <label class="pull-left required" for="Projects_title">@lang('bank.name')</label>
                      <input class="form-control form-control clear" name="name"  id="contact_name" type="text" maxlength="85" value="">
                    </div>
                    <div class="col-md-6">
                      <label class="pull-left required" for="Projects_title">@lang('bank.phone')</label>
                      <input class="form-control form-control clear" name="phone" id="contact_phone" type="text" maxlength="10" placeholder="(00) 0000-0000" value="" onkeypress="return isNumber(event)"  >
                    </div>
                  </div>
                </div>
                <div class="form-group prepend-top">
                  <div class="row">
                    <div class="col-md-6">
                      <label class="pull-left required" for="Projects_title">@lang('bank.mobile')</label>
                      <input class="form-control form-control clear" name="mobile" id="contact_mobile" type="text" maxlength="11" placeholder="(00) 00000-0000" value="" onkeypress="return isNumber(event)" >
                    </div>
                    <div class="col-md-6">
                      <label class="pull-left required" for="Projects_title">@lang('bank.email')</label>
                      <input class="form-control form-control clear" name="email" id="contact_email"  type="text" maxlength="85" value="">
                    </div>
                  </div>
                </div>
                <div class="form-group prepend-top">
                    <div class="row">
                        <div class="col-md-6">
                          <label class="pull-left required" for="Projects_title">@lang('bank.status')</label>
                          <div class="col-md-6">
                            <input type="radio" id="contact_status_active" name="status" value="active" >@lang('bank.active')
                          </div>
                          <input type="radio" id="contact_status_inactive" name="status" value="inactive">@lang('bank.inactive')
                        </div>
                    </div>
                  </div>
                <div class="form-group prepend-top">
                      <label class="pull-left required" for="Projects_proj_desc">@lang('bank.comments')</label>
                      <textarea class="form-control autoresized-textarea clear" rows="3" id="contact_comments" name="comments" value=""></textarea>
                </div>
                <div class="form-group prepend-top">  
                  <div class="row">
                    <div class="col-md-4">
                      <button class="btn btn-read btn-inverted contact_form_submit_button" type="submit" name="submit" value="submit">
                        @lang('bank.create')
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@push('js')
<script>
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
    $('#contact_phone').mask('(00) 0000-0000');
    $('#contact_mobile').mask('(00) 00000-0000');


</script>
@endpush