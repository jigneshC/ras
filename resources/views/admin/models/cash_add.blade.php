<!-- Add Cash Add Modal -->
<div class="modal fade" id="cashAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">@lang('customer.add_cash')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                      
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="cash_add_form" name="form">
                                <input  name="customer_id"  id="customer_id" type="hidden" value="">
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="pull-left cash_history required" for="Projects_title">@lang('customer.cur_avail_cash') : </label> <p id="total_amount"  class="cash_label_text"></p>    
                                        </div>

                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">@lang('customer.cashavailable')</label>
                                            <input class="form-control form-control clear" name="cash" id="add_cash_input"   type="text"  value=""  onkeypress="return isNumber(event)" >
                                        </div>
                                         <div class="col-md-12">
                                            <label class="pull-left cash_history required" for="Projects_title">@lang('customer.cur_expi_date') : </label> <p id="expiry_date" class="cash_label_text"></p>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">@lang('customer.cashexpiry')</label>
                                            <input class="form-control form-control clear" name="cash_expiry" id="cash_expiry" type="text" value="">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="Cash Add">
                                                @lang('customer.create')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Cash Available Modal Close-->
@push('js')
<script>
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


</script>
@endpush