<!-- Add Cash History Modal -->
<div class="modal fade" id="cash_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">@lang('customer.use_of_available_cash_history')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">


                            
                            <div class="table-responsive">
                                <table id="cash-table" class="table table-borderless  datatable responsive">
                                    <thead>
                                        <tr>
                                            <th data-priority="1">@lang('customer.supplier_name')</th>
                                            <th data-priority="2">@lang('customer.action')</th>
                                            <th data-priority="3">@lang('customer.date')</th>
                                            <th data-priority="4">@lang('customer.sales_user')</th>
                                            <th data-priority="5">@lang('customer.amount')</th>
                                            <th data-priority="6">@lang('customer.balance')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Cash History Modal Close-->
