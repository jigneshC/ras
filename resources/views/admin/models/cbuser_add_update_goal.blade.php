<!-- Goal Modal -->
<div class="modal fade" id="changeGoal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">@lang('user.label.changegoaluser')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="change_goal_form" name="form">
                                <input  name="cbuser_id"  id="cbuser_id" type="hidden" value="0" >
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">@lang('user.label.username') : </label>
                                            <p id="cbUSerName">45454</p>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">@lang('user.label.changeamount') : </label>
                                            <input class="form-control form-control clear" name="goalAmount" id="goalAmount" type="text" maxlength="85" value="">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="@lang('user.label.changegoal')">
                                                @lang('user.label.changegoal')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Goal Modal Close-->
