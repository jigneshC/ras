<!-- Manage Supplier Log Modal -->
<div class="modal fade" id="supplier_log" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">@lang('supplier.suppliers_log')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            
                           <div class="table-responsive">
                            <table class="table table-borderless" id="log-table">
                                <thead>
                                    <tr>
                                        
                                        <th data-priority="1">@lang('supplier.supplier')</th>
                                        <th data-priority="2">@lang('supplier.commercial_user')</th>
                                        <th data-priority="3">@lang('supplier.user_who_made_change')</th>
                                        <th data-priority="4">@lang('supplier.date')</th>
                                        
                                    </tr>
                                </thead>
                            </table>
                            
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Manage Supplier Log Modal Close-->
@push('script-head')
<script>

</script>
@endpush