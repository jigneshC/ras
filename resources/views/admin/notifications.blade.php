@extends('layouts.backend')

@section('title',"Notifications")


@section('pageTitle')
    <i class="icon-tint"></i>
    <span>Notifications</span>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i>Notifications
                </div>
            </div>
            <div class="panel-body">
                    <div class="row">

                        <div class="table-responsive">
                            <table class="table table-borderless">
                               
                                <tbody>
                                @foreach($notifications as $notification)
                                    <tr>
                                        <td>
                                           
                                        @if($notification->type == "App\Notifications\AddBank")
                
                                                <a href="{{url('/admin/banks')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-bank'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                                                            A New Bank "{{$notification->data['bank_name']}}" is added by {{$notification->data['added_by']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                            
                                        @if($notification->type == "App\Notifications\AddBankAccounts")
                                            
                                                <a href="{{url('/admin/bank/account/'.$notification->data['bank_id'])}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-bank'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            A New Bank Account "{{$notification->data['bank_account_number']}}" is added for the  {{$notification->data['bank_name']}} Bank
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\AddBankCompany")
                                            
                                                <a href="{{url('/admin/bank/company/'.$notification->data['bank_id'])}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-bank'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            "{{$notification->data['bank_name']}}" Bank added amount of {{$notification->data['amount']}} to {{$notification->data['customer_name']}} Company on {{$notification->data['date']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\AddBankContacts")
                                            
                                                <a href="{{url('/admin/bank/contact/'.$notification->data['bank_id'])}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-bank'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            "{{$notification->data['bank_name']}}" Bank added a new Contact - "{{$notification->data['bank_contact_name']}}"
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\AddCash")
                                            
                                                <a href="#">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-money'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            "{{$notification->data['added_by']}}" added amount of
                                                            {{$notification->data['cash']}} to {{$notification->data['customer_name']}} Company
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                            
                                        @if($notification->type == "App\Notifications\AddCustomer")
                                            
                                                <a href="{{url('/admin/customer')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-users-o'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            {{$notification->data['added_by']}} added a new Company - {{$notification->data['customer_name']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\AddSupplier")
                                            
                                                <a href="{{url('/admin/suppliers')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-cubes'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            {{$notification->data['added_by']}} added a new Supplier - {{$notification->data['supplier_name']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\AddSupplierBank")
                                            
                                                <a href="{{url('/admin/supplier/bank/'.$notification->data['supplier_id'])}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-bank'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            {{$notification->data['added_by']}} added a new Bank - {{$notification->data['bank_name']}} for the Supplier {{$notification->data['supplier_name']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                            
                                        @if($notification->type == "App\Notifications\AddUser")
                                            
                                                <a href="{{url('/admin/users')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-user'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            A new User - "{{$notification->data['user_name']}}" is added as a  {{$notification->data['user_role']}} by {{$notification->data['added_by']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\ChangeCBCommercial")
                                            
                                                <a href="{{url('/admin/suppliers'.$notification->data['supplier_id'].'/logs')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-user'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            CB-Commercial User is Changed from {{$notification->data['old_commercial_user_name']}} to {{$notification->data['new_commercial_user_name']}} for Supplier - {{$notification->data['supplier_name']}} by {{$notification->data['changed_by']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\ChangeCBSales")
                                            
                                                <a href="{{url('/admin/customer/'.$notification->data['customer_id'].'/logs')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-user'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            CB-Sales User is Changed from {{$notification->data['old_sales_user_name']}} to {{$notification->data['new_sales_user_name']}} for Company - {{$notification->data['customer_name']}} by {{$notification->data['changed_by']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\EditBankCPNJ")
                                           
                                                <a href="{{url('/admin/banks')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-bank'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            {{$notification->data['bank_name']}} Bank's CPNJ is changed by {{$notification->data['changed_by']}} from
                                                            {{$notification->data['old_cpnj']}} to {{$notification->data['new_cpnj']}}
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\SupplierCustomer")
                                            
                                                <a href="{{url('/admin/customer/'.$notification->data['customer_id'].'/customersupplier')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-handshake-o'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            A New Relation of Company - "{{$notification->data['customer_name']}}" to Supplier - "{{$notification->data['supplier_name']}}" is added. 
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\TransactionApproved")
                                            
                                                <a href="{{url('/admin/transactions/'.$notification->data['transaction_id'])}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-money'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            Transaction ID - {{$notification->data['transaction_id']}} is approved by - {{$notification->data['approved_by']}} 
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\TransactionCancelled")
                                           
                                                <a href="{{url('/admin/transactions/'.$notification->data['transaction_id'])}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-money'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            Transaction ID - {{$notification->data['transaction_id']}} is cancelled by - {{$notification->data['cancelled_by']}} 
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                            
                                        @if($notification->type == "App\Notifications\UserStatus")
                                            
                                                <a href="{{url('/admin/users')}}">
                                                    <div class='widget-body'>
                                                        <div class='pull-left icon'>
                                                            <i class='fa fa-money'></i>
                                                        </div>
                                                        <div class='pull-left text'>
                            
                                                            "{{$notification->data['user_name']}}'s" status changed to {{$notification->data['user_status']}} by {{$notification->data['changed_by']}} 
                            
                                                            <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            
                                        @endif
                                           
                                        </td>
                                        <td>
                                            @if($notification->read_at == null) 
                                                <input type="checkbox" name="mark" title="Mark As Read" value="{{ $notification->id }}" class="read"/>
                                            @else
                                                <input type="checkbox" name="mark"  title="Mark As Unread" checked value="{{ $notification->id }}" class="unread" /> 
                                            @endif
                                        </td>
                                        <td>
                                            <a class="action-notification-dismiss pull-right" data-toggle="tooltip" title="Delete" href="{!! url('/admin/notifications/'.$notification->id) !!}?notification={{$notification->id}}"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $notifications->links() !!} </div>
                        </div>  

                </div>
        </div>
    </div>
</div>



@endsection

@push('js')
<script>
    $('.read').on('change',function() {
            var checked = $(this).is(':checked');
            var id = $(this).val();
            if(this.checked){
            $.ajax({
                type: "GET",
                url:  "{{ url('/admin/markasread') }}",
                data: { id : id}, 
                success: function(data) {
                    data =  JSON.parse(data);
                    if(data.code == 200){
                        toastr.success(data.message)
                        location.reload();
                    }
                },
                 error: function() {
                   // alert('it broke');
                },
 
            });

        }
    });  
    $('.unread').on('change',function() {
            var checked = $(this).is(':checked');
            var id = $(this).val();
            if(!this.checked){
            $.ajax({
                type: "GET",
                url:  "{{ url('/admin/markasunread') }}",
                data: { id : id}, 
                success: function(data) {
                    data =  JSON.parse(data);
                    if(data.code == 200){
                        toastr.success(data.message)
                        location.reload();
                    }
                },
                 error: function() {
                   // alert('it broke');
                },
 
            });

        }
    });  
</script>
@endpush