@extends('layouts.backend')


@section('title',"Settings")

@push('js')
<style>

</style>
@endpush

@section('content')

    <div class="box bordered-box blue-border">
        <div class="box-header blue-background">
            <div class="title">
                <i class="icon-circle-blank"></i>
                System Settings
            </div>

        </div>
        <div class="panel-body">


            <div class="row">

                <div class='box-content box-double-padding'>
                    
                      <form class='form' style='margin-bottom: 0;' method="POST" action="{{ url('admin/system-setting') }}">
                          {{ csrf_field() }}
                        <fieldset>
                          <div class='col-sm-4'>
                            <div class='box'>
                              <div class='lead'>
                                <i class='icon-cog text-contrast'></i>
                                Switch System On/Off
                              </div>
                              <small class='text-muted'>No User Can access Transaction module while system is off.</small>
                            </div>
                          </div>
                          <div class='col-sm-7 col-sm-offset-1'>
                            <div class='form-group'>
                              <label class='checkbox-inline'>
								{{ Form::checkbox('switch_system', 1, $setting['switch_system'], ['class' => '']) }}
                                On/Off 
                              </label>
                            </div>
                          </div>
                        </fieldset>
                          <hr class='hr-normal'>
                          <fieldset>
                          <div class='col-sm-4'>
                            <div class='box'>
                              <div class='lead'>
                                <i class='icon-cog text-contrast'></i>
                                Working Time
                              </div>
                              <small class='text-muted'>User Can access system  transaction module withing working time only.</small>
                            </div>
                          </div>
                          <div class='col-sm-7 col-sm-offset-1'>
                            <div class="form-group">
                              <label>Start Time</label>
                              {!! Form::select('working_start_time',\config('constant.workin_time'),(isset($setting) && isset($setting['working_start_time']))? $setting['working_start_time']:null, ['class' => 'form-control','id'=>'working_start_time']) !!}
                            </div>
                              <div class="form-group">
                              <label>End Time</label>
                              {!! Form::select('working_end_time',\config('constant.workin_time'),(isset($setting) && isset($setting['working_end_time']))? $setting['working_end_time']:null, ['class' => 'form-control','id'=>'working_end_time']) !!}
                            </div>
                          </div>
                        </fieldset>
                        <hr class='hr-normal'>
                        <fieldset>
                          <div class='col-sm-4'>
                            <div class='box'>
                              <div class='lead'>
                                <i class='icon-calendar contrast'></i>
                                Working Days
                              </div>
                              <small class='text-muted'>User Can access system  transaction module in working days of week only.</small>
                            </div>
                          </div>
                          <div class='col-sm-7 col-sm-offset-1'>
                            <div class='form-group'>
                              <label class='checkbox-inline'>
								{{ Form::checkbox('working_day_mon', 1, $setting['working_day_mon'], ['class' => '']) }}
                                Monday
                              </label>
                              <label class='checkbox-inline'>
								{{ Form::checkbox('working_day_tue', 1, $setting['working_day_tue'], ['class' => '']) }}
                                Tuseday
                              </label>
                              <label class='checkbox-inline'>
							  {{ Form::checkbox('working_day_wed', 1, $setting['working_day_wed'], ['class' => '']) }}
                                Wednesday
                              </label>
                                <label class='checkbox-inline'>
								{{ Form::checkbox('working_day_thu', 1, $setting['working_day_thu'], ['class' => '']) }}
                                Thusday
                              </label>
                                <label class='checkbox-inline'>
								{{ Form::checkbox('working_day_fri', 1, $setting['working_day_fri'], ['class' => '']) }}
                                Friday
                              </label>
                                <label class='checkbox-inline'>
								{{ Form::checkbox('working_day_sat', 1, $setting['working_day_sat'], ['class' => '']) }}
                                Saturday
                              </label>
                                <label class='checkbox-inline'>
								{{ Form::checkbox('working_day_sun', 1, $setting['working_day_sun'], ['class' => '']) }}
                                Sunday
                              </label>
                            </div>
                            
                          </div>
                        </fieldset>
                        
                        
                        
                        <div class='form-actions'>
                          <div class='row'>
                            <div class='col-sm-7 col-sm-offset-5'>
                              <button class="btn btn-primary" type="submit">
                                        <i class="icon-save"></i>
                                        Save 
                                      </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
            </div>


           <div class="row">

                    <hr class='hr-normal'>
                     
                        <fieldset>
                          <div class='col-sm-4'>
                            <div class='box'>
                              <div class='lead'>
                                <i class='icon-calendar-empty text-contrast'></i>
                                Add or Remove Holiday
                              </div>
                              <small class='text-muted'>User Can access system  transaction module in working days.</small>
                            </div>
                          </div>
                          <div class='col-sm-7 col-sm-offset-1'>
                              
                            <div class='row'>
                                <form class='form' style='margin-bottom: 0;' method="POST" action="{{ url('admin/hollyday') }}">
                                     {{ csrf_field() }}
                          
                                <div class='col-sm-5'>
                                    <input class="form-control" name="name" placeholder="Hollyday Name" id="hollyday_name" type="text">
                                </div>
                                <div class='col-sm-5'>
                                    <input class="form-control" name="holiday_date" placeholder="Select Date" id="holiday_date" type="text">
                                </div>
                                <div class='col-sm-2'>
                                    <button class="btn btn-primary form-control" type="submit">
                                        <i class="icon-save"></i>
                                        Add Hollyday
                                      </button>
                                </div>
                                </form>      
                            </div>
                              <div class='row'>
                                  <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <thead>
                                        <tr>
                                            <th>Holiday</th>
                                            <th>Date</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($hollydays as $item)
                                                <tr>
                                                    <th>{{$item->name}}</th>
                                                    <th>{{$item->holiday_date}}</th>
                                                    <th>
                                                        {!! Form::open([
                                                            'method'=>'DELETE',
                                                            'url' => ['/admin/hollyday', $item->id],
                                                            'style' => 'display:inline'
                                                        ]) !!}
                                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                                'type' => 'submit',
                                                                'class' => 'btn btn-danger btn-xs',
                                                                'title' => 'Delete Item',
                                                                'onclick'=>'return confirm("Confirm delete?")'
                                                        )) !!}
                                                        {!! Form::close() !!}
                                                    </th>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                  
                              </div>
                          </div>
                          </div>
                        </fieldset>
                          
                         
                     
            </div> 
           

        </div>
    </div>

@endsection



@push('js')
<script>

    $(function () {
        $('#holiday_date').datetimepicker({
            format: 'YYYY-MM-DD',
            
        });
        
    });
</script>


@endpush
