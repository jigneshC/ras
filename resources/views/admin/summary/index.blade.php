@extends('layouts.backend')

@section('title',trans('summary.recievable_summary'))
@section('pageTitle',trans('summary.recievable_summary'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('summary.recievable_summary')
                                                  </div>

                               </div>
                               <div class="box-content ">

                                
                                    
                                <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('summary.back')</button></a>
                                
                                <div class="row"> 
                                    <div class="col-md-6">
                                        <label>@lang('summary.period'):</label>
                                        <label>@lang('summary.from'):</label>
                                        <input type="text" name="firstday" id="startdate" value="{{$startdate}}">
                                        <label>@lang('summary.to'):</label>
                                        <input type="text" name="lastday" id="enddate" value="{{$enddate}}">
                                        <button name="submit" value="submit" id="changedate" >@lang('summary.show')</button>
                                    </div>

                                    <div class="col-md-6">
                                    <label>@lang('summary.search_by_supplier')</label>
                                    <input type="text" name="supp_name" id="supp_name" value="">
                                    <button name="submit" value="submit" id="supplier_name" ><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <br/>
                                <br/>

                    @if(\Auth::user()->role[0]->name == "CBC" || \Auth::user()->role[0]->name == "SU")
                        <div class="table-responsive col-md-7">
                            <table class="table bordered-box">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('summary.goals')</th>
                                        <th data-priority="2">@lang('summary.performed')</th>
                                        <th data-priority="3">@lang('summary.percentage')(%)</th>
                                    </tr>
                                </thead>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                
                            </table>
                        </div>
                    @endif

                 

                        <br/>
                        <br/>
                        <div class="table-responsive ">
                            <table id="summary-table" class="table table-borderless datatable responsive">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('summary.supplier')</th>
                                        <th data-priority="2">@lang('summary.total_invoices')</th>
                                        <th data-priority="3">@lang('summary.total_amount')</th>
                                        <th data-priority="4">@lang('summary.anticipated_amount')</th>
                                        <th data-priority="5">@lang('summary.available_for_anticipation')</th>
                                        <th data-priority="6">@lang('summary.mean_rate')(%)</th>
                                        {{--
                                        <th data-priority="7">@lang('summary.minimum_rate')</th>
                                        <th data-priority="8">@lang('summary.maximum_rate')</th>
                                        --}}
                                        <th data-priority="9">@lang('summary.operation_user')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/summary/') }}";
     
        
        
        datatable = $('#summary-table').DataTable({
            processing: true,
            serverSide: true,
            searching : false,
            ajax: 
            {
                url: '{!! route("SummaryControllerSummaryData") !!}', // json datasource
                type: "get", // method , by default get
                data: function (d) {
                    d.startdate = $('#startdate').val();
                    d.enddate = $('#enddate').val();
                    d.supp_name = $('#supp_name').val();
                }
            },
             
                columns: [
                    

                    { data: 'name', name: 'name', "searchable": false },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                            if(o.summary_invoice != "" )
                                    return o.summary_invoice[0].total_invoice;
                            else 
                                    return '-';
                        }
                            
                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.summary_invoice != "" )
                                    return o.summary_invoice[0].invoicesum;
                            else 
                                    return '-';
                        }
                            
                    },

                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.summary_anticipation != "" )
                                    return o.summary_anticipation[0].anticipation;
                            else 
                                    return '-';
                        }
                            
                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.summary_cession != "" )
                                    return o.summary_cession[0].cession;
                            else 
                                    return '-';
                        }
                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.mean_rate){
                                return o.mean_rate+'%';
                            }else{
                                return '-';
                            }
                        }
                    },
                    /*
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.rate){
                                
                                return o.rate.discount_rate.min+'%';
   
                            }else{
                                return '-';
                            }
                        }
                    },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.rate){
                                
                                return o.rate.discount_rate.max+'%';
   
                            }else{
                                return '-';
                            }
                        }
                    },
                    */
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

                            if(o.cbuser != "" )
                                    return o.cbuser[0].user_name;
                            else 
                                    return '-';
                        }
                    },
    
                ]
        });


$('#changedate').click(function(){
    datatable.draw();
});
$('#supplier_name').click(function(){
    datatable.draw();
});


$(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    })



</script>
@endpush