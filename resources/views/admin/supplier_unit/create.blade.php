@extends('layouts.backend')

@section('title',trans('supplier.supplier_unit'))
@section('pageTitle',trans('supplier.create_new_supplier_unit'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                      <div class="box-header blue-background">
                          <div class="title">
                              <i class="icon-circle-blank"></i>
                              @lang('supplier.create_new_supplier_unit')
                          </div>

                      </div>
                      <div class="box-content ">




                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/supplier_unit/', 'class' => 'form-horizontal', 'files' => true,'id'=> 'formSupplier','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.supplier_unit.form')

                        {!! Form::close() !!}




                    </div>
                </div>
            </div>
        </div>
@endsection


