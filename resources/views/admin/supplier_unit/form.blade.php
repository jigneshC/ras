
<div class="form-group{{ $errors->has('supplier_id') ? ' has-error' : ''}}">
        {!! Form::label('supplier_id', trans('supplier.supplier_group_name'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            @if(isset($supplier_id))
                {!! Form::select('supplier_id', $supplier,null, ['class' => 'form-control state selectTag','selected'=>'selected']) !!}
            @else
                {!! Form::select('supplier_id', $supplier,null, ['class' => 'form-control state selectTag']) !!}
            @endif
        </div>
</div>

<div class="form-group {{ $errors->has('unit_name') ? 'has-error' : ''}}">
    {!! Form::label('unit_name',trans('supplier.unit_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('unit_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('unit_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('unit_legal_name') ? 'has-error' : ''}}">
    {!! Form::label('unit_legal_name', trans('supplier.unit_legal_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('unit_legal_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('unit_legal_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cpnj') ? 'has-error' : ''}}">
    {!! Form::label('cpnj', trans('supplier.cpnj'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cpnj', null, ['class' => 'form-control', 'required' => 'required','id'=>'cpf','placeholder' => '000.000.000-00']) !!}
        {!! $errors->first('cpnj', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}}">
    {!! Form::label('zip', trans('supplier.zip'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('zip', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
    </div>
</div>
    
<div class="form-group {{ $errors->has('address_street') ? 'has-error' : ''}}">
    {!! Form::label('address_street',trans('supplier.address_street'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_street', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address_street', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address_number') ? 'has-error' : ''}}">
    {!! Form::label('address_number',trans('supplier.address_number'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('complement') ? 'has-error' : ''}}">
    {!! Form::label('complement',trans('supplier.complement'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('complement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('complement', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('neighborhood') ? 'has-error' : ''}}">
    {!! Form::label('neighborhood',trans('supplier.neighborhood'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('neighborhood', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('neighborhood', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(Route::currentRouteName() == 'supplier_unit.create')
<div class="form-group{{ $errors->has('country') ? ' has-error' : ''}} ">
    {!! Form::label('country', trans('supplier.country'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
	 {!!Form::select('country',$countries,'null',array('class'=>'form-control','id'=>'country'));!!}
       {!! $errors->first('country', '<p class="help-block">:message</p>') !!}   
     </div>
</div>

<div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
    
    {!! Form::label('state', trans('supplier.state'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('state', ['' => 'Select State'],'null', ['class' => 'form-control  selectTag','id'=>'state']) !!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}

    </div>
</div>

<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}} ">
    {!! Form::label('city', trans('supplier.city'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">
       {!! Form::select('city', ['' => 'Select City'],'null', ['class' => 'form-control  selectTag','id'=>'city']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
    
</div>

@else
<div class="form-group{{ $errors->has('country') ? ' has-error' : ''}} ">
    {!! Form::label('country', trans('supplier.country'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
	 {!!Form::select('country',$countries,$country_id,array('class'=>'form-control','id'=>'country'));!!}
       {!! $errors->first('country', '<p class="help-block">:message</p>') !!}   
     </div>
</div>

<div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
    
    {!! Form::label('state', trans('supplier.state'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('state',$states,$state_id, ['class' => 'form-control  selectTag','id'=>'state']) !!}
        
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}

    </div>
</div>

<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}} ">
    {!! Form::label('city', trans('supplier.city'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">
       {!! Form::select('city', $cities,$city_id, ['class' => 'form-control  selectTag','id'=>'city']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
    
</div>

@endif






<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('supplier.create'), ['class' => 'btn btn-primary', 'id' => 'SupplierCreate']) !!}
    </div>
</div>


@push('script-head')

<script type="text/javascript">
    $('#phone').mask('(00) 00000-0000');
    $('#cpf').mask('00.000.000/0000-00');
    $("#formSupplier").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
            cpnj: {
                required: true,
                validateCpf: true
            }
        },
        messages: {
            cpnj: {
                required: "CPNJ is required",
                validateCpf: "Enter Valid CPNJ Code"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod(
        "validateCpf", 
        function(value, element) {
            cnpj = value.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
            
        },
        "Enter Valid CPNJ Code"
    );
    $('#country').change(function(){
    var countryID = $(this).val(); 
    console.log(countryID);   
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
</script>
@endpush