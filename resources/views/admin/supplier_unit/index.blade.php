@extends('layouts.backend')

@section('title',trans('supplier.supplier_unit'))
@section('pageTitle',trans('supplier.supplier_unit'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('supplier.list_of_units_from_supplier') - {{$supplier_data->name}}
                                                  </div>

                               </div>
                               <div class="box-content ">
                                    <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>

                                @if(Auth::user()->can('add.supplier.unit'))
                                    <a href="{{ url('/admin/supplier_unit/create') }}?supplier_id={{$supplier}}" class="btn btn-success btn-sm" title="Add New Supplier Unit">
                                        <i class="fa fa-plus" aria-hidden="true"></i>@lang('supplier.add_new_supplier_unit')
                                    </a>
                                @endif

                        <br/>
                        <br/>
                      
                        <div class="table-responsive">
                            
                            <table id="suppliers-table" class="table table-borderless datatable responsive">
                                <thead>
                                    <tr>
                                        <th data-priority="0">@lang('supplier.id')</th>
                                        <th data-priority="1">@lang('supplier.unit_name')</th>
                                        <th data-priority="2">@lang('supplier.unit_legal_name')</th>
                                        <th data-priority="3">@lang('supplier.cpnj')</th>
                                        <th data-priority="4">@lang('supplier.address')</th>
                                        <th data-priority="5">@lang('supplier.neighborhood')</th>
                                        <th data-priority="6">@lang('supplier.state')</th>
                                        <th data-priority="7">@lang('supplier.city')</th>
                                        <th data-priority="8">@lang('supplier.status')</th>
                                        <th data-priority="9">@lang('supplier.actions')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>


@endsection
@push('script-head')
<script>
var url ="{{ url('admin/supplier-unit-data/') }}/{{$supplier}}";
var supplier_url = "{{ url('admin/supplier_unit/') }}/{{$supplier}}";
var delete_url = "{{ url('admin/supplier_unit/') }}";

//Permission
    var unit_edit = "<?php echo Auth::user()->can('edit.supplier.unit'); ?>";
    var unit_delete = "<?php echo Auth::user()->can('delete.supplier.unit'); ?>";
    var unit_status = "<?php echo Auth::user()->can('change.status.supplier.unit'); ?>";

        datatable = $('#suppliers-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 0, "desc" ]],
            ajax: 
                {
                    url: url ,// json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.status = "{{ $status }}"
                    }
                },
                columns: [
                    { data: 'id',name:'id',visible:false},
                   { data: 'unit_name', name: 'unit_name',"searchable": false },
                   { data: 'unit_legal_name', name: 'unit_legal_name',"searchable": false },
                   { data: 'cpnj', name: 'cpnj',"searchable": false },
                   { 
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        return  o.address_street+ " , "+o.address_number+" , "+o.complement
                        }
                    },
                 //  { data: 'address_number', name: 'address_number',"searchable": false },
                   { data: 'neighborhood', name: 'neighborhood',"searchable": false },
                // { data: 'complement', name: 'complement',"searchable": false },
                   { 
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        return o.state.name
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return o.city.name
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "defaultContent" : '-',
                        "render": function (o) {
                            var status = '';

                            if(unit_status){
                                if(o.status == 'active')
                                    status = "<a href='{{url("admin/supplier_unit")}}/"+o.id+"?status=active' data-id="+o.id+"><button title='Inactive' class='btn btn-warning btn-xs'>@lang('supplier.active')</button></a>&nbsp;";
                                else
                                    status = "<a href='{{url("admin/supplier_unit")}}/"+o.id+"?status=inactive' data-id="+o.id+"><button title='Active' class='btn btn-warning btn-xs'>@lang('supplier.inactive')</button></a>&nbsp;";
                            
                                return status;
                            }else{

                                return o.status;
                            }
                            
                                            
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var edit=""; var d="";

                            if(unit_edit){
                                edit= "<a href='"+supplier_url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            if(unit_delete){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            return edit+d;
                        }
                    }
                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = delete_url + "/" + id;
        var r = confirm("Are you sure you want to delete Supplier ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });



</script>
@endpush