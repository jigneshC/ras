@extends('layouts.backend')

@section('title',trans('supplier.supplier_unit'))
@section('pageTitle',trans('supplier.supplier_unit'))

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               @lang('supplier.supplier_unit') 
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ URL::previous() }}"title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>

                        @if(Auth::user()->can('edit.supplier.unit'))
                            <a href="{{ url('/admin/supplier_unit/' . $supplier_unit->id . '/edit') }}" title="Edit Supplier"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('supplier.edit')</button></a>
                        @endif

                        @if(Auth::user()->can('delete.supplier.unit'))
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/supplier_unit', $supplier->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Supplier Unit',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                            {!! Form::close() !!}
                        @endif
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>@lang('supplier.id')</th><td>{{ $supplierunit->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.unit_name')</th><td> {{ $supplierunit->unit_name }} </td>
                                    </tr>
                                    <tr>
                                            <th>@lang('supplier.unit_legal_name')</th><td> {{ $supplierunit->unit_legal_name }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.cpnj')</th><td> {{ $supplierunit->cpnj }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.neighborhood')</th><td> {{ $supplierunit->neighborhood }} </td>
                                    </tr>
                                    
                                    <tr>
                                        <th> @lang('supplier.country')</th><td> {{ $supplierunit->country }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('supplier.state')</th><td> {{ $supplierunit->state }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('supplier.city')</th><td> {{ $supplierunit->city }} </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
