@extends('layouts.backend')

@section('title',trans('supplier.edit_supplier'))
@section('pageTitle',trans('supplier.edit_supplier'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('supplier.edit_supplier') #{{ $supplier->id }}
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ url('/admin/suppliers') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($supplier, [
                            'method' => 'PATCH',
                            'url' => ['/admin/suppliers', $supplier->id],
                            'class' => 'form-horizontal',
                            'files' => true,
                            'id'=> 'formSupplier',
                            'enctype'=>'multipart/form-data'
                        ]) !!}

                        @include ('admin.suppliers.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script-head')
<script>
$(document).ready(function(){
    $( ".countryOnchange" ).change(function() {
        var countryID = $(this).val();    
        if(countryID){
            $.ajax({
                type:"GET",
                url:"{{url('admin/getstate')}}?country_id="+countryID,
                success:function(res){               
                    if(res){
                        console.log(res);
                        $("#state").empty();
                        $.each(res,function(key,value){
                            $("#state").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else{
                       $("#state").empty();
                    }
                }
            });
        }else{
            $("#state").empty();
        }      
    });
    
});
   
</script>
@endpush
