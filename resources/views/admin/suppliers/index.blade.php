@extends('layouts.backend')

@section('title',trans('supplier.supplier'))
@section('pageTitle',trans('supplier.supplier'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('supplier.suppliers')
                                                  </div>

                               </div>
                               <div class="box-content ">

                        @if(Auth::user()->can('access.suppliers.create'))
                        <a href="{{ url('/admin/suppliers/create') }}" class="btn btn-success btn-sm" title="Add New Supplier">
                            <i class="fa fa-plus" aria-hidden="true"></i>@lang('supplier.add_new')
                        </a>
                        @endif

                        @if(Auth::user()->can('add.supplier.unit'))
                        <a href="{{ url('/admin/supplier_unit/create') }}" class="btn btn-success btn-sm" title="Add New Supplier Unit">
                            <i class="fa fa-plus" aria-hidden="true"></i>@lang('supplier.add_new_supplier_unit')
                        </a>
                        @endif
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table id="suppliers-table" class="table table-borderless datatable responsive">
                                <thead>
                                    <tr>
                                        <th data-priority="0"></th>
                                        <th data-priority="1">#</th>
                                        <th data-priority="2">{{--@lang('supplier.image')--}}</th>
                                        <th data-priority="3">@lang('supplier.supplier_name')</th>
                                        <th data-priority="4">@lang('supplier.total_amount_of_receivables')</th>
                                        <th data-priority="5">@lang('supplier.anticipations_last_30_days')</th>
                                        <th data-priority="6">@lang('supplier.cessions_last_30_days')</th>
                                        <th data-priority="7">@lang('supplier.commercial')</th>
                                        <th data-priority="8">@lang('supplier.cb_rate_class')</th>
                                        <th data-priority="9">@lang('supplier.actions')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include("admin.models.cb_change_commercial_rep")

@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/suppliers/') }}";

var img_path = "{{asset('images')}}";
//Permission
    var supp_bank = "{{Auth::user()->can('access.suppliers.bank.add')}}";
    var edit = "<?php echo Auth::user()->can('access.suppliers.edit'); ?>";
    var supp_delete = "<?php echo Auth::user()->can('access.suppliers.delete'); ?>";
    var supp_cbchange = "<?php echo Auth::user()->can('change.cb.rep.supplier'); ?>";
    var supp_users = "<?php echo Auth::user()->can('access.users'); ?>";
    var supp_units = "<?php echo Auth::user()->can('access.supplier.unit'); ?>";
    var cb_rate_class = "<?php echo Auth::user()->can('access.cbrateclass'); ?>";

        datatable = $('#suppliers-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 0, "desc" ]],
             ajax: '{!! route('SupplierControllerSuppliersData') !!}',
                columns: [
                    { data: 'id',name:'id',visible:false},
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return "<input type='checkbox' />";
                        }
                    },

                    { 
                        "data": null,
                        "name": 'image',
                        "orderable": false,
                        "searchable" : false,
                        "render": function(o){
                            var image = o.image;
                            if(image){

                                return '<img src="'+img_path+'/'+image+'" width="50" height="50"></td>';
                            }
                            else{
                                
                                return '<img src="'+img_path+'/avatar.jpg" width="50" height="50"></td>';
                            }
                        }
                    } ,
                    { data: 'name',name:'supplier.name',},
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoicedue_supplier.length > 0)
                                return o.invoicedue_supplier[0].invoicesum;
                            else 
                                return 0;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoice_anticipastion.length > 0)
                                return o.invoice_anticipastion[0].invoicesum;
                            else 
                                return 0;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoice_cession.length > 0)
                                return o.invoice_cession[0].invoicesum;
                            else 
                                return 0;
                        }
                    },
                    { 
                        data: 'user_name',
                        name : 'users.name',
                        "orderable": false,
                        
                        
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "defaultContent": '-',
                        "render": function (o) {
                            if(cb_rate_class){
                                if(o.cb_rate_class != null)
                                    return o.cb_rate_class.class+" ( "+ o.cb_rate_class.min + "% to "+ o.cb_rate_class.max + "% )";
                                else
                                    return 0;
                            }else{
                                return '-' ;
                            }
                        }
                    },
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            //var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i></button></a>&nbsp;";
                            //  var b =  "<a href='"+url+"/"+o.id+"/bank' data-id="+o.id+"><button class='btn btn-warning btn-xs'> @lang('supplier.bank')</button></a>&nbsp;";

                            var e=""; var d=""; var bank=""; var cbchange=""; var users=""; var units="";

                            if(edit){
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button title='Edit' class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                                
                            if(supp_delete){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button title='Delete' class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            if(supp_bank){
                                bank = "<a href='{{url("admin/supplier/bank")}}/"+o.id+"' data-id="+o.id+"><button title='Accounts' class='btn btn-warning btn-xs'><i class='fa fa-bank' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                                
                            if(supp_cbchange){
                                cbchange =  "<a href='#' class='open_model_for_change_cb_rep' data-suppliername='"+o.name+"' data-supplierid ='"+o.id+"' data-oldcbrep='"+o.user_id+"'><button title='Change CB-Rep'  class='btn btn-warning btn-xs'><i class='fa fa-user' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            
                            if(supp_users){
                                users = "<a href='{{url("admin/users")}}?supplier_id="+o.id+"' data-id="+o.id+"><button title='Users' id='user-list' class='btn btn-info btn-xs '><i class='fa fa-user' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            if(supp_units){
                                units = "<a href='{{url("admin/supplier_unit/list")}}/"+o.id+"' data-id="+o.id+"><button title='Units' class='btn btn-warning btn-xs'><i class='fa fa-sitemap' aria-hidden='true'></i></button></a>&nbsp;";
                            }

                            return e+d+users+bank+cbchange+units;
                        }
                    }
                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Supplier ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


    $(document).on('click', '.open_model_for_change_cb_rep', function (e) {
    $('#supplierChangeCbRep').html($(this).attr('data-suppliername'));
    $('#cbsupplier_id').val($(this).attr('data-supplierid'));
    $('#oldcbrep_id').val($(this).attr('data-oldcbrep'));
    $("select#cbcommercial").val($(this).attr('data-oldcbrep'));
    var formid = $("#change_cb_rep_form");
    $('#changeCbRepresentative').modal('show');
});

$("#change_cb_rep_form").validate({
    rules: {
        cbrepforsupplier: {
            required: true,
        }
    },
    messages: {
        cbrepforsupplier: {
            required: "Select CB Representative for Supplier",
        }
    },
    submitHandler: function (form) {
        var url = "{{url('admin/suppliers/changeCbRep')}}";
        var method = "post"
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {
                result = JSON.parse(result)
                if(result.msg == 'Success')
                    toastr.success('Changed Successfully')
                else
                    toastr.error('Something Went Wrong. Try Again!')
                $('#changeCbRepresentative').modal('hide');
                datatable.draw(); 
            },
            error: function (error) {
                $('#changeCbRepresentative').modal('hide');
                datatable.draw();
            }
        }); 
        return false;
    }
});

</script>
@endpush