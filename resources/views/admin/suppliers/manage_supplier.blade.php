@extends('layouts.backend')

@section('title',trans('supplier.manage_suppliers'))
@section('pageTitle',trans('supplier.manage_suppliers'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('supplier.manage_suppliers')
                                                  </div>

                               </div>
                               <div class="box-content ">
                                <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table id="manage-table" class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th data-priority="1">#</th>
                                        <th data-priority="2">@lang('supplier.supplier_name')</th>
                                        <th data-priority="3">@lang('supplier.total_amount_of_receivables')</th>
                                        <th data-priority="4">@lang('supplier.anticipated_by_company')</th>
                                        <th data-priority="5">@lang('supplier.cession_to_banks')</th>
                                        <th data-priority="6">@lang('supplier.commercial_user')</th>
                                        <th data-priority="7">@lang('supplier.actions')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
{{--
<!-- Logs of change commercial user  -->
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     Logs
                                                  </div>

                               </div>
                               <div class="box-content ">

                        <br/>
                        <br/>
                        @if($sup_id > 0)
                        <div class="table-responsive">
                            <table class="table table-borderless" id="log-table">
                                <thead>
                                    <tr>
                                        <th>@lang('supplier.supplier')</th>
                                        <th>@lang('supplier.commercial_user')</th>
                                        <th>@lang('supplier.user_who_made_change')</th>
                                        <th>@lang('supplier.date')</th>
                                        
                                    </tr>
                                </thead>
                            </table>
                            
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        --}}


        @include('admin.models.manage_supplier_log')

@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/manage/') }}";

var log_url = "{{url('/admin/manage/?sup=')}}"; 
     
        var edit = "<?php echo Auth::user()->can('access.suppliers.edit'); ?>";
        var Supplierdelete = "<?php echo Auth::user()->can('access.suppliers.delete'); ?>";

        
        datatable = $('#manage-table').DataTable({
            processing: true,
            serverSide: true,
             ajax: '{!! route('SupplierControllerManageData') !!}',
                columns: [
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            return "<input type='checkbox' name='checkbox'/>";
                        }
                    },
                    {   "data":null,
                        name:'supplier.name',
                        "orderable": false,
                        "render": function (o) {
                            
                                var e="";
                                
                                e =  "<a href='{{url("admin/suppliers")}}/"+o.id+"/logs' data-id="+o.id+">"+o.name+"</a>&nbsp;";

                                return  e;
                            
                        }
                    },
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoicedue_supplier.length > 0)
                                return o.invoicedue_supplier[0].invoicesum;
                            else 
                                return 0;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoice_anticipastion.length > 0)
                                return o.invoice_anticipastion[0].invoicesum;
                            else 
                                return 0;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoice_cession.length > 0)
                                return o.invoice_cession[0].invoicesum;
                            else 
                                return 0;
                        }
                    },

                    { 
                        data:'user_name',
                        name:'users.name',
                        "searchable": true,
                        "orderable": false,
                        
                    },
                    
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e= "<a href='{{url("admin/suppliers")}}/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('supplier.edit')</button></a>&nbsp;";
                            var t = "<a href='{{url("admin/transactions/view")}}/"+o.id+"' data-id="+o.id+"><button class='btn btn-warning btn-xs'>@lang('supplier.transaction')</button></a>&nbsp;";

                            var l = "<a href='{{url("admin/suppliers")}}/"+o.id+"/logs' data-id="+o.id+"><button class='btn btn-info btn-xs'>@lang('supplier.logs')</button></a>&nbsp;";

                            var users = "<a href='{{url("admin/users")}}?supplier_id="+o.id+"' data-id="+o.id+"><button id='user-list' class='btn btn-warning btn-xs '>@lang('supplier.users')</button></a>&nbsp;";
                            
                            return e+l+t+users;
                        }
                    }
                ]
        });


        
/*
    $(document).on('click', '#log', function (e) {

        var id = $(this).attr('data-id');
       // $('#supplier_log').modal('show');

        var burl = "{{url('admin/suppliers')}}/"+id+"/logs";
        $.get(burl, function (result) {
            var data =result.data;
            console.log('data');
            
        })
        

        datatable_supp = $('#log-table').DataTable({

            processing: true,
            serverSide: true,
            retrieve:true,
            ajax: 
            {
                url:'{!! route('SupplierControllerManageLogData') !!}', // json datasource
                type: "get", // method , by default get
                data: function (d) {
                    d.supp_id = id;
                    
                }
            },
              
                columns: [
                    { 
                       "data":null,
                        "searchable": true,
                        "orderable": false,
                        "render": function (o) {
                            if(o.supplier != '')
                                return o.supplier;
                            return '-';
                        } 
                    },
                    { 
                       "data":null,
                        "searchable": true,
                        "orderable": false,
                        "render": function (o) {
                            return o.cbuser;
                        } 
                    },
                    { 
                       "data":null,
                        "searchable": true,
                        "orderable": false,
                        "render": function (o) {
                            return o.changed_by;
                        } 
                    },
                    { 
                       "data":null,
                        "searchable": true,
                        "orderable": false,
                        "render": function (o) {
                            return o.date;
                        } 
                    }
                    
                    
                ]
        }); 

    }); */
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Supplier ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>
@endpush