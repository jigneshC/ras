@extends('layouts.backend')

@section('title',trans('supplier.transactionHistory'))
@section('pageTitle',trans('supplier.transactionHistory'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                            @lang('supplier.transactionHistory')
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>@lang('supplier.id')</th>
                                        <th>@lang('supplier.name')</th>
                                        <th>@lang('supplier.view')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($supplier as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                           
                                            <a href="{{ url('/admin/supplier/transaction/' . $item->id) }}" title="Summary"><button class="btn btn-primary btn-sm"><i class="fa fa-money" aria-hidden="true"></i> Check Transaction Summary</button></a>
                                           
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $supplier->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>  

                </div>
            </div>
        </div>
    </div>

@endsection
