@extends('layouts.backend')

@section('title',trans('supplier.suppliers_bank'))
@section('pageTitle',trans('supplier.suppliers_bank'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('supplier.suppliers_bank')
                                                  </div>

                               </div>
                               <div class="box-content ">
                                <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>

                                <a href="#" id="open_model_supbank_account_add"  class="btn btn-success btn-sm" title="Add Bank Accounts">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('supplier.add_supplier_bank_account')
                        </a>

                        <br/>
                        <br/>
                        Supplier Name : {{ $supplier->name}}
                        <div class="table-responsive">
                            <table id="supbank-table" class="table table-borderless table-responsive">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('supplier.bank_id')</th>
                                        <th data-priority="2">@lang('supplier.bank_name')</th>
                                        <th data-priority="3">@lang('supplier.agency_name')</th>
                                        <th data-priority="4">@lang('supplier.agency_number')</th>
                                        <th data-priority="5">@lang('supplier.account_number')</th>
                                        <th data-priority="6">@lang('supplier.account_info')</th>
                                        <th data-priority="7">@lang('supplier.status')</th>
                                        <th data-priority="8">@lang('supplier.actions')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        @include("admin.models.supplier_bank")

@endsection

@push('script-head')
<script>
        var url ="{{ url('admin/supplier/bank/datatable') }}/{{$supplier->id}}";
        var statusUrl = "{{url('admin/supplier/bank/statusupdate')}}";

        //Permission
        var edit = "<?php echo Auth::user()->can('access.supplier.bank.edit'); ?>";
        var sup_bank_delete = "<?php echo Auth::user()->can('access.supplier.bank.delete'); ?>";
        var sup_bank_status =  "<?php echo Auth::user()->can('access.supplier.bank.change.status'); ?>";
        
        datatable = $('#supbank-table').DataTable({
            processing: true,
            serverSide: true,
             ajax: url,
                columns: [
                    { data: 'id',name:'id'},
                    { data: 'bank_name',name:'bank_name'},
                    { data: 'agency_name',name:'agency_name'},
                    { data: 'agency_number',name:'agency_number'},
                    { data: 'account_number',name:'account_number'},
                    { data: 'account_info',name:'account_info'},
                 { 
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "defaultContent" : '-',
                    "render": function (o) {
                        var status="";

                        if(sup_bank_status){
                            if(o.status == 'inactive')
                            status = '<a href="'+statusUrl+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("supplier.inactive")</button></a>';
                            else
                            status = "<a href='"+statusUrl+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('supplier.active')</button></a>";
                            return status;
                        }else{
                            return o.status;
                        }
                        
                        
                    }
                },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            
                            if(edit){
                                e= "<a href='#' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('supplier.edit')</button></a>&nbsp;";
                            }
                            if(sup_bank_delete){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('supplier.delete')</button></a>&nbsp;";
                            }
                            return e+d;
                        }
                    }
                ]
        });

    $(document).on('click', '#open_model_supbank_account_add', function (e) {
        var formid = $("#supplier_bank_account_form");
        resetupdateform(formid);
        $('#supplierbank').modal('show');
    });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = "{{url('admin/supplier/bank')}}/" + id;
        var r = confirm("Are you sure you want to delete Supplier's Bank ?");
        alert(url);
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

     $(document).on('click', '.edit-item', function (e) {
        var id = $(this).attr('data-id');

       var burl = "{{url('admin/supplier/bank')}}/"+id+"/edit";
        $.get(burl, function (result) {
            var data =result.data;
            $('#bank_account_id').val(data.id);
            $('#supplier_id').val(data.supplier_id);
            $('#bank_name').val(data.bank_name);
            $('#bank_number').val(data.bank_number);
            $('#agency_name').val(data.agency_name);
            $('#agency_number').val(data.agency_number);
            $('#account_number').val(data.account_number);
            $('#account_info').val(data.account_info);
            $('#info').val(data.info);
            $('.error').html("");
            $('.account_form_submit_button').html("Update");
            $('#account_form_model_lable').html("Edit Supplier Bank Account Detail");

            $('#supplierbank').modal('show');
        })
    });

    function resetupdateform(formid) {
        $(formid)[0].reset();
        $('.error').html("");
        $("#bank_account_id").val("");
        $('#account_form_model_lable').html("Add Supplier Bank Account Detail");
        $(".account_form_submit_button").html("Create");
    }

$("#supplier_bank_account_form").validate({
    rules: {
        bank_name:{
            required: true,
            
        },
        bank_number:{
            required: true,
            number : true,
        },
        agency_name:{
            required: true,
            
        },
        agency_number: {
            required: true,
        },
        account_number: {
            required: true,
        },
        account_info: {
            required: true,
        },
        info:{
            required: true,
        
        },
    },
    messages: {
        bank_name:{
            required: "Please enter Bank Name",
            
        },
        bank_number:{
            required: "Please enter Bank Number",
            number: "Must be number",                      
            
        },
        agency_name:{
            required: "Please enter Agency Name",
            
        },
        agency_number: {
            required: "Please enter Agency Number",
        },
        account_number: {
            required: "Please enter Account Number",
        },
        account_info: {
            required: "Please enter Account Info",
        },
        info:{
            required: "Enter Information",
        
        },
    },

    submitHandler: function (form) {


        var url = "{{url('admin/supplier/bank')}}";
        var method = "post"

        if($('#bank_account_id').val()!="" && $('#bank_account_id').val()!=0){
            var url = url+"/"+$('#bank_account_id').val();
            var method = "put";
        }

        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {

                $('#supplierbank').modal('hide');
                datatable.draw();
                toastr.success('Added Successfully',result.message)

            },
            error: function (error) {
                $('#supplierbank').modal('hide');
              //  toastr.error(result.message)
            }
        });
        return false;
    }
});
</script>
@endpush