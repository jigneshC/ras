@extends('layouts.backend')
{{-- 
@section('title',trans('supplier.transaction_history_per_supplier'))
@section('pageTitle',trans('supplier.transaction_history_per_supplier')) --}}
@section('title',trans('supplier.transaction_summary_per_customer'))
@section('pageTitle',trans('supplier.transaction_summary_per_customer'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                            @lang('supplier.transaction_summary_per_customer')
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">

                            <div class="col-md-12">
                                    <a href="{{ URL::previous() }}" title="Back">
                                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back')
                                            </button>
                                    </a><br>
                            </div>   

                        <div class="box-content">
                            <div class="col-md-7">
                                <label>@lang('supplier.supplier') : </label>
                                <span> {{$supplier->name}} </span><br>
                                @if(isset($supp_user))
                                <label >@lang('supplier.main_contact_at_cb') : </label>
                                <span > {{$supp_user->user->name}} </span><br>
                                <label >@lang('supplier.phone') : </label>
                                <span > {{$supp_user->user->phone}} </span><br>
                                <label >@lang('supplier.mobile') : </label>
                                <span > {{$supp_user->user->mobile}} </span><br>
                                @endif
                                <br>
                                <label >@lang('supplier.period') : </label>
                                <input type="text" name="startdate" value="{{$startdate}}" id="startdate" /> @lang('supplier.to') <input type="text" name="enddate" id="enddate" value="{{$enddate}}" /> 
                                {!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i> Submit', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-success btn-xs',
                                    'title' => 'Change Period',
                                    'id' => 'changedate'
                                    
                                ))!!} <br/>
                                <br/>
                                <label >@lang('supplier.search'):</label>
                                <input type="text" name="searchText" id="searchText" value="" /> 
                            </div>
                            {{--
                            <div class="col-md-5">
                                <label>@lang('supplier.cash_made_available_for_anticipation') :</label>
                                <span> </span><br>
                                <label>@lang('supplier.anticipations_done') :</label>
                                <span>  </span><br>
                                <label>@lang('supplier.anticipations_to_be_paid') :</label>
                                <span>  </span><br>
                                <label>@lang('supplier.cash_available') :</label>
                                <span>  </span><br>
                                <label>@lang('supplier.date_limit') :</label>
                                <span>  </span><br>
                                
                            </div>
                            --}}
                        
                        

                        </div>

                    <div class="table-responsive">
                        <table id="suppliers-table" class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('supplier.customer')</th>
                                <th data-priority="2">@lang('supplier.total_invoices_amount')</th>
                                <th data-priority="3">@lang('supplier.anticipations_amount')</th>
                                {{-- <th data-priority="4">@lang('supplier.transaction_qty')</th> --}}
                                <th data-priority="4">@lang('supplier.anticipation_transaction')</th> 
                                <th data-priority="5">@lang('supplier.cession_amount')</th>
                                <th data-priority="6">@lang('supplier.cession_transaction')</th>
                                <th data-priority="7">@lang('supplier.mean_rate')(%)</th>
                                <th data-priority="8">@lang('supplier.anticipation_cost')</th>
                                <th data-priority="9">@lang('supplier.cessions_cost')</th>
                                <th data-priority="10">@lang('supplier.total_financial_cost')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
@push('script-head')
<script> 

        datatable = $('#suppliers-table').DataTable({
            processing: true,
            serverSide: true,
            "bFilter": false,
            ajax: 
            {
                url: '{!! route('SuppliersControllerTransactionHistoryData') !!}', // json datasource
                type: "get", // method , by default get
                data: function (d) {
                    d.supplier_id = '{{$supplier->id}}';
                    d.startdate = $('#startdate').val();
                    d.enddate = $('#enddate').val();
                    d.searchText = $('#searchText').val();
                }
            },
                columns: [
                    {
                        'data':null,
                        "searchable": false,
                        render : function(o){
                            return o.name;
                        }
                    },
                    {data:'amounttotal',"searchable": false,
                        "orderable": false,name:'amounttotal'},
                    {data:'anticipationtotal',"searchable": false,
                        "orderable": false,name:'anticipationtotal'},
                    {data:'totalInvoiceAnticipation',"searchable": false,
                        "orderable": false,name:'totalInvoiceAnticipation'},
                    // {
                    //     "data":null,
                    //     "searchable": false,
                    //     "orderable": false,
                    //     render : function(o){
                    //         return o.totalInvoice;
                    //     }
                    // },
                    {data:'cessiontotal',"searchable": false,
                        "orderable": false,name:'cessiontotal'},
                    {data:'totalInvoiceCession',"searchable": false,
                        "orderable": false,name:'totalInvoiceCession'},
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return '0';
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return '0';
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return '0';
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return '0';
                        }
                    }

                ]
        });
    
    
    $('#changedate').click(function(){
         datatable.draw();
    });
    $(document).on('keypress', '#searchText', function (e) {
        datatable.draw();
    });
    $(document).on('focusout', '#searchText', function (e) {
        datatable.draw();
    });
    $(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            //defaultDate : "{{$startdate}}",
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
            //defaultDate : "{{$enddate}}",
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            //defaultDate : "{{$enddate}}",
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    });

        

</script>
@endpush
