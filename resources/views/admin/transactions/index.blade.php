@extends('layouts.backend')

@section('title',trans('transaction.transactions'))
@section('pageTitle',trans('transaction.transactions'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                      @lang('transaction.transactions')
                                                  </div>

                               </div>
                               <div class="box-content ">
                            <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('transaction.back')</button></a>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table id="transactions-table" class="table table-borderless datatable responsive">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('transaction.transaction_number')</th>
                                        <th data-priority="2">@lang('transaction.customer')</th>
                                        <th data-priority="3">@lang('transaction.supplier')</th>
                                        <th data-priority="4">@lang('transaction.type')</th>
                                        <th data-priority="5">@lang('transaction.quantity_of_invoices')</th>
                                        <th data-priority="6">@lang('transaction.total_original_amount')</th>
                                        <th data-priority="7">@lang('transaction.discount_value')</th>
                                        <th data-priority="8">@lang('transaction.net_value')</th>
                                        <th data-priority="9">@lang('transaction.status')</th>
                                        <th data-priority="10">@lang('transaction.requested_by')</th>
                                        <th data-priority="11">@lang('transaction.approved_by')</th>
                                        <th data-priority="12">@lang('transaction.bank')</th>
                                        <th data-priority="13">@lang('transaction.bank_cost')</th>
                                        <th data-priority="14">@lang('transaction.approval_date')</th>
                                        <th data-priority="15">@lang('transaction.action')</th>
                                        {{--
                                        @lang('transaction.schedule_pay_day')</th>
                                        <th data-priority="14">@lang('transaction.date_of_anticipation')</th>
                                        --}}
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script-head')
<script> 
var url ="{{ url('/admin/transactions/') }}";
var id = '<?php if($id != "") echo $id; else "0";?>'
    datatable = $('#transactions-table').DataTable({
        processing: true,
        serverSide: true,
        "order": [[ 0, "desc" ]],
            ajax: { 
                url : '{!! route('TransactionsControllerTransactionsData') !!}',
                type: "get", // method , by default get
                data: function (d) {
                    d.supplier = id;
                },
            },
            columns: [
                { data: 'id', name: 'Id',"searchable": false },
                { data:'customer_name',name:'customer.name',"searchable": true},
                { data:'supplier_name',name:'supplier.name',"searchable": true},
                { data:'type',name:'type',"searchable": false},
                { 
                    "data":null,
                    "searchable": false,
                    "orderable": false,
                    "defaultContent" : '-',
                    "render": function(o){
                        if(o.total_invoice == ''){
                            return '-';
                        }else{
                            return o.total_invoice[0].total; //quantity of invoice
                        }
                        
                    }
                },
                { 
                    "data":null,
                    "searchable": false,
                    "orderable": false,
                    "render": function(o){
                        if(o.total_invoice == ''){
                            return '-';
                        }else{
                            return o.total_invoice[0].invoicesum;
                        }
                        
                    }
                },
                { 
                    data:null,
                    "searchable": false,
                    "orderable": false,
                    render : function (o){
                        return o.discount_value;
                    }
                },
                { 
                    data:null,
                    "searchable": false,
                    "orderable": false,
                    render : function (o){
                       return  o.net_value;
                    }
                },
                { 
                    data:null,
                    "searchable": false,
                    "orderable": false,
                    render : function (o){
                        if(o.payment_status != null)
                            return  o.payment_status;
                        else
                            return '-'; 
                    }
                },
                { 
                    data:null,
                    "searchable": false,
                    "orderable": false,
                    render : function (o){
                        if(o.requester != null) 
                            return  o.requester.name;
                        else
                            return '-'; 
                    }
                },
                { 
                    data:null,
                    "searchable": false,
                    "orderable": false,
                    render : function (o){
                        if(o.approver != null)
                            return  o.approver.name;
                        else
                            return '-'; 
                    }
                },
                { 
                    data:'bank_name',
                    name:'banks.name',
                    "searchable": true,
                    "orderable": false,
                    "defaultContent": "-"
                },
                { 
                    data:null,
                    "searchable": false,
                    "orderable": false,
                    render : function (o){
                        return o.cost;
                    }
                },
                { 
                    "data":null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                            var approval_date = o.approval_date;
                            if(approval_date!=null){
                                var newdate = approval_date.replace('-', '/').replace('-', '/');                               
                                var first_date = moment(newdate).format('DD/MM/YYYY');
                                return first_date;
                            }else{
                                var newdate ='';
                                return newdate;
                            } 
                    }
                },
                { 
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-warning btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('customer.view')</button></a>&nbsp;";
                        return v;
                    }
                }

            ]
    });
    

</script>
@endpush