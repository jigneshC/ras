@extends('layouts.backend')

@section('title',trans('transaction.view_transaction'))
@section('pageTitle',trans('transaction.view_transaction'))
@section('content')
<div class="row">
    <div class="col-md-12"> 
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i>
                    @lang('transaction.transaction'){{ $transaction->id }}
                </div>
            </div>
            <div class="box-content ">
            <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('transaction.back')</button></a>
                <br/>
                <br/>
                @if(Auth::user()->can('access.approve.transaction'))
                    @if(Auth::user()->role[0]->name == "SLA" || Auth::user()->role[0]->name == "SLO")
                        {{-- @if($trans->payment_status == "Approved")
                            <div class="form-group col-md-3">
                                {!! Form::select('bank',$bank,null,['class' => 'form-control selectTag','disabled' => 'disabled']) !!}
                            </div>
                        @else --}}
                        @if($trans->payment_status == "Pending")
                            <div class="form-group col-md-3">
                                {!! Form::select('bank',$bank,null,['class' => 'form-control selectTag', 'id' => 'select_bank']) !!}
                            </div>
                        @else
                        @endif
                    @endif
                @endif    

                <div class="col-md-3">
                    <label>@lang('transaction.supplier') : </label>
                    <span>{{$transaction->supplier->name}}</span><br>
                    <label>@lang('transaction.transaction_id') : </label>
                    <span>{{ $transaction->id}}</span>
                </div>
                <div class="col-md-3">
                    <label>@lang('transaction.operator') : </label>
                    <span>{{$transaction->requester->name}}</span><br>
                    <label>@lang('transaction.date') : </label>
                    <span>{{ date('d-m-Y',strtotime($transaction->created_at)) }}</span>
                </div>
                <div class="col-md-3">
                    <label>@lang('transaction.status') : </label>
                    <span>{{ $transaction->payment_status}}</span><br>
                    @if($transaction->supplier_bank)
                    <label>@lang('transaction.bank') : </label>
                    <span>{{ $transaction->supplier_bank->bank_name}} - {{ $transaction->supplier_bank->account_info}}</span><br>
                    @endif
                </div>
                <br>
                <br>
                
                <div class="table responsive">
                        <table class="table table-borderless dataTables_wrapper no-footer">
                                
                                <th>@lang('transaction.type')</th>
                                <th>@lang('transaction.original_value')</th>
                                <th>@lang('transaction.discount_value')</th>
                                <th>@lang('transaction.discounted_value')</th>
                                <th>@lang('transaction.costs')</th>
                                <th>@lang('transaction.net_value')</th>
                                <tr>
                                    <td>{{ $transaction->type}}</td>
                                    <td>{{ $transaction->total_amount}}</td>
                                    <td>{{ $transaction->discount_value}}</td>
                                    <td>{{ $transaction->discounted_value}}</td>
                                    <td>{{ $transaction->cost }}</td>
                                    <td>{{ $transaction->net_value}}</td>
                                </tr>
                        </table>
                  
                    <table id="getInvoices-table" class="table table-borderless datatable responsive " width="100%">
                        <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('transaction.id')</th> -->
                                <th data-priority="2">@lang('transaction.invoice_number')</th>
                                <th data-priority="3">@lang('transaction.supplier_unit')</th>
                                <th data-priority="4">@lang('transaction.customer')</th>
                                <th data-priority="5">@lang('transaction.original_due_date')</th>
                                <th data-priority="6">@lang('transaction.date_of_anticipation')</th>
                                <th data-priority="7">@lang('transaction.days_upto_due_date')</th>
                                <th data-priority="8">@lang('transaction.original_value')</th>
                                <th data-priority="9">@lang('transaction.discount_rate')</th>
                                <th data-priority="10">@lang('transaction.net_value')</th>
                                <th data-priority="11">@lang('transaction.costs')</th>
                                <th data-priority="13">@lang('transaction.type')</th>
                            </tr>
                        </thead>
                    </table>

                   

                                @if(Auth::user()->can('access.approve.transaction'))

                                    @if(Auth::user()->role[0]->name == "SLA" || Auth::user()->role[0]->name == "SLO")
                                    <table class="table table-borderless dataTables_wrapper no-footer">

                                        @if($trans->payment_status == "Approved")
                                            <tr> 
                                                <td colspan="2"><p>@lang('transaction.already_approved')</p></td>
                                            </tr> 
                                            <tr> 
                                                    <td ><p>@lang('transaction.approved_by')</p></td>
                                                    <td>{{$transaction->approver->name}}</td>
                                            </tr>
                                            <tr> 
                                                    <td ><p>@lang('transaction.approval_date_time')</p></td>
                                                    <td>{{$transaction->approval_date}}</td>
                                            </tr>
                                        
                                        @elseif($trans->payment_status == "Cancelled")
                                            <tr>
                                                <td colspan="2"><p>@lang('transaction.transaction_cancelled')</p></td>
                                            </tr>
                                            <tr> 
                                                <td ><p>@lang('transaction.cancelled_by')</p></td>
                                                <td>{{ $transaction->canceller->name}}</td>
                                            </tr>
                                            <tr> 
                                                <td ><p>@lang('transaction.cancelled_date_time')</p></td>
                                                <td>{{ date('d-m-Y H:i:s', strtotime($transaction->cancelled_date)) }}</td>
                                            </tr>
                                        @elseif($trans->payment_status == "Expired")
                                            <tr>
                                                <td colspan="2"><p>Transaction Expired </p></td>
                                            </tr>
                                        
                                        @else
                                            <tr>
                                                <td><button class='btn btn-success btn-xs' id="pwd_modal" data-value="approve"> @lang('transaction.approve_transaction')</button></td>
                                                <td><button  class='btn btn-success btn-xs' id="pwd_modal" data-value="cancel"> @lang('transaction.cancel_transaction') </button></td>
                                            </tr>
                                        
                                        @endif

                                    @elseif(Auth::user()->role[0]->name == "CM")

                                            @if($trans->customer_status ==  '0')
                                            <tr>
                                                <td><button id="approveTransaction" class='btn btn-success btn-xs'> @lang('transaction.approve_transaction')</button></td>
                                                <td><button id="cancleTransaction" class='btn btn-success btn-xs'> @lang('transaction.cancel_transaction') </button></td>
                                            </tr>
                                            @elseif($trans->customer_status == "Approved")
                                            <tr> 
                                                <td colspan="2"><p>@lang('transaction.already_approved')</p></td>
                                            </tr>
                                            @elseif($trans->customer_status == "Cancelled")
                                            <tr>
                                                <td colspan="2"><p>@lang('transaction.transaction_cancelled')</p></td>
                                            </tr>
                                            @endif
                                    
                                    </table>        
                                    @endif
                                
                                @else

                                    <table class="table table-borderless dataTables_wrapper no-footer">
                                    
                                        @if($trans->payment_status == "Approved")
                                            <tr> 
                                                <td colspan="2"><p>@lang('transaction.already_approved')</p></td>
                                            </tr> 
                                            <tr> 
                                                <td ><p>@lang('transaction.approved_by')</p></td>
                                                <td>{{$transaction->approver->name}}</td>
                                            </tr>
                                            <tr> 
                                                <td ><p>@lang('transaction.approval_date_time')</p></td>
                                                <td>{{ date('d-m-Y H:i:s', strtotime($transaction->approval_date)) }}</td>
                                            </tr>
                                                                            
                                        @elseif($trans->payment_status == "Cancelled")
                                            <tr>
                                                <td colspan="2"><p>@lang('transaction.transaction_cancelled')</p></td>
                                            </tr>
                                            <tr> 
                                                <td ><p>@lang('transaction.cancelled_by')</p></td>
                                                <td>{{ $transaction->canceller->name}}</td>
                                            </tr>
                                            <tr> 
                                                <td ><p>@lang('transaction.cancelled_date_time')</p></td>
                                                <td>{{ date('d-m-Y H:i:s', strtotime($transaction->cancelled_date)) }}</td>
                                            </tr>
                                        @elseif($trans->payment_status == "Expired")
                                            <tr>
                                                <td colspan="2"><p>Transaction Expired</p></td>
                                            </tr> 
                                        @else
                                            <tr>
                                                <td colspan="2"><p>@lang('transaction.transaction_pending')</p></td>
                                            </tr>                               
                                        @endif
                                    </table>

                                @endif
                   
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enter Password : </h4>
        </div>
        <div class="modal-body">
          <form id="pass_form" name="pass_form" > 
              <input type="hidden" name="button_value" id="button_value" value="">
              <input type="password" name="password" id="password">
              <button  name="submit" class="btn btn-default">Submit</button>
              <p id="error"></p>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
  
    </div>
  </div>
@endsection
@push('script-head')
<script> 
// ---------------Verify user -------------------
$(document).on('click', '#pwd_modal', function (e) {
    var supp_bank = $("#select_bank").val();
    var btn_value = $(this).attr('data-value');
    if(btn_value != 'cancel'){
        if(supp_bank == 0 || supp_bank == null || supp_bank == ''){
        alert('You must Select a Bank Account to Receive Value');
        return false;
        }
    }
    var formid = $("#pass_form");
    resetupdateform(formid);
    $('#button_value').val($(this).attr('data-value'));
    $('#myModal').modal('show');
});

function resetupdateform(formid) {
        $(formid)[0].reset();
        $('#error').html("");
    }

$("#pass_form").validate({
    
    submitHandler: function (form) {
        var password = $('#password').val();
        if(password == ""){
            document.getElementById("error").innerHTML = "Password field cannot be blank ";
            return false;
        }
        var url = "{{url('admin/password-verify')}}";
        var method = "get";
        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {
                $('#myModal').modal('hide');

                if(result.data.button_value == "approve"){
                    var supp_bank = $("#select_bank").val();
                   // var r = confirm("Are You Sure Approve Transaction ?");
                   // if(r == true){
                        url = "{{ url('/admin/transactions/approve') }}/{{ $transaction->id}}?bank_id="+supp_bank;
                        //load_url = "{{ url('/admin/transactions') }}/{{ $transaction->id}}";
                        window.location = url;
                        toastr.success("Transaction Approved");
                    //} 
                }
                if(result.data.button_value == "cancel"){
                    //var r = confirm("Are You Sure Cancel Transaction ?");
                    //if(r == true){
                        url = "{{ url('/admin/transactions/cancle') }}/{{ $transaction->id}}";
                       // load_url = "{{ url('/admin/transactions') }}/{{ $transaction->id}}";
                        window.location = url;
                        toastr.success("Transaction Cancelled");
                    //}
                }
               
            },
            error: function (error) {
                
                toastr.error(error.responseJSON.messages);
                
                $('#myModal').modal('hide');
                
            }
        }); 
        return false;
    }
});
    
$('#approveTransaction').click(function() {
    var r = confirm("Are You Sure Approve Transaction ?");
    if(r == true){
        url = "{{ url('/admin/transactions/approve') }}/{{ $transaction->id}}";
        window.location = url;
        toastr.success("Transaction Approved");
    } 
});
$('#cancleTransaction').click(function() {
    var r = confirm("Are You Sure Cancel Transaction ?");
    if(r == true){
        url = "{{ url('/admin/transactions/cancle') }}/{{ $transaction->id}}";
        window.location = url;
        toastr.success("Transaction Cancelled");
    }
});

var d = new Date();
var anticipationDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
function getdueday(duedate,startdate){
  
    var end = new Date(duedate);
   
     var start   = new Date(startdate);


        var startDate = moment(start, "DD.MM.YYYY");
        var endDate = moment(end, "DD.MM.YYYY");
        return  endDate.diff(startDate, 'days');
}

    table = $('#getInvoices-table').DataTable({
        processing: true,
        serverSide: true,
        "bFilter": false,
        "lengthChange": false,
            ajax: "{{ url('/admin/transactions/invoiceData') }}/{{ $transaction->id}}",
            columns: [
 
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        return o.invoice.invoice_id;
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    "defaultContent" : '-',
                    render : function(o){
                        if(o.invoice.supplier_unit != null)
                            return o.invoice.supplier_unit.unit_name;
                        else
                            return '-';
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        return o.customer.name;
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        var date =  o.invoice.duedate;
                        var newdate = date.replace('-', '/').replace('-', '/');                               
                        var due_date = moment(newdate).format('DD/MM/YYYY');
                        return due_date;
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        var date = o.anticipation_date;
                        var newdate = date.replace('-', '/').replace('-', '/');                               
                        var anticipations_date = moment(newdate).format('DD/MM/YYYY');
                        return anticipations_date;
                    }
                },
                {
                    data:null,
                    "orderable": false,
                    "searchable": false,
                    render : function(o){
                        return getdueday(o.invoice.duedate,o.anticipation_date);
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        return o.invoice.total_amount;
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        return o.invoice.discount_rate+' %';
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        
                        var day = getdueday(o.invoice.duedate,o.anticipation_date);
                        var original_value = o.invoice.total_amount;

                        //calculate cost
                        var bank_rate = o.invoice.bank_rate;
                        var cost = (day/30) * original_value * (bank_rate/100);

                        //calculate discount_value 
                        var discount_rate = o.invoice.discount_rate;
                        var discount_value = (day/30) * original_value * (discount_rate/100);
                        discount_value = discount_value.toFixed(2);

                        var net_value = original_value - discount_value;
                        return net_value.toFixed(2);
                      
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        var bank_rate = o.invoice.bank_rate;
                        if(bank_rate == 0 || bank_rate == null){
                            return 0.00
                        }else{
                            var day = getdueday(o.invoice.duedate,o.anticipation_date);
                            var original_value = o.invoice.total_amount;
                            var discount_rate = o.invoice.discount_rate;
    
                            //var cost = (day/30) * original_value * (bank_rate/100);
                            var cost = ( ( original_value * ( 1 - ( discount_rate / 100) / 30*day ) * ( bank_rate / 100 ) / 30*day ) / ( 1 - ( bank_rate / 100 ) / 30*day ) ) ;
                            return cost.toFixed(2);
                        }
                        
                    }
                },
                { 
                    'data': null, 
                    "searchable": false,
                    render : function(o){
                        return o.transaction.type;
                    }
                }
            ]
    });

</script>
@endpush
