@extends('layouts.backend')

@section('title',trans('user.label.cbusers'))
@section('pageTitle',trans('user.label.cbusers'))




@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                            @lang('user.label.cbusers')
                    </div>

                </div>
                <div class="box-content">
            
                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive" id="users-table" width="100%">
                            <thead>
                            <tr>
                                <th data-priority="1">{{--@lang('user.label.profile')--}}</th>
                                <th data-priority="2">@lang('user.label.name')</th>
                               <th data-priority="3">@lang('user.label.phone')</th>
                               <th data-priority="4">@lang('user.label.mobile')</th>
                                <th data-priority="5">@lang('user.label.role')</th>
                                <th data-priority="6">@lang('user.label.goal')</th>
                                <th data-priority="7">@lang('user.label.cbusertransaction')</th>
                                <th data-priority="8">@lang('user.label.assigncustorsupp')</th>
                                <th data-priority="9">@lang('user.label.status')</th>
                                <th data-priority="10">@lang('user.label.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
                @include("admin.models.cbuser_add_update_goal")
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/users/') }}";
var img_path = "{{asset('images')}}";
    $(document).on('click', '.open_model_for_change_goal', function (e) {
        $('#cbUSerName').html($(this).attr('data-cbusername'));
        $('#cbuser_id').val($(this).attr('data-cbuserid'));
        var formid = $("#change_goal_form");
        $('#changeGoal').modal('show');
    });
        datatable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('CbUsersControllerUsersData') !!}',
                    type: "get", 
                    data: function (d) {
                    }
                },
                columns: [
                    { 
                        "data": null,
                        "name": 'image',
                        "orderable": false,
                        "searchable" : false,
                        "render": function(o){
                            var image = o.image;
                            if(image){

                                return '<img src="'+img_path+'/'+image+'" width="50" height="50"></td>';
                            }
                            else{
                                
                                return '<img src="'+img_path+'/avatar.jpg" width="50" height="50"></td>';
                            }
                        }
                    } ,
                    { data: 'name',name:'users.name',"searchable" : false},
                    { data: 'phone',name:'users.phone',"searchable" : false},
                    { data: 'mobile',name:'users.mobile',"searchable" : false},
                    { 
                        "data" : null, 
                        "name" : 'role_label',
                        "searchable" : false,
                        render : function(o){
                          return o.role_label;  
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "render" : function(o){
                            if(o.role_name == 'CBC')
                                return (o.goal > 0 )? o.goal : '0';
                            else
                                return '-';
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "render" : function(o){
                            return 0;
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "render" : function(o){
                            if(o.role_name == "CBC")
                                return o.supplier_count.length;
                            if(o.role_name == "CBS")
                                return o.customer_count.length;
                                
                        }
                    },
                    {
                        "data": null,
                        
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.user_status=='blocked')
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.label.blocked')</button></a>";
                            else if(o.user_status == 'inactive')
                            
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' title='inactive'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.label.inactive')</button></a>";
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.label.active') </button></a>";
                            return status;
                                            
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e = '';
                            e = "<a href='#' class='open_model_for_change_goal' data-cbusername='"+o.name+"' data-cbuserid ='"+o.id+"'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('user.label.changegoal')</button></a>&nbsp;";
                            if(o.role_name == 'CBC')    
                                return e;
                            else 
                                return '-';    
                        }
                    }
                ]
        });
    $('#filter_role').change(function() {
        datatable.draw();
    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Customer ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

    $("#change_goal_form").validate({
        rules: {
            goalAmount: {
                required: true,
            }
        },
        messages: {
            SelectClass: {
                required: "If You want to change goal then add change gaol amount",
            }
        },
        submitHandler: function (form) {
            var url = "{{url('admin/cb-users/changegoal')}}";
            var method = "post"
            $.ajax({
                type: method,
                url: url,
                data: $(form).serialize(),
                beforeSend: function () {
                },
                success: function (result)
                {
                    result = JSON.parse(result)
                    if(result.msg == 'Success')
                        toastr.success('Added Successfully')
                    else
                        toastr.error('Added Successfully')
                    $('#changeGoal').modal('hide');
                    datatable.draw(); 
                },
                error: function (error) {
                    $('#changeGoal').modal('hide');
                    datatable.draw();
                }
            }); 
            return false;
        }
    });
</script>
@endpush
