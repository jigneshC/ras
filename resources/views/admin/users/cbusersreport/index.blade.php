@extends('layouts.backend')

@section('title',trans('user.cbuser.dealclosed'))
@section('pageTitle',trans('user.cbuser.dealclosed'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i>
                    @lang('user.cbuser.dealclosed')
                </div>
            </div>
            <div class="box-content ">
                {!! Form::open(['url' => 'admin/cbusers/deals-done', 'class' => 'form-horizontal', 'files' => true,'id'=> 'formSupplier','enctype'=>'multipart/form-data']) !!}

                    <div class="form-group">
                        {!! Form::label('banklist',trans('user.cbuser.suppliers'), ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('supplier_id',$suppliers,null,['class' => 'form-control selectTag','required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('periode',trans('user.cbuser.periode'), ['class' => 'col-md-4 control-label']) !!}
                         <div class="col-md-6">
                           <label> @lang('user.cbuser.start_date') :</label>
                            <input type="text" name="startdate" value="{{$start_date}}" id="startdate" />
                            <label>@lang('user.cbuser.end_date') </label>
                            <input type="text" name="enddate" id="enddate" value="{{$end_date}}" />
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-4">
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'Submit']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection
@push('script-head')
<script>
$(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
@endpush