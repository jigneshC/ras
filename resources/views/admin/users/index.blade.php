@extends('layouts.backend')

@section('title',trans('user.users'))
@section('pageTitle',trans('user.users'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('user.users')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">

                            <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('user.back')
                        </button>
                    </a>

                            @if(Auth::user()->can('access.user.create'))
                                @if(isset($customer))
                                    <a href="{{ url('/admin/users/create') }}?customer_id={{$customer->id}}" class="btn btn-success btn-sm"  title="Add New User">
                                     <i class="fa fa-plus" aria-hidden="true"></i> @lang('user.add_new_user')
                                 </a>

                                @elseif(isset($supplier))
                                <a href="{{ url('/admin/users/create') }}?supplier_id={{$supplier->id}}" class="btn btn-success btn-sm"
                                   title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('user.add_new_user')
                                </a>
                                @else
                                <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm"
                                   title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('user.add_new_user')
                                </a>
                                @endif

                            @endif
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <div class="form-group">
                            {!! Form::select('role',$role,request()->get('role'),['class'=>'form-control', 'id'=>'filter_role']) !!}
                            </div>
                            <div class="form-group">
                            {!! Form::select('status',array('active'=>'Active','inactive'=>'Inactive','all'=>'All'),'',['class'=>'form-control', 'id'=>'filter_status']) !!}
                            </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    
                        @if(isset($customer))
                            Users of Customer : {{$customer->name}}

                        @endif

                        @if(isset($supplier))
                            Users of Supplier : {{$supplier->name}}
                        @endif

                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive" id="users-table">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('user.id')</th>
                                <th data-priority="2">@lang('user.profile')</th>
                                <th data-priority="3">@lang('user.name')</th>
                                <th data-priority="4">@lang('user.email')</th>
                                <th data-priority="5">@lang('user.role')</th>
                                <th data-priority="6">@lang('user.company')</th>
                                <th data-priority="7">@lang('user.status')</th>
                                <th data-priority="8">@lang('user.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/users/') }}";
var admin ="{{ url('/admin/loginasuser/') }}";
var img_path = "{{asset('images')}}";

        //Permissions
        var edit = "<?php echo Auth::user()->can('access.user.edit'); ?>";
        var user_delete = "<?php echo Auth::user()->can('access.user.delete'); ?>";
        var change_status = "<?php echo Auth::user()->can('access.users.status'); ?>";
        var user_logs = "<?php echo Auth::user()->can('access.users.logs'); ?>";
        
        datatable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 0, "desc" ]],
             ajax: {
                    url: '{!! route('UsersControllerUsersData') !!}',//"{{ url('admin/tickets/datatable') }}", // json datasource
                    type: "get", // method , by default get
                    data: function (d) {
                        d.filter_role = $('#filter_role').val();

                        d.customer_id = "{{$customer_id}}";

                        d.supplier_id = "{{$supplier_id}}";

                        d.filter_status = $('#filter_status').val();

                    }
                },
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { 
                        "data": null,
                        "name": 'image',
                        "orderable": false,
                        "searchable" : false,
                        "render": function(o){
                            var image = o.image;
                            if(image){

                                return '<img src="'+img_path+'/'+image+'" width="50" height="50"></td>';
                            }
                            else{
                                
                                return '<img src="'+img_path+'/avatar.jpg" width="50" height="50"></td>';
                            }
                        }
                    } ,
                    { data: 'name',name:'users.name',"searchable" : false},
                    { data: 'email',name:'users.email',"searchable" : false},
                    { 
                        "data" : null, 
                        "name" : 'role_label',
                        "searchable" : false,
                        render : function(o){
                          return o.role_label;  
                        }
                    },
                    {
                        "data" : null,
                        "name" : 'customer_name',
                        "searchable": false,
                        render: function (o){
                            
                            var role = o.role_name;
                            if(role == 'CM'){
                                return o.customer_name;
                            }
                            
                            if(role == 'SLO' || role == 'SLA' )
                                return o.supplier_name;
                            if(role == 'SU' || role == 'CBA' || role == 'CBS' || role == 'CBC' || role == 'CBB')
                                return 'ConnectBahn';
                            if(role == '')
                                return 'Not Assign';
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "deafultContent" : '-',
                        "render": function (o) {
                            var status = '';

                            if(change_status){

                                if(o.user_status=='blocked')
                                status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('user.blocked')</button></a>";
                                else if(o.user_status == 'inactive')
                                
                                status = '<a href="'+url+'/'+o.id+'?status=blocked" title="inactive"><button class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("user.inactive")</button></a>';
                                else
                                status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-warning btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('user.active')</button></a>";
                                
                                return status;

                            }else{
                                return o.user_status;
                            }
                            
                                            
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d=""; var logs="";

                            if(edit){
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>  </button></a>&nbsp;";

                            if(user_logs){
                                logs = "<a href='{{url("admin/action-log")}}?user_id="+o.id+"' data-id="+o.id+"><button id='user-list' class='btn btn-warning btn-xs '>@lang('user.logs')</button></a>&nbsp;";
                            }
                            

                            return v+e+logs;
                        }
                    }
                ]
        });
    $('#filter_role').change(function() {
        datatable.draw();
    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Customer ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush
