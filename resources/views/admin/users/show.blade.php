@extends('layouts.backend')

@section('title',trans('view_user'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                            @lang('user.user')
                    </div>

                </div>
                <div class="box-content">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('user.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                        <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                @lang('user.edit_user')
                            </button>
                        </a>
                    @endif
                    @if(Auth::user()->can('access.user.delete'))
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/users', $user->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Delete User',
                                'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                    @endif
                    <br/>
                    <br/>


                    <?php
                    $role = join(' + ', $user->roles()->pluck('label')->toArray());
                    $rolename = join(' + ', $user->roles()->pluck('name')->toArray());
                    ?>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('user.id')</td>
                                <td>{{ $user->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('user.name')</td>
                                <td>{{ $user->name }}</td>
                            </tr>

                            <tr>
                                <td>@lang('user.user_image')</td>
                                @if($user->image != '')
                                         <td><img src="{{ asset('images/'.$user->image) }}" alt="" width="100px"></td>
                                     @else
                                          <td></td>
                                     @endif
                            </tr>


                            <tr>
                                <td>@lang('user.email')</td>
                                <td>{{ $user->email }}</td>
                            </tr>

                            <tr>
                                <td>@lang('user.role')</td>
                                <td>{{ $role }}</td>
                            </tr>

                            <tr>
                                <td>@lang('user.company')</td>
                                <td>
                                        @if($rolename == 'CM')
                                            @foreach ($user->customer as $customer)
                                                {{$customer->name}}
                                            @endforeach
                                        @endif
                                        @if($rolename == 'SLO' || $rolename == 'SLA')
                                            @foreach($user->supplier as $supplier)
                                                    {{$supplier->name}}<br>
                                            @endforeach
                                        @endif
                                        @if($rolename == 'SU' || $rolename == 'CBA' || $rolename == 'CBS' || $rolename == 'CBC' || $rolename == 'CBB')
                                                 ConnectBahn
                                        @endif
                                </td>
                            </tr>

                            <tr>
                                <td>@lang('user.cpf')</td>
                                <td>{{ $user->cpnj }}</td>
                            </tr>


                            <tr>
                                <td>@lang('user.mobile')</td>
                                <td>{{ $user->mobile }}</td>
                            </tr>


                            <tr>
                                <td>@lang('user.phone')</td>
                                <td>{{ $user->phone }}</td>
                            </tr>

                            <tr>
                                <td>@lang('user.comments')</td>
                                <td>{{ $user->comments }}</td>
                            </tr>

                            <tr>
                                <td>@lang('user.last_login_at')</td>
                                <td>{{ $user->is_login }}</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection