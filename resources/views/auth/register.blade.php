@extends('layouts.app')


@section('title',trans('register.register'))


@section('content')



    <div class='login-container'>
        <div class='container'>
            <div class='row'>

                <div class='col-sm-10 col-sm-offset-1'>
                    <h1 class='text-center title'>@lang('register.register')</h1>
                    <form class='validate-form' role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class='form-group{{ $errors->has('name') ? ' has-error' : '' }}'>

                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="@lang('register.name')" class="form-control" data-rule-required="true"
                                       name="name" type="text" value="{{ old('name') }}"  required autofocus/>
                                <i class='icon-user text-muted'></i>
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="@lang('register.e_mail')" class="form-control" data-rule-required="true"
                                       name="email" type="email" value="{{ old('email') }}" required/>
                                <i class='icon-user text-muted'></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class='form-group{{ $errors->has('password') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="@lang('register.password')" class="form-control" data-rule-required="true"
                                       name="password" type="password"/>
                                <i class='icon-lock text-muted'></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class='form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="@lang('register.confirm_password')" class="form-control" data-rule-required="true"
                                       name="password_confirmation" type="password" required />
                                <i class='icon-lock text-muted'></i>
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <button class='btn btn-block btn-sign-in' type="submit">@lang('register.register')</button>
                    </form>
                    <div class='text-center'>
                        <hr class='hr-normal'>
                        <a href='{{ route('password.request') }}'>@lang('register.forgot_your_password')?</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class='login-container-footer'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='text-center'>
                        <a href='{!! route('login') !!}'>
                            <i class='icon-user'></i>
                            @lang('register.already_register')?
                            <strong>@lang('register.log_in')</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection




{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Register</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"--}}
                                       {{--required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email"--}}
                                       {{--value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control"--}}
                                       {{--name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Register--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}