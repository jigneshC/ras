<!-- / jquery [required] --> 
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script> 
<!-- / jquery mobile (for touch events) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>


<script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"
        type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->


<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>
<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>



{{-- Data Table--}}
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js" ></script>

<script src="{!! asset('assets/javascripts/plugins/validate/jquery.validate.min.js')!!}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>

@push('js')
<script type="text/javascript">
//sidebar keep reduced once closed 
if("{{ \Session::has('click_count')}}"){
	var click_count = parseInt("{{ \Session::get('click_count') }}");
}else{
	var click_count = 0;
}
$('.toggle-nav').on('click', function (e) {
	click_count++;	
	if((click_count % 2) == 0){
		var menubar = 'open' ;
	}else{
		var menubar = 'close' ;
	}
	var data = { menubar:menubar,click_count:click_count}
        $.ajax({
        type: "GET",
        url: "{{ url('/setInSession')}}",
        data: data,
            success: function(){ 
            }
        });
	
});
</script>
@endpush

