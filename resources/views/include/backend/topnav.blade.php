{{--  <header>
    <nav class='navbar navbar-default'>
        <a class='navbar-brand' href='{!! url('/') !!}'>
            <img src="{{asset('assets/images/logo-pn.png')}}" width="165" >
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>
        <ul class='nav'>
            {{--  <li class='dropdown light only-icon'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                    <i class='icon-cog'></i>
                </a>
                <ul class='dropdown-menu color-settings'>
                    <li class='color-settings-body-color'>
                        <div class='color-title'>Body color</div>
                        <a data-change-to='{!! asset('assets/stylesheets/light-theme.css') !!}' href='#'>
                            Light
                            <small>(default)</small>
                        </a>
                        <a data-change-to='{!! asset('assets/stylesheets/dark-theme.css') !!}' href='#'>
                            Dark
                        </a>
                        <a data-change-to='{!! asset('assets/stylesheets/dark-blue-theme.css') !!}' href='#'>
                            Dark blue
                        </a>
                    </li>
                    <li class='divider'></li>
                    <li class='color-settings-contrast-color'>
                        <div class='color-title'>Contrast color</div>
                        <a data-change-to="contrast-red" href="#"><i class='icon-cog text-red'></i>
                            Red
                            <small>(default)</small>
                        </a>

                        <a data-change-to="contrast-blue" href="#"><i class='icon-cog text-blue'></i>
                            Blue
                        </a>

                        <a data-change-to="contrast-orange" href="#"><i class='icon-cog text-orange'></i>
                            Orange
                        </a>

                        <a data-change-to="contrast-purple" href="#"><i class='icon-cog text-purple'></i>
                            Purple
                        </a>

                        <a data-change-to="contrast-green" href="#"><i class='icon-cog text-green'></i>
                            Green
                        </a>

                        <a data-change-to="contrast-muted" href="#"><i class='icon-cog text-muted'></i>
                            Muted
                        </a>

                        <a data-change-to="contrast-fb" href="#"><i class='icon-cog text-fb'></i>
                            Facebook
                        </a>

                        <a data-change-to="contrast-dark" href="#"><i class='icon-cog text-dark'></i>
                            Dark
                        </a>

                        <a data-change-to="contrast-pink" href="#"><i class='icon-cog text-pink'></i>
                            Pink
                        </a>

                        <a data-change-to="contrast-grass-green" href="#"><i class='icon-cog text-grass-green'></i>
                            Grass green
                        </a>

                        <a data-change-to="contrast-sea-blue" href="#"><i class='icon-cog text-sea-blue'></i>
                            Sea blue
                        </a>

                        <a data-change-to="contrast-banana" href="#"><i class='icon-cog text-banana'></i>
                            Banana
                        </a>

                        <a data-change-to="contrast-dark-orange" href="#"><i class='icon-cog text-dark-orange'></i>
                            Dark orange
                        </a>

                        <a data-change-to="contrast-brown" href="#"><i class='icon-cog text-brown'></i>
                            Brown
                        </a>

                    </li>
                </ul>
            </li>  

            -}}

            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle contrast-blue' data-toggle='dropdown' href='#'>
                    <span class='user-name contrast-blue'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu contrast-blue'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='icon-signout'></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
        </ul>
    </nav>
</header>  --}}
<header>
    <nav class='navbar navbar-default '>
        <a class='navbar-brand' href='{!! url('/') !!}'>
            <img height="31" src="{{asset('assets/images/logo_ras.png')}}"/>
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>
        <ul class='nav'>

                <li class='dropdown dark user-menu'>
                        <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
        
                           Language<b class='caret'></b>
                        </a>
                        <ul class='dropdown-menu'>
                                @foreach($_lang as $lang)
                                <li>
                                    <a href="{{request()->fullUrlWithQuery(['lang'=> $lang->lang_code])}}" class="navigation_link" title="{{$lang->name}}">
                                        <i class="fa fa-language fa-lg"></i>{{$lang->name}} </a>
                                </li>
                                @endforeach
        
                        </ul>
                    </li>

     

            {{--@include('partials.language')--}}

            @include('partials.notifications')

            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

                    @if(isset(Auth::user()->people->photo))
                        <img width="23" height="23" alt="{!! Auth::user()->name !!}"
                             src="{!! asset('uploads/'.Auth::user()->people->photo) !!}"/>
                    @endif
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='icon-signout'></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
        </ul>
        
    </nav>
</header>