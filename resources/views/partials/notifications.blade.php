<li class='dropdown medium only-icon widget'>
    <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
        <i class='fa fa-bell-o'></i>
        @if(\Auth::user()->unreadNotifications->count() != 0)
        <div class='label'>
            
            {{\Auth::user()->unreadNotifications->count()}}

        </div>
        @endif
    </a>
    <ul class='scroll_notification dropdown-menu'>
        @php $notifications = auth()->user()->unreadNotifications->take(5);  @endphp
        @foreach($notifications as $notification)
            <li>
           
            @if($notification->type == "App\Notifications\AddBank")
                
                    <a href="{{url('/admin/banks')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-bank'></i>
                            </div>
                            <div class='pull-left text'>
                                A New Bank "{{$notification->data['bank_name']}}" is added by {{$notification->data['added_by']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif


            @if($notification->type == "App\Notifications\AddBankAccounts")
                
                    <a href="{{url('/admin/bank/account/'.$notification->data['bank_id'])}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-bank'></i>
                            </div>
                            <div class='pull-left text'>

                                A New Bank Account "{{$notification->data['bank_account_number']}}" is added for the  {{$notification->data['bank_name']}} Bank

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\AddBankCompany")
                
                    <a href="{{url('/admin/bank/company/'.$notification->data['bank_id'])}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-bank'></i>
                            </div>
                            <div class='pull-left text'>

                                "{{$notification->data['bank_name']}}" Bank added amount of {{$notification->data['amount']}} to {{$notification->data['customer_name']}} Company on {{$notification->data['date']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\AddBankContacts")
                
                    <a href="{{url('/admin/bank/contact/'.$notification->data['bank_id'])}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-bank'></i>
                            </div>
                            <div class='pull-left text'>

                                "{{$notification->data['bank_name']}}" Bank added a new Contact - "{{$notification->data['bank_contact_name']}}"

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\AddCash")
                
                    <a href="#">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-money'></i>
                            </div>
                            <div class='pull-left text'>

                                "{{$notification->data['added_by']}}" added amount of
                                {{$notification->data['cash']}} to {{$notification->data['customer_name']}} Company

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif


            @if($notification->type == "App\Notifications\AddCustomer")
                
                    <a href="{{url('/admin/customer')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-users-o'></i>
                            </div>
                            <div class='pull-left text'>

                                {{$notification->data['added_by']}} added a new Company - {{$notification->data['customer_name']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\AddSupplier")
                
                    <a href="{{url('/admin/suppliers')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-cubes'></i>
                            </div>
                            <div class='pull-left text'>

                                {{$notification->data['added_by']}} added a new Supplier - {{$notification->data['supplier_name']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\AddSupplierBank")
                
                    <a href="{{url('/admin/supplier/bank/'.$notification->data['supplier_id'])}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-bank'></i>
                            </div>
                            <div class='pull-left text'>

                                {{$notification->data['added_by']}} added a new Bank - {{$notification->data['bank_name']}} for the Supplier {{$notification->data['supplier_name']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif


            @if($notification->type == "App\Notifications\AddUser")
                
                    <a href="{{url('/admin/users')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-user'></i>
                            </div>
                            <div class='pull-left text'>

                                A new User - "{{$notification->data['user_name']}}" is added as a  {{$notification->data['user_role']}} by {{$notification->data['added_by']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\ChangeCBCommercial")
                
                    <a href="{{url('/admin/suppliers'.$notification->data['supplier_id'].'/logs')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-user'></i>
                            </div>
                            <div class='pull-left text'>

                                CB-Commercial User is Changed from {{$notification->data['old_commercial_user_name']}} to {{$notification->data['new_commercial_user_name']}} for Supplier - {{$notification->data['supplier_name']}} by {{$notification->data['changed_by']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\ChangeCBSales")
                
                    <a href="{{url('/admin/customer/'.$notification->data['customer_id'].'/logs')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-user'></i>
                            </div>
                            <div class='pull-left text'>

                                CB-Sales User is Changed from {{$notification->data['old_sales_user_name']}} to {{$notification->data['new_sales_user_name']}} for Company - {{$notification->data['customer_name']}} by {{$notification->data['changed_by']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\EditBankCPNJ")
               
                    <a href="{{url('/admin/banks')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-bank'></i>
                            </div>
                            <div class='pull-left text'>

                                {{$notification->data['bank_name']}} Bank's CPNJ is changed by {{$notification->data['changed_by']}} from
                                {{$notification->data['old_cpnj']}} to {{$notification->data['new_cpnj']}}

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\SupplierCustomer")
                
                    <a href="{{url('/admin/customer/'.$notification->data['customer_id'].'/customersupplier')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-handshake-o'></i>
                            </div>
                            <div class='pull-left text'>

                                A New Relation of Company - "{{$notification->data['customer_name']}}" to Supplier - "{{$notification->data['supplier_name']}}" is added. 

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\TransactionApproved")
                
                    <a href="{{url('/admin/transactions/'.$notification->data['transaction_id'])}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-money'></i>
                            </div>
                            <div class='pull-left text'>

                                Transaction ID - {{$notification->data['transaction_id']}} is approved by - {{$notification->data['approved_by']}} 

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\TransactionCancelled")
               
                    <a href="{{url('/admin/transactions/'.$notification->data['transaction_id'])}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-money'></i>
                            </div>
                            <div class='pull-left text'>

                                Transaction ID - {{$notification->data['transaction_id']}} is cancelled by - {{$notification->data['cancelled_by']}} 

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif

            @if($notification->type == "App\Notifications\UserStatus")
                
                    <a href="{{url('/admin/users')}}">
                        <div class='widget-body'>
                            <div class='pull-left icon'>
                                <i class='fa fa-money'></i>
                            </div>
                            <div class='pull-left text'>

                                "{{$notification->data['user_name']}}'s" status changed to {{$notification->data['user_status']}} by {{$notification->data['changed_by']}} 

                                <small class='text-muted'>{{$notification->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </a>
                
            @endif
            </li>
            <li class='divider'></li>
        @endforeach
        <li> <a href="{{ url('/admin/notifications') }}">View All Notifications</a></li>
        @if(\Auth::user()->notifications->count() == 0)
        <li class='widget-footer'>
            <a href='#'>No new Notifications</a>
        </li>
        @endif


    </ul>
</li>
