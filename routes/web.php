<?php

Auth::routes();

Route::get('complete-profile/{email}/{remember_token}', 'Admin\ProfileController@completeProfile');
Route::get('/', 'Admin\AdminController@index');
Route::get('/setInSession','Admin\AdminController@setInSession');

Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin']], function () {

    Route::get('/', 'Admin\AdminController@index');
    
    //dashboard
    Route::get('/salesdashboard', 'Admin\AdminController@salesDashboard');
    Route::get('/dashboard', 'Admin\AdminController@adminDashboard');
    Route::get('/commercialdashboard', 'Admin\AdminController@commercialDashboard');
	Route::get('/backofficedashboard', 'Admin\AdminController@backofficeDashboard');
    Route::get('/supplierdashboard', 'Admin\AdminController@supplierOpDashboard');
    Route::get('/supplierdashboard', 'Admin\AdminController@supplierADashboard');
    Route::get('/companydashboard', 'Admin\AdminController@companyDashboard');

    //my profile
    Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
    Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
    Route::patch('/profile/edit', 'Admin\ProfileController@update');
    Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
    Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');

    
    //System Setting
    Route::get('/system-off', 'Admin\SettingsController@systemOff');    
    Route::resource('/system-setting', 'Admin\SettingsController');

    //Holiday
    Route::resource('/hollyday', 'Admin\HollydayController');

    Route::group(['middleware' => ['is_complete_profile','system_time']], function () {
		
        //generator
        Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
        Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

        //Role Permission
        Route::resource('/permissions', 'Admin\PermissionsController');
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        
        //users
        Route::resource('/users', 'Admin\UsersController');
        Route::get('/users/create', 'Admin\UsersController@create');
        Route::get('/users/{id}/edit', 'Admin\UsersController@edit');
        Route::get('/usersdata', ['as' => 'UsersControllerUsersData', 'uses' => 'Admin\UsersController@datatable']);

        //CB-Users
        Route::get('/cb-users', 'Admin\UsersController@cbUsers');
        Route::get('/cbusersdata', ['as' => 'CbUsersControllerUsersData', 'uses' => 'Admin\UsersController@cbdatatable']);
        Route::post('/cb-users/changegoal', 'Admin\UsersController@cbUsersAddGoal');

        // Cb-USers Report
        Route::get('/cbusers/deals-done', 'Admin\ReportsForCbUsersController@index');
        Route::post('/cbusers/deals-done', 'Admin\ReportsForCbUsersController@index');
        Route::get('/cbusers/connectbahn-revenue', 'Admin\ReportsForCbUsersController@revenueForConnectBahn');
        Route::post('/cbusers/connectbahn-revenue', 'Admin\ReportsForCbUsersController@revenueForConnectBahn');


        //roles
        Route::resource('/roles', 'Admin\RolesController');
        Route::get('/role', ['as' => 'RoleControllerRolesData', 'uses' => 'Admin\RolesController@datatable']);

        //customers
		Route::resource('/customer','Admin\CustomerController');
        Route::get('customer-data', ['as' => 'CustomerControllerCustomersData', 'uses' => 'Admin\CustomerController@datatable']);
        Route::post('/customer/changeCbRep','Admin\CustomerController@changeCbRepCustomer');
        Route::get('/customer/{id}/customersupplier','Admin\CustomerController@customersupplier');
        Route::post('/customer/{id}/customersupplier','Admin\CustomerController@customersupplier');
        Route::delete('/customer/deleteCustSup/{id}','Admin\CustomerController@deleteCustSup');
        Route::get('/customer/{id}/logs', 'Admin\CustomerController@customer_log');
        Route::get('/customer/{id}/banks', 'Admin\CustomerController@bank_assigned');
        Route::post('/customer/changeperiod/', 'Admin\CustomerController@transactionfilter');
        Route::get('customer-bank-data', ['as' => 'CustomerControllerCustomerBankData', 'uses' => 'Admin\CustomerController@bank_assigned_datatable']);

        //Transaction History per Company
        Route::get('/customer-summary','Admin\CustomerController@summary_customer');
        Route::get('/customer/transaction/{id}','Admin\CustomerController@transactionHistory');
        Route::get('transactionhistory', ['as' => 'CustomerControllerTransactionHistoryData', 'uses' => 'Admin\CustomerController@transactionHistoryDatatable']);
        Route::resource('/cash','Admin\CashAddController');
		
        //supplierBank
        Route::get('supplier/bank/{id}','Admin\SupplierBankController@index');
        Route::get('supplier/bank/datatable/{id}','Admin\SupplierBankController@datatableSupplierBank');
        Route::resource('supplier/bank','Admin\SupplierBankController');
        Route::get('supplier/bank/statusupdate/{id}', 'Admin\SupplierBankController@statusupdate');

        //suppliers
        Route::resource('/suppliers', 'Admin\SuppliersController');
        Route::get('supplier-data', ['as' => 'SupplierControllerSuppliersData', 'uses' => 'Admin\SuppliersController@datatable']);
        Route::get('supplierdata', ['as' => 'SupplierControllerSupplier', 'uses' => 'Admin\SuppliersController@customercreate']);
        Route::post('/suppliers/changeCbRep','Admin\SuppliersController@changeCbRepSupplier');
        Route::get('/suppliers/{id}/logs', 'Admin\SuppliersController@supplier_log');
        
        //suppliers Unit
        Route::resource('supplier_unit', 'Admin\SuppliersUnitController');
        Route::get('/supplier_unit/list/{id}','Admin\SuppliersUnitController@listSupplier');
        Route::get('/supplier_unit/{id}/{id2}/edit', 'Admin\SuppliersUnitController@editSupplierUnits');
        Route::get('supplier-unit-data/{id}', ['as' => 'SupplierUnitControllerSuppliersUnitData', 'uses' => 'Admin\SuppliersUnitController@datatable']);
         
        //Suppliers Report
        Route::get('supplierreports/daily', 'Admin\SuppliersReportsController@index');
        Route::post('supplierreports/daily', 'Admin\SuppliersReportsController@index');
        Route::get('supplierreports/daily/cbuser', 'Admin\SuppliersReportsController@supplierdailyreportswithcbuser');
        Route::post('supplierreports/daily/cbuser', 'Admin\SuppliersReportsController@supplierdailyreportswithcbuser');
        Route::get('supplierreports/monthly', 'Admin\SuppliersReportsController@monthlyFinancialCosts');
        Route::post('supplierreports/monthly', 'Admin\SuppliersReportsController@monthlyFinancialCosts');
        Route::get('supplierreports/userdaily', 'Admin\SuppliersReportsController@commercialusertransaction');
        Route::post('supplierreports/userdaily', 'Admin\SuppliersReportsController@commercialusertransaction');
        //Route::post('supplierreports/getdailycsv','Admin\SuppliersReportsController@getdailydetail');

        //Transaction History per Supplier
        Route::get('/supplier-summary','Admin\SuppliersController@summary_supplier');
        Route::get('/supplier/transaction/{id}','Admin\SuppliersController@transactionHistory');
        Route::get('transactionhistory-supplier', ['as' => 'SuppliersControllerTransactionHistoryData', 'uses' => 'Admin\SuppliersController@transactionHistoryDatatable']);

        //manage suppliers
        Route::get('/manage', 'Admin\SuppliersController@manage');
        Route::get('manage-supplier-data', ['as' => 'SupplierControllerManageData', 'uses' => 'Admin\SuppliersController@managedatatable']);
        Route::get('manage-log-supplier-data', ['as' => 'SupplierControllerManageLogData', 'uses' => 'Admin\SuppliersController@managelogdatatable']);

        //Discount Rate
        Route::resource('/discount-rate', 'Admin\DiscountRateController');
        Route::get('/discountrate', ['as' => 'DiscountControllerDiscountsData', 'uses' => 'Admin\DiscountRateController@datatable']);
        Route::post('/discount-rate/add-class-customer', 'Admin\DiscountRateController@addClassByCustomer');
        Route::post('/discount-rate/suppliertocustomer', 'Admin\DiscountRateController@addClassSupplierToCustomer');
        
        //bank
        Route::get('banks/summary/', 'Admin\BanksController@bankSummary');
        Route::resource('/banks', 'Admin\BanksController');
        Route::get('bank/cession/{id}','Admin\BanksController@cession');
        Route::get('bank-data', ['as' => 'BankControllerBanksData', 'uses' => 'Admin\BanksController@datatable']);
        Route::get('cession-data/{id}', ['as' => 'BankControllerCessionsData', 'uses' => 'Admin\BanksController@cessiondatatable']);
        Route::get('summary-data', ['as' => 'BankControllerSummaryData', 'uses' => 'Admin\BanksController@summarydatatable']);
        
        //bank contacts
        Route::get('bank/contact/{id}','Admin\BankContactsController@index');
        Route::get('bank/contact/datatable/{id}','Admin\BankContactsController@datatableBankContact');
        Route::resource('bank/contact','Admin\BankContactsController');

        //bank Reprots
        Route::get('bankreports/daily', 'Admin\BankReportsController@index');
        Route::post('bankreports/daily', 'Admin\BankReportsController@index');
        Route::get('bankreports/daily/cbuser', 'Admin\BankReportsController@bankdailyreportswithcbuser');
        Route::post('bankreports/daily/cbuser', 'Admin\BankReportsController@bankdailyreportswithcbuser');
        Route::get('bankreports/monthlyrevenue', 'Admin\BankReportsController@monthlyFinancialRevenue');
        Route::post('bankreports/monthlyrevenue', 'Admin\BankReportsController@monthlyFinancialRevenue'); 
        

        //bank accounts
        Route::get('bank/account/{id}','Admin\BankAccountsController@index');
        Route::get('bank/account/datatable/{id}','Admin\BankAccountsController@datatableBankAccount');
        Route::resource('bank/account','Admin\BankAccountsController');

        //bank company
        Route::get('bank/company/{id}','Admin\BankCompanyController@index');
        Route::get('bank/company/add/{id}/{id2}','Admin\BankCompanyController@add');
        Route::post('bank/company/{id}','Admin\BankCompanyController@update');
        Route::get('bank/company/datatable/{id}','Admin\BankCompanyController@datatableBankCompany');
        Route::resource('bank/company','Admin\BankCompanyController');

        //available cash history
        Route::get('/cashhistory', ['as' => 'CustomerControllerCashHistoryData', 'uses' => 'Admin\CustomerController@cashHistoryDatatable']);

        //transaction
        Route::get('/transactions/view/{id}', 'Admin\TransactionsController@supplierTransaction');
        Route::resource('/transactions', 'Admin\TransactionsController');
        Route::get('/password-verify', 'Admin\TransactionsController@verifyuser');
        Route::post('/transactions/{id}', 'Admin\TransactionsController@show');
        Route::get('transactionsdata', ['as' => 'TransactionsControllerTransactionsData', 'uses' => 'Admin\TransactionsController@datatable']);
        Route::get('/transactions/invoiceData/{id}','Admin\TransactionsController@invoiceData');
        Route::get('/transactions/approve/{id}','Admin\TransactionsController@approveTransaction');
        Route::get('/transactions/cancle/{id}', 'Admin\TransactionsController@cancleTransaction');

        //invoice
        Route::get('customerdata/customer/', 'Admin\InvoicesController@customerClassRate')->name('getCustomer');
        Route::get('customerbankdata/', 'Admin\InvoicesController@customer_bank_data');
        Route::resource('/invoice', 'Admin\InvoicesController');
        Route::get('invoicedata', ['as' => 'InvoiceControllerInvoicesData', 'uses' => 'Admin\InvoicesController@datatable']);

        //Get City,State,Country
		Route::get('/api/get-state-list', ['as' => 'APIController@getStateList', 'uses' => 'Admin\APIController@getStateList']);
		Route::get('/api/get-city-list',['as' => 'APIControllergetCityList', 'uses' => 'Admin\APIController@getCityList']);

        //Action Log
        Route::resource('/action-log', 'Admin\ActionLogController');
        Route::get('/actionlog', ['as' => 'ActionLogControllerLogsData', 'uses' => 'Admin\ActionLogController@datatable']);

        //summary
        Route::resource('/summary', 'Admin\SummaryController');
        Route::get('/receivable-summary', ['as' => 'SummaryControllerSummaryData', 'uses' => 'Admin\SummaryController@datatable']);

        //Customer Reports
        Route::get('/customerreports', 'Admin\ReportsController@index');
        Route::get('/customerreports/cbuser', 'Admin\ReportsController@customeranticipationwithcbuser');
        Route::post('/customerreports', 'Admin\ReportsController@index');
        Route::post('/customerreports/cbuser', 'Admin\ReportsController@customeranticipationwithcbuser');
        Route::get('/customerreports/monthlyreports', 'Admin\ReportsController@customermonthlyreports');
        Route::post('/customerreports/monthlyreports', 'Admin\ReportsController@customermonthlyreports');
        Route::get('/customerreports/duemonthlyreports', 'Admin\ReportsController@customermonthlyduereports');
        Route::post('/customerreports/duemonthlyreports', 'Admin\ReportsController@customermonthlyduereports');
        Route::get('/customerreports/connectbahnrevenue', 'Admin\ReportsController@connectbahnrevenuereports');
        Route::post('/customerreports/connectbahnrevenue', 'Admin\ReportsController@connectbahnrevenuereports');

        //notification
        Route::get('/markasread', 'Admin\NotificationController@markasread');
        Route::get('/markasunread', 'Admin\NotificationController@markasunread');
        Route::get('/notifications', 'Admin\NotificationController@notifications');
        Route::get('/notifications/{id}', 'Admin\NotificationController@delete');
        
       
    });

});

Route::get('/home', 'HomeController@index')->name('home');

//Crone Job to Expire the Invoices
Route::get('/expire-invoice-crone', function()
{
    Artisan::queue('ExpireInvoice:expireinvoice');
});

//Crone Job to Expire the Transactions
Route::get('/expire-transaction-crone', function()
{
    Artisan::queue('ExpireTransaction:expiretransaction');
});
